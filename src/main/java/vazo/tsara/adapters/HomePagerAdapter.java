package vazo.tsara.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

import vazo.tsara.R;
import vazo.tsara.fragments.TabhomeArtistsFragment;
import vazo.tsara.fragments.TabhomePlaylistsFragment;
import vazo.tsara.fragments.TabhomeVideosFragment;
import vazo.tsara.helpers.Config;

/**
 * Created by rabehasy on 29/05/2015.
 */
public class HomePagerAdapter extends FragmentPagerAdapter {

    static String TAG = Config.TAGKEY;
    FragmentActivity activity;



    public HomePagerAdapter(android.support.v4.app.FragmentManager fm, FragmentActivity activity ) {
        super(fm);
        this.activity = activity;

    }

    //public String[] TITLES = { this.activity.getString(R.string.tab_home_video) , this.activity.getString(R.string.tab_home_playlists), this.activity.getString(R.string.tab_home_artists)   };
    //public String[] TITLES = { "Onglet 1" , "Onglet 2", "Onglet 3"   };

    //private Fragment[] fragments = new Fragment[] { new VideoRecyclerFragment(), new PlaylistsRecyclerFragment()   };

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                return TabhomeVideosFragment.newInstance();
            /*case 1:
                return TabhomePlaylistsFragment.newInstance();*/

            case 1:
                return TabhomeArtistsFragment.newInstance();
        }

        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        switch(position) {
            case 0:
                return this.activity.getString(R.string.tab_home_video);
            /*case 1:
                return this.activity.getString(R.string.tab_home_playlists);*/

            case 1:
                return this.activity.getString(R.string.tab_home_artists);
        }
        return "";
    }

    @Override
    public int getCount() {
        return 2;
    }
}

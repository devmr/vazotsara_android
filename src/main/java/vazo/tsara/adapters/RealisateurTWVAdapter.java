package vazo.tsara.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.models.relatedvideomodel;

/**
 * Created by rabehasy on 11/06/2015.
 */
public class RealisateurTWVAdapter extends BaseAdapter implements Parcelable {
    private Activity activity;
    private LayoutInflater inflater;
    private List<relatedvideomodel> itemsartistes;
    static String TAG = "VAZOTSARA";

    public RealisateurTWVAdapter(Activity activity, List<relatedvideomodel> itemsartistes)
    {
        this.activity = activity;
        this.itemsartistes = itemsartistes;
    }

    public String getFieldAt(int position, String field )
    {
        switch( field ) {
            case "nom":
                return itemsartistes.get(position).getObjetnom()+" ("+itemsartistes.get(position).getNbVideos()+" vidéos)";


            default:
                return "";

        }


    }

    @Override
    public int getCount() {
        return itemsartistes.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsartistes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if ( convertView == null ) {
            Log.d(TAG, "convertView == null");
            convertView = inflater.inflate(R.layout.adaper_artiste_item, null);
        } else {
            Log.d(TAG, "convertView.toString(): " + convertView.toString());
        }

        relatedvideomodel i = itemsartistes.get(position);


        TextView nom = (TextView) convertView.findViewById(R.id.nom);
        TextView uid = (TextView) convertView.findViewById(R.id.uid);
        TextView nbvideos = (TextView) convertView.findViewById(R.id.nbvideos);
        //ImageView logo = (ImageView) convertView.findViewById(R.id.logo);
        SimpleDraweeView logo = (SimpleDraweeView) convertView.findViewById(R.id.logo);

        nom.setText(i.getObjetnom());
        uid.setText(i.getUid());
        nbvideos.setText(" (" + i.getNbVideos() + " vidéo" + (Integer.parseInt(i.getNbVideos()) > 1 ? "s" : "") + ")");

        if ( i.getPathplayliste()!="") {
            //Glide.with(activity).load(i.getPathplayliste()).into( logo );
            //Picasso.with(activity).load(i.getPathplayliste()).into(logo);
            Uri uri = Uri.parse(i.getPathplayliste());
            logo.setImageURI(uri);
        }


        return convertView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(this, flags);
    }
}

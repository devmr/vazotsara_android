package vazo.tsara.adapters;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.List;

import vazo.tsara.R;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.video;

/**
 * Created by rabehasy on 25/06/2015.
 */
public class VideoRelatedBaseAdapter extends BaseAdapter {
    private Context activity;
    private LayoutInflater inflater;
    private List<video> videoItems;
    private int rowLayout;
    static String TAG = Config.TAGKEY;



    public VideoRelatedBaseAdapter(Context activity, List<video> videoItems, int rowLayout)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
    }
    @Override
    public int getCount() {
        return videoItems.size();
    }

    @Override
    public Object getItem(int position) {
        return videoItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean idInAdapter(String id) {
        for( int i = 0; i < videoItems.size(); i++ ) {
            if ( videoItems.get(i).getId().equals(id) ) {
                return true;
            }
        }
        return false;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if ( convertView == null ) {
            convertView = inflater.inflate(rowLayout, null);
        }

        TextView titre = (TextView) convertView.findViewById(R.id.titre);
        TextView infotype = (TextView) convertView.findViewById(R.id.infotype);
        TextView uid = (TextView) convertView.findViewById(R.id.uid);
        View overflow = convertView.findViewById(R.id.album_overflow);
        TextView statistics_viewCount = (TextView) convertView.findViewById(R.id.statistics_viewCount);
        TextView statistics_likeCount = (TextView) convertView.findViewById(R.id.statistics_likeCount);
        //ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
        SimpleDraweeView thumbnail = (SimpleDraweeView) convertView.findViewById(R.id.thumbnail);
        ImageButton ImageButton = (ImageButton) convertView.findViewById(R.id.album_overflow);

        TextView videoytid = (TextView) convertView.findViewById(R.id.videoytid);
        TextView contentDetailsDefinition = (TextView) convertView.findViewById(R.id.contentDetailsDefinition);


        final video v = videoItems.get(position);

        /**
         * Remplir
         */
        titre.setText(v.getTitre() );
        uid.setText(v.getId());
        videoytid.setText(v.getVideoytid());
        contentDetailsDefinition.setText(v.getContentDetails_definition());
        statistics_viewCount.setText("{fa_youtube_play} " + v.getStatistics_viewCount());
        statistics_likeCount.setText("{fa_thumbs_up} " + v.getStatistics_likeCount());

        Iconify.addIcons(statistics_viewCount);
        Iconify.addIcons(statistics_likeCount);


        infotype.setVisibility(View.GONE);

        if (v.getType().equals("realisateur") && v.getPlaylisteinfo().size()>0 ) {
            infotype.setVisibility(View.VISIBLE);

            List<String> listreal = new ArrayList<String>();

            for (int i=0; i<v.getPlaylisteinfo().size(); i++ ) {
                if ( v.getPlaylisteinfo().get(i).getObjetype().equals("realisateur") ) {
                    listreal.add( v.getPlaylisteinfo().get(i).getObjetnom() );
                }
            }

            if ( listreal.size()>0 ) {
                infotype.setText( String.format( activity.getResources().getString(R.string.sprintf_realise_par) , TextUtils.join(", ", listreal) ) );
            }

        }



        /**
         * Image Picasso
         */
        if( v.getThumburl() != null && !v.getThumburl().isEmpty() ) {
            //Picasso.with(activity).load(v.getThumburl()).into( thumbnail );
            Uri uri = Uri.parse(v.getThumburl());
            thumbnail.setImageURI(uri);
        }

        ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popupMenu = new PopupMenu(activity, view) {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.album_overflow_delete:

                                delete(v.getId(), position);


                                return true;

                            case R.id.album_overflow_share:
                                VazotsaraShareFunc.share(new video(v.getTitre(), v.getStatistics_viewCount(), v.getStatistics_commentCount(), v.getStatistics_likeCount()), activity);
                                return true;





                            default:
                                return super.onMenuItemSelected(menu, item);
                        }
                    }
                };

                popupMenu.inflate(R.menu.menu_popupvideolist);

                if (!Useful.isAdmin(activity) ) {
                    popupMenu.getMenu().removeItem(R.id.album_overflow_delete);
                }


                popupMenu.show();
            }
        });




        return convertView;
    }





    void delete(final String uid, int position)
    {
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //Delete video
                        VazotsaraShareFunc.deleteVideo(uid, activity);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }


}

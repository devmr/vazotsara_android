package vazo.tsara.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import de.greenrobot.event.EventBus;
import vazo.tsara.R;
import vazo.tsara.activities.PlaylistEditActivity;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;

/**
 * Created by rabehasy on 25/06/2015.
 */
public class PlaylistsBaseAdapter extends BaseAdapter {
    private FragmentActivity activity;
    private LayoutInflater inflater;
    private List<Playlist> videoItems;
    private int rowLayout;
    static String TAG = Config.TAGKEY;

    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;

    // the serverListSize is the total number of items on the server side,
    // which should be returned from the web request results
    protected int serverListSize = -1;

    Fragment frag;

    public PlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout )
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;

    }
    public PlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int serverListSize)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.serverListSize = serverListSize;
    }

    public PlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int serverListSize, Fragment frag)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.serverListSize = serverListSize;
        this.frag = frag;
    }

    @Override
    public int getCount() {
        return videoItems.size()+1;
    }

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }

    @Override
    public Object getItem(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? videoItems.get(position):null);
    }

    @Override
    public long getItemId(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? position : -1);
    }

    public View getFooterView(int position, View convertView,
                              ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.customview_loading, parent, false);
        }

        return row;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        setServerListSize(serverListSize);
        Log.d(TAG,"Adapter this.serverListSize: "+serverListSize );

        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            // display the last row
            return getFooterView(position, convertView, parent);
        }

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        ViewHolder holder = new ViewHolder();

        if(convertView == null) {
            convertView = inflater.inflate(rowLayout, null);

            holder.titre = (TextView) convertView.findViewById(R.id.titre);
            holder.uid = (TextView) convertView.findViewById(R.id.uid);
            holder.nbvideos = (TextView) convertView.findViewById(R.id.nbvideos);
            holder.overflow = convertView.findViewById(R.id.album_overflow);
            //holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.image = (SimpleDraweeView) convertView.findViewById(R.id.image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }








        final Playlist plinfo = videoItems.get(position);

        /**
         * Remplir
         */
        holder.titre.setText(plinfo.getNom());
        holder.uid.setText(plinfo.getId());
        holder.nbvideos.setText( ( Integer.parseInt( plinfo.getNbVideos() ) == 1?activity.getString(R.string.sprintf_onevideo):(Integer.parseInt( plinfo.getNbVideos() ) == 0  ? activity.getString(R.string.sprintf_novideo) : String.format(activity.getString(R.string.sprintf_nbvideos), plinfo.getNbVideos()) ) ));

        if (holder.overflow != null ) {
            holder.overflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    PopupMenu popupMenu = new PopupMenu(activity, v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_delete:
                                    delete(String.valueOf(plinfo.getId()), position);
                                    return true;

                                case R.id.item_edit:
                                    edit( plinfo );
                                    return true;

                                case R.id.item_share:
                                    //share(vid);
                                    return true;

                                case R.id.item_rename:
                                    //frag.updateItemText()
                                    /*videoItems.get(position).setNom("TOTOTO");
                                    notifyDataSetChanged();*/
                                    //openEditDialog(plinfo, PlaylistsRecyclerAdapter.this);
                                    return true;


                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };

                    popupMenu.inflate(R.menu.menu_popup_playlist);

                    if (!Useful.isAdmin(activity)) {
                        popupMenu.getMenu().removeItem(R.id.item_delete);
                        popupMenu.getMenu().removeItem(R.id.item_edit);
                        popupMenu.getMenu().removeItem(R.id.item_rename);
                    }

                    popupMenu.show();
                }
            });
        }

        if ( holder.image != null ) {
            Log.d(TAG, "String.format( Config.PATHPLAYLIST ,v.getImage() ) : " + String.format(Config.PATHPLAYLIST, plinfo.getImage()));
            //Picasso.with(this.activity).load( String.format( Config.PATHPLAYLIST ,plinfo.getImage() ) ).placeholder(R.drawable.user).into(holder.image);
            Uri uri = Uri.parse(String.format( Config.PATHPLAYLIST ,plinfo.getImage() ));
            holder.image.setImageURI(uri);
        }

        return convertView;
    }

    void edit(Playlist v){
        Intent i = new Intent(activity, PlaylistEditActivity.class);
        EventBus.getDefault().postSticky(v);
        activity.startActivityForResult(i,0);
    }
    void openEditDialog(Playlist v, PlaylistsRecyclerAdapter adapter)
    {
        VazotsaraShareFunc.showDialogEditPlaylist(v, activity, adapter);
    }
    void delete(final String uid, final int position)
    {


        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //Delete video
                        VazotsaraShareFunc.deletePlaylist(uid, activity);
                        videoItems.remove(position);
                        notifyDataSetChanged();
                        VazotsaraShareFunc.showSnackMsg( activity.getString(R.string.msg_confirm_delete_playlist), activity );
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }

    public class ViewHolder
    {
        ImageView image;
        TextView titre;
        TextView uid;
        TextView nbvideos;
        View overflow;
    }

    public int getViewTypeCount(){
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return (position >= videoItems.size()) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;
    }
}

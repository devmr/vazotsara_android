package vazo.tsara.adapters;

/**
 * Created by rabehasy on 27/05/2015.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.joanzapata.android.iconify.Iconify;
import com.squareup.picasso.Picasso;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.helpers.Useful;
import vazo.tsara.models.video;
import vazo.tsara.services.Dbhandler;


//public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
public class VideoAdapter extends SelectableAdapter<RecyclerView.ViewHolder>{

    private int rowLayoutfooter;
    private int rowLayout;
    private int nb_video_par_page;

    static String TAG = "VAZOTSARA";

    private static List<video> videoItems;
    private static FragmentActivity activity;
    private View v;

    private final int VIEW_ITEM = 1;
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_DECONNECTE = 2;
    public static final int VIEW_TYPE_ENDPAGEREACHED = 3;
    public static final int VIEW_TYPE_ENDPAGEREACHED_EMPTY = 4;

    private String[] viewtypes = {"LOADING","ITEM","DECONNECTE", "ENDPAGEREACHED", "ENDPAGEREACHED_EMPTY"};


    protected int serverListSize = -1;
    protected int loadedSize = -1;
    protected boolean deconnecte;

    Dbhandler db;

    public VideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout  ) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
    }

    public VideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int rowLayoutfooter ) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
    }

    public VideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int rowLayoutfooter, int nb_video_par_page ) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
        this.nb_video_par_page = nb_video_par_page;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Log.d("ANDYROHYHIDY", "viewtype : " + viewType);
        if (viewType==VIEW_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new ViewHolder0(v);
        } else if ( viewType==VIEW_TYPE_DECONNECTE || getDeconnecte() == true ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_videodeconnecte, parent, false);
            return new ViewHolderDeconnecte(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached, parent, false);
            return new ViewHolderEndreached(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED_EMPTY ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached_empty, parent, false);
            return new ViewHolderEndreachedEmpty(v);
        }  else {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayoutfooter, parent, false);
            return new ProgressViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewtype = VIEW_ITEM;

        /*if (  videoItems.get(position)!=null )
            viewtype = VIEW_ITEM;*/

        //if ( position>2 && getItemCount()-position <= 1  ) {
        if ( position+1 == getItemCount()  ) {
            if ( getDeconnecte() ) {
                viewtype = VIEW_TYPE_DECONNECTE;
            } else if (!getDeconnecte() && serverListSize>0 && serverListSize==getItemCount()-1 ) {
                if (nb_video_par_page>0 && serverListSize>nb_video_par_page )
                    viewtype = VIEW_TYPE_ENDPAGEREACHED;
                else
                    viewtype = VIEW_TYPE_ENDPAGEREACHED_EMPTY;
            } else if(position == 0) {
                viewtype = VIEW_TYPE_ENDPAGEREACHED_EMPTY;
            } else {
                viewtype = VIEW_TYPE_LOADING;
            }
        }



        Log.d(TAG,"getItemViewType : "+viewtypes[viewtype]+" - position : "+position+" - getItemCount() : "+getItemCount()+" - serverListSize: "+serverListSize+" - nb_video_par_page:"+nb_video_par_page+" - getDeconnecte(): "+getDeconnecte()+" - loadedSize: "+loadedSize);
        return viewtype;
    }
    public void setLoadedSize(int loadedSize){
        this.loadedSize = loadedSize;
    }
    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }
    public void setDeconnecte(boolean deconnecte){
        this.deconnecte = deconnecte;
    }
    public boolean getDeconnecte(){
        return this.deconnecte;
    }


    @Override
    //public void onBindViewHolder(ViewHolder viewHolder, int position) {
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {



        if( viewHolder instanceof ViewHolder0){

            final video vid = videoItems.get(position);


            Log.d(TAG, "Position: " + position + " - getTitre : " + vid.getTitre() + " - videoItems.size(): " + videoItems.size());


            ( (ViewHolder0) viewHolder).titre.setText(vid.getTitre());
            //( (ViewHolder0) viewHolder).statistics_commentCount.setText("{fa_comment} "+vid.getStatistics_commentCount());
            ( (ViewHolder0) viewHolder).statistics_viewCount.setText("{fa_youtube_play} "+vid.getStatistics_viewCount());
            ( (ViewHolder0) viewHolder).statistics_likeCount.setText("{fa_thumbs_up} " + vid.getStatistics_likeCount());

            //Iconify.addIcons(((ViewHolder0) viewHolder).statistics_commentCount);
            Iconify.addIcons(( (ViewHolder0) viewHolder).statistics_viewCount);
            Iconify.addIcons(( (ViewHolder0) viewHolder).statistics_likeCount );


            ( (ViewHolder0) viewHolder).uid.setText(vid.getId());


            /**
             * Image Picasso
             */
            if( vid.getThumburl() != null && !vid.getThumburl().isEmpty() ) {

                //Glide.with(activity).load(vid.getThumburl()).into(((ViewHolder0) viewHolder).thumbnail);
                Picasso.
                        with(activity).
                        load(vid.getThumburl()).
                        placeholder(R.drawable.placeholdervideo)
                        .into(( (ViewHolder0) viewHolder).thumbnail);
            }


            if ( !vid.getFormat().equals("hd"))
                ( (ViewHolder0) viewHolder).hdformat.setVisibility(View.GONE);



            if ( vid.getDuree()!=null)
                ( (ViewHolder0) viewHolder).duree.setText( Useful.getDurationBreakdown( Long.parseLong(vid.getDuree()) * 1000 ) );




            // Do view inflation, etc.

             //db = new Dbhandler(activity.getApplicationContext());
            ( (ViewHolder0) viewHolder).overflow.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final PopupMenu popupMenu = new PopupMenu( activity  , v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {




                            switch (item.getItemId()) {
                                case R.id.album_overflow_delete:

                                    delete(vid.getId(), position );


                                    return true;

                                case R.id.album_overflow_share:
                                    share(vid);
                                    return true;

                                /*case R.id.album_overflow_add_favorite:



                                    addFavorite(vid);
                                    return true;

                                case R.id.album_overflow_remove_favorite:

                                    removeFavorite(vid);
                                    return true;*/

                                case R.id.album_overflow_detailfm:
                                    showDetailIntent(vid, position);
                                    return true;

                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };



                   // popupMenu.getMenu().findItem(R.id.album_overflow_share).setEnabled(false);

                    int menuRes;
                    menuRes = R.menu.menu_popupvideolist;

                    popupMenu.inflate(menuRes);

                    /*if ( db.VideoInFavoris(Integer.parseInt( vid.getId()) ) ) {
                        popupMenu.getMenu().removeItem(R.id.album_overflow_add_favorite);
                    } else {
                        popupMenu.getMenu().removeItem(R.id.album_overflow_remove_favorite);
                    }*/

                    popupMenu.getMenu().removeItem(R.id.album_overflow_delete);






                    popupMenu.show();
                }
            });

            /*( (ViewHolder0) viewHolder).thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                    //showDetailIntent(vid);
                }
            });*/
            final ViewGroup.LayoutParams lp = viewHolder.itemView.getLayoutParams();
            ((ViewHolder0) viewHolder).selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);

            ((ViewHolder0) viewHolder).selectedOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                }
            });
        }




    }
    void share(video vid)
    {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setType("text/plain");

        String shareBody = "Titre : "+vid.getTitre()+" - vues : "+vid.getStatistics_viewCount()+" - Commentaires : "+vid.getStatistics_commentCount();

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Video : "+vid.getTitre());

        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);

        activity.startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    static void showDetailFragment(video vid)
    {

        Log.d(TAG, "activity.getSupportFragmentManager().getBackStackEntryCount() : " + activity.getSupportFragmentManager().getBackStackEntryCount());



        if (  activity.getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        //ft.replace(R.id.frame_container, new FragmentVideoDetail().newInstance(vid.getId(), vid.getTitre()) ).addToBackStack(null).commit();

        //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( vid.getTitre() ) );

    }

    static void showDetailIntent(video vid, int position)
    {
        Log.d(TAG, "VideoAdapter - showDetailIntent - uid : "+vid.getId()+" - videoytid :"+vid.getVideoytid());
        Intent VDetailActivity = new Intent( activity, VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId() );
        VDetailActivity.putExtra("videoytid", vid.getVideoytid() );
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition() );
        VDetailActivity.putExtra("position", position );
        activity.startActivityForResult(VDetailActivity, 100);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    void delete(String uid, int position)
    {
        /**
         * Menu 3 - Afficher Confirm
         */
        AlertDialog.Builder confirmDialog = new AlertDialog.Builder( activity )  ;
        confirmDialog.setTitle("Supprimer");
        confirmDialog.setMessage("Voulez-vous vraiment supprimer cet element ?");

        confirmDialog.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "Clic OUI", Toast.LENGTH_SHORT).show();



            }
        }).setNegativeButton("NON", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "Clic NON", Toast.LENGTH_SHORT).show();
            }
        });
        confirmDialog.show();
    }

    @Override
    public int getItemCount() {
        return videoItems == null ? 0 : videoItems.size()+1 ;
    }

    public boolean idInAdapter(String id) {
        for( int i = 0; i < videoItems.size(); i++ ) {
            if ( videoItems.get(i).getId().equals(id) ) {
                return true;
            }
        }
        return false;
    }

    public static class ViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titre;
        //public TextView statistics_commentCount;
        public TextView statistics_viewCount;
        public TextView statistics_likeCount;
        public TextView uid;
        public TextView hdformat;
        public TextView duree;
        public ImageView thumbnail;
        public View overflow;

        public LinearLayout fond;

        public View selectedOverlay;
        private ClickListener listener;

        public ViewHolder0(View convertView) {
            super(convertView);

            fond = (LinearLayout) convertView.findViewById(R.id.fond);

            titre = (TextView) convertView.findViewById(R.id.titre);
            //statistics_commentCount = (TextView) convertView.findViewById(R.id.statistics_commentCount);
            statistics_viewCount = (TextView) convertView.findViewById(R.id.statistics_viewCount);
            statistics_likeCount = (TextView) convertView.findViewById(R.id.statistics_likeCount);
            uid = (TextView) convertView.findViewById(R.id.uid);
            thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);

            hdformat = (TextView) convertView.findViewById(R.id.hdformat);
            duree = (TextView) convertView.findViewById(R.id.duree);
            overflow = convertView.findViewById(R.id.album_overflow);

            selectedOverlay = convertView.findViewById(R.id.selected_overlay);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getPosition());
            }
            Log.d(TAG,"Ligne 276");
            fond.setBackgroundColor(activity.getResources().getColor(R.color.grisclair));

            //Reinitialiser le fond
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fond.setBackgroundColor(Color.TRANSPARENT);
                }
            }, 500);

            VideoAdapter.showDetailIntent(videoItems.get(getPosition()), getPosition());
        }

        public interface ClickListener {
            public void onItemClicked(int position);
            //public boolean onItemLongClicked(int position);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolderDeconnecte extends RecyclerView.ViewHolder {

        public ViewHolderDeconnecte(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreached extends RecyclerView.ViewHolder {

        public ViewHolderEndreached(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreachedEmpty extends RecyclerView.ViewHolder {

        public ViewHolderEndreachedEmpty(View v) {
            super(v);
        }
    }
}

package vazo.tsara.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import vazo.tsara.R;
import vazo.tsara.helpers.Config;

/**
 * Created by Miary on 23/08/2015.
 */
public class VideoSearchSuggestionsAdapter extends SimpleCursorAdapter
{
    private static final String tag=VideoSearchSuggestionsAdapter.class.getName();
    private Context context=null;
    static String TAG = Config.TAGKEY;

    public VideoSearchSuggestionsAdapter(Context context, int layout, Cursor video, String[] from, int[] to, int flags) {
        //super(context, layout, c, from, to, flags);
        super(context, layout, video, from, to, flags);
        this.context=context;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String urlimg = "";

        //ImageView thumburl=(ImageView)view.findViewById(R.id.thumburl);
        SimpleDraweeView thumburl=(SimpleDraweeView)view.findViewById(R.id.thumburl);
        //ImageView logoartiste=(ImageView)view.findViewById(R.id.logoartiste);
        SimpleDraweeView logoartiste=(SimpleDraweeView)view.findViewById(R.id.logoartiste);
        TextView titre=(TextView)view.findViewById(R.id.titre);
        TextView titreartiste=(TextView)view.findViewById(R.id.titreartiste);
        TextView id=(TextView)view.findViewById(R.id.id);
        TextView contentDetails_definition=(TextView)view.findViewById(R.id.contentDetails_definition);

        View hrliste = (View) view.findViewById(R.id.hrliste);
        View hrartiste = (View) view.findViewById(R.id.hrartiste);



        id.setText(cursor.getString(3));

        Log.d(TAG, "cursor.getString(4):#" + cursor.getString(4) + "# - cursor.getString(1):"+cursor.getString(1).contains("::"));

        if ( cursor.getString(1).contains("::") ) {
            titre.setVisibility(View.VISIBLE);
            thumburl.setVisibility(View.VISIBLE);
            hrliste.setVisibility(View.VISIBLE);

            titre.setText(cursor.getString(1));
            contentDetails_definition.setText(cursor.getString(4));
            urlimg = "http://i.ytimg.com/vi/" + cursor.getString(2) + "/hqdefault.jpg";
            //Picasso.with(context).load(urlimg).into(thumburl);
            Uri uri = Uri.parse(urlimg);
            thumburl.setImageURI(uri);

            logoartiste.setVisibility(View.GONE);
            titreartiste.setVisibility(View.GONE);
            hrartiste.setVisibility(View.GONE);
        } else {

            logoartiste.setVisibility(View.VISIBLE);
            titreartiste.setVisibility(View.VISIBLE);
            hrartiste.setVisibility(View.VISIBLE);

            titreartiste.setText(String.format(context.getString(R.string.sprintf_videos_de_artiste), cursor.getString(1)));
            urlimg = String.format( Config.PATHPLAYLIST , cursor.getString(2)) ;
            contentDetails_definition.setVisibility(View.GONE);
            //Picasso.with(context).load(urlimg).into(logoartiste);
            Uri uri = Uri.parse(urlimg);
            logoartiste.setImageURI(uri);

            thumburl.setVisibility(View.GONE);
            titre.setVisibility(View.GONE);
            hrliste.setVisibility(View.GONE);
        }



    }






}

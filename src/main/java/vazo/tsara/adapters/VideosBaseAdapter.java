package vazo.tsara.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.video;

/**
 * Created by rabehasy on 25/06/2015.
 */
public class VideosBaseAdapter extends BaseAdapter {
    private FragmentActivity activity;
    private LayoutInflater inflater;
    private List<video> videoItems;
    private int rowLayout;
    static String TAG = Config.TAGKEY;

    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;

    String sortfields;

    // the serverListSize is the total number of items on the server side,
    // which should be returned from the web request results
    protected int serverListSize = -1;

    public VideosBaseAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;

    }
    public VideosBaseAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int serverListSize)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.serverListSize = serverListSize;
    }

    public VideosBaseAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, String sortfields)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.sortfields = sortfields;
    }
    public boolean idInAdapter(String id) {
        for( int i = 0; i < videoItems.size(); i++ ) {
            if ( videoItems.get(i).getId().equals(id) ) {
                return true;
            }
        }
        return false;
    }
    @Override
    public int getCount() {
        return videoItems.size()+1;
    }

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }

    @Override
    public Object getItem(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? videoItems.get(position):null);
    }

    @Override
    public long getItemId(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? position : -1);
    }

    public View getFooterView(int position, View convertView, ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.customview_tabhome_endlist, parent, false);

            Button btn = (Button) row.findViewById(R.id.btn_plus);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(activity,"eeeeee",Toast.LENGTH_LONG).show();
                    //open activity
                    Intent VideoSearchSortActivity = new Intent(activity, vazo.tsara.activities.VideoSearchSortActivity.class);
                    VideoSearchSortActivity.putExtra("sortfields", sortfields);
                    VideoSearchSortActivity.putExtra("search", "" );
                    activity.startActivityForResult(VideoSearchSortActivity, 100);
                }
            });
        }

        return row;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        setServerListSize(serverListSize);
        Log.d(TAG,"Adapter this.serverListSize: "+serverListSize );

        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            // display the last row
            return getFooterView(position, convertView, parent);
        }

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        ViewHolder holder;

        if(convertView == null) {
            convertView = inflater.inflate(rowLayout, null);
            holder = new ViewHolder();

            holder.titre = (TextView) convertView.findViewById(R.id.titre);
            holder.uid = (TextView) convertView.findViewById(R.id.uid);
            holder.videoytid = (TextView) convertView.findViewById(R.id.videoytid);
            holder.contentDetails_definition = (TextView) convertView.findViewById(R.id.contentDetails_definition);
            holder.image = (SimpleDraweeView) convertView.findViewById(R.id.image);
            //SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.my_image_view);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }








        video v = videoItems.get(position);

        /**
         * Remplir
         */
        //holder.titre.setText(v.getNom());
        if ( holder != null ) {
            holder.titre.setText(v.getTitre());
            holder.uid.setText(v.getId());
            holder.videoytid.setText(v.getVideoytid());
            holder.contentDetails_definition.setText(v.getContentDetails_definition());


            //holder.nbvideos.setText( ( Integer.parseInt( v.getNbVideos() ) == 1?activity.getString(R.string.sprintf_onevideo):(Integer.parseInt( v.getNbVideos() ) == 0  ? activity.getString(R.string.sprintf_novideo) : String.format(activity.getString(R.string.sprintf_nbvideos), v.getNbVideos()) ) ));

            if (holder.overflow != null) {
                holder.overflow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(activity, v) {
                            @Override
                            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.album_overflow_delete:

                                        //delete(vid.getId(), position);


                                        return true;

                                    case R.id.album_overflow_share:
                                        //share(vid);
                                        return true;


                                    case R.id.album_overflow_detailfm:
                                        //showDetailIntent(vid);
                                        return true;


                                    default:
                                        return super.onMenuItemSelected(menu, item);
                                }
                            }
                        };

                        popupMenu.inflate(R.menu.menu_popupvideolist);


                        popupMenu.show();
                    }
                });
            }

            if (holder.image != null && !v.getThumburl().isEmpty()) {
                //Picasso.with(this.activity).load(v.getThumburl()).placeholder(R.drawable.user).into(holder.image);
                Uri uri = Uri.parse(v.getThumburl());
                holder.image.setImageURI(uri);
            }
        }

        return convertView;
    }

    public class ViewHolder
    {
        //ImageView image;
        SimpleDraweeView image;
        TextView titre;
        TextView uid;
        TextView videoytid;
        TextView contentDetails_definition;
        View overflow;
    }

    public int getViewTypeCount(){
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return (position >= videoItems.size()) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;

    }
}

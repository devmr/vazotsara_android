package vazo.tsara.adapters;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.Playlist;

/**
 * Created by rabehasy on 25/06/2015.
 */
public class FavoriPlaylistsBaseAdapter extends BaseAdapter {
    private FragmentActivity activity;
    private LayoutInflater inflater;
    private List<Playlist> videoItems;
    private int rowLayout;
    static String TAG = Config.TAGKEY;

    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_ACTIVITY = 1;
    public static final int VIEW_TYPE_ENDPAGEREACHED = 3;
    public static final int VIEW_TYPE_ENDPAGEREACHED_EMPTY = 4;

    private String[] viewtypes = {"LOADING","ITEM","DECONNECTE", "ENDPAGEREACHED", "ENDPAGEREACHED_EMPTY"};

    private int nb_item_par_page;

    // the serverListSize is the total number of items on the server side,
    // which should be returned from the web request results
    protected int serverListSize = -1;

    public FavoriPlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;

    }
    public FavoriPlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int serverListSize)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.serverListSize = serverListSize;
    }

    public FavoriPlaylistsBaseAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int serverListSize, int nb_item_par_page)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.serverListSize = serverListSize;
        this.nb_item_par_page = nb_item_par_page;
    }
    @Override
    public int getCount() {
        return videoItems.size()+1;
    }

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }

    public void setNbItemParPage(int nb) {
        this.nb_item_par_page = nb;
    }

    @Override
    public Object getItem(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? videoItems.get(position):null);
    }

    @Override
    public long getItemId(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY ? position : -1);
    }

    public View getFooterView(int position, View convertView,
                              ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.customview_loading, parent, false);
        }

        return row;
    }
    public View getEndreachedView(int position, View convertView,
                              ViewGroup parent) {
        if (position >= serverListSize && serverListSize > 0) {
            // the ListView has reached the last row
            TextView tvLastRow = new TextView(activity);
            tvLastRow.setHint("");
            tvLastRow.setGravity(Gravity.CENTER);
            return tvLastRow;
        }

        View row = convertView;
        if (row == null) {
            row = activity.getLayoutInflater().inflate(
                    R.layout.adapter_endreached, parent, false);
        }

        return row;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        setServerListSize(serverListSize);
        Log.d(TAG,"Adapter this.serverListSize: "+serverListSize );

        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            // display the last row
            return getFooterView(position, convertView, parent);
        }

        if (getItemViewType(position) == VIEW_TYPE_ENDPAGEREACHED || getItemViewType(position) == VIEW_TYPE_ENDPAGEREACHED_EMPTY) {
            // display the last row
            return getEndreachedView(position, convertView, parent);
        }

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        ViewHolder holder = new ViewHolder();

        if(convertView == null) {
            convertView = inflater.inflate(rowLayout, null);

            holder.titre = (TextView) convertView.findViewById(R.id.titre);
            holder.uid = (TextView) convertView.findViewById(R.id.uid);
            holder.nbvideos = (TextView) convertView.findViewById(R.id.nbvideos);
            holder.overflow = convertView.findViewById(R.id.album_overflow);
            //holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.image = (SimpleDraweeView) convertView.findViewById(R.id.image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }








        Playlist v = videoItems.get(position);

        /**
         * Remplir
         */
        holder.titre.setText(v.getNom());
        holder.uid.setText(v.getId());
        if ( v.getNbVideos() != null )
            holder.nbvideos.setText( ( Integer.parseInt( v.getNbVideos() ) == 1?activity.getString(R.string.sprintf_onevideo):(Integer.parseInt( v.getNbVideos() ) == 0  ? activity.getString(R.string.sprintf_novideo) : String.format(activity.getString(R.string.sprintf_nbvideos), v.getNbVideos()) ) ));

        if (holder.overflow != null ) {
            holder.overflow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(activity, v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.album_overflow_delete:

                                    //delete(vid.getId(), position);


                                    return true;

                                case R.id.album_overflow_share:
                                    //share(vid);
                                    return true;


                                case R.id.album_overflow_detailfm:
                                    //showDetailIntent(vid);
                                    return true;


                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };

                    popupMenu.inflate(R.menu.menu_popup_playlist_favori);


                    popupMenu.show();
                }
            });
        }

        if ( holder.image != null ) {
            Log.d(TAG, "String.format( Config.PATHPLAYLIST ,v.getImage() ) : " + String.format(Config.PATHPLAYLIST, v.getImage()));
            //Picasso.with(this.activity).load( String.format( Config.PATHPLAYLIST ,v.getImage() ) ).placeholder(R.drawable.user).into(holder.image);
            Uri uri = Uri.parse(String.format( Config.PATHPLAYLIST ,v.getImage() ));
            holder.image.setImageURI(uri);
        }

        return convertView;
    }

    public class ViewHolder
    {
        ImageView image;
        TextView titre;
        TextView uid;
        TextView nbvideos;
        View overflow;
    }

    public int getViewTypeCount(){
        return 2;
    }



    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        int viewtype = VIEW_TYPE_ACTIVITY;

        if ( position+1 == getCount()  ) {

            if ( serverListSize>0 && serverListSize==getCount()-1 ) {
                if (nb_item_par_page>0 && serverListSize>nb_item_par_page )
                    viewtype = VIEW_TYPE_ENDPAGEREACHED;
                else
                    viewtype = VIEW_TYPE_ENDPAGEREACHED_EMPTY;
            }
            else
                viewtype = VIEW_TYPE_LOADING;
        }
        Log.d(TAG,"FavoritePlaylistsBaseAdapter - getItemViewType : "+viewtypes[viewtype]+" - position : "+position+" - getCount() : "+getCount()+" - nb_total: "+serverListSize);


        return viewtype;
        /*return (position >= videoItems.size()) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;*/
    }
}

package vazo.tsara.adapters;

/**
 * Created by rabehasy on 27/05/2015.
 */

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.fragments.TabfavoriteVideosFragment;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.video;


//public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
public class FavoriVideoAdapter extends SelectableAdapter<RecyclerView.ViewHolder>{

    private int rowLayoutfooter;
    private int rowLayout;
    private int nb_video_par_page;

    private RecyclerView recyclerView;

    TabfavoriteVideosFragment frag;

    static String TAG = "VAZOTSARA";

    private static List<video> videoItems;
    private static FragmentActivity activity;
    private View v;

    private final int VIEW_ITEM = 1;
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_DECONNECTE = 2;
    public static final int VIEW_TYPE_ENDPAGEREACHED = 3;
    public static final int VIEW_TYPE_ENDPAGEREACHED_EMPTY = 4;

    private String[] viewtypes = {"LOADING","ITEM","DECONNECTE", "ENDPAGEREACHED", "ENDPAGEREACHED_EMPTY"};


    protected int serverListSize = -1;
    protected boolean deconnecte;

    public FavoriVideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
    }

    public FavoriVideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int rowLayoutfooter) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
    }

    public FavoriVideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int rowLayoutfooter, int nb_video_par_page) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
        this.nb_video_par_page = nb_video_par_page;
    }
    public FavoriVideoAdapter(FragmentActivity activity, List<video> videoItems, int rowLayout, int rowLayoutfooter, RecyclerView recyclerView, TabfavoriteVideosFragment frag) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
        this.recyclerView = recyclerView;
        this.frag = frag;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Log.d("ANDYROHYHIDY", "viewtype : " + viewType);
        if (viewType==VIEW_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new ViewHolder0(v);
        } else if ( viewType==VIEW_TYPE_DECONNECTE || getDeconnecte() == true ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_videodeconnecte, parent, false);
            return new ViewHolderDeconnecte(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached, parent, false);
            return new ViewHolderEndreached(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED_EMPTY ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached_empty, parent, false);
            return new ViewHolderEndreachedEmpty(v);
        }  else {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayoutfooter, parent, false);
            return new ProgressViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewtype = VIEW_ITEM;

        /*if (  videoItems.get(position)!=null )
            viewtype = VIEW_ITEM;*/

        //if ( position>2 && getItemCount()-position <= 1  ) {
        if ( position+1 == getItemCount()  ) {
            if ( getDeconnecte() )
                viewtype = VIEW_TYPE_DECONNECTE;
            else if (!getDeconnecte() && serverListSize>0 && serverListSize==getItemCount()-1 ) {
                if (nb_video_par_page>0 && serverListSize>nb_video_par_page )
                    viewtype = VIEW_TYPE_ENDPAGEREACHED;
                else
                    viewtype = VIEW_TYPE_ENDPAGEREACHED_EMPTY;
            }
            else
                viewtype = VIEW_TYPE_LOADING;
        }



        Log.d(TAG,"getItemViewType : "+viewtypes[viewtype]+" - position : "+position+" - getItemCount() : "+getItemCount()+" - nb_total: "+serverListSize);
        return viewtype;
    }
    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }
    public void setNbItemParPage(int nb) {
        this.nb_video_par_page = nb;
    }
    public void setDeconnecte(boolean deconnecte){
        this.deconnecte = deconnecte;
    }
    public boolean getDeconnecte(){
        return this.deconnecte;
    }


    @Override
    //public void onBindViewHolder(ViewHolder viewHolder, int position) {
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {



        if( viewHolder instanceof ViewHolder0){

            final video vid = videoItems.get(position);


            Log.d(TAG, "Position: " + position + " - getTitre : " + vid.getTitre() + " - videoItems.size(): " + videoItems.size());


            ( (ViewHolder0) viewHolder).titre.setText(vid.getTitre());



            ( (ViewHolder0) viewHolder).uid.setText(vid.getId());


            /**
             * Image Picasso
             */
            if( vid.getThumburl() != null && !vid.getThumburl().isEmpty() ) {

                //Glide.with(activity).load(vid.getThumburl()).into(((ViewHolder0) viewHolder).thumbnail);
                Picasso.
                        with(activity).
                        load(vid.getThumburl()).
                        placeholder(R.drawable.placeholdervideo)
                        .into(( (ViewHolder0) viewHolder).thumbnail);
            }


            if ( !vid.getFormat().equals("hd"))
                ( (ViewHolder0) viewHolder).hdformat.setVisibility(View.GONE);



            if ( vid.getDuree()!=null)
                ( (ViewHolder0) viewHolder).duree.setText( Useful.getDurationBreakdown( Long.parseLong(vid.getDuree()) * 1000 ) );




            // Do view inflation, etc.


            ( (ViewHolder0) viewHolder).overflow.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popupMenu = new PopupMenu( activity  , v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.album_overflow_delete:

                                    delete(vid.getId(), position );


                                    return true;

                                case R.id.album_overflow_share:
                                    share( vid );
                                    return true;



                                case R.id.album_overflow_detailfm:
                                    showDetailIntent(vid);
                                    return true;







                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };

                    popupMenu.inflate(R.menu.menu_popupvideolist);



                    popupMenu.show();
                }
            });

            /*( (ViewHolder0) viewHolder).thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                    //showDetailIntent(vid);
                }
            });*/
            final ViewGroup.LayoutParams lp = viewHolder.itemView.getLayoutParams();
            ((ViewHolder0) viewHolder).selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);

            ((ViewHolder0) viewHolder).selectedOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                }
            });
        }




    }
    void share(video vid)
    {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setType("text/plain");

        String shareBody = "Titre : "+vid.getTitre()+" - vues : "+vid.getStatistics_viewCount()+" - Commentaires : "+vid.getStatistics_commentCount();

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Video : "+vid.getTitre());

        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);

        activity.startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    static void showDetailFragment(video vid)
    {

        Log.d(TAG, "activity.getSupportFragmentManager().getBackStackEntryCount() : " + activity.getSupportFragmentManager().getBackStackEntryCount());



        if (  activity.getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        //ft.replace(R.id.frame_container, new FragmentVideoDetail().newInstance(vid.getId(), vid.getTitre()) ).addToBackStack(null).commit();

        //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( vid.getTitre() ) );

    }

    static void showDetailIntent(video vid)
    {
        Log.d(TAG, "VideoAdapter - showDetailIntent - uid : "+vid.getId()+" - videoytid :"+vid.getVideoytid());
        Intent VDetailActivity = new Intent( activity, VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId() );
        VDetailActivity.putExtra("videoytid", vid.getVideoytid() );
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition() );
        activity.startActivityForResult(VDetailActivity, 100);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    void delete(final String uid, final int position)
    {
        /**
         * Menu 3 - Afficher Confirm
         */

        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .positiveColorRes(R.color.ColorPrimaryDark)
                .negativeColorRes(R.color.light_gray)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        /*Dbhandler db = new Dbhandler(activity.getApplicationContext());
                        db.deleteFavoriVideo(Integer.parseInt(uid));*/

                        TableFavorivideos.deleteVideoInFavori(uid);

                        VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.msg_confirm_remove_favorite), activity);
                        videoItems.remove(position);
                        recyclerView.getAdapter().notifyDataSetChanged();

                        Log.d(TAG, "recyclerView.getAdapter().getItemCount() : " + recyclerView.getAdapter().getItemCount());
                        if (recyclerView.getAdapter().getItemCount() == 1)
                            frag.setEmptyView(activity.getString(R.string.empty_favori_video));
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        //Toast.makeText(activity, "Clic NON", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();

    }

    @Override
    public int getItemCount() {
        return videoItems == null ? 0 : videoItems.size()+1 ;
    }

    public static class ViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titre;
        //public TextView statistics_commentCount;
        public TextView statistics_viewCount;
        public TextView statistics_likeCount;
        public TextView uid;
        public TextView hdformat;
        public TextView duree;
        public ImageView thumbnail;
        public View overflow;

        public View selectedOverlay;
        private ClickListener listener;
        public LinearLayout fond;

        public ViewHolder0(View convertView) {
            super(convertView);

            fond = (LinearLayout) convertView.findViewById(R.id.fond);

            titre = (TextView) convertView.findViewById(R.id.titre);
            //statistics_commentCount = (TextView) convertView.findViewById(R.id.statistics_commentCount);
            statistics_viewCount = (TextView) convertView.findViewById(R.id.statistics_viewCount);
            statistics_likeCount = (TextView) convertView.findViewById(R.id.statistics_likeCount);
            uid = (TextView) convertView.findViewById(R.id.uid);
            thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);

            hdformat = (TextView) convertView.findViewById(R.id.hdformat);
            duree = (TextView) convertView.findViewById(R.id.duree);
            overflow = convertView.findViewById(R.id.album_overflow);

            selectedOverlay = convertView.findViewById(R.id.selected_overlay);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getPosition());
            }
            Log.d(TAG,"Ligne 276");
            fond.setBackgroundColor(activity.getResources().getColor(R.color.grisclair));

            //Reinitialiser le fond
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fond.setBackgroundColor(Color.TRANSPARENT);
                }
            }, 500);
            FavoriVideoAdapter.showDetailIntent(videoItems.get(getPosition()));
        }

        public interface ClickListener {
            public void onItemClicked(int position);
            //public boolean onItemLongClicked(int position);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolderDeconnecte extends RecyclerView.ViewHolder {

        public ViewHolderDeconnecte(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreached extends RecyclerView.ViewHolder {

        public ViewHolderEndreached(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreachedEmpty extends RecyclerView.ViewHolder {

        public ViewHolderEndreachedEmpty(View v) {
            super(v);
        }
    }
}

package vazo.tsara.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import vazo.tsara.R;
import vazo.tsara.helpers.Config;

/**
 * Created by Miary on 23/08/2015.
 */
public class ArtistsSearchSuggestionsAdapter extends SimpleCursorAdapter
{
    private static final String tag=ArtistsSearchSuggestionsAdapter.class.getName();
    private Context context=null;

    public ArtistsSearchSuggestionsAdapter(Context context, int layout, Cursor video, String[] from, int[] to, int flags) {
        //super(context, layout, c, from, to, flags);
        super(context, layout, video, from, to, flags);
        this.context=context;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String urlimg = "";

        //ImageView thumburl=(ImageView)view.findViewById(R.id.thumburl);
        SimpleDraweeView thumburl = (SimpleDraweeView)view.findViewById(R.id.thumburl);
        TextView titre=(TextView)view.findViewById(R.id.titre);
        TextView id=(TextView)view.findViewById(R.id.id);
        TextView contentDetails_definition=(TextView)view.findViewById(R.id.contentDetails_definition);


        titre.setText( cursor.getString(1) );
        id.setText( cursor.getString(3) );


        urlimg = String.format( Config.PATHPLAYLIST , cursor.getString(2) ) ;
        contentDetails_definition.setVisibility(View.GONE);


        if ( !urlimg.isEmpty() ) {
            //Picasso.with(context).load(urlimg).into(thumburl);
            Uri uri = Uri.parse(urlimg);
            thumburl.setImageURI(uri);
        }

    }






}

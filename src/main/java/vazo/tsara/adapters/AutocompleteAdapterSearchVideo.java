package vazo.tsara.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.SearchVideo;
import vazo.tsara.models.SingleResultSearch;
import vazo.tsara.services.ApiRetrofit;

/**
 * Created by Miary on 08/09/2015.
 */
public class AutocompleteAdapterSearchVideo extends BaseAdapter implements Filterable {
    private Context mContext;
    private List<SingleResultSearch> resultListv = new ArrayList<>();
    static String TAG = Config.TAGKEY;

    String type = null;


    public AutocompleteAdapterSearchVideo(Context context)
    {
        mContext = context;
    }



    @Override
    public int getCount() {
        return resultListv.size();
    }

    @Override
    public Object getItem(int position) {
        return resultListv.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            convertView = inflater.inflate(R.layout.adapter_videosearchsuggestion_item, parent, false);
        }
        if ( position < resultListv.size() ) {

            //ImageView thumburl=(ImageView)convertView.findViewById(R.id.thumburl);
            SimpleDraweeView thumburl=(SimpleDraweeView)convertView.findViewById(R.id.thumburl);
            TextView titre=(TextView)convertView.findViewById(R.id.titre);
            TextView id=(TextView)convertView.findViewById(R.id.id);
            TextView contentDetails_definition=(TextView)convertView.findViewById(R.id.contentDetails_definition);
            TextView thumburlpath=(TextView)convertView.findViewById(R.id.thumburlpath);
            TextView nbvideos=(TextView)convertView.findViewById(R.id.nbvideos);
            TextView firstvideoyt=(TextView)convertView.findViewById(R.id.firstvideoyt);
            TextView playlisteyt=(TextView)convertView.findViewById(R.id.playlisteyt);

            titre.setText( resultListv.get(position).getTitre() );
            id.setText( resultListv.get(position).getId() );
            thumburlpath.setText( String.format(Config.PATHPLAYLIST, resultListv.get(position).getVideoytid()) );
            nbvideos.setText( "" );
            firstvideoyt.setText( "" );
            playlisteyt.setText( "" );


            String urlimg = String.format( Config.THUMB_PATH , resultListv.get(position).getVideoytid() ) ;
            contentDetails_definition.setVisibility(View.GONE);


            if ( !urlimg.isEmpty() ) {
                //Picasso.with(mContext).load(urlimg).into(thumburl);
                Uri uri = Uri.parse(urlimg);
                thumburl.setImageURI(uri);
            }


            //((TextView) convertView.findViewById(android.R.id.text1)).setText(resultList.get(position).getObjetnom());
        }
        return convertView;
    }

    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults filterResults = new FilterResults();
                if (constraint != null)
                {
                    List<SingleResultSearch> predicationList = findPlaylist(constraint.toString());;

                    // Assign the data to the FilterResults
                    filterResults.values = predicationList;
                    filterResults.count = predicationList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                Log.d(TAG,"getFilter - publishResults - results.count:"+results.count);
                if (results != null && results.count > 0)
                {
                    resultListv = (List<SingleResultSearch>) results.values;
                    notifyDataSetChanged();
                }
                else
                {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    /**
     * Returns a search result for the given city prefix
     */
    private List<SingleResultSearch> findPlaylist(String input)
    {
        Log.d(TAG,"findPlaylist type : "+type);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI)
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.getSearchVideo(input, "v", new Callback<SearchVideo>() {

            @Override
            public void success(SearchVideo searchVideo, Response response) {
                if (searchVideo != null) {
                    resultListv = searchVideo.getResults();
                    notifyDataSetChanged();
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        return resultListv;
    }
}

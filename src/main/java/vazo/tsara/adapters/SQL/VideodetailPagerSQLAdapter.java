package vazo.tsara.adapters.SQL;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import vazo.tsara.R;
import vazo.tsara.fragments.SQL.VideoDetailTabInfoSQLFragment;
import vazo.tsara.fragments.SQL.VideoDetailTabRelatedSQLFragmentList;
import vazo.tsara.fragments.VideoDetailTabInfoFragment;
import vazo.tsara.fragments.VideoDetailTabRelatedFragmentList;
import vazo.tsara.fragments.VideoDetailTabStatFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;

/**
 * Created by rabehasy on 29/05/2015.
 */
public class VideodetailPagerSQLAdapter extends FragmentPagerAdapter {

    String id;
    static String TAG = Config.TAGKEY;
    FragmentActivity activity;

    int position;



    public VideodetailPagerSQLAdapter(android.support.v4.app.FragmentManager fm, String id, int position, FragmentActivity activity) {
        super(fm);
        this.id = id;
        this.activity = activity;
        this.position = position;
    }

    //public String[] TITLES = { "Info", "Stats", "En relation"   };

    //private Fragment[] fragments = new Fragment[] { new VideoRecyclerFragment(), new PlaylistsRecyclerFragment()   };

    @Override
    public Fragment getItem(int position) {
        //return SuperAwesomeCardFragment.newInstance(position);
        //return fragments[position];
        Log.d(TAG, "VideodetailPagerAdapter - getItem - ID : " + id);
        switch(position) {
            case 0:
                return VideoDetailTabInfoSQLFragment.newInstance(id, position);


            case 1:
                //return VideoDetailTabRelatedFragment.newInstance(id);
                return VideoDetailTabRelatedSQLFragmentList.newInstance(id);

            case 2:
                return VideoDetailTabStatFragment.newInstance(id);
        }

        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0:
                return this.activity.getString(R.string.tab_videosingle_info);


            case 1:
                return this.activity.getString(R.string.tab_videosingle_related);

            case 2:
                return this.activity.getString(R.string.tab_videosingle_stat);
        }
        return "";

    }

    @Override
    public int getCount() {

        if (Useful.isAdmin(activity) )
            return 3;
        return 2;
    }
}

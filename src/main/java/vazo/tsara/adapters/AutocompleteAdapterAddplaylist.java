package vazo.tsara.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.SearchPlaylist;
import vazo.tsara.models.SingleResultSearchArtiste;
import vazo.tsara.services.ApiRetrofit;

/**
 * Created by Miary on 08/09/2015.
 */
public class AutocompleteAdapterAddplaylist  extends BaseAdapter implements Filterable {
    private Context mContext;
    private List<SingleResultSearchArtiste> resultList = new ArrayList<>();
    static String TAG = Config.TAGKEY;

    String type = null;


    public AutocompleteAdapterAddplaylist(Context context )
    {
        mContext = context;
    }

    public AutocompleteAdapterAddplaylist(Context context, String type )
    {
        mContext = context;
        this.type = type;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
            convertView = inflater.inflate(R.layout.adapter_videosearchsuggestion_item, parent, false);
        }
        if ( position < resultList.size() ) {

            //ImageView thumburl=(ImageView)convertView.findViewById(R.id.thumburl);
            SimpleDraweeView thumburl=(SimpleDraweeView)convertView.findViewById(R.id.thumburl);
            TextView titre=(TextView)convertView.findViewById(R.id.titre);
            TextView id=(TextView)convertView.findViewById(R.id.id);
            TextView contentDetails_definition=(TextView)convertView.findViewById(R.id.contentDetails_definition);
            TextView thumburlpath=(TextView)convertView.findViewById(R.id.thumburlpath);
            TextView nbvideos=(TextView)convertView.findViewById(R.id.nbvideos);
            TextView firstvideoyt=(TextView)convertView.findViewById(R.id.firstvideoyt);
            TextView playlisteyt=(TextView)convertView.findViewById(R.id.playlisteyt);

            titre.setText( resultList.get(position).getObjetnom() );
            id.setText( resultList.get(position).getId() );
            thumburlpath.setText( String.format(Config.PATHPLAYLIST, resultList.get(position).getImage()) );
            nbvideos.setText( resultList.get(position).getNbvideos() );
            firstvideoyt.setText( resultList.get(position).getFirstvideoid() );
            playlisteyt.setText( resultList.get(position).getPlaylisteid() );


            String urlimg = String.format( Config.PATHPLAYLIST , resultList.get(position).getImage() ) ;
            contentDetails_definition.setVisibility(View.GONE);


            if ( !urlimg.isEmpty() ) {
                //Picasso.with(mContext).load(urlimg).into(thumburl);
                Uri uri = Uri.parse(urlimg);
                thumburl.setImageURI(uri);
            }


            //((TextView) convertView.findViewById(android.R.id.text1)).setText(resultList.get(position).getObjetnom());
        }
        return convertView;
    }

    @Override
    public Filter getFilter()
    {
        Filter filter = new Filter()
        {
            @Override
            protected Filter.FilterResults performFiltering(CharSequence constraint)
            {
                FilterResults filterResults = new FilterResults();
                if (constraint != null)
                {
                    List<SingleResultSearchArtiste> predicationList = findPlaylist(constraint.toString());;

                    // Assign the data to the FilterResults
                    filterResults.values = predicationList;
                    filterResults.count = predicationList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                Log.d(TAG,"getFilter - publishResults - results.count:"+results.count);
                if (results != null && results.count > 0)
                {
                    resultList = (List<SingleResultSearchArtiste>) results.values;
                    notifyDataSetChanged();
                }
                else
                {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    /**
     * Returns a search result for the given city prefix
     */
    private List<SingleResultSearchArtiste> findPlaylist(String input)
    {
        Log.d(TAG,"findPlaylist type : "+type);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI)
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.getSearchPlaylist(input, type, new Callback<SearchPlaylist>() {
            @Override
            public void success(SearchPlaylist searchResult, retrofit.client.Response response) {
                //setSuccessInfo(videoPojo);
                  //   Log.d(TAG,"searchResult.getResults() : "+searchResult.getResults().size());
                if (searchResult != null) {
                    resultList = searchResult.getResults() ;
                    notifyDataSetChanged();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d(TAG, "searchResult.getResults() error : " + error.getMessage());

            }
        });
        return resultList;
    }
}

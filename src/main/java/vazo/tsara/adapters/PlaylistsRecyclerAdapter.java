package vazo.tsara.adapters;

/**
 * Created by rabehasy on 27/05/2015.
 */

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import de.greenrobot.event.EventBus;
import vazo.tsara.R;
import vazo.tsara.activities.PlaylistEditActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;


//public class PlaylistsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
public class PlaylistsRecyclerAdapter extends SelectableAdapter<RecyclerView.ViewHolder>{

    private int rowLayout;
    private int rowLayoutfooter;

    static String TAG = "VAZOTSARA";

    private static List<Playlist> videoItems;
    private static FragmentActivity activity;
    private View v;

    private final int VIEW_ITEM = 1;
    public static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_DECONNECTE = 2;
    public static final int VIEW_TYPE_ENDPAGEREACHED = 3;
    public static final int VIEW_TYPE_ENDPAGEREACHED_EMPTY = 4;
    protected int serverListSize = -1;
    private int nb_video_par_page;

    Fragment frag;

    Playlist plinfo;

    protected int loadedSize = -1;

    protected boolean deconnecte;



    public PlaylistsRecyclerAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout) {
        this.videoItems = videoItems;
        this.activity = activity;
        this.rowLayout = rowLayout;

    }

    public PlaylistsRecyclerAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int rowLayoutfooter)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
    }

    public PlaylistsRecyclerAdapter(FragmentActivity activity, List<Playlist> videoItems, int rowLayout, int rowLayoutfooter, int nb_video_par_page)
    {
        this.activity = activity;
        this.videoItems = videoItems;
        this.rowLayout = rowLayout;
        this.rowLayoutfooter = rowLayoutfooter;
        this.nb_video_par_page = nb_video_par_page;
    }


    public int getCount() {
        return videoItems.size()+1;
    }

    public boolean idInAdapter(String id) {
        for( int i = 0; i < videoItems.size(); i++ ) {
            if ( videoItems.get(i).getId().equals(id) ) {
                return true;
            }
        }
        return false;
    }

    public void setServerListSize(int serverListSize){
        this.serverListSize = serverListSize;
    }
    public void setLoadedSize(int loadedSize){
        this.loadedSize = loadedSize;
    }
    public void setDeconnecte(boolean deconnecte){
        this.deconnecte = deconnecte;
    }
    public boolean getDeconnecte(){
        return this.deconnecte;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder0(v);*/

        if (viewType==VIEW_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new ViewHolder0(v);
        } else if ( viewType==VIEW_TYPE_DECONNECTE || getDeconnecte() == true ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_videodeconnecte, parent, false);
            return new ViewHolderDeconnecte(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached, parent, false);
            return new ViewHolderEndreached(v);
        } else if ( viewType==VIEW_TYPE_ENDPAGEREACHED_EMPTY ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_endreached_empty, parent, false);
            return new ViewHolderEndreachedEmpty(v);
        }  else {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayoutfooter, parent, false);
            return new ProgressViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        /*int viewtype = VIEW_ITEM;
        return viewtype;*/

        int viewtype = VIEW_ITEM;

        if ( position+1 == getItemCount()  ) {
            if ( getDeconnecte() )
                viewtype = VIEW_TYPE_DECONNECTE;
            else if (!getDeconnecte() && serverListSize>0 && serverListSize==getItemCount()-1 ) {
                if (nb_video_par_page>0 && serverListSize>nb_video_par_page )
                    viewtype = VIEW_TYPE_ENDPAGEREACHED;
                else
                    viewtype = VIEW_TYPE_ENDPAGEREACHED_EMPTY;
            }
            else
                viewtype = VIEW_TYPE_LOADING;
        }
        return viewtype;
    }

    @Override
    //public void onBindViewHolder(ViewHolder viewHolder, int position) {
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {



        if( viewHolder instanceof ViewHolder0){

            plinfo = videoItems.get(position);

            Log.d(TAG, "isSelected(position) : " +position+" - "+ isSelected(position));




            Log.d(TAG, "PlaylistsAdapter  " + position + " - getObjetnom : " + plinfo.getObjetnom() );


            ( (ViewHolder0) viewHolder).titre.setText(plinfo.getObjetnom());
            ( (ViewHolder0) viewHolder).uid.setText(plinfo.getId());
            ( (ViewHolder0) viewHolder).nbvideos.setText( ( Integer.parseInt( plinfo.getNbVideos() ) == 1?activity.getString(R.string.sprintf_onevideo):(Integer.parseInt( plinfo.getNbVideos() ) == 0  ? activity.getString(R.string.sprintf_novideo) : String.format(activity.getString(R.string.sprintf_nbvideos), plinfo.getNbVideos()) ) ));
            if ( ( (ViewHolder0) viewHolder).image != null ) {
                Log.d(TAG, "String.format( Config.PATHPLAYLIST ,v.getImage() ) : " + String.format(Config.PATHPLAYLIST, plinfo.getImage()));
                //Picasso.with(this.activity).load( String.format( Config.PATHPLAYLIST ,plinfo.getImage() ) ).placeholder(R.drawable.user).into(((ViewHolder0) viewHolder).image);
                Uri uri = Uri.parse( String.format( Config.PATHPLAYLIST ,plinfo.getImage() ));
                ((ViewHolder0) viewHolder).image.setImageURI(uri);
            }




            // Do view inflation, etc.

            if ( ( (ViewHolder0) viewHolder).overflow != null ) {
                ((ViewHolder0) viewHolder).overflow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(activity, v) {
                            @Override
                            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.item_delete:
                                        delete(String.valueOf(plinfo.getId()), position);
                                        return true;

                                    case R.id.item_edit:
                                        edit(plinfo);
                                        return true;

                                    case R.id.item_share:
                                        //share(vid);
                                        return true;

                                    case R.id.item_rename:
                                        //frag.updateItemText()
                                    /*videoItems.get(position).setNom("TOTOTO");
                                    notifyDataSetChanged();*/
                                        openEditDialog(plinfo, PlaylistsRecyclerAdapter.this);
                                        return true;


                                    default:
                                        return super.onMenuItemSelected(menu, item);
                                }
                            }
                        };

                        popupMenu.inflate(R.menu.menu_popup_playlist);

                        if (!Useful.isAdmin(activity)) {
                            popupMenu.getMenu().removeItem(R.id.item_delete);
                            popupMenu.getMenu().removeItem(R.id.item_edit);
                            popupMenu.getMenu().removeItem(R.id.item_rename);
                        }


                        popupMenu.show();
                    }
                });
            }


            final ViewGroup.LayoutParams lp = viewHolder.itemView.getLayoutParams();

            //((ViewHolder0) viewHolder).selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
            ((ViewHolder0) viewHolder).selectedOverlay.setVisibility( View.VISIBLE  );

            ((ViewHolder0) viewHolder).selectedOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                }
            });
        }
    }

    void edit(Playlist v){
        Intent i = new Intent(activity, PlaylistEditActivity.class);
        EventBus.getDefault().postSticky(v);
        activity.startActivityForResult(i, 0);
    }
    void openEditDialog(Playlist v, PlaylistsRecyclerAdapter adapter)
    {
        VazotsaraShareFunc.showDialogEditPlaylist(v, activity, adapter);
    }
    static void showListeVideoByPlaylists(Playlist p )
    {
        Intent VideoSearchActivity = new Intent(activity, VideosByArtistActivity.class);
        EventBus.getDefault().postSticky(p);
        /*VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre );*/
        activity.startActivityForResult(VideoSearchActivity, 100);
    }
    void delete(final String uid, final int position)
    {
        VazotsaraShareFunc.showDialogDeletePlaylist(activity, uid, position, this, videoItems);
    }
    void share(Playlist vid)
    {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Nom : "+vid.getObjetnom()+" - videos : "+vid.getNbVideos();
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Video : "+vid.getObjetnom());
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    static void showDetailFragment(Playlist vid)
    {

        Log.d(TAG,"activity.getSupportFragmentManager().getBackStackEntryCount() : "+activity.getSupportFragmentManager().getBackStackEntryCount() );



        if (  activity.getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        //ft.replace(R.id.frame_container, new FragmentVideoDetail().newInstance(vid.getId(), vid.getTitre()) ).addToBackStack(null).commit();

        //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( vid.getTitre() ) );

    }
    static void showDetailIntent(Playlist vid)
    {

        Intent VDetailActivity = new Intent( activity, VideoDetailActivity.class);
        VDetailActivity.putExtra("uid", vid.getId());
        activity.startActivityForResult(VDetailActivity, 100);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    @Override
    public int getItemCount() {
        return videoItems == null ? 0 : videoItems.size();
    }

    public class ViewHolder0 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titre;
        public TextView nbvideos;
        public TextView uid;
        //public ImageView image;
        public SimpleDraweeView image;
        public View overflow;
        public RelativeLayout fond;

        public View selectedOverlay;
        //private ClickListener listener;

        public ViewHolder0(View convertView) {
            super(convertView);


            fond = (RelativeLayout) convertView.findViewById(R.id.fond);
            titre = (TextView) convertView.findViewById(R.id.titre);
            nbvideos = (TextView) convertView.findViewById(R.id.nbvideos);
            uid = (TextView) convertView.findViewById(R.id.uid);
            //image = (ImageView) convertView.findViewById(R.id.image);
            image = (SimpleDraweeView) convertView.findViewById(R.id.image);
            overflow = convertView.findViewById(R.id.album_overflow);

            selectedOverlay = convertView.findViewById(R.id.selected_overlay);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            /*if (listener != null) {
                listener.onItemClicked(getPosition());
            }*/
            Log.d(TAG, "Ligne 276");
            /*final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {*/
                    //PlaylistsRecyclerAdapter.showDetailIntent(videoItems.get(getPosition()));
            /*if ( isSelected(getPosition()) )
                Log.d(TAG,"");*/

            //toggleSelection(getPosition());


            fond.setBackgroundColor(activity.getResources().getColor(R.color.grisclair));

            //Reinitialiser le fond
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fond.setBackgroundColor(Color.TRANSPARENT);
                }
            }, 500);


                    //PlaylistsRecyclerAdapter.showListeVideoByPlaylists(videoItems.get(getPosition()).getId(), videoItems.get(getPosition()).getObjetnom());
                    PlaylistsRecyclerAdapter.showListeVideoByPlaylists( videoItems.get(getPosition()) );
            /*    }
            }, 500);*/

        }

        /*public interface ClickListener {
            public void onItemClicked(int position);
            //public boolean onItemLongClicked(int position);
        }*/
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolderDeconnecte extends RecyclerView.ViewHolder {

        public ViewHolderDeconnecte(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreached extends RecyclerView.ViewHolder {

        public ViewHolderEndreached(View v) {
            super(v);
        }
    }

    public static class ViewHolderEndreachedEmpty extends RecyclerView.ViewHolder {

        public ViewHolderEndreachedEmpty(View v) {
            super(v);
        }
    }
}

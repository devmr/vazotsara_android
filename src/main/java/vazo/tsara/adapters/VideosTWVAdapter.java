package vazo.tsara.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.models.Videogson;

/**
 * Created by rabehasy on 11/06/2015.
 */
public class VideosTWVAdapter extends BaseAdapter implements Parcelable {
    private Activity activity;
    private LayoutInflater inflater;
    private int layout = 0;
    private List<Videogson> itemsartistes;
    static String TAG = "VAZOTSARA";

    public VideosTWVAdapter(Activity activity, List<Videogson> itemsartistes)
    {
        this.activity = activity;
        this.itemsartistes = itemsartistes;
    }

    public VideosTWVAdapter(Activity activity, List<Videogson> itemsartistes, int layout)
    {
        this.activity = activity;
        this.itemsartistes = itemsartistes;
        this.layout = layout;
    }

    public String getFieldAt(int position, String field )
    {
        switch( field ) {
            case "titre":
                return itemsartistes.get(position).getTitre() ;

            case "thumbnail":
                return itemsartistes.get(position).getThumburl() ;

            case "id":
                return itemsartistes.get(position).getId();

            default:
                return "";

        }


    }

    @Override
    public int getCount() {
        return itemsartistes.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsartistes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if( layout == 0 )
            layout = R.layout.adaper_artiste_item;

        if ( convertView == null ) {
            Log.d(TAG, "convertView == null");
            convertView = inflater.inflate(layout, null);
        } else {
            Log.d(TAG, "convertView.toString(): " + convertView.toString());
        }

        Videogson i = itemsartistes.get(position);


        TextView titre = (TextView) convertView.findViewById(R.id.titre);
        TextView uid = (TextView) convertView.findViewById(R.id.uid);

        //ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
        SimpleDraweeView thumbnail = (SimpleDraweeView) convertView.findViewById(R.id.thumbnail);
        Button croix = (Button) convertView.findViewById(R.id.croix);

        titre.setText(i.getTitre() );
        uid.setText(i.getId());


        if ( i.getThumburl()!="") {
            //Picasso.with(activity).load(i.getThumburl()).into(thumbnail);
            Uri uri = Uri.parse(i.getThumburl());
            thumbnail.setImageURI(uri);
        }

        if ( croix != null ) {
            croix.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemsartistes.remove(position);
                    notifyDataSetChanged();
                }
            });
        }

        return convertView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(this, flags);
    }
}

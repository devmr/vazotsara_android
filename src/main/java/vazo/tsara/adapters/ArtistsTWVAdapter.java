package vazo.tsara.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.models.relatedvideomodel;

/**
 * Created by rabehasy on 11/06/2015.
 */
public class ArtistsTWVAdapter extends BaseAdapter implements Parcelable {
    private Activity activity;
    private LayoutInflater inflater;
    private int layout = 0;
    private List<relatedvideomodel> itemsartistes;
    static String TAG = "VAZOTSARA";

    public ArtistsTWVAdapter(Activity activity, List<relatedvideomodel> itemsartistes)
    {
        this.activity = activity;
        this.itemsartistes = itemsartistes;
    }

    public ArtistsTWVAdapter(Activity activity, List<relatedvideomodel> itemsartistes, int layout)
    {
        this.activity = activity;
        this.itemsartistes = itemsartistes;
        this.layout = layout;
    }

    public String getFieldAt(int position, String field )
    {
        switch( field ) {
            case "nom":
                return itemsartistes.get(position).getObjetnom() ;

            case "firstvideoid":
                return itemsartistes.get(position).getFirstvideoid() ;

            case "playlisteid":
                return itemsartistes.get(position).getPlaylisteid() ;

            case "id":
                return itemsartistes.get(position).getUid();

            default:
                return "";

        }


    }

    @Override
    public int getCount() {
        return itemsartistes.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsartistes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if( layout == 0 )
            layout = R.layout.adaper_artiste_item;

        if ( convertView == null ) {
            Log.d(TAG, "convertView == null");
            convertView = inflater.inflate(layout, null);
        } else {
            Log.d(TAG, "convertView.toString(): " + convertView.toString());
        }

        relatedvideomodel i = itemsartistes.get(position);


        TextView nom = (TextView) convertView.findViewById(R.id.nom);
        TextView uid = (TextView) convertView.findViewById(R.id.uid);
        TextView nbvideos = (TextView) convertView.findViewById(R.id.nbvideos);
        TextView logoreference = (TextView) convertView.findViewById(R.id.logoreference);
        //ImageView logo = (ImageView) convertView.findViewById(R.id.logo);
        SimpleDraweeView logo = (SimpleDraweeView) convertView.findViewById(R.id.logo);
        Button croix = (Button) convertView.findViewById(R.id.croix);

        nom.setText(i.getObjetnom() );
        uid.setText(i.getUid());
        logoreference.setText(i.getImage());
        nbvideos.setText( " ("+i.getNbVideos()+" vidéo"+(Integer.parseInt(i.getNbVideos())>1?"s":"")+")");

        if ( i.getPathplayliste()!="") {
            Log.d(TAG, "i.getPathplayliste: " + i.getPathplayliste());
            //Glide.with(activity).load(i.getPathplayliste()).into(logo);
            //Picasso.with(activity).load(i.getPathplayliste()).into(logo);
            Uri uri = Uri.parse(i.getPathplayliste());
            logo.setImageURI(uri);
        }

        if ( croix != null ) {
            croix.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemsartistes.remove(position);
                    notifyDataSetChanged();
                }
            });
        }

        return convertView;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(this, flags);
    }
}

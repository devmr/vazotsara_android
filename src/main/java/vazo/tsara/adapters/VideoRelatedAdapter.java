package vazo.tsara.adapters;

/**
 * Created by rabehasy on 27/05/2015.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.joanzapata.android.iconify.Iconify;

import java.util.List;

import vazo.tsara.R;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.models.video;


//public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
public class VideoRelatedAdapter extends SelectableAdapter<RecyclerView.ViewHolder>{

    private int rowLayout;

    static String TAG = "VAZOTSARA";

    private static List<video> videoItems;
    private static List<video> videorealItems;

    private static FragmentActivity activity;
    private View v;

    private final int VIEW_ITEM_ARTISTE = 1;
    private final int VIEW_ITEM_REALISATEUR = 3;
    private final int VIEW_ITEM_ARTISTE_H = 0;
    private final int VIEW_ITEM_REALISATEUR_H = 2;

    String itemlabel[] = {"VIEW_ITEM_ARTISTE_H","VIEW_ITEM_ARTISTE","VIEW_ITEM_REALISATEUR_H","VIEW_ITEM_REALISATEUR"};
    private int viewtype = 0;




    public VideoRelatedAdapter(FragmentActivity activity, List<video> videoItems, List<video> videorealItems, int rowLayout) {
        this.videoItems = videoItems;
        this.videorealItems = videorealItems;
        this.activity = activity;
        this.rowLayout = rowLayout;

    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if ( viewType == VIEW_ITEM_ARTISTE  ) {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new ViewHolderArtist(v);
        }

        if ( viewType == VIEW_ITEM_REALISATEUR ) {
            v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
            return new ViewHolderRealisateur(v);
        }

        if ( viewType == VIEW_ITEM_ARTISTE_H ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaper_artiste_header, parent, false);
            return new VHHeaderArtiste(v);
        }

        if ( viewType == VIEW_ITEM_REALISATEUR_H ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaper_realisateur_header, parent, false);
            return new VHHeaderRealisateur(v);
        }

        return null;

    }
    private boolean isPositionVideoHeader(int position) {
        return position == 0;
    }
    private boolean isPositionRealisateurHeader(int position) {
        return position==(videoItems.size()+1);
    }
    @Override
    public int getItemViewType(int position) {
        viewtype = VIEW_ITEM_ARTISTE;
       if (videorealItems.size()>0 && position > videoItems.size() ) {
           viewtype = VIEW_ITEM_REALISATEUR;
           //Log.d(TAG, "getItemViewType viewtype : VIEW_ITEM_REALISATEUR - position : "+position+" - videorealItems.size :"+videorealItems.size()+" - videoItems.size(): "+videoItems.size());
       }

         if ( isPositionVideoHeader(position) ) {
             viewtype = VIEW_ITEM_ARTISTE_H;
             //Log.d(TAG, "getItemViewType viewtype : VIEW_ITEM_ARTISTE_H - position : "+position+" - videorealItems.size :"+videorealItems.size()+" - videoItems.size(): "+videoItems.size());
         }

         if (videorealItems.size()>0 && isPositionRealisateurHeader(position) ) {
             viewtype = VIEW_ITEM_REALISATEUR_H;
             //Log.d(TAG, "getItemViewType viewtype : VIEW_ITEM_REALISATEUR_H - position : "+position+" - videorealItems.size :"+videorealItems.size()+" - videoItems.size(): "+videoItems.size());
         }



        Log.d(TAG, "getItemViewType viewtype : "+itemlabel[viewtype]+" - position : "+position+" - videorealItems.size() : "+videorealItems.size()+" - videoItems.size(): "+videoItems.size()+" - isPositionRealisateurHeader : "+isPositionRealisateurHeader(position)+" - position==videoItems.size()+1 : "+(position)+"=="+(videoItems.size()+1)+" - getItemCount(): "+getItemCount());
        return viewtype;
    }

    @Override
    //public void onBindViewHolder(ViewHolder viewHolder, int position) {
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        Log.d(TAG, "onBindViewHolder - Position : " + position+"  - videoItems.size(): "+videoItems.size()+" - videorealItems.size(): "+videorealItems.size()+" - getItemCount(): "+getItemCount());


        if(videoItems.size() > 0 && viewHolder instanceof ViewHolderArtist){


            Log.d(TAG, "Position ViewHolderArtist: (L140) " + position+"  - videoItems.size(): "+videoItems.size()+" - Header ? "+(position==0?"Oui":"Non") );
            Log.d(TAG,"onBindViewHolder videoItems.size(): "+videoItems.size());
            Log.d(TAG,"onBindViewHolder videorealItems.size(): "+videorealItems.size());





            Log.d(TAG, "Position ViewHolderArtist: " + position );

            if ( position-1 == videoItems.size() )
                return;

            final video vid = videoItems.get(position-1);

            Log.d(TAG, "Position ViewHolderArtist: "+position+" - titre : "+vid.getTitre());


            ( (ViewHolderArtist) viewHolder).titre.setText(vid.getTitre());
            //( (ViewHolder0) viewHolder).statistics_commentCount.setText("{fa_comment} "+vid.getStatistics_commentCount());
            ( (ViewHolderArtist) viewHolder).statistics_viewCount.setText("{fa_youtube_play} "+vid.getStatistics_viewCount());
            ( (ViewHolderArtist) viewHolder).statistics_likeCount.setText("{fa_thumbs_up} " +vid.getStatistics_likeCount());
            ( (ViewHolderArtist) viewHolder).uid.setText(vid.getId());

            //Iconify.addIcons(((ViewHolder0) viewHolder).statistics_commentCount);
            Iconify.addIcons(((ViewHolderArtist) viewHolder).statistics_viewCount);
            Iconify.addIcons(((ViewHolderArtist) viewHolder).statistics_likeCount);


            /**
             * Image Picasso
             */
            if( vid.getThumburl() != null && !vid.getThumburl().isEmpty() ) {

                    //Glide.with(activity).load(vid.getThumburl()).into(((ViewHolder0) viewHolder).thumbnail);
                //Picasso.with(activity).load(vid.getThumburl()).into(( (ViewHolderArtist) viewHolder).thumbnail);
                Uri uri = Uri.parse(vid.getThumburl());
                ( (ViewHolderArtist) viewHolder).thumbnail.setImageURI(uri);
            }


            // Do view inflation, etc.


            ( (ViewHolderArtist) viewHolder).overflow.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu( activity  , v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.album_overflow_delete:

                                    //delete(vid.getId(), position );


                                    return true;

                                case R.id.album_overflow_share:
                                    share( vid );
                                    return true;



                                case R.id.album_overflow_detailfm:
                                    showDetailIntent(vid);
                                    return true;

                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };

                    popupMenu.inflate(R.menu.menu_popupvideolist);



                    popupMenu.show();
                }
            });

            /*( (ViewHolder0) viewHolder).thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                    //showDetailIntent(vid);
                }
            });*/
            final ViewGroup.LayoutParams lp = viewHolder.itemView.getLayoutParams();
            ((ViewHolderArtist) viewHolder).selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);

            ((ViewHolderArtist) viewHolder).selectedOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                }
            });
        }

        if(videorealItems.size()>0 && viewHolder instanceof ViewHolderRealisateur){

            int posafter = 0;

            if ( position==videoItems.size()+2 ) {
                posafter = 0;
                Log.d(TAG, "L228 :");
            } else {
                Log.d(TAG, "L229 : (("+videorealItems.size()+" - 3) - (("+videorealItems.size()+" - 1) - ("+position+" + 2))) - 3" );
                Log.d(TAG, "L230 : (("+position+"-"+videorealItems.size()+")-("+videoItems.size()+"-"+videorealItems.size()+"))-2" );
                if ( videorealItems.size() > videoItems.size() ) {

                    if ( videoItems.size() == 0 )
                        posafter = ((videorealItems.size() - 3) - ((videorealItems.size() - 1) - (position + 2))) - 2;
                    else
                        posafter = ((videorealItems.size() - 3) - ((videorealItems.size() - 1) - (position + 2))) - 3;
                } else {
                    posafter = ((position - videorealItems.size()) - (videoItems.size() - videorealItems.size())) - 2;
                }
            }

            Log.d(TAG, "Position ViewHolderRealisateur: " + position+" - videorealItems.size: "+videorealItems.size()+" - Realpos : "+posafter+" - Calcul : "+(videorealItems.size()-3)+"-("+(videorealItems.size()-1)+"-"+(position+2)+")-3");


            if(posafter<0 || posafter >= videorealItems.size() ) {
                 Log.d(TAG, "Position END : "+posafter);
                 return;
             }

            final video vid = videorealItems.get(posafter);




            ( (ViewHolderRealisateur) viewHolder).titre.setText(vid.getTitre());
            //( (ViewHolder0) viewHolder).statistics_commentCount.setText("{fa_comment} "+vid.getStatistics_commentCount());
            ( (ViewHolderRealisateur) viewHolder).statistics_viewCount.setText("{fa_youtube_play} "+vid.getStatistics_viewCount());
            ( (ViewHolderRealisateur) viewHolder).statistics_likeCount.setText("{fa_thumbs_up} " +vid.getStatistics_likeCount());
            ( (ViewHolderRealisateur) viewHolder).uid.setText(vid.getId());

            //Iconify.addIcons(((ViewHolder0) viewHolder).statistics_commentCount);
            Iconify.addIcons(((ViewHolderRealisateur) viewHolder).statistics_viewCount);
            Iconify.addIcons(((ViewHolderRealisateur) viewHolder).statistics_likeCount);


            /**
             * Image Picasso
             */
            if( vid.getThumburl() != null && !vid.getThumburl().isEmpty() ) {

                    //Glide.with(activity).load(vid.getThumburl()).into(((ViewHolder0) viewHolder).thumbnail);
                //Picasso.with(activity).load(vid.getThumburl()).into(( (ViewHolderRealisateur) viewHolder).thumbnail);
                Uri uri = Uri.parse(vid.getThumburl());
                ( (ViewHolderRealisateur) viewHolder).thumbnail.setImageURI(uri);
            }


            // Do view inflation, etc.


            ( (ViewHolderRealisateur) viewHolder).overflow.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu( activity  , v) {
                        @Override
                        public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.album_overflow_delete:

                                    //delete(vid.getId(), position );


                                    return true;

                                case R.id.album_overflow_share:
                                    share( vid );
                                    return true;



                                case R.id.album_overflow_detailfm:
                                    showDetailIntent(vid);
                                    return true;







                                default:
                                    return super.onMenuItemSelected(menu, item);
                            }
                        }
                    };

                    popupMenu.inflate(R.menu.menu_popupvideolist);



                    popupMenu.show();
                }
            });

            /*( (ViewHolder0) viewHolder).thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                    //showDetailIntent(vid);
                }
            });*/
            final ViewGroup.LayoutParams lp = viewHolder.itemView.getLayoutParams();
            ((ViewHolderRealisateur) viewHolder).selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);

            ((ViewHolderRealisateur) viewHolder).selectedOverlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //showDetailFragment(vid);
                }
            });

        }


        if( viewHolder instanceof VHHeaderRealisateur){

            String textheader = activity.getString(R.string.empty_video_realisateur);

            if ( videorealItems.size()>1) {
                if ( videorealItems.size()>1)
                    textheader = activity.getString(R.string.other_videos_bys);
                else
                    textheader = activity.getString(R.string.other_video_bys);

            } else if (videorealItems.size()==1) {
                textheader = activity.getString(R.string.other_video_by);
            }

            ((VHHeaderRealisateur) viewHolder).txtheader.setText(textheader);
        }

        if( viewHolder instanceof VHHeaderArtiste){

            String textheader = activity.getString(R.string.empty_video_artiste);

            if ( videoItems.size()>1) {
                if ( videoItems.size()>1)
                    textheader = activity.getString(R.string.other_videos_artiste);
                else
                    textheader = activity.getString(R.string.other_video_artistes);

            } else if (videoItems.size()==1) {
                textheader = activity.getString(R.string.other_video_artiste);
            }

            ((VHHeaderArtiste) viewHolder).txtheader.setText(textheader);
        }




    }
    void share(video vid)
    {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setType("text/plain");

        String shareBody = "Titre : "+vid.getTitre()+" - vues : "+vid.getStatistics_viewCount()+" - Commentaires : "+vid.getStatistics_commentCount();

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Video : "+vid.getTitre());

        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);

        activity.startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    static void showDetailFragment(video vid)
    {

        Log.d(TAG, "activity.getSupportFragmentManager().getBackStackEntryCount() : " + activity.getSupportFragmentManager().getBackStackEntryCount());



        if (  activity.getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        //ft.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_up, R.animator.slide_down);
        //ft.replace(R.id.frame_container, new FragmentVideoDetail().newInstance(vid.getId(), vid.getTitre()) ).addToBackStack(null).commit();

        //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( vid.getTitre() ) );

    }
    static void showDetailIntent(video vid)
    {

        Intent VDetailActivity = new Intent( activity, VideoDetailActivity.class);
        VDetailActivity.putExtra("uid", vid.getId() );
        activity.startActivityForResult(VDetailActivity, 100);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    void delete(String uid, int position)
    {
        /**
         * Menu 3 - Afficher Confirm
         */
        AlertDialog.Builder confirmDialog = new AlertDialog.Builder( activity )  ;
        confirmDialog.setTitle("Supprimer");
        confirmDialog.setMessage("Voulez-vous vraiment supprimer cet element ?");

        confirmDialog.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "Clic OUI", Toast.LENGTH_SHORT).show();



            }
        }).setNegativeButton("NON", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(activity, "Clic NON", Toast.LENGTH_SHORT).show();
            }
        });
        confirmDialog.show();
    }

    @Override
    public int getItemCount() {

        int nb = 0;

        nb += (videoItems.size()+videorealItems.size());

        if ( videoItems.size()>0 ) {
            nb += 1;
        }

        if ( videorealItems.size()>0 && videoItems.size()==0) {
            nb += 2;
        }

        if ( videorealItems.size()==1 && videoItems.size()==1) {
            nb += 1;
        }

        return nb;
    }

    public static class ViewHolderArtist extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titre;
        //public TextView statistics_commentCount;
        public TextView statistics_viewCount;
        public TextView statistics_likeCount;
        public TextView uid;
        //public ImageView thumbnail;
        public SimpleDraweeView thumbnail;
        public View overflow;

        public View selectedOverlay;
        private ClickListener listener;

        public ViewHolderArtist(View convertView) {
            super(convertView);


            titre = (TextView) convertView.findViewById(R.id.titre);
            //statistics_commentCount = (TextView) convertView.findViewById(R.id.statistics_commentCount);
            statistics_viewCount = (TextView) convertView.findViewById(R.id.statistics_viewCount);
            statistics_likeCount = (TextView) convertView.findViewById(R.id.statistics_likeCount);
            uid = (TextView) convertView.findViewById(R.id.uid);
            //thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            thumbnail = (SimpleDraweeView) convertView.findViewById(R.id.thumbnail);
            overflow = convertView.findViewById(R.id.album_overflow);

            selectedOverlay = convertView.findViewById(R.id.selected_overlay);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getPosition());
            }
            Log.d(TAG,"onClick getPosition() L457 - "+getPosition());
            VideoRelatedAdapter.showDetailIntent(videoItems.get(getPosition()-1));
        }

        public interface ClickListener {
            public void onItemClicked(int position);
            //public boolean onItemLongClicked(int position);
        }
    }

    public static class ViewHolderRealisateur extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titre;
        //public TextView statistics_commentCount;
        public TextView statistics_viewCount;
        public TextView statistics_likeCount;
        public TextView uid;
        //public ImageView thumbnail;
        public SimpleDraweeView thumbnail;
        public View overflow;

        public View selectedOverlay;
        private ClickListener listener;

        public ViewHolderRealisateur(View convertView) {
            super(convertView);


            titre = (TextView) convertView.findViewById(R.id.titre);
            //statistics_commentCount = (TextView) convertView.findViewById(R.id.statistics_commentCount);
            statistics_viewCount = (TextView) convertView.findViewById(R.id.statistics_viewCount);
            statistics_likeCount = (TextView) convertView.findViewById(R.id.statistics_likeCount);
            uid = (TextView) convertView.findViewById(R.id.uid);
            //thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            thumbnail = (SimpleDraweeView) convertView.findViewById(R.id.thumbnail);
            overflow = convertView.findViewById(R.id.album_overflow);

            selectedOverlay = convertView.findViewById(R.id.selected_overlay);
            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getPosition());
            }

            int posafter = 0;


            if ( getPosition()==videoItems.size()+2 ) {
                posafter = 0;
                Log.d(TAG, "L228 :");
            } else {
                if ( videorealItems.size() > videoItems.size() ) {

                    if ( videoItems.size() == 0 )
                        posafter = ((videorealItems.size() - 3) - ((videorealItems.size() - 1) - (getPosition() + 2))) - 2;
                    else
                        posafter = ((videorealItems.size() - 3) - ((videorealItems.size() - 1) - (getPosition() + 2))) - 3;
                } else
                    posafter = ((getPosition()-videorealItems.size())-(videoItems.size()-videorealItems.size()))-2;
            }


            Log.d(TAG, "onClick getPosition() L500 - getPosition(): " + getPosition() + " - videoItems.size(): "+videoItems.size()+" - videorealItems.size(): "+videorealItems.size()+" - pos : "+ posafter);


                    VideoRelatedAdapter.showDetailIntent(videorealItems.get(posafter));
        }

        public interface ClickListener {
            public void onItemClicked(int position);
            //public boolean onItemLongClicked(int position);
        }
    }

    public class VHHeaderArtiste  extends RecyclerView.ViewHolder {
        public TextView txtheader;
        public VHHeaderArtiste (View convertView) {

            super(convertView);
            txtheader = (TextView) convertView.findViewById(R.id.txtheader);
        }
    }

    public class VHHeaderRealisateur  extends RecyclerView.ViewHolder {
        public TextView txtheader;
        public VHHeaderRealisateur (View convertView) {

            super(convertView);
            txtheader = (TextView) convertView.findViewById(R.id.txtheader);
        }
    }
}

package vazo.tsara.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import vazo.tsara.R;
import vazo.tsara.models.relatedvideomodel;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class ArtistsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<relatedvideomodel> itemsartistes;

    private FragmentActivity activity;

    private View header;
    private View v;

    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;
    private static final int ITEM_VIEW_TYPE_ITEMREALISATEUR = 2;
    private static final int ITEM_VIEW_TYPE_ITEMREALISATEURH = 3;

    static String TAG = "VAZOTSARA";



    public ArtistsAdapter(FragmentActivity activity, List<relatedvideomodel> itemsartistes ) {
        this.itemsartistes = itemsartistes;
        this.activity = activity;

        Log.d(TAG, "ArtistsAdapter - L 30" );
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }





    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if ( viewType == ITEM_VIEW_TYPE_ITEM ) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaper_artiste_item, parent, false);
            Log.d(TAG, "ArtistsRecyclerAdapter - L 71" );
            return new VHItem(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adaper_artiste_header, parent, false);
            Log.d(TAG, "ArtistsRecyclerAdapter - L 43" );
            return new VHHeader(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Log.d(TAG, "onBindViewHolder - L 90 - position : " + position+ " - isPositionHeader(position) : "+isPositionHeader(position ) );

        if(!isPositionHeader(position) && holder instanceof VHItem){
            Log.d(TAG, "onBindViewHolder - L 93 ");
            position = position - 1;
            final relatedvideomodel artiste = itemsartistes.get(position);
            ((VHItem) holder).nom.setText( artiste.getObjetnom() );
            Log.d(TAG, "ArtistsRecyclerAdapter - L 44 " + artiste.getObjetnom());
        }



        if( holder instanceof VHHeader){
            Log.d(TAG, "onBindViewHolder - L 108 ");


        }

        //holder.nom.setText( artiste.getObjetnom() );
    }




    @Override
    public int getItemViewType(int position) {

        Log.d(TAG,"getItemViewType - position : "+position+" - itemsartistes.size : "+itemsartistes.size() );

        int viewtype = ITEM_VIEW_TYPE_HEADER;

        if (!isPositionHeader(position))
            viewtype = ITEM_VIEW_TYPE_ITEM;

        Log.d(TAG,"getItemViewType - viewtype : "+viewtype+ " - position : "+position);


        return viewtype;
    }

    @Override
    public int getItemCount() {
        //Log.d("ANDYROHYHIDY", "ArtistsRecyclerAdapter - L 55" );
        int nb = 0;
        if (itemsartistes.size()>0)
            nb = (itemsartistes.size() );



        return nb;
    }

    public class VHItem  extends RecyclerView.ViewHolder {

        public TextView nom;


        public VHItem(View convertView) {
            super(convertView);
            nom = (TextView) convertView.findViewById(R.id.nom);
            Log.d(TAG, "ArtistsAdapter - L 67" );
        }

    }


    public class VHHeader  extends RecyclerView.ViewHolder {


        public VHHeader (View convertView) {
            super(convertView);
        }
    }


}


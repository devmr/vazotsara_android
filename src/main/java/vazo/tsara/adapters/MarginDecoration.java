package vazo.tsara.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import vazo.tsara.R;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class MarginDecoration  extends RecyclerView.ItemDecoration  {
    private int margin;

    public MarginDecoration(Context context) {
        margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //outRect.set(margin, margin, margin, margin);
        outRect.set(margin, 0, margin, 0);
    }
}


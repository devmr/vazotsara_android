package vazo.tsara.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

import vazo.tsara.R;
import vazo.tsara.fragments.TabfavoriteArtistsFragment;
import vazo.tsara.fragments.TabfavoriteVideosFragment;
import vazo.tsara.fragments.TabfavoriteVideosinplaylistsfavoriteFragment;
import vazo.tsara.helpers.Config;

/**
 * Created by rabehasy on 29/05/2015.
 */
public class FavoritePagerAdapter extends FragmentPagerAdapter {

    static String TAG = Config.TAGKEY;
    FragmentActivity activity;



    public FavoritePagerAdapter(android.support.v4.app.FragmentManager fm, FragmentActivity activity) {
        super(fm);
        this.activity = activity;

    }


    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                return TabfavoriteVideosFragment.newInstance();
            /*case 1:
                return TabhomePlaylistsFragment.newInstance();*/

            case 1:
                return TabfavoriteArtistsFragment.newInstance();

            case 2:
                return TabfavoriteVideosinplaylistsfavoriteFragment.newInstance();
        }

        return null;
    }
    @Override
    public CharSequence getPageTitle(int position) {

        switch(position) {
            case 0:
                return this.activity.getString(R.string.tab_home_video);
            /*case 1:
                return this.activity.getString(R.string.tab_home_playlists);*/

            case 1:
                return this.activity.getString(R.string.tab_home_artists);

            case 2:
                return this.activity.getString(R.string.tab_favorite_videosbyartists_favorite);
        }
        return "";
    }

    @Override
    public int getCount() {
        return 3;
    }
}

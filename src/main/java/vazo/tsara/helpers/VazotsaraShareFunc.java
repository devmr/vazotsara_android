package vazo.tsara.helpers;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.cocosw.bottomsheet.BottomSheet;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import vazo.tsara.R;
import vazo.tsara.activities.AddVideoActivity;
import vazo.tsara.activities.LoginActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.adapters.PlaylistsRecyclerAdapter;
import vazo.tsara.adapters.SQL.PlaylistsRecyclerSQLAdapter;
import vazo.tsara.models.FieldAddPlaylist;
import vazo.tsara.models.FieldAddUser;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.ServerMsgPojo;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.TableVideosPlaylists;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.profileModel;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * Created by rabehasy on 17/09/2015.
 */
public class VazotsaraShareFunc {

    static String TAG = Config.TAGKEY;

    static String[] idCategoryYt = { "15","2","24", "27", "1", "23", "20", "10", "29", "22", "28", "17", "26", "19" };

    static EditText edit_playlistenom;
    static TextInputLayout textInputLayoutNomplayliste;
    static View positiveActionPlayliste;
    static MaterialSpinner spinn_playlistetype;
    static MaterialSpinner spinn_status;
    static ArrayAdapter<CharSequence> adapter_spinn_playlistetype;
    static ArrayAdapter<CharSequence>  adapter_spinn_status;


    static EditText edit_urlvideo;
    static TextInputLayout textInputLayoutUrl;
    static RelativeLayout bloc_clipboard;

    public static void checkFavoriServer() {

    }
    public static String getAdminToken(Context context) {
        //return PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        return context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("token", "");
    }

    public static List<Videogson> convertModelToGson(List<TableVideos> videos ){


        List<Videogson> listres = new ArrayList<Videogson>();

        for( int i = 0; i < videos.size(); i++ ) {

            TableVideos row = videos.get(i);

            Videogson  res = new Videogson();
            res.setId(row.serverid);
            res.setTitre(row.titre);
            res.setTitreYt(row.titreYt);
            res.setStatisticsViewCount(String.valueOf(row.statisticsViewCount));
            res.setStatisticsLikeCount(String.valueOf(row.statisticsLikeCount));
            res.setStatisticsCommentCount(String.valueOf(row.statisticsCommentCount));
            res.setStatisticsDislikeCount(String.valueOf(row.statisticsDislikeCount));
            res.setVideoytid(row.videoytid);
            res.setThumburl(row.thumburl);
            res.setContentDetailsDuration(String.valueOf(row.contentDetailsDuration));
            res.setContentDetailsDefinition(row.contentDetailsDefinition);


            listres.add(res);
        }

        return listres;

    }

    public static void showDialogDeleteVideo(final String uid, final FragmentActivity activity){
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //Delete video
                        VazotsaraShareFunc.deleteVideo(uid, activity);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }
    public static void showDialogDeletePlaylist(final FragmentActivity activity, final String uid, final int position, final PlaylistsRecyclerAdapter adapter, final List<Playlist> videoItems){
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //Delete video
                        VazotsaraShareFunc.deletePlaylist(uid, activity);
                        if ( videoItems != null )
                            videoItems.remove(position);
                        if ( adapter != null )
                            adapter.notifyDataSetChanged();
                        VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.msg_confirm_delete_playlist), activity);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }

    public static void showDialogDeletePlaylistSQL(final FragmentActivity activity, final String uid, final int position, final PlaylistsRecyclerSQLAdapter adapter, final List<Playlist> videoItems){
        new MaterialDialog.Builder(activity)
                .title(R.string.dialog_supprfavori_title)
                .content(R.string.dialog_supprfavori_message)
                .positiveText(R.string.dialog_supprfavori_positivebutton)
                .negativeText(R.string.dialog_supprfavori_negativebutton)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        //Delete video
                        VazotsaraShareFunc.deletePlaylist(uid, activity);
                        if ( videoItems != null )
                            videoItems.remove(position);
                        if ( adapter != null )
                            adapter.notifyDataSetChanged();
                        VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.msg_confirm_delete_playlist), activity);
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                })
                .show();
    }


    public static boolean isLoggedin(final FragmentActivity context){
        boolean twitter_loggedin = Useful.isTwitterLoggedin(context);
        boolean facebook_loggedin = Useful.isFacebookLoggedin(context);
        boolean google_loggedin = Useful.isGoogleLoggedin(context);
        boolean admin_loggedin = Useful.isAdmin(context);

        boolean connected = (twitter_loggedin || facebook_loggedin || google_loggedin || admin_loggedin);
        return connected;
    }
    public static void alertLogin(final FragmentActivity context) {

            new MaterialDialog.Builder(context)
                    .title(R.string.dialog_alwaysadmin_title)
                    .content(R.string.dialog_alwaysconnected_message)
                    .positiveText(R.string.dialog_alwaysadmin_positivebutton)
                    .positiveColorRes(R.color.ColorPrimaryDark)
                    .positiveText(R.string.dialog_supprfavori_positivebutton)
                    .negativeText(R.string.dialog_supprfavori_negativebutton)
                    .positiveColorRes(R.color.ColorPrimaryDark)
                    .negativeColorRes(R.color.light_gray)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            VazotsaraShareFunc.setLogout(context);
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {

                        }
                    })

                    .show();




    }

    public static void openLoginActivity(final FragmentActivity context){
        if ( isLoggedin(context) ) {
            alertLogin(context);
            return;
        }

        Intent loginActivity = new Intent(context, LoginActivity.class);
        context.startActivityForResult(loginActivity, 200);
    }

    public static void openLoginActivity(final FragmentActivity context, String option){
        if ( isLoggedin(context) ) {
            alertLogin(context);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("option", option);

        Intent loginActivity = new Intent(context, LoginActivity.class);
        loginActivity.putExtras(bundle);
        context.startActivityForResult(loginActivity, 200);
    }



    public static void clickFabVideoFragment(final FragmentActivity context) {

        if (!Useful.isAdmin(context)) {
            VazotsaraShareFunc.showSnackMsg(context.getString(R.string.error_connexion_action), context);
            return;
        }


        new BottomSheet.Builder(context,R.style.BottomSheetCustomDialog).title(context.getString(R.string.title_bottomsheet)).sheet(R.menu.mnu_bottomsheet_videos).listener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case R.id.addvideo:
                        VazotsaraShareFunc.showDialogAddVideo(context);
                        break;
                    case R.id.addplaylists:
                        VazotsaraShareFunc.showDialogAddPlaylist(context);
                        break;

                }
            }
        }).show();
    }

    public static void clickFabArtistsFragment(final FragmentActivity context) {

        if (!Useful.isAdmin(context)) {
            VazotsaraShareFunc.showSnackMsg(context.getString(R.string.error_connexion_action), context);
            return;
        }


        new BottomSheet.Builder(context,R.style.BottomSheetCustomDialog).title(context.getString(R.string.title_bottomsheet)).sheet(R.menu.mnu_bottomsheet_artists).listener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case R.id.addvideo:
                        VazotsaraShareFunc.showDialogAddVideo(context);
                        break;
                    case R.id.addplaylists:
                        VazotsaraShareFunc.showDialogAddPlaylist(context);
                        break;

                }
            }
        }).show();
    }


    public static void clickFabHome(final FragmentActivity context)
    {
        if (!Useful.isAdmin(context)) {
            VazotsaraShareFunc.showSnackMsg(context.getString(R.string.error_connexion_action), context);
            return;
        }


        new BottomSheet.Builder(context,R.style.BottomSheetCustomDialog).title(context.getString(R.string.title_bottomsheet)).sheet(R.menu.mnu_bottomsheet_home).listener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case R.id.addvideo:
                        VazotsaraShareFunc.showDialogAddVideo(context);
                        break;
                    case R.id.addplaylists:
                        VazotsaraShareFunc.showDialogAddPlaylist(context);
                        break;
                }
            }
        }).show();
    }


    public static void showDialogAddVideo(final FragmentActivity context)
    {



        MaterialDialog dialogv = new MaterialDialog.Builder(context)
                .title(R.string.dialog_addvideo)
                .customView(R.layout.dialog_addvideo, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        String txturl = edit_urlvideo.getText().toString();




                        //getActivity().setTitle( String.format( getString(R.string.sprintf_search), search) );
                        //MainActivity.setTitleActivity(String.format(getString(R.string.sprintf_search), search[0]));

                        if (!Useful.isErrorUrl(txturl, context)) {
                            //open activity
                            context.startActivityForResult(new Intent(context, AddVideoActivity.class).putExtra("url", txturl), 100);
                        }
                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }
                }).build();

        final View positiveAction = dialogv.getActionButton(DialogAction.POSITIVE);
        //noinspection ConstantConditions
        edit_urlvideo = (EditText) dialogv.getCustomView().findViewById(R.id.edit_text_urlvideo);
        textInputLayoutUrl = (TextInputLayout) dialogv.getCustomView().findViewById(R.id.textInputLayoutUrl);
        bloc_clipboard = (RelativeLayout) dialogv.getCustomView().findViewById(R.id.bloc_clipboard);
        TextView askclip = (TextView) dialogv.getCustomView().findViewById(R.id.askclip);

        final Button btn_askclip_no = (Button) dialogv.getCustomView().findViewById(R.id.btn_askclip_no);

        final ClipboardManager myClipboard =  (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);



        if (myClipboard.getPrimaryClip()!=null && !Useful.isErrorUrl( myClipboard.getPrimaryClip().getItemAt(0).getText().toString(), context )) {

            bloc_clipboard.setVisibility(View.VISIBLE);
            askclip.setText( String.format( context.getString(R.string.sprintf_askclipboard), myClipboard.getPrimaryClip().getItemAt(0).getText().toString() ) ) ;

        }

        bloc_clipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bloc_clipboard.setVisibility(View.GONE);

                if (myClipboard.getPrimaryClip() != null && !Useful.isErrorUrl(myClipboard.getPrimaryClip().getItemAt(0).getText().toString(), context))
                    edit_urlvideo.setText(myClipboard.getPrimaryClip().getItemAt(0).getText().toString());
            }
        });

        btn_askclip_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bloc_clipboard.setVisibility(View.GONE);
            }
        });




        edit_urlvideo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if ( Useful.isErrorUrl(s.toString(), context )) {
                    //Afficher erreurs sinon
                    textInputLayoutUrl.setError(context.getString(R.string.error_urlvideo));
                    textInputLayoutUrl.setErrorEnabled(true);
                } else
                    positiveAction.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        dialogv.show();

        positiveAction.setEnabled(false); // disabled by default

    }

    public static void createPlaylist(String nom, String status, String type, final FragmentActivity context ) {
        Log.d(TAG,"createPlaylist - Nom : "+nom+" - type : "+type+" - statut : "+status );
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(context) ) {
                            request.addHeader("Authorization", " Bearer "+ VazotsaraShareFunc.getAdminToken(context));
                        }
                    }
                })
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.postCreatePlaylist(new FieldAddPlaylist(nom, type, "", status, Config.CREATEDBY), new Callback<TokenPojo>() {
            public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), context);
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);


                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), context);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    VazotsaraShareFunc.showSnackMsg(context.getString(R.string.error_failure_retrofit), context);
                }
            }
        });


    }

    public static void showDialogAddPlaylist(final FragmentActivity context){





        MaterialDialog dialogpl = new MaterialDialog.Builder(context)
                .title(R.string.dialog_addplaylist)
                .customView(R.layout.dialog_addplaylist, true)
                .positiveText(R.string.dialog_addvideo_btn_create)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .


                        callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {

                                String nomplayliste = edit_playlistenom.getText().toString();
                                String status = spinn_status.getSelectedItem().toString();
                                String type = spinn_playlistetype.getSelectedItem().toString();

                                Log.d(TAG, "spinn_status: " + spinn_status.getSelectedItemPosition() + " - spinn_playlistetype: " + spinn_playlistetype.getSelectedItemPosition());

                                switch (spinn_status.getSelectedItemPosition()) {

                                    case 1:
                                        status = "public";
                                        break;
                                    case 2:
                                    case 3:
                                        status = "private";
                                        break;
                                }

                                switch (spinn_playlistetype.getSelectedItemPosition()) {

                                    case 1:
                                        //Artiste
                                        type = "artiste";
                                        break;
                                    case 2:
                                        //Genre
                                        type = "genre";
                                        break;
                                    case 3:
                                        //Réalisateur
                                        type = "realisateur";
                                        break;
                                    case 4:
                                        //Autre
                                        type = "autre";
                                        break;
                                }

                                if (!nomplayliste.isEmpty() && nomplayliste.length() > 2) {
                                    VazotsaraShareFunc.createPlaylist(
                                            nomplayliste,
                                            status,
                                            type,
                                            context
                                    );
                                } else
                                    VazotsaraShareFunc.showSnackMsg(context.getString(R.string.error_add_playlist), context);

                            }


                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                .build();

        positiveActionPlayliste = dialogpl.getActionButton(DialogAction.POSITIVE);
        //noinspection ConstantConditions
        final EditText edit_playlistenom = (EditText) dialogpl.getCustomView().findViewById(R.id.edit_playlistenom);
        final TextInputLayout textInputLayoutNomplayliste = (TextInputLayout) dialogpl.getCustomView().findViewById(R.id.textInputLayoutNomplayliste);
        final MaterialSpinner spinn_playlistetype = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_playlistetype);
        final MaterialSpinner spinn_status = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_status);


        final ArrayAdapter<CharSequence>  adapter_spinn_playlistetype = ArrayAdapter.createFromResource(context, R.array.playliste_type, android.R.layout.simple_spinner_item);
        adapter_spinn_playlistetype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_playlistetype.setAdapter(adapter_spinn_playlistetype);
        spinn_playlistetype.setSelection(1);

        final ArrayAdapter<CharSequence>   adapter_spinn_status = ArrayAdapter.createFromResource(context, R.array.status_video, android.R.layout.simple_spinner_item);
        adapter_spinn_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_spinn_status);
        spinn_status.setSelection(1);

        edit_playlistenom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int itemStatus = spinn_status.getSelectedItemPosition();
                int itemType = spinn_playlistetype.getSelectedItemPosition();

                if (!charSequence.toString().isEmpty() && charSequence.length() > 1 && itemStatus > 0 && itemType > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //Au changement du type status
        spinn_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length() > 1 && spinn_playlistetype.getSelectedItemPosition() > 0 && spinn_status.getSelectedItemPosition() > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Au changement du type type playliste
        spinn_playlistetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length() > 1 && spinn_status.getSelectedItemPosition() > 0 && spinn_playlistetype.getSelectedItemPosition() > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        dialogpl.show();

        positiveActionPlayliste.setEnabled(false); // disabled by default
    }

    public static void  deletePlaylist(String id , final Context context) {
        Log.d(TAG, "deletePlaylist - id : " + id);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(context) ) {
                            request.addHeader("Authorization", " Bearer "+VazotsaraShareFunc.getAdminToken(context));
                        }
                    }
                })
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.getDeletePlaylist(id, new Callback<ServerMsgPojo>() {
            public void success(ServerMsgPojo sourceinfo, retrofit.client.Response response) {
                Toast.makeText(context, (sourceinfo.getMessage() != null ? sourceinfo.getMessage() : ""), Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);
                //Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        Toast.makeText(context, "Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.error_failure_retrofit), Toast.LENGTH_LONG).show();
                }
            }
        });


    }
    public static void  deleteVideo(String id , final Context context) {
        Log.d(TAG, "deleteVideo - id : " + id);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(context) ) {
                            request.addHeader("Authorization", " Bearer "+VazotsaraShareFunc.getAdminToken(context));
                        }
                    }
                })
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.getDeleteVideo(id, new Callback<ServerMsgPojo>() {
            public void success(ServerMsgPojo sourceinfo, retrofit.client.Response response) {
                Toast.makeText(context, (sourceinfo.getMessage() != null ? sourceinfo.getMessage() : ""), Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);
                //Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        Toast.makeText(context, "Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.error_failure_retrofit), Toast.LENGTH_LONG).show();
                }
            }
        });


    }
    public static void saveTokenInPreferences(String token, Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("token", token);
        //Save
        editor.commit();

    }


    public static void saveLastAccessAppInPreferences(String lastaccesststamp, Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("lastaccesststamp", lastaccesststamp);
        //Save
        editor.commit();

    }

    public static String getLastAccessAppInPreferences(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        return sharedpref.getString("lastaccesststamp", "");
    }
    public static void setTotalVideoinServer(){
        final int[] count = {0};
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.getListVideos(1, "endtime_uploaded", 0, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, Response response) {
                Log.d(TAG, "getTotalVideoinServer success");
                if (videoPojo.getTotal() > 0) {
                    count[0] = videoPojo.getTotal();
                    Log.d(TAG, "getTotalVideoinServer success - count : " + count[0]);

                    new saveTotalvideos().execute(count[0]);

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "getTotalVideoinServer failure");
            }
        });


    }

    private static class saveTotalvideos extends AsyncTask<Integer, String, String> {

        @Override
        protected String doInBackground(Integer... strings) {
            Log.d(TAG,"saveTotalvideos doInBackground count : " +strings[0] );
            TableLastcheck.setTotalvideosinserver( strings[0] );
            return null;
        }
    }
    public static Integer getLastPageInPreferences(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        return sharedpref.getInt("lastpage", 0);
    }

    public static void setLastPageInPreferences(int lastpage, Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putInt( "lastpage", lastpage);
        //Save
        editor.commit();
    }

    public static String getLastVideoidInPreferences(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        return sharedpref.getString("lastvideoid", "");
    }

    public static void createOrUpdateUser(final profileModel pr, final FragmentActivity activity) {
        //Save in preferences
        SharedPreferences sharedpref = activity.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString(pr.getProvider()+"_id", pr.getId());
        //Save
        editor.commit();

        Log.d(TAG,"createOrUpdateUser - Name : "+pr.getNom()+" - ID : "+pr.getId()+" - Avatar : "+pr.getAvatar()+" - Provider : "+pr.getProvider()+" - Createdby : "+Config.CREATEDBY );
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.postCreateOrUpdateUser(new FieldAddUser(pr.getId(), pr.getNom(), "", pr.getAvatar(), pr.getProvider(), Config.CREATEDBY), new Callback<TokenPojo>() {
            public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                //VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), activity);


            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);


                //Log.d(TAG, "postLogin - error : "  + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        //VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), activity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        //VazotsaraShareFunc.showSnackMsg(String.format(activity.getString(R.string.error_sprintf_failure_retrofit), errorMessage), activity);
                    }
                } else {
                    //VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.error_failure_retrofit), activity);
                }
            }
        });

    }

    public static void setLastVideoidInPreferences(String videoid, Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("lastvideoid", videoid);
        //Save
        editor.commit();
    }

    public static String getLastPlaylistidInPreferences(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        if ( context != null ) {
            SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
            return sharedpref.getString("lastplaylistid", "");
        } else {
            Log.e(TAG,"getLastPlaylistidInPreferences context null");
        }
        return "";
    }

    public static void setLastPlaylistidInPreferences(String videoid, Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("lastplaylistid", videoid);
        //Save
        editor.commit();
    }



    public static void setLogout(Context context){

        boolean twitter_loggedin = Useful.isTwitterLoggedin(context);
        boolean facebook_loggedin = Useful.isFacebookLoggedin(context);
        boolean google_loggedin = Useful.isGoogleLoggedin(context);
        boolean admin_loggedin = Useful.isAdmin(context);


        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();

        if ( admin_loggedin ) {
            editor.remove("token");
        }

        if ( facebook_loggedin ) {
            editor.remove("facebook_id");
            LoginManager.getInstance().logOut();
        }
        if ( twitter_loggedin ) {
            editor.remove("twitter_id");
        }
        if ( google_loggedin ) {
            editor.remove("google_id");

            GoogleApiClient mGoogleApiClient;
            final GoogleApiClient.Builder googleApiClientBuilder = new GoogleApiClient.Builder(context);
            googleApiClientBuilder.addApi(Plus.API);
            mGoogleApiClient = googleApiClientBuilder.build();
            mGoogleApiClient.disconnect();
        }


        //Save
        editor.commit();
        Toast.makeText(context, context.getString(R.string.msg_confirm_deconnexion), Toast.LENGTH_LONG).show();

    }

    public static void share(video vid, Context context)
    {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Titre : "+vid.getTitre()+" - vues : "+vid.getStatistics_viewCount()+" - Commentaires : "+vid.getStatistics_commentCount();
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Video : " + vid.getTitre());
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Partager via"));
    }

    public static void showDetailVideo(video vid, Context context)
    {
        Log.d(TAG, "VideoAdapter - showDetailIntent - uid : " + vid.getId() + " - videoytid :" + vid.getVideoytid());
        Intent VDetailActivity = new Intent( context, VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId());
        VDetailActivity.putExtra("videoytid", vid.getVideoytid() );
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition() );
        context.startActivity(VDetailActivity);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    public static void showSnackMsg(String error, FragmentActivity activity)
    {
        Snackbar snack = Snackbar.make(activity.findViewById(R.id.content), error, Snackbar.LENGTH_LONG);
        snack.show();

        //Color en blanc le text
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }

    public static void showSnackMsg(String error, Context activity, View viewsnack)
    {
        Snackbar snack = Snackbar.make(viewsnack, error, Snackbar.LENGTH_LONG);
        snack.show();

        //Color en blanc le text
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }

    public static void setBadge(int nb, Context context) {
        if ( Badge.isBadgingSupported(context)) {
            Badge badge = new Badge();
            badge.mPackage = context.getPackageName();
            badge.mClass = "vazo.tsara.activities.MainActivity";
            badge.mBadgeCount = nb;
            badge.save(context);
        }
    }

    public static void clearBadge(Context context) {
        if ( Badge.isBadgingSupported(context)) {
            Badge badge = new Badge();
            badge.deleteAllBadges(context);
        }
    }

    public static void alert(String message, FragmentActivity activity) {
        MaterialDialog dialogpl = new MaterialDialog.Builder(activity)
                .title( R.string.dialog_information_title )
                .content(message)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .build();
        dialogpl.show();
    }

    public static void showDialogEditPlaylist(final Playlist plinfo, final FragmentActivity activity, final PlaylistsRecyclerAdapter adapter){

        Log.d(TAG,"showDialogEditPlaylist Nom :"+plinfo.getNom()+" - Type : "+plinfo.getObjetype()+" - ID: "+plinfo.getId()+" - Statut : "+plinfo.getStatus());
        MaterialDialog dialogpl = new MaterialDialog.Builder(activity)
                .title(String.format(activity.getString(R.string.dialog_sprintf_editplaylist), plinfo.getObjetnom() ) )
                        .customView(R.layout.dialog_addplaylist, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                        .negativeText(android.R.string.cancel)
                        .negativeColor(Color.parseColor("#000000"))
                        .positiveColor(Color.parseColor("#000000"))
                        .


                                callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {

                                        String nomplayliste = edit_playlistenom.getText().toString();
                                        String status = spinn_status.getSelectedItem().toString();
                                        String type = spinn_playlistetype.getSelectedItem().toString();

                                        Log.d(TAG, "spinn_status: " + spinn_status.getSelectedItemPosition() + " - spinn_playlistetype: " + spinn_playlistetype.getSelectedItemPosition());

                                        switch (spinn_status.getSelectedItemPosition()) {

                                            case 1:
                                                status = "public";
                                                break;
                                            case 2:
                                            case 3:
                                                status = "private";
                                                break;
                                        }

                                        switch (spinn_playlistetype.getSelectedItemPosition()) {

                                            case 1:
                                                //Artiste
                                                type = "artiste";
                                                break;
                                            case 2:
                                                //Genre
                                                type = "genre";
                                                break;
                                            case 3:
                                                //Réalisateur
                                                type = "realisateur";
                                                break;
                                            case 4:
                                                //Autre
                                                type = "autre";
                                                break;
                                        }

                                        if (!nomplayliste.isEmpty() && nomplayliste.length() > 2) {
                                            VazotsaraShareFunc.updatePlaylist(plinfo, nomplayliste, status, type, activity, adapter);
                                        } else
                                            VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.error_save_playlist), activity);

                                    }


                                    @Override
                                    public void onNegative(MaterialDialog dialog) {

                                    }
                                })
                .build();

        positiveActionPlayliste = dialogpl.getActionButton(DialogAction.POSITIVE);
        //noinspection ConstantConditions
        edit_playlistenom = (EditText) dialogpl.getCustomView().findViewById(R.id.edit_playlistenom);
        textInputLayoutNomplayliste = (TextInputLayout) dialogpl.getCustomView().findViewById(R.id.textInputLayoutNomplayliste);
        spinn_playlistetype = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_playlistetype);
        spinn_status = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_status);


        adapter_spinn_playlistetype = ArrayAdapter.createFromResource(activity, R.array.playliste_type, android.R.layout.simple_spinner_item);
        adapter_spinn_playlistetype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_playlistetype.setAdapter(adapter_spinn_playlistetype);


        adapter_spinn_status = ArrayAdapter.createFromResource(activity, R.array.status_video, android.R.layout.simple_spinner_item);
        adapter_spinn_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_spinn_status);

        Log.d(TAG,"adapter_spinn_status : "+adapter_spinn_status.getPosition( plinfo.getStatus() )+" - adapter_spinn_playlistetype : "+adapter_spinn_playlistetype.getPosition(plinfo.getObjetype() ));

        //Remplir les champs
        String statut, type;
        switch ( plinfo.getStatus() ) {
            case "public" :
                statut = "Public";
                break;
            case "unlisted" :
                statut = "Non répertorié";
                break;
            default:
                statut = "Privé";
                break;
        }

        switch ( plinfo.getObjetype() ) {
            case "artiste" :
                type = "Artiste";
                break;
            case "genre" :
                type = "Genre";
                break;
            case "realisateur":
                type = "Réalisateur";
                break;
            default:
                type = "Autre";
                break;
        }




        edit_playlistenom.setText(plinfo.getObjetnom());
        spinn_status.setSelection( adapter_spinn_status.getPosition( statut )+1 );
        spinn_playlistetype.setSelection( adapter_spinn_playlistetype.getPosition( type )+1 );

        edit_playlistenom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int itemStatus = spinn_status.getSelectedItemPosition();
                int itemType = spinn_playlistetype.getSelectedItemPosition();

                if (!charSequence.toString().isEmpty() && charSequence.length() > 1 && itemStatus > 0 && itemType > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //Au changement du type status
        spinn_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length()>1 && spinn_playlistetype.getSelectedItemPosition() > 0 && spinn_status.getSelectedItemPosition() > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Au changement du type type playliste
        spinn_playlistetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length()>1 && spinn_status.getSelectedItemPosition()>0 && spinn_playlistetype.getSelectedItemPosition() > 0 ) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        dialogpl.show();

        positiveActionPlayliste.setEnabled(false); // disabled by default

    }

    public static void showDialogEditPlaylistSQL(final Playlist plinfo, final FragmentActivity activity, final PlaylistsRecyclerSQLAdapter adapter){

        Log.d(TAG,"showDialogEditPlaylist Nom :"+plinfo.getNom()+" - Type : "+plinfo.getObjetype()+" - ID: "+plinfo.getId()+" - Statut : "+plinfo.getStatus());
        MaterialDialog dialogpl = new MaterialDialog.Builder(activity)
                .title(String.format(activity.getString(R.string.dialog_sprintf_editplaylist), plinfo.getObjetnom() ) )
                .customView(R.layout.dialog_addplaylist, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .


                        callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {

                                String nomplayliste = edit_playlistenom.getText().toString();
                                String status = spinn_status.getSelectedItem().toString();
                                String type = spinn_playlistetype.getSelectedItem().toString();

                                Log.d(TAG, "spinn_status: " + spinn_status.getSelectedItemPosition() + " - spinn_playlistetype: " + spinn_playlistetype.getSelectedItemPosition());

                                switch (spinn_status.getSelectedItemPosition()) {

                                    case 1:
                                        status = "public";
                                        break;
                                    case 2:
                                    case 3:
                                        status = "private";
                                        break;
                                }

                                switch (spinn_playlistetype.getSelectedItemPosition()) {

                                    case 1:
                                        //Artiste
                                        type = "artiste";
                                        break;
                                    case 2:
                                        //Genre
                                        type = "genre";
                                        break;
                                    case 3:
                                        //Réalisateur
                                        type = "realisateur";
                                        break;
                                    case 4:
                                        //Autre
                                        type = "autre";
                                        break;
                                }

                                if (!nomplayliste.isEmpty() && nomplayliste.length() > 2) {
                                    VazotsaraShareFunc.updatePlaylistSQL(plinfo, nomplayliste, status, type, activity, adapter);
                                } else
                                    VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.error_save_playlist), activity);

                            }


                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                .build();

        positiveActionPlayliste = dialogpl.getActionButton(DialogAction.POSITIVE);
        //noinspection ConstantConditions
        edit_playlistenom = (EditText) dialogpl.getCustomView().findViewById(R.id.edit_playlistenom);
        textInputLayoutNomplayliste = (TextInputLayout) dialogpl.getCustomView().findViewById(R.id.textInputLayoutNomplayliste);
        spinn_playlistetype = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_playlistetype);
        spinn_status = (MaterialSpinner) dialogpl.getCustomView().findViewById(R.id.spinn_status);


        adapter_spinn_playlistetype = ArrayAdapter.createFromResource(activity, R.array.playliste_type, android.R.layout.simple_spinner_item);
        adapter_spinn_playlistetype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_playlistetype.setAdapter(adapter_spinn_playlistetype);


        adapter_spinn_status = ArrayAdapter.createFromResource(activity, R.array.status_video, android.R.layout.simple_spinner_item);
        adapter_spinn_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_spinn_status);

        Log.d(TAG,"adapter_spinn_status : "+adapter_spinn_status.getPosition( plinfo.getStatus() )+" - adapter_spinn_playlistetype : "+adapter_spinn_playlistetype.getPosition(plinfo.getObjetype() ));

        //Remplir les champs
        String statut, type;
        switch ( plinfo.getStatus() ) {
            case "public" :
                statut = "Public";
                break;
            case "unlisted" :
                statut = "Non répertorié";
                break;
            default:
                statut = "Privé";
                break;
        }

        switch ( plinfo.getObjetype() ) {
            case "artiste" :
                type = "Artiste";
                break;
            case "genre" :
                type = "Genre";
                break;
            case "realisateur":
                type = "Réalisateur";
                break;
            default:
                type = "Autre";
                break;
        }




        edit_playlistenom.setText(plinfo.getObjetnom());
        spinn_status.setSelection( adapter_spinn_status.getPosition( statut )+1 );
        spinn_playlistetype.setSelection( adapter_spinn_playlistetype.getPosition( type )+1 );

        edit_playlistenom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int itemStatus = spinn_status.getSelectedItemPosition();
                int itemType = spinn_playlistetype.getSelectedItemPosition();

                if (!charSequence.toString().isEmpty() && charSequence.length() > 1 && itemStatus > 0 && itemType > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //Au changement du type status
        spinn_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length()>1 && spinn_playlistetype.getSelectedItemPosition() > 0 && spinn_status.getSelectedItemPosition() > 0) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Au changement du type type playliste
        spinn_playlistetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (edit_playlistenom.getText().toString().length()>1 && spinn_status.getSelectedItemPosition()>0 && spinn_playlistetype.getSelectedItemPosition() > 0 ) {
                    positiveActionPlayliste.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        dialogpl.show();

        positiveActionPlayliste.setEnabled(false); // disabled by default

    }

    public static void updatePlaylist(final Playlist plinfo, final String nom, String status, String type, final FragmentActivity activity, final PlaylistsRecyclerAdapter adapter ) {
        Log.d(TAG,"updatePlaylist - Nom : "+nom+" - type : "+type+" - statut : "+status );
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(activity) ) {
                            request.addHeader("Authorization", " Bearer "+ VazotsaraShareFunc.getAdminToken(activity));
                        }
                    }
                })
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.postUpdatePlaylist(plinfo.getId(), new FieldAddPlaylist(nom, type, "", "", status, Config.CREATEDBY), new Callback<TokenPojo>() {
            public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), activity);
                plinfo.setNom(nom);
                if ( adapter != null )
                    adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);


                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), activity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        VazotsaraShareFunc.showSnackMsg(String.format( activity.getString(R.string.error_sprintf_failure_retrofit), errorMessage), activity);
                    }
                } else {
                    VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.error_failure_retrofit), activity);
                }
            }
        });


    }

    public static void updatePlaylistSQL(final Playlist plinfo, final String nom, String status, String type, final FragmentActivity activity, final PlaylistsRecyclerSQLAdapter adapter ) {
        Log.d(TAG,"updatePlaylist - Nom : "+nom+" - type : "+type+" - statut : "+status );
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(activity) ) {
                            request.addHeader("Authorization", " Bearer "+ VazotsaraShareFunc.getAdminToken(activity));
                        }
                    }
                })
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.postUpdatePlaylist(plinfo.getId(), new FieldAddPlaylist(nom, type, "", "", status, Config.CREATEDBY), new Callback<TokenPojo>() {
            public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), activity);
                plinfo.setNom(nom);
                if (adapter != null)
                    adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);


                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error != null && error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), activity);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        VazotsaraShareFunc.showSnackMsg(String.format(activity.getString(R.string.error_sprintf_failure_retrofit), errorMessage), activity);
                    }
                } else {
                    VazotsaraShareFunc.showSnackMsg(activity.getString(R.string.error_failure_retrofit), activity);
                }
            }
        });


    }

    public static void saveInPlaylists(List<Playlist> plist) {
        if ( plist.size()<=0 ) {
            return;
        }

        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < plist.size(); i++) {

                if ( TablePlaylists.getAllCount(plist.get(i).getId())<=0 ) {

                    TablePlaylists playl = new TablePlaylists();
                    playl.nom = plist.get(i).getNom();
                    playl.objetnom = plist.get(i).getObjetnom();
                    playl.created = plist.get(i).getCreated();
                    playl.createdAt = plist.get(i).getCreatedAt();
                    playl.createdby = plist.get(i).getCreatedby();
                    playl.descriptionYt = plist.get(i).getDescriptionYt();
                    playl.firstvideoid = plist.get(i).getFirstvideoid();
                    playl.image = plist.get(i).getImage();
                    playl.nbVideos = plist.get(i).getNbVideos();
                    playl.objetid = plist.get(i).getObjetid();
                    playl.objetype = plist.get(i).getObjetype();
                    playl.playlisteid = plist.get(i).getPlaylisteid();
                    playl.serverid = plist.get(i).getId();
                    playl.status = plist.get(i).getStatus();
                    playl.save();

                    Log.d(TAG,"saveInPlaylists - Insert : "+plist.get(i).getObjetnom());
                } else {
                    Log.d(TAG,"saveInPlaylists - Insert - Existe deja" );
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void deleteVideoidInFavori(String videoid, final Context context) {
        if ( Useful.isLoggedin(context) ) {
            String user_id = Useful.getLoggedinID(context);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
            apipl.deletevideoidInFavori(user_id, videoid, new Callback<TokenPojo>() {
                public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                    //VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), context);
                }

                @Override
                public void failure(RetrofitError error) { }
            });
        }
    }

    public static void addVideoidInFavori(String videoid, final Context context) {
        if ( Useful.isLoggedin(context) ) {
            String user_id = Useful.getLoggedinID(context);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
            apipl.addvideoidInFavori(user_id, videoid, new Callback<TokenPojo>() {
                public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                    //VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), context);
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    public static void deletePlaylisteidInFavori(String videoid, final Context context) {
        if ( Useful.isLoggedin(context) ) {
            String user_id = Useful.getLoggedinID(context);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
            apipl.deleteplaylisteidInFavori(user_id, videoid, new Callback<TokenPojo>() {
                public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                    //VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), context);
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    public static void addPlaylisteidInFavori(String videoid, final Context context) {
        if ( Useful.isLoggedin(context) ) {
            String user_id = Useful.getLoggedinID(context);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
            apipl.addplaylisteidInFavori(user_id, videoid, new Callback<TokenPojo>() {
                public void success(TokenPojo sourceinfo, retrofit.client.Response response) {
                    //VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), context);
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    public static void saveUniqueVideo(Videogson model, String option){

        Log.d(TAG,"saveInVideos - option: "+option);

        TableVideos playl;

        if ( TableVideos.getAllCount(model.getId())<=0 ) {
            playl = new TableVideos();
            Log.d(TAG,"saveInVideos - INSERT : "+model.getTitre()+" ("+model.getId()+")");
        } else {
            playl = new Select().from(TableVideos.class).where("serverid = ?",model.getId()).executeSingle();
            Log.d(TAG,"saveInVideos - UPDATE - "+model.getTitre()+" (" + model.getId()+") " );
        }

        playl.titre = model.getTitre();
        playl.serverid = model.getId();
        playl.titreYt = model.getTitreYt();
        playl.videoytid = model.getVideoytid();
        playl.categoryYt = model.getCategoryYt();
        playl.contentDetailsDefinition = model.getContentDetailsDefinition();
        playl.createdAt = model.getCreatedAt();
        playl.contentDetailsDuration = Integer.parseInt(model.getContentDetailsDuration() != null ? model.getContentDetailsDuration() : "0");
        playl.deletedAt = model.getDeletedAt();
        playl.descriptionYt = model.getDescriptionYt();
        playl.endtimeDownloaded = model.getEndtimeDownloaded();
        playl.starttimeDownloaded = model.getStarttimeDownloaded();
        playl.endtimeUploaded = model.getEndtimeUploaded();
        playl.fileDetailsContainer = model.getFileDetailsContainer();
        playl.fileDetailsVideoStreamsCodec = model.getFileDetailsVideoStreamsCodec();
        playl.fileDetailsVideoStreamsAspectRatio = model.getFileDetailsVideoStreamsAspectRatio();
        playl.fileDetailsVideoStreamsFrameRateFps = model.getFileDetailsVideoStreamsFrameRateFps();
        playl.fileDetailsVideoStreamsHeightPixels = model.getFileDetailsVideoStreamsHeightPixels();
        playl.fileDetailsVideoStreamsWidthPixels = model.getFileDetailsVideoStreamsWidthPixels();
        playl.starttimeUploaded = model.getStarttimeUploaded();
        playl.statisticsCommentCount = Integer.parseInt(model.getStatisticsCommentCount()!=null?model.getStatisticsCommentCount():"0");
        playl.statisticsLikeCount = Integer.parseInt(model.getStatisticsLikeCount()!=null?model.getStatisticsLikeCount():"0");
        playl.statisticsDislikeCount = Integer.parseInt(model.getStatisticsDislikeCount()!=null?model.getStatisticsDislikeCount():"0");
        playl.statisticsViewCount = Integer.parseInt(model.getStatisticsViewCount()!=null?model.getStatisticsViewCount():"0");
        playl.statisticsViewCountHier = Integer.parseInt(model.getStatisticsViewCountHier()!=null?model.getStatisticsViewCountHier():"0");
        playl.statisticsFavoriteCount = Integer.parseInt(model.getStatisticsFavoriteCount() != null ? model.getStatisticsFavoriteCount() : "0");
        playl.statusEmbeddable = model.getStatusEmbeddable();
        playl.statusLicense = model.getStatusLicense();
        playl.statusPrivacyStatus = model.getStatusPrivacyStatus();
        playl.statusPublicStatsViewable = model.getStatusPublicStatsViewable();
        playl.statusUploadStatus = model.getStatusUploadStatus();
        playl.publishedAt = model.getPublishedAt();
        playl.publishedCommentLast = Integer.parseInt(model.getPublishedCommentLast()!=null?model.getPublishedCommentLast():"0");
        playl.removefile = model.getRemovefile();
        playl.isDownloaded = model.getIsDownloaded();
        playl.isUploaded = model.getIsUploaded();
        playl.source = model.getSource();
        playl.thumburl = model.getThumburl();



        if ( option.equals("publishedAt") || option.isEmpty()  ) {
            playl.listeNew = 1;
        }
        if (!option.isEmpty() && option.equals("statistics_viewCount") ) {
            playl.listeView = 1;
        }
        if (!option.isEmpty() && option.equals("statistics_likeCount") ) {
            playl.listeLike = 1;
        }
        if (!option.isEmpty() && option.equals("statistics_commentCount") ) {
            playl.listeComment = 1;
        }
        if (!option.isEmpty() && option.equals("contentDetails_duration") ) {
            playl.listeDuration = 1;
        }
        if (!option.isEmpty() && option.equals("publishedCommentLast") ) {
            playl.listeCommentDate = 1;
            playl.listeCommentlast = 1;
        }
        if (!option.isEmpty() && option.equals("statistics_viewCount_hier") ) {
            playl.listeViewHier = 1;
        }








        playl.save();

        //Pour serialize un activeandroid object
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Log.d(TAG,"saveInVideos playl: "+gson.toJson(playl));


        //save relation
        if ( model.getPlaylisteinfo().size() > 0 ) {

            //Delete
            new Delete().from(TableVideosPlaylists.class).where("videoid = ?", model.getId() ).execute();

            for (int j = 0; j < model.getPlaylisteinfo().size(); j++) {
                //CREATE or UPDATE playlists
                TablePlaylists.saveOrNew( model.getPlaylisteinfo().get(j) );
                //Save relation
                TableVideosPlaylists.saveRelation(model.getId(), model.getPlaylisteinfo().get(j).getId() );
            }
        }
    }
    public static void saveInVideos(List<Videogson> plist, String option) {

        Log.d(TAG, "saveInVideos - Insert - TableVideos.getAllCount() : " + TableVideos.getAllCount());
        if ( plist.size()<=0 ) {
            return;
        }



        ActiveAndroid.beginTransaction();
        try {
            for (int i = 0; i < plist.size(); i++) {

                saveUniqueVideo(plist.get(i), option);

            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
    }


    public static boolean introIsShown(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        return sharedpref.getBoolean("introIsShown", false);
    }

    public static void setIntroIsShown(Context context) {
        //SharedPreferences sharedpref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedpref = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putBoolean("introIsShown", true);
        //Save
        editor.commit();
    }

}

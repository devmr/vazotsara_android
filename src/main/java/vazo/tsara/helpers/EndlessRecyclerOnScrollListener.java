package vazo.tsara.helpers;

/**
 * Created by rabehasy on 10/07/2015.
 */

import android.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    public static String TAG = Config.TAGKEY;

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 1; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private int current_page = 0;

    private LinearLayoutManager mLinearLayoutManager;
    ActionBar ab;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager, ActionBar ab) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.ab = ab;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);



        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            Log.d(TAG, "EndlessRecyclerOnScrollListener L36 - totalItemCount: "+totalItemCount+" - previousTotal: "+previousTotal);
            if (totalItemCount >= previousTotal || previousTotal > totalItemCount ) {
                loading = false;
                previousTotal = totalItemCount;
                Log.d(TAG, "EndlessRecyclerOnScrollListener L40 - totalItemCount: "+totalItemCount+" - previousTotal: "+previousTotal);
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {

            // End has been reached

            // Do something

            current_page++;

            onLoadMore(current_page);
            Log.d(TAG, "EndlessRecyclerOnScrollListener current_page : " + current_page);

            Log.d(TAG, "EndlessRecyclerOnScrollListener L52 - totalItemCount: " + totalItemCount + " - visibleItemCount: " + visibleItemCount + " - firstVisibleItem:" + firstVisibleItem + " - visibleThreshold: " + visibleThreshold+" - current_page: "+current_page);

            loading = true;
        }
    }

    public abstract void onLoadMore(int current_page);
}
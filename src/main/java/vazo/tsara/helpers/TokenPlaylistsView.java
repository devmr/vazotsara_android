package vazo.tsara.helpers;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tokenautocomplete.TokenCompleteTextView;

import vazo.tsara.R;
import vazo.tsara.fragments.VideosFragment;
import vazo.tsara.models.Person;
import vazo.tsara.models.artistemodel;

/**
 * Created by rabehasy on 18/06/2015.
 */
public class TokenPlaylistsView  extends TokenCompleteTextView<Person> {

    public TokenPlaylistsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject( Person artistemodel) {
        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout)l.inflate(R.layout.customview_tokenplaylists, (ViewGroup) TokenPlaylistsView.this.getParent(), false);
        ((TextView)view.findViewById(R.id.name)).setText(artistemodel.getName());

        return view;
    }



    @Override
    protected Person defaultObject(String s) {

            return new Person( s, s);

    }
}

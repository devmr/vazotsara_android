package vazo.tsara.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import vazo.tsara.R;
import vazo.tsara.models.ServerMsgPojo;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * Created by rabehasy on 05/06/2015.
 */
public class Useful {

    static String TAG = Config.TAGKEY;

    static public String ucfirst(String chaine) {
        return chaine.substring(0, 1).toUpperCase()+ chaine.substring(1).toLowerCase();
    }
    static public String usingDateFormatter(long input, String pattern){
        Date date = new Date(input);
        Calendar cal = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setCalendar(cal);
        cal.setTime(date);
        return sdf.format(date);
    }
    /**
     * Convert a millisecond duration to a string format
     *
     * @param millis A duration to convert to a string form
     * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
     */
    public static String getDurationBreakdown(long millis)
    {
        if(millis < 0)
        {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        return String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );

    }
    public static String getLoggedinID(Context context) {
        if (!isLoggedin(context)) {
            return "";
        }

        if ( isFacebookLoggedin(context) ) {
            return context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("facebook_id", "");
        }

        if ( isGoogleLoggedin(context) ) {
            return context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("google_id", "");
        }

        if ( isTwitterLoggedin(context) ) {
            return context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("twitter_id", "");
        }

        return "";
    }
    public static boolean isLoggedin(Context context){
        boolean twitter_loggedin = Useful.isTwitterLoggedin(context);
        boolean facebook_loggedin = Useful.isFacebookLoggedin(context);
        boolean google_loggedin = Useful.isGoogleLoggedin(context);

        Log.d(TAG,"twitter_loggedin "+twitter_loggedin);
        Log.d(TAG,"facebook_loggedin "+facebook_loggedin);
        Log.d(TAG,"google_loggedin "+google_loggedin);

        boolean connected = (twitter_loggedin || facebook_loggedin || google_loggedin  );

        return connected;
    }

    public static boolean isAdmin(Context context) {
        //String token = PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        String token = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("token", "");
        return !token.isEmpty();
    }

    public static boolean isTwitterLoggedin(Context context) {
        //String token = PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        String token = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("twitter_id", "");
        return !token.isEmpty();
    }

    public static boolean isFacebookLoggedin(Context context) {
        //String token = PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        String token = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("facebook_id", "");
        return !token.isEmpty();
    }

    public static boolean isGoogleLoggedin(Context context) {
        //String token = PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        String token = context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("google_id", "");
        return !token.isEmpty();
    }



    public static Boolean isErrorUrl(String s, Context context) {
        Boolean isError = false;
        if ( s.isEmpty() ) {
            isError = true;
        } else if ( s.length() > 0 && !Patterns.WEB_URL.matcher(s.toString()).matches() ) {
            isError = true;
        } else if(s.length() > 0 && Patterns.WEB_URL.matcher(s.toString()).matches()) {
            //parse URL
            try {
                URL aURL = new URL( s.toString() );
                String host = aURL.getHost();

                String[] myResArray = context.getResources().getStringArray(R.array.host_videos);
                Boolean inarray = Arrays.asList(myResArray).contains(host);
                //Log.d(TAG,"showDialogAddVideo onTextChanged Host : "+host+" in array ? "+inarray);

                if ( !inarray )
                    isError = true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return isError;
    }

    public static Boolean isYoutubeUrl(String s, Context context) {
        Boolean isError = true;
        if ( s.isEmpty() ) {
            isError = false;
        } else if ( s.length() > 0 && !Patterns.WEB_URL.matcher(s.toString()).matches() ) {
            isError = false;
        } else if(s.length() > 0 && Patterns.WEB_URL.matcher(s.toString()).matches()) {
            //parse URL
            try {
                URL aURL = new URL( s.toString() );
                String host = aURL.getHost();

                String[] myResArray = context.getResources().getStringArray(R.array.host_videos_youtube);
                Boolean inarray = Arrays.asList(myResArray).contains(host);
                //Log.d(TAG,"showDialogAddVideo onTextChanged Host : "+host+" in array ? "+inarray);

                if ( !inarray )
                    isError = false;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return isError;
    }

    public static Boolean isDailymotionUrl(String s, Context context) {
        Boolean isError = true;
        if ( s.isEmpty() ) {
            isError = false;
        } else if ( s.length() > 0 && !Patterns.WEB_URL.matcher(s.toString()).matches() ) {
            isError = false;
        } else if(s.length() > 0 && Patterns.WEB_URL.matcher(s.toString()).matches()) {
            //parse URL
            try {
                URL aURL = new URL( s.toString() );
                String host = aURL.getHost();

                String[] myResArray = context.getResources().getStringArray(R.array.host_videos_dailymotion);
                Boolean inarray = Arrays.asList(myResArray).contains(host);
                //Log.d(TAG,"showDialogAddVideo onTextChanged Host : "+host+" in array ? "+inarray);

                if ( !inarray )
                    isError = false;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return isError;
    }

    public static String getIdYoutubeVideo(String s, Context context ) {
         if(!s.isEmpty() && s.length() > 0 && Patterns.WEB_URL.matcher(s.toString()).matches() && isYoutubeUrl(s, context) ) {
            //parse URL
            try {
                URL aURL = new URL( s.toString() );
                String host = aURL.getHost();
                String query = aURL.getQuery();
                String path = aURL.getPath();

                Log.d(TAG, "parse_url - URL : "+s+" - query : "+query+" - path : "+path );


                if ( host.equals("youtu.be") ) {
                    return path.replace("/","");
                } else {
                    //watch?v=Lc06vtsCAF0&list=PLzp99h66R9XERjaVrbx5HiSKU0bZR2Wx91

                    String[] partsquery = query.split("&");
                    for (String parse : partsquery) {
                        String[] partsqueryinner = query.split("=");
                        String key = partsqueryinner[0];
                        String value = partsqueryinner[1];

                        if (key.equals("v"))
                            return value;
                    }
                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getIdDailymotionVideo(String s, Context context ) {
        if(!s.isEmpty() && s.length() > 0 && Patterns.WEB_URL.matcher(s.toString()).matches() && isDailymotionUrl(s, context)) {
            //parse URL
            try {
                URL aURL = new URL( s.toString() );
                String host = aURL.getHost();
                String query = aURL.getQuery();
                String path = aURL.getPath();

                Log.d(TAG, "parse_url - URL : "+s+" - query : "+query+" - path : "+path );


                if ( host.equals("dai.ly") ) {
                    return path.replace("/","");
                } else {
                    //watch?v=Lc06vtsCAF0&list=PLzp99h66R9XERjaVrbx5HiSKU0bZR2Wx91

                    String[] partsquery = path.split("/");
                    Log.d(TAG, "parse_urlDM - partsquery[1] : " + partsquery[2].substring(0, partsquery[2].indexOf("_")));
                    return partsquery[2].substring(0, partsquery[2].indexOf("_")) ;
                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }




}

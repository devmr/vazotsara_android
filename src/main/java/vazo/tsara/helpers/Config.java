package vazo.tsara.helpers;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class Config {

    /**
     * Cle Google API
     */
    //public static final String DEVELOPER_KEY = "AIzaSyDpmYaiAJSCz987DWsBhqDFpXWGI11j9rw";
    //public static final String DEVELOPER_KEY = "AIzaSyDQoijs2onYW87K0r20JDDacbWheP8K4-Y";
    public static final String DEVELOPER_KEY = "AIzaSyDdXxzLGd6uxaoZH3BRlu1kC9Cf701ydMk";

    //Ad mob
    public static final String BANNER_AD_UNITID = "ca-app-pub-0184608766708661/9006247834";

    /**
     * URL REtrofit
     */
    public static final String URLDOMAIN = "https://www.vazotsara.com";
    public static final String URLAPI = URLDOMAIN + "/api";

    /**
     * Info a passer dans le form creation
     */
    public static final String CREATEDBY = "android";

    /**
     * TAGS LOGS
     */
    public static final String TAGKEY = "VAZOTSARA";

    /**
     * ID Google Analytics
     */
    public static final String GAID = "UA-2014032-18";

    /**
     * Sprintf chemin image
     */
    public static final String THUMB_PATH = "http://i.ytimg.com/vi/%s/hqdefault.jpg";
    public static final String THUMB_PATH_MAX = "http://i.ytimg.com/vi/%s/maxresdefault.jpg";

    /**
     * Path Playlists
     */
    public static final String PATHPLAYLIST = URLDOMAIN + "/asset/images/playlists/%s";

    /**
     * Database
     */
    public static final String DBNAME = "vazotsara";
    public static final String TABLE_FAVORI_VIDEO = "favori_video";
    public static final String TABLE_FAVORI_PLAYLIST = "favori_playliste";

    /**
     * Fields TABLE_FAVORI_VIDEO
     */
    public static final String FAVORI_VIDEO_KEY_ID = "id";
    public static final String FAVORI_VIDEO_KEY_NAME = "titrevideo";
    public static final String FAVORI_VIDEO_KEY_WEBID = "videoid";
    public static final String FAVORI_VIDEO_KEY_VIDEOYT = "videoyt";
    public static final String FAVORI_VIDEO_KEY_VIDEOFORMAT = "videoformat";

    /**
     * Fields TABLE_FAVORI_PLAYLIST
     */
    public static final String FAVORI_PLAYLIST_KEY_ID = "id";
    public static final String FAVORI_PLAYLIST_KEY_NAME = "artistenom";
    public static final String FAVORI_PLAYLIST_KEY_WEBID = "videoid";
    public static final String FAVORI_PLAYLIST_KEY_IMAGE = "image";

    /**
     * Color
     */
    public static final String COLOR_FAVORI_IN = "#0000ff";
    public static final String COLOR_FAVORI_OUT = "#000000";

    /**
     * DAte pattern last connexion
     */
    public static final String DATE_LAST_CONNEXION = "dd MMM yyyy - HH:mm";

    public static final String PREFERENCE_FILENAME = "Vazotsarapreferences";

    public static final int LIMIT_GENERAL = 20;

    //Parse.com
    public static final String PARSE_APPID = "nXB4JA8JVA1KYxnZv9R1dZbBNENDDoW9mPasFYsL";
    public static final String PARSE_CLIENTKEY = "yXIUP8ZgvjbNGm0Z6nQgOiRKLhykHcJ8cI28yEPh";
    public static final String PARSE_CHANNEL = "Vazotsara";
    public static final int NOTIFICATION_ID = 100;


    public static final String SEPARATOR = "::";

}

package vazo.tsara.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.query.Select;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoPojo;

/**
 * Created by rabehasy on 03/09/2015.
 */
public class VazotsaraBCSaveBDD extends BroadcastReceiver {

    static String TAG = Config.TAGKEY;
    String dateserver = "";

    ApiRetrofit vidapi;
    RestAdapter restAdapter;
    String datesqlite;
    Context context;

    int totalvideoinserver;
    int totalvideoinbdd;
    int lastpage = 0;


    @Override
    public void onReceive(final Context context, Intent intent) {

        Log.d(TAG, "VazotsaraBCSaveBDD onReceive");
        this.context = context;

        totalvideoinbdd = TableVideos.getAllCount();
        totalvideoinserver = TableLastcheck.getTotalvideosinserver();

        if ( totalvideoinserver<=0 ) {
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive totalvideoinserver <= 0 ");
            VazotsaraShareFunc.setTotalVideoinServer();
            totalvideoinserver = TableLastcheck.getTotalvideosinserver();
        }

        if( totalvideoinbdd < totalvideoinserver ) {
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive totalvideoinbdd (" + totalvideoinbdd + ") < totalvideoinserver (" + totalvideoinserver+") ");
            lastpage = Integer.parseInt(TableLastcheck.getLastPagelisteNew());
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive lastpage : "+lastpage);
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive lastpage (Pref) : "+VazotsaraShareFunc.getLastPageInPreferences(context) );
        } else if ( totalvideoinbdd>0 && totalvideoinbdd >= totalvideoinserver ) {
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive totalvideoinbdd ("+totalvideoinbdd+") > totalvideoinserver ("+totalvideoinserver+") - FIN DU SCRIPT");
            return;
        }

        Log.d(TAG, "VazotsaraBCSaveBDD onReceive totalvideoinbdd: "+totalvideoinbdd+" - totalvideoinserver:"+totalvideoinserver);

        if ( lastpage >= 0 ) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
            apipl.getListVideos(100, "endtime_uploaded", lastpage, new Callback<VideoPojo>() {
                @Override
                public void success(VideoPojo videoPojo, Response response) {
                    Log.d(TAG,"VazotsaraBCSaveBDD onReceive getListVideos success");
                    if ( videoPojo.getTotal() > 0 ) {
                        new saveAsyncVideos().execute(videoPojo);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG,"VazotsaraBCSaveBDD onReceive getListVideos failure");
                }
            });
        }

    }

    private class saveAsyncVideos extends AsyncTask<VideoPojo,String,String>{
        @Override
        protected String doInBackground(VideoPojo... params) {

            Log.d(TAG, "VazotsaraBCSaveBDD onReceive saveAsyncVideos doInBackground - TableLastcheck count : " + new Select().from(TableLastcheck.class).count()  );
            Log.d(TAG, "VazotsaraBCSaveBDD onReceive saveAsyncVideos doInBackground - size : " + params[0].getVideos().size() + " - Page: " + String.valueOf(lastpage)+" - nextpage : "+String.valueOf( lastpage+1 ) );

            TableLastcheck.setLastPagelisteNew(String.valueOf(lastpage + 1));
            VazotsaraShareFunc.setLastPageInPreferences((lastpage + 1), context);

            for(int i=0; i<params[0].getVideos().size(); i++ ) {
                VazotsaraShareFunc.saveUniqueVideo( params[0].getVideos().get(i), "" );
            }


            return null;
        }
    }
}

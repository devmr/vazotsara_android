package vazo.tsara.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
/**
 * Created by rabehasy on 27/05/2015.
 */
public class ConnectionDetector extends BroadcastReceiver {

    String TAG = "VAZOTSARA";

    private Context _context;
    private ConnectivityManager connectivity;

    /**
     *
     * @param context
     */
    public ConnectionDetector(Context context){
        this._context = context;
    }

    public ConnectionDetector() {

    }

    public boolean isConnectingToInternet(){
        connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Log.d(TAG,"ConnectionDetector > isConnectingToInternet() - connectivity : "+connectivity);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String getConnexionType()
    {

        connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netw = connectivity.getActiveNetworkInfo();


        if (netw != null && netw.getType() == ConnectivityManager.TYPE_WIFI)
            return netw.getTypeName()+" "+netw.getDetailedState();

        if ( netw != null && netw.getType() == ConnectivityManager.TYPE_MOBILE ) {
            return netw.getTypeName()+" - "+netw.getSubtypeName();
        }

        if (netw != null && netw.getType() == ConnectivityManager.TYPE_BLUETOOTH )
            return netw.getTypeName()+" "+netw.getDetailedState();

        return "NON CONNECTE";

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "ConnectionDetector onReceive - intent.getAction(): "+intent.getAction());
    }
}

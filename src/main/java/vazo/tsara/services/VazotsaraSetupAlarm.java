package vazo.tsara.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import vazo.tsara.helpers.Config;

/**
 * Created by Miary on 06/09/2015.
 */
public class VazotsaraSetupAlarm  extends BroadcastReceiver {

    static String TAG = Config.TAGKEY;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "VazotsaraSetupAlarm onReceive");

        Intent i = new Intent(context, VazotsaraStarterService.class);
        context.startService(i);
    }
}

package vazo.tsara.services;


import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.TokenPojo;

/**
 * Created by rabehasy on 03/09/2015.
 */
public class VazotsaraBCToken extends BroadcastReceiver {

    static String TAG = Config.TAGKEY;
    String dateserver = "";

    ApiRetrofit vidapi;
    RestAdapter restAdapter;
    String datesqlite;
    Context context;
    NotificationManager mNotificationManager;

    @Override
    public void onReceive(final Context context, Intent intent) {

            Log.d(TAG,"Dans VazotsaraBCToken onReceive - Useful.isAdmin : "+Useful.isAdmin(context)+" -  Token "+ PreferenceManager.getDefaultSharedPreferences(context).getString("token", "") );

            this.context = context;
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            /**
             * Retrofit - Check if favori artist exist and check if new video exists after lastcheck
             */
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            if (Useful.isAdmin(context)) {
                                request.addHeader("Authorization", " Bearer " + VazotsaraShareFunc.getAdminToken(context));
                            }
                        }
                    })
                    .build();
            vidapi = restAdapter.create(ApiRetrofit.class);

            if (Useful.isAdmin(context) ) {
                vidapi.getTokenRefresh(new Callback<TokenPojo>() {
                    @Override
                    public void success(TokenPojo videoPojo, retrofit.client.Response response) {
                        VazotsaraShareFunc.saveTokenInPreferences( videoPojo.getToken() , context);
                        //Toast.makeText(context,"TOKEN MAJ",Toast.LENGTH_LONG).show();
                        Log.d(TAG,"Dans VazotsaraBCToken onReceive - MAJ TOKEN ADMIN " );
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d(TAG,"Dans VazotsaraBCToken onReceive - MAJ TOKEN ADMIN Error : "+(error.getMessage()!=null?error.getMessage():"") );
                    }
                });
            }

    }

}

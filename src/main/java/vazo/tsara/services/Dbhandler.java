package vazo.tsara.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import vazo.tsara.helpers.Config;
import vazo.tsara.models.FavoriPlaylistSQL;
import vazo.tsara.models.FavoriVideoSQL;
import vazo.tsara.models.relatedvideomodel;

/**
 * Created by Miary on 28/08/2015.
 */
public class Dbhandler extends SQLiteOpenHelper {
    static String TAG = Config.TAGKEY;
    /**
     * Variables
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Nom de la BDD
     */
    private static final String DATABASE_NAME = Config.DBNAME;

    /**
     * Nom de la table contact
     */
    private static final String TABLE_FAVORI_VIDEO = Config.TABLE_FAVORI_VIDEO;

    /**
     * Champs de la table contacts
     */
    private static final String FAVORI_VIDEO_KEY_ID = Config.FAVORI_VIDEO_KEY_ID;
    private static final String FAVORI_VIDEO_KEY_NAME = Config.FAVORI_VIDEO_KEY_NAME;
    private static final String FAVORI_VIDEO_KEY_WEBID = Config.FAVORI_VIDEO_KEY_WEBID;
    private static final String FAVORI_VIDEO_KEY_VIDEOYT = Config.FAVORI_VIDEO_KEY_VIDEOYT;
    private static final String FAVORI_VIDEO_KEY_VIDEOFORMAT = Config.FAVORI_VIDEO_KEY_VIDEOFORMAT;

    /**
     * Nom de la table contact
     */
    private static final String TABLE_FAVORI_PLAYLIST = Config.TABLE_FAVORI_PLAYLIST;

    /**
     * Champs de la table contacts
     */
    private static final String FAVORI_PLAYLIST_KEY_ID = Config.FAVORI_PLAYLIST_KEY_ID;
    private static final String FAVORI_PLAYLIST_KEY_NAME = Config.FAVORI_PLAYLIST_KEY_NAME;
    private static final String FAVORI_PLAYLIST_KEY_WEBID = Config.FAVORI_PLAYLIST_KEY_WEBID;
    private static final String FAVORI_PLAYLIST_KEY_IMAGE = Config.FAVORI_PLAYLIST_KEY_IMAGE;

    /**
     * Crée la BDD
     */
    public Dbhandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        /**
         * Crée la requete
         */
        String SQL_CREATE = "CREATE TABLE " + TABLE_FAVORI_VIDEO + "("
                + FAVORI_VIDEO_KEY_ID + " INTEGER PRIMARY KEY," + FAVORI_VIDEO_KEY_NAME + " TEXT,"
                + FAVORI_VIDEO_KEY_WEBID + " INTEGER, IS_DELETED INTEGER, " + FAVORI_VIDEO_KEY_VIDEOYT + " TEXT, "+FAVORI_VIDEO_KEY_VIDEOFORMAT+" TEXT)";

        /**
         * Executer la requete
         */
        db.execSQL(SQL_CREATE);

        /**
         * Crée la requete
         */
        String SQL_CREATE_FAVORI_PLAYLIST = "CREATE TABLE " + TABLE_FAVORI_PLAYLIST + "("
                + FAVORI_PLAYLIST_KEY_ID + " INTEGER PRIMARY KEY," + FAVORI_PLAYLIST_KEY_NAME + " TEXT,"
                + FAVORI_PLAYLIST_KEY_WEBID + "  TEXT, IS_DELETED INTEGER, IS_HIDDEN INTEGER, "+FAVORI_PLAYLIST_KEY_IMAGE+" TEXT)";

        /**
         * Executer la requete
         */
        db.execSQL(SQL_CREATE_FAVORI_PLAYLIST);

        /**
         * Crée la requete
         */
        String SQL_CREATE_LAST_UPDATE = "CREATE TABLE LAST_CHECK(id INTEGER PRIMARY KEY,date TEXT)";

        /**
         * Executer la requete
         */
        db.execSQL(SQL_CREATE_LAST_UPDATE);



        db.execSQL("INSERT INTO LAST_CHECK(id,date) VALUES(1,'"+String.valueOf(  System.currentTimeMillis()/1000L  ) +"')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /**
         * DROP Table
         */
         //db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORI_VIDEO);

        /**
         * Recréer la table
         */
         //onCreate(db);
    }

    /**
     * Ajouter un contact
     */
    public void addFavori( FavoriVideoSQL favorivideo, List<relatedvideomodel> artistList)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Instancier
         */
        ContentValues values = new ContentValues();

        /**
         * Remplir avec le model
         */
        values.put(FAVORI_VIDEO_KEY_NAME, favorivideo.getTitre() );
        values.put(FAVORI_VIDEO_KEY_WEBID, favorivideo.getVideoid() );
        values.put("IS_DELETED", favorivideo.getIsdeleted() );
        values.put(FAVORI_VIDEO_KEY_VIDEOYT, favorivideo.getVideoyt() );
        values.put(FAVORI_VIDEO_KEY_VIDEOFORMAT, favorivideo.getVideoformat() );

        /**
         * Save in DB
         */
        db.insert(TABLE_FAVORI_VIDEO, null, values);

        Log.d(TAG, "addFavori - values: " + values);

        /**
         * Ajouter ou non dans la table favoriartists
         */
        for ( int i = 0; i < artistList.size(); i++ ) {

            Log.d(TAG, " cursor.getInt(0): " + getCountFavoriArtist(Integer.parseInt(artistList.get(i).getUid())));
            if ( getCountFavoriArtist( Integer.parseInt( artistList.get(i).getUid() ) ) == 0) {
                //Insert
                addFavoriArtist(new FavoriPlaylistSQL(artistList.get(i).getObjetnom(), Integer.parseInt(artistList.get(i).getUid()), 0, 1, ""));
            }
        }
        /**
         * Fermer
         */
        db.close();


    }
    /**
     * Afficher un enregistrement
     */
    public  FavoriVideoSQL getSingleVideoFavori( int id )
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        /**
         * Requete
         */
        Cursor cursor = db.query(TABLE_FAVORI_VIDEO, new String[] { FAVORI_VIDEO_KEY_ID, FAVORI_VIDEO_KEY_NAME, FAVORI_VIDEO_KEY_WEBID, "IS_DELETED", FAVORI_VIDEO_KEY_VIDEOYT, FAVORI_VIDEO_KEY_VIDEOFORMAT }, FAVORI_VIDEO_KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        //FavoriVideoSQL favorivideosingle = new FavoriVideoSQL( Integer.parseInt( cursor.getString(0) ) , cursor.getString(1), Integer.parseInt(cursor.getString(2)), Integer.parseInt(cursor.getString(3)));
         FavoriVideoSQL favorivideosingle = new  FavoriVideoSQL( cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getInt(3),cursor.getString(4),cursor.getString(5) );


        return favorivideosingle;
    }
    /**
     * Afficher tous les enregistrements
     */
    public List<FavoriVideoSQL> getAllFavoriVideo()
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        List<FavoriVideoSQL> favorivideoList = new ArrayList<FavoriVideoSQL>();

        /**
         * Select * FROM
         */
        String selectQuery = "SELECT  * FROM " + TABLE_FAVORI_VIDEO;

        /**
         * ExecSQL
         */
        Cursor cursor = db.rawQuery(selectQuery, null);

        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {
            do {
                FavoriVideoSQL favoriRow = new FavoriVideoSQL();
                favoriRow.setId(Integer.parseInt(cursor.getString(0)));
                favoriRow.setTitre(cursor.getString(1));
                favoriRow.setVideoid(Integer.parseInt(cursor.getString(2)));
                favoriRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                favoriRow.setVideoyt(cursor.getString(4));
                favoriRow.setVideoformat(cursor.getString(5));


                /**
                 * Ajouter la ligne à la liste
                 */
                favorivideoList.add(favoriRow);
            } while( cursor.moveToNext() );
        }

        return favorivideoList;
    }
    /**
     * Update
     */
    public int updateFavoriVideo(FavoriVideoSQL favorivideo)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        /**
         * Remplir avec le model
         */
        values.put(FAVORI_VIDEO_KEY_NAME, favorivideo.getTitre() );
        values.put(FAVORI_VIDEO_KEY_WEBID, favorivideo.getVideoid() );
        values.put("IS_DELETED", favorivideo.getIsdeleted() );
        values.put(FAVORI_VIDEO_KEY_VIDEOYT, favorivideo.getVideoyt());
        values.put(FAVORI_VIDEO_KEY_VIDEOFORMAT, favorivideo.getVideoformat() );

        /**
         * Requete
         */
        return db.update(TABLE_FAVORI_VIDEO, values, FAVORI_VIDEO_KEY_ID+"=?", new String[] { String.valueOf(favorivideo.getId()) } );

    }

    /**
     * Delete all contact
     */
    public void deleteAllFavoriVideo()
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_VIDEO, null, null);

        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Delete un contact
     */
    public void deleteFavoriVideo(FavoriVideoSQL favorivideo)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_VIDEO, FAVORI_VIDEO_KEY_ID+"=?", new String[] { String.valueOf(favorivideo.getId()) } );


        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Delete un contact
     */
    public void deleteFavoriVideo(int uid, List<relatedvideomodel> artistList)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_VIDEO, FAVORI_VIDEO_KEY_WEBID+"=?", new String[] { String.valueOf(uid) } );


        /**
         * Ajouter ou non dans la table favoriartists
         */
        for ( int i = 0; i < artistList.size(); i++ ) {

            Log.d(TAG, " cursor.getInt(0): " + getCountFavoriArtist(Integer.parseInt(artistList.get(i).getUid())));
            if ( getCountFavoriArtist(Integer.parseInt(artistList.get(i).getUid()), 1) > 0) {
                //Insert
                deleteSingleFavoriArtist(Integer.parseInt(artistList.get(i).getUid()) );
            }
        }
        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Delete un contact
     */
    public void deleteFavoriVideo(int uid )
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_VIDEO, FAVORI_VIDEO_KEY_WEBID+"=?", new String[] { String.valueOf(uid) } );

        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Nombre de résultats
     */
    public int getFavoriVideoCount()
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        /**
         * Requete
         */
        String countQuery = "SELECT  * FROM " + TABLE_FAVORI_VIDEO;

        Cursor cursor = db.rawQuery(countQuery, null);



        int nbrow = cursor.getCount();

        /**
         * Fermer
         */
        cursor.close();


        return nbrow;
    }

    public boolean VideoInFavoris( int videoid ) {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        String SQL = "SELECT COUNT(*) FROM "+TABLE_FAVORI_VIDEO+" WHERE "+FAVORI_VIDEO_KEY_WEBID+" = "+videoid;
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        Log.d(TAG, "SQL : " + SQL + " - cursor.getInt(0): " + cursor.getInt(0));
        if ( cursor.getInt(0) > 0 ) {
            return true;
        }
        return false;
    }

    /**
     * Ajouter un contact
     */
    public void addFavoriArtist(FavoriPlaylistSQL model)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Instancier
         */
        ContentValues values = new ContentValues();

        /**
         * Remplir avec le model
         */
        values.put(FAVORI_PLAYLIST_KEY_NAME, model.getTitre() );
        values.put(FAVORI_PLAYLIST_KEY_WEBID, model.getVideoid() );
        values.put(FAVORI_PLAYLIST_KEY_IMAGE, model.getImage() );
        values.put("IS_DELETED", model.getIsdeleted() );
        values.put("IS_HIDDEN", model.getIshidden() );


        /**
         * Save in DB
         */
        db.insert(TABLE_FAVORI_PLAYLIST, null, values);

        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Afficher un enregistrement
     */
    public FavoriPlaylistSQL getSingleFavoriArtist( int id )
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        /**
         * Requete
         */
        Cursor cursor = db.query(TABLE_FAVORI_PLAYLIST, new String[] { FAVORI_PLAYLIST_KEY_ID, FAVORI_PLAYLIST_KEY_NAME, FAVORI_PLAYLIST_KEY_WEBID, "IS_DELETED", "IS_HIDDEN", FAVORI_PLAYLIST_KEY_IMAGE }, FAVORI_PLAYLIST_KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        //FavoriVideoSQL favorivideosingle = new FavoriVideoSQL( Integer.parseInt( cursor.getString(0) ) , cursor.getString(1), Integer.parseInt(cursor.getString(2)), Integer.parseInt(cursor.getString(3)));
        FavoriPlaylistSQL single = new FavoriPlaylistSQL( cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getInt(3),cursor.getInt(4), cursor.getString(5) );


        return single;
    }

    /**
     * Afficher tous les enregistrements
     */
    public List<FavoriPlaylistSQL> getAllFavoriArtist(int hidden)
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        List<FavoriPlaylistSQL> modelList = new ArrayList<FavoriPlaylistSQL>();

        /**
         * Select * FROM
         */
        String selectQuery = "SELECT  * FROM " + TABLE_FAVORI_PLAYLIST+" WHERE IS_HIDDEN = "+hidden;

        /**
         * ExecSQL
         */
        Cursor cursor = db.rawQuery(selectQuery, null);

        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {
            do {
                FavoriPlaylistSQL modelRow = new FavoriPlaylistSQL();
                modelRow.setId(Integer.parseInt(cursor.getString(0)));
                modelRow.setTitre(cursor.getString(1));
                modelRow.setVideoid(Integer.parseInt(cursor.getString(2)));
                modelRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                modelRow.setIshidden(Integer.parseInt(cursor.getString(4)));
                modelRow.setImage(cursor.getString(5));



                /**
                 * Ajouter la ligne à la liste
                 */
                modelList.add(modelRow);
            } while( cursor.moveToNext() );
        }

        return modelList;
    }
    /**
     * Afficher tous les enregistrements
     */
    public List<FavoriPlaylistSQL> getAllFavoriArtist()
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        List<FavoriPlaylistSQL> modelList = new ArrayList<FavoriPlaylistSQL>();

        /**
         * Select * FROM
         */
        String selectQuery = "SELECT  * FROM " + TABLE_FAVORI_PLAYLIST;

        /**
         * ExecSQL
         */
        Cursor cursor = db.rawQuery(selectQuery, null);

        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {
            do {
                FavoriPlaylistSQL modelRow = new FavoriPlaylistSQL();
                modelRow.setId(Integer.parseInt(cursor.getString(0)));
                modelRow.setTitre(cursor.getString(1));
                modelRow.setVideoid(Integer.parseInt(cursor.getString(2)));
                modelRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                modelRow.setIshidden(Integer.parseInt(cursor.getString(4)));
                modelRow.setImage(cursor.getString(5));


                /**
                 * Ajouter la ligne à la liste
                 */
                modelList.add(modelRow);
            } while( cursor.moveToNext() );
        }

        return modelList;
    }
    /**
     * Update
     */
    public int updateFavoriArtist(FavoriPlaylistSQL model)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        /**
         * Remplir avec le model
         */
        values.put(FAVORI_PLAYLIST_KEY_NAME, model.getTitre() );
        values.put(FAVORI_PLAYLIST_KEY_WEBID, model.getVideoid() );
        values.put("IS_DELETED", model.getIsdeleted() );
        values.put("IS_HIDDEN", model.getIshidden());
        values.put(FAVORI_PLAYLIST_KEY_IMAGE, model.getImage());


        /**
         * Requete
         */
        return db.update(TABLE_FAVORI_PLAYLIST, values, FAVORI_PLAYLIST_KEY_WEBID+"=?", new String[] { String.valueOf(model.getVideoid()) } );

    }

    /**
     * Delete all contact
     */
    public void deleteAllFavoriArtist()
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_PLAYLIST, null, null);

        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Delete un contact
     */
    public void deleteSingleFavoriArtist(int id)
    {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        /**
         * Requete
         */
        db.delete(TABLE_FAVORI_PLAYLIST, FAVORI_PLAYLIST_KEY_WEBID + "=?", new String[]{String.valueOf(id)});

        /**
         * Fermer
         */
        db.close();
    }

    /**
     * Nombre de résultats
     */
    public int getCountFavoriArtist()
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        /**
         * Requete
         */
        String SQL = "SELECT COUNT(*) FROM " + TABLE_FAVORI_PLAYLIST ;
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor.getInt(0);
    }
    /**
     * Nombre de résultats
     */
    public int getCountFavoriArtistHidden(int hidden)
    {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        /**
         * Requete
         */
        String SQL = "SELECT COUNT(*) FROM " + TABLE_FAVORI_PLAYLIST+" WHERE IS_HIDDEN = "+hidden ;
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor.getInt(0);
    }

    public int getCountFavoriArtist(int id) {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        String SQL = "SELECT COUNT(*) FROM " + TABLE_FAVORI_PLAYLIST + " WHERE " + FAVORI_PLAYLIST_KEY_WEBID + " = " + id;
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor.getInt(0);
    }

    public boolean isArtistInFavori(int id) {
        return (getCountFavoriArtist(id, 0)>0);
    }

    public boolean isArtistInFavori(int id, int hidden) {
        return (getCountFavoriArtist(id, hidden)>0);
    }

    public int getCountFavoriArtist(int id, int hidden) {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        String SQL = "SELECT COUNT(*) FROM " + TABLE_FAVORI_PLAYLIST + " WHERE IS_HIDDEN = "+hidden+" AND " + FAVORI_PLAYLIST_KEY_WEBID + " = " + id;
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor.getInt(0);
    }

    public String getLastCheck() {
        /**
         * Instancier la database en "readable"
         */
        SQLiteDatabase db = this.getReadableDatabase();

        String SQL = "SELECT date FROM LAST_CHECK WHERE id = 1 ";
        Cursor cursor = db.rawQuery(SQL, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor.getString(0);
    }

    public void updateLastCheck(String dateserver) {
        /**
         * Instancier la database
         */
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        /**
         * Remplir avec le model
         */

        values.put("date", dateserver );



        /**
         * Requete
         */
        db.update("LAST_CHECK", values,  "id=1", null);

    }
}

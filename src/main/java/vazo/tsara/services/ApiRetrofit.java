package vazo.tsara.services;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import vazo.tsara.models.Embedly;
import vazo.tsara.models.FieldAddPlaylist;
import vazo.tsara.models.FieldAddUser;
import vazo.tsara.models.FieldAddVideo;
import vazo.tsara.models.FieldUpdatePlaylist;
import vazo.tsara.models.FieldUpdateVideo;
import vazo.tsara.models.FormatResults;
import vazo.tsara.models.LoginPassword;
import vazo.tsara.models.PlaylisteSingle;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.SearchPlaylist;
import vazo.tsara.models.SearchVideo;
import vazo.tsara.models.ServerMsgPojo;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.VideoStatPojo;

/**
 * Created by rabehasy on 27/05/2015.
 */
public interface ApiRetrofit {
    @GET("/videos")
    public void getListVideos(@Query("limit") int limit, @Query("sortfields") String sortfields, @Query("page") int page,  Callback<VideoPojo> response);

    @GET("/videos")
    public void getListVideos(@Query("limit") int limit, @Query("sortfields") String sortfields, @Query("page") int page, @Query("q") String q,  Callback<VideoPojo> response);

    @GET("/allvideosinplaylist/{id}")
    public void getAllVideosInPlaylists(@Path("id") String id,  Callback<VideoPojo> response);


    @GET("/videos")
    public void getVideosInPlaylists(@Query("limit") int limit, @Query("page") int page, @Query("plid") String plid,  Callback<VideoPojo> response);

    @GET("/videosperiode")
    public void getListVideosPeriode(@Query("limit") int limit, @Query("sort") String sortfields, @Query("periode") String periode, @Query("page") int page,  Callback<VideoPojo> response);


    @GET("/video/{id}")
    public void getSingleVideo(@Path("id") String id, Callback<VideoSingleGson> response);

    @GET("/playliste/{id}")
    public void getSinglePlaylist(@Path("id") String id, Callback<PlaylisteSingle> response);



    @GET("/statvideo/{id}")
    public void getStatVideo(@Path("id") String id, Callback<VideoStatPojo> response);

    @GET("/relatedvideo/{id}")
    public void getRelatedVideo(@Path("id") String id, @Query("t") String t, Callback<VideoPojo> response);

    @GET("/playlists")
    public void getPlaylists( @Query("objetype") String objetype, Callback<Playlists> response);

    @GET("/playlists")
    public void getPlaylists( @Query("objetype") String objetype, @Query("limit") int limit, @Query("page") int page, Callback<Playlists> response);

    @GET("/playlists")
    public void getPlaylists( @Query("objetype") String objetype, @Query("limit") int limit, @Query("page") int page, @Query("q") String q, Callback<Playlists> response);

    @GET("/playlists")
    public void getPlaylists( @Query("objetype") String objetype, @Query("limit") int limit, @Query("page") int page, @Query("orderby") String orderby, @Query("orderbyascdesc") String orderbyascdesc, Callback<Playlists> response);


    @GET("/searchvideo")
    public void getSearchVideo(@Query("q") String q, @Query("t") String t, Callback<SearchVideo> response);

    @GET("/searchplaylist")
    public void getSearchPlaylist(@Query("q") String q, @Query("t") String t, Callback<SearchPlaylist> response);

    @GET("/videosinfavori")
    public void getVideosInFavori(@Query("playliste") String playliste, @Query("datesqlite") String datesqlite, Callback<VideoPojo> response);

    //@FormUrlEncoded
    @POST("/login")
    public void postLogin(@Body LoginPassword loginPassword, Callback<TokenPojo> response);
    //public void postLogin(@Field("email") String email, @Field("password") String password, Callback<TokenPojo> response);


    @GET("/embedly")
    public void getEmbedly(@Query("url") String url, Callback<Embedly> response);

    @POST("/createvideo")
    public void postCreateVideo(@Body FieldAddVideo addv, Callback<TokenPojo> response );

    @POST("/updatevideo/{id}")
    public void postUpdateVideo(@Path("id")  String id,@Body FieldUpdateVideo addv, Callback<TokenPojo> response );

    @GET("/formatvideo")
    public void getFormatVideo(@Query("url") String url, Callback<FormatResults> response);

    @GET("/tokenrefresh")
    public void getTokenRefresh( Callback<TokenPojo> response );

    @POST("/createplaylist")
    public void postCreatePlaylist(@Body FieldAddPlaylist addv, Callback<TokenPojo> response );

    @POST("/updateplaylist/{id}")
    public void postUpdatePlaylist(@Path("id")  String id, @Body FieldAddPlaylist addv, Callback<TokenPojo> response );

    @POST("/updateplaylist/{id}")
    public void postUpdatePlaylist(@Path("id")  String id, @Body FieldUpdatePlaylist addv, Callback<TokenPojo> response );

    @GET("/deletevideo/{id}")
    public void getDeleteVideo(@Path("id")  String id, Callback<ServerMsgPojo> response);

    @GET("/videosafterdate")
    public void getAllVideosAfterDate(@Query("datesqlite") String datesqlite, Callback<VideoPojo> response );

    @GET("/videosafterdate")
    public void getAllVideosAfterDateHD(@Query("datesqlite") String datesqlite, @Query("format") String format, Callback<VideoPojo> response );

    @GET("/deleteplaylist/{id}")
    public void getDeletePlaylist(@Path("id")  String id, Callback<ServerMsgPojo> response);

    @GET("/videosbeforeid/{id}")
    public void getListVideosBeforeId(@Path("id")  String id ,  Callback<VideoPojo> response);

    @GET("/playlistebeforeid/{id}")
    public void getListPlaylisteBeforeId(@Path("id")  String id , Callback<Playlists> response );

    @GET("/playlistebeforeid/{id}")
    public void getListPlaylisteBeforeId(@Path("id")  String id , @Query("objetype") String objetype,  Callback<Playlists> response );

    @POST("/createorupdateuser")
    public void postCreateOrUpdateUser( @Body FieldAddUser addv, Callback<TokenPojo> response );

    @GET("/addvideoidinfavori")
    public void addvideoidInFavori(@Query("user_id") String user_id, @Query("videoid") String videoid , Callback<TokenPojo> response );

    @GET("/deletevideoidinfavori")
    public void deletevideoidInFavori(@Query("user_id") String user_id, @Query("videoid") String videoid , Callback<TokenPojo> response );

    @GET("/addplaylisteidinfavori")
    public void addplaylisteidInFavori(@Query("user_id") String user_id, @Query("playlisteid") String playlisteid , Callback<TokenPojo> response );

    @GET("/deleteplaylisteidinfavori")
    public void deleteplaylisteidInFavori(@Query("user_id") String user_id, @Query("playlisteid") String playlisteid , Callback<TokenPojo> response );

    @GET("/favoritevideobyuser/{id}")
    public void getFavoritevideobyuser(@Path("id")  String id ,  Callback<VideoPojo> response);

    @GET("/favoriteplaylistebyuser/{id}")
    public void getFavoriteplaylistebyuser(@Path("id")  String id ,  Callback<Playlists> response);

    @GET("/allvideosinfavoriteplaylistebyuser/{id}")
    public void getVideosbyfavoriteplaylist(@Path("id")  String id ,  Callback<VideoPojo> response);


}

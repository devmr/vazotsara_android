package vazo.tsara.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.activities.MainActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.TableFavoriplaylists;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.TableVideosHome;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.VideoSingleGsonPlaylist;
import vazo.tsara.models.Videogson;

/**
 * Created by rabehasy on 03/09/2015.
 */
public class VazotsaraBC  extends ParsePushBroadcastReceiver  {

    static String TAG = Config.TAGKEY;
    String dateserver = "";
    //Dbhandler db;
    ApiRetrofit vidapi;
    RestAdapter restAdapter;
    String datesqlite;
    Context context;
    NotificationManager mNotificationManager;

    int limitvideo = 20;
    int page = 0;
    int limitplaylist = 20;

    private Intent parseIntent;

    JSONObject json;
    String strjson;

    Gson gson = new Gson();

    boolean multivideos = false;
    boolean isComment = false;

    String imageartiste = "";

    public VazotsaraBC() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        this.context = context;
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (intent == null)
            return;

        Log.d(TAG, "Push com.parse.Data : " + intent.getExtras().getString("com.parse.Data"));

        try {


            json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            strjson = intent.getExtras().getString("com.parse.Data");

            Log.e(TAG, "Push received: " + json);
            Log.e(TAG, "Push received strjson: " + strjson);

            parseIntent = intent;

            parsePushJson(context, json);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    private void parsePushJson(Context context, JSONObject json) {
        try {

            Log.d(TAG, "Push parsePushJson" );


            if ( json.getJSONArray("videos")!=null && json.getJSONArray("videos").length() > 1 ) {
                multivideos = true;
            }

            Log.d(TAG, "Push parsePushJson - multivideos? " + multivideos);



            if ( json.getString("iscomment")!=null && json.getString("iscomment").equals("1") ) {
                isComment = true;
            }

            Log.d(TAG, "Push parsePushJson - isComment? " + isComment);




            Intent resultIntent = new Intent(context, MainActivity.class);
            //showNotificationMessage(context, title, message, resultIntent);

            setNotifVideo();




        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }




    void setNotifVideo() {

        Log.d(TAG, "Push setNotifVideo");

        boolean favoriExists = false;
        List<String> idfavoriplaylists = checkFavoriPlaylistsServer();
        List<String> idfavorivideos = checkFavoriVideosServer();

        Log.d(TAG,"Push setNotifVideo idfavoriplaylists : "+idfavoriplaylists.toString());
        Log.d(TAG,"Push setNotifVideo idfavorivideos : "+idfavorivideos );

        boolean showNotif = false;

        for( String id: idfavorivideos) {
            for( int i = 0; i < gson.fromJson(strjson, VideoPojo.class).getVideos().size(); i++ ) {
                if ( gson.fromJson(strjson, VideoPojo.class).getVideos().get(0).getId().equals(id) ) {
                    favoriExists = true;
                }
            }
        }

        if ( !multivideos ) {

            Log.d(TAG, "Push setNotifVideo multivideos false"  );

            for( String id: idfavoriplaylists) {
                for( int i = 0; i < gson.fromJson(strjson, VideoPojo.class).getVideos().get(0).getPlaylisteinfo().size(); i++ ) {
                    if ( gson.fromJson(strjson, VideoPojo.class).getVideos().get(0).getPlaylisteinfo().get(i).getId().equals(id) ) {
                        favoriExists = true;
                    }
                }
            }



            Log.d(TAG, "Push setNotifVideo favoriExists? "+favoriExists  );
            Log.d(TAG, "Push setNotifVideo notifications_new_video? "+PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video", false)  );
            Log.d(TAG, "Push setNotifVideo notifications_new_video_hd? "+PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video_hd", false)  );




            //if (context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getBoolean("notifications_new_video", false)) {
            if (!isComment && PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video", true)) {
                showNotif = true;
                Log.d(TAG, "Push setNotifVideo showNotif Ligne 173"  );
            //} else if ( !context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getBoolean("notifications_new_video", false) && context.getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getBoolean("notifications_new_video_hd", false)) {
            } else if (!isComment &&  !PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video", true) && PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video_hd", true)) {
                showNotif = true;
                Log.d(TAG, "Push setNotifVideo showNotif Ligne 176"  );
            } else if(favoriExists) {
                showNotif = true;
                Log.d(TAG, "Push setNotifVideo showNotif Ligne 179"  );
            }

            Log.d(TAG, "Push setNotifVideo showNotif? "+showNotif  );

            final Videogson video = gson.fromJson(strjson, VideoPojo.class).getVideos().get(0);

            String artistename = "";
            if ( getArtistsName(video).size() > 0 ) {
                artistename = getArtistsName(video).get(0);
                if (getArtistsName(video).size() == 2) {
                    artistename += " et " + getArtistsName(video).get(1);
                } else if (getArtistsName(video).size() > 2) {
                    artistename += " et d'autres artistes...";
                }
            }

            String[] titrevideosplit = video.getTitre().split(Config.SEPARATOR);
            String titreavantSeparator = titrevideosplit[0];

            String str_notifsingle_title_newvideo = String.format( context.getString(R.string.notifsingle_title_newvideo) , artistename );
            String str_notifsingle_text = String.format( context.getString(R.string.notifsingle_text) , titreavantSeparator );
            String str_notifsingle_title_ticker_newvideo = String.format( context.getString(R.string.notifsingle_title_ticker_newvideo) , artistename );

            if ( isComment ) {
                str_notifsingle_title_newvideo = String.format( context.getString(R.string.notifsingle_title_newcommentvideo) , titreavantSeparator );
                str_notifsingle_title_ticker_newvideo = String.format( context.getString(R.string.notifsingle_title_ticker_newcommentvideo) , artistename );
            }

            if ( showNotif ) {
                try {
                    showSingleNotif(gson.fromJson(strjson, VideoPojo.class), str_notifsingle_title_newvideo, str_notifsingle_text, str_notifsingle_title_ticker_newvideo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            saveVideoinDb(video.getId());

        }
        else {
            Log.d(TAG, "Push setNotifVideo multivideos true"  );

            if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_video", false)) {
                showNotif = true;
            }

            Log.d(TAG, "Push setNotifVideo showNotif? "+showNotif  );

            if ( showNotif ) {
                showMultipleNotif(gson.fromJson(strjson, VideoPojo.class), context.getString(R.string.notifmultiple_text_newvideo));
            }
        }

        try {
            if (!isComment  ) {
                VazotsaraShareFunc.setBadge((multivideos ? json.getJSONArray("videos").length() : 1), context);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    void saveVideoinDb(String videoid ) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.getSingleVideo(videoid, new Callback<VideoSingleGson>() {
            @Override
            public void success(VideoSingleGson serverResponse, Response response) {
                saveBDD(serverResponse.getInfo());
            }

            @Override
            public void failure(RetrofitError error) {   }
        });
    }

    void saveBDD(Videogson serverResponse) {
        if ( serverResponse !=null ) {
            new SaveSQL().execute(serverResponse);
        }
    }
    private class SaveSQL extends AsyncTask<Videogson, String, String>
    {

        @Override
        protected String doInBackground(Videogson... params) {
            Videogson v = params[0];
            VazotsaraShareFunc.saveUniqueVideo(v,(!isComment?"listeNew":"listeCommentlast"));

            //Save in home
            TableVideosHome h = new TableVideosHome();
            h.videoid = v.getId();
            if (!isComment ) {
                h.rubrique = "listeCommentlast";
            } else {
                h.rubrique = "listeNew";
            }
            h.save();
            return null;
        }
    }

    List<String> checkFavoriVideosServer() {
        final List<String> listA = new ArrayList<String>();


        int nbfav = new Select().
                from(TableFavorivideos.class)
                .count();

        List<TableFavorivideos> fa = new Select().
                from(TableFavorivideos.class)
                .execute();

        //Get all favoriplayliste
        if ( nbfav > 0 ) {
            for(int i = 0; i < nbfav; i++ ) {
                String artisteid = String.valueOf( fa.get(i).videoid );
                listA.add(artisteid);
            }
            Log.d(TAG, "Implode : "+TextUtils.join(",", listA) );
        }

        return listA;
    }

    List<String> checkFavoriPlaylistsServer() {
        final List<String> listA = new ArrayList<String>();


        int nbfav = new Select().
                from(TableFavoriplaylists.class)
                .count();

        List<TableFavoriplaylists> fa = new Select().
                from(TableFavoriplaylists.class)
                .execute();

        //Get all favoriplayliste
        if ( nbfav > 0 ) {
            for(int i = 0; i < nbfav; i++ ) {
                String artisteid = String.valueOf( fa.get(i).playlisteid );
                listA.add(artisteid);
            }
            Log.d(TAG, "Implode : "+TextUtils.join(",", listA) );
        }

        return listA;
    }


    void showSingleNotif(final VideoPojo info, final String titleNotif, final String contentText, final String tickerText) throws IOException {

        Log.d(TAG, "Push showSingleNotif " );
        //Toast.makeText(context,"VazotsaraBC - onReceive - checkFavoriServer - showSingleNotif",Toast.LENGTH_SHORT).show();
        final Videogson video = info.getVideos().get(0);

        final String sprintf;
        //Si HD
        if (video.getContentDetailsDefinition().equals("hd") )
            sprintf = Config.THUMB_PATH_MAX;
        else
            sprintf = Config.THUMB_PATH;



        final String imagevideo = String.format(sprintf, video.getVideoytid());

        imageartiste = getImageFirstArtist(video);

        if ( imageartiste.isEmpty() ) {
            imageartiste = imagevideo;
        }



        Log.d(TAG, "Push showSingleNotif Image " + String.format(sprintf, video.getVideoytid()));


  /*        Target mtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.d(TAG, "Push showSingleNotif onBitmapLoaded" );
                sendNotifBigPictureStyle(bitmap, video, titleNotif, contentText, tickerText);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d(TAG, "Push showSingleNotif onBitmapFailed" );
                sendSimpleNotif(video, titleNotif, contentText, tickerText);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d(TAG, "Push showSingleNotif onPrepareLoad" );
                sendSimpleNotif(video, titleNotif, contentText, tickerText);
            }
        };
         Picasso.with(context)
                .load( String.format(sprintf, video.getVideoytid() ) )
                .into(mtarget);

*/
        //Afficher la notif que 2mn apres la reception = 2*60*1000
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        sendNotifBigPictureStyle(getBitmapFromURL(imageartiste), getBitmapFromURL(imagevideo), video, titleNotif, contentText, tickerText);
                    }
                }).start();
            }

        }, (2*60*1000));


            }

            List<String> getArtistsName(Videogson video) {

                List<String> artists = new ArrayList<String>();

                for( int i = 0; i < video.getPlaylisteinfo().size(); i++ ) {
                    VideoSingleGsonPlaylist artist = video.getPlaylisteinfo().get(i);
                    if ( artist.getObjetype().equals("artiste")  ) {
                        artists.add( artist.getObjetnom() );
                    }
                }

                return artists;

            }

            String getImageFirstArtist(Videogson video) {

                for( int i = 0; i < video.getPlaylisteinfo().size(); i++ ) {
                    VideoSingleGsonPlaylist artist = video.getPlaylisteinfo().get(i);
                    if ( artist.getObjetype().equals("artiste") && !artist.getImage().isEmpty() ) {
                        return String.format( Config.PATHPLAYLIST , artist.getImage() );
                    }
                }

                return "";

            }
            public static Bitmap getBitmapFromURL(String src) {
                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            void sendSimpleNotif(Videogson video, String titleNotif, String contentText, String tickerText) {
                // Setup an explicit intent for an ResultActivity to receive.
                Intent resultIntent = new Intent(context, VideoDetailActivity.class);

                resultIntent.putExtra("uid", video.getId());
                resultIntent.putExtra("videoytid", video.getVideoytid());
                resultIntent.putExtra("contentDetails_definition", video.getContentDetailsDefinition());

                // TaskStackBuilder ensures that the back button follows the recommended convention for the back key.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(VideoDetailActivity.class);

                // Adds the Intent that starts the Activity to the top of the stack.
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Builder noti = new NotificationCompat.Builder(context);
                noti.setSmallIcon(R.mipmap.ic_notif);
                noti.setAutoCancel(true);
                //noti.setContentTitle(context.getString(R.string.notifsingle_title) );
                noti.setContentTitle(titleNotif);
                //noti.setContentText(String.format(context.getString(R.string.notifsingle_text),video.getTitre()));
                noti.setContentText(String.format(contentText, video.getTitre()));
                //noti.setTicker(context.getString(R.string.notifsingle_title_ticker));
                noti.setTicker(tickerText);
                noti.setWhen(System.currentTimeMillis());
                noti.setContentIntent(resultPendingIntent);

                //if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                    noti.setVibrate(new long[]{1000, 1000, 0, 0, 0});

                //if ( PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null ) {
                if (PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null) {
                    //Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).
                    Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).
                            getString("notifications_new_message_ringtone", "content://settings/system/notification_sound"));
                    noti.setSound(uri);
                }

                mNotificationManager.notify(2, noti.build());
            }

            void sendNotifBigPictureStyle(Bitmap bmp_largeicon, Bitmap bmp_remote, Videogson video, String titleNotif, String contentText, String tickerText) {
                int numMessagesTwo = 0;

                //Toast.makeText(context,"VazotsaraBC - onReceive - checkFavoriServer - showSingleNotif - sendNotifBigPictureStyle",Toast.LENGTH_SHORT).show();


                Log.d(TAG, "android.os.Build.VERSION.SDK_INT : " + android.os.Build.VERSION.SDK_INT);
                Log.d(TAG, "Build.VERSION_CODES.JELLY_BEAN : " + Build.VERSION_CODES.JELLY_BEAN);

                Log.d(TAG, "Push sendNotifBigPictureStyle");

                // Setup an explicit intent for an ResultActivity to receive.
                Intent resultIntent = new Intent(context, VideoDetailActivity.class);

                resultIntent.putExtra("uid", video.getId());
                resultIntent.putExtra("videoytid", video.getVideoytid());
                resultIntent.putExtra("contentDetails_definition", video.getContentDetailsDefinition());

                // TaskStackBuilder ensures that the back button follows the recommended convention for the back key.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(VideoDetailActivity.class);

                // Adds the Intent that starts the Activity to the top of the stack.
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Builder noti = new NotificationCompat.Builder(context);
                noti.setSmallIcon(R.mipmap.ic_notif);
                noti.setAutoCancel(true);
                //noti.setContentTitle(context.getString(R.string.notifsingle_title) );
                noti.setContentTitle(titleNotif);
                //noti.setContentText(String.format(context.getString(R.string.notifsingle_text),video.getTitre()));
                noti.setContentText(String.format(contentText, video.getTitre()));
                //noti.setTicker(context.getString(R.string.notifsingle_title_ticker));
                noti.setTicker(tickerText);
                noti.setWhen(System.currentTimeMillis());
                noti.setContentIntent(resultPendingIntent);

                if ( bmp_largeicon != null ) {
                    noti.setLargeIcon(bmp_largeicon);
                }



                NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle();
                //s.setBigContentTitle(context.getString(R.string.notifsingle_title) );
                s.setBigContentTitle(titleNotif);
                //s.setSummaryText(String.format(context.getString(R.string.notifsingle_text), video.getTitre()));
                s.setSummaryText(String.format(contentText, video.getTitre()));
                if (bmp_remote != null) {

                    s.bigPicture(bmp_remote);
                }

                noti.setStyle(s);


                //if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                    noti.setVibrate(new long[]{1000, 1000, 0, 0, 0});

                //if ( PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null ) {
                if (PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null) {
                    //Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).
                    Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).
                            getString("notifications_new_message_ringtone", "content://settings/system/notification_sound"));
                    noti.setSound(uri);
                }

                mNotificationManager.notify(2, noti.build());
            }

            void showMultipleNotif(VideoPojo videos, String contentText) {
                //Toast.makeText(context,"VazotsaraBC - onReceive - checkFavoriServer - showMultipleNotif",Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Push showMultipleNotif");
                // Setup an explicit intent for an ResultActivity to receive.
                Intent resultIntent = new Intent(context, MainActivity.class);
                // TaskStackBuilder ensures that the back button follows the recommended convention for the back key.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(MainActivity.class);

                // Adds the Intent that starts the Activity to the top of the stack.
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder noti = new NotificationCompat.Builder(context)
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_notif)
                        .setContentTitle(context.getString(R.string.notifmultiple_title))
                                //.setContentText(String.format(context.getString(R.string.notifmultiple_text), videos.getTotal()))
                        .setContentText(String.format(contentText, videos.getTotal()))
                        .setContentIntent(resultPendingIntent);

                //if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notifications_new_message_vibrate", false))
                    noti.setVibrate(new long[]{1000, 1000, 0, 0, 0});

                //if ( PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null ) {
                if (PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound") != null) {
                    //Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound"));
                    Uri uri = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).getString("notifications_new_message_ringtone", "content://settings/system/notification_sound"));
                    noti.setSound(uri);
                }


                mNotificationManager.notify(1, noti.build());
            }


        }

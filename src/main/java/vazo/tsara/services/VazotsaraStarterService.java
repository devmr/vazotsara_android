package vazo.tsara.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;

import vazo.tsara.helpers.Config;

/**
 * Created by Miary on 06/09/2015.
 */
public class VazotsaraStarterService extends Service {
//public class VazotsaraStarterService extends IntentService {

    static String TAG = Config.TAGKEY;



    @Override
    //public void onStart(Intent intent, int startid) {
    public int onStartCommand(Intent intent, int flags, int startId) {

        // BroadCase Receiver Intent Object
        Intent alarmIntent = new Intent(getApplicationContext(), VazotsaraBCSaveBDD.class);
        // Pending Intent Object
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Alarm Manager Object
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        // 2mn
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 5000, 3 * 1000, pendingIntent);

        //1 Heure
        PendingIntent pendingIntentR = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(getApplicationContext(), VazotsaraBCToken.class), PendingIntent.FLAG_UPDATE_CURRENT);
        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 5000, 3600 * 1000, pendingIntentR);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 5000, 600 * 1000, pendingIntentR);


        //Toast.makeText(this, "VazotsaraStarterService onStartCommand", Toast.LENGTH_LONG).show();

        Log.d(TAG, "VazotsaraStarterService onStartCommand");
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}

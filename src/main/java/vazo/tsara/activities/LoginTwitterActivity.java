package vazo.tsara.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import vazo.tsara.R;

public class LoginTwitterActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_twitter);

        WebView webview = (WebView) findViewById(R.id.loginWebView);
        String url = getIntent().getStringExtra("AuthenticationURL");
        webview.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if (url.contains("twitter-callback:///") == true)
                {
                    final Uri uri = Uri.parse(url);
                    final String oauthVerifierParam = uri.getQueryParameter("oauth_verifier");
                    final Intent intent = new Intent();
                    intent.putExtra("oauth_verifier", oauthVerifierParam);
                    setResult(RESULT_OK, intent);
                    finish();

                    return true;
                }
                return false;
            }
        });
        webview.loadUrl(url);
    }

}

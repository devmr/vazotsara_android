package vazo.tsara.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;

import vazo.tsara.R;

public class BaseAppCompatActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        //Old Analytics
        //EasyTracker.getInstance(this).activityStart(this);
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Old Analytics
        // EasyTracker.getInstance(this).activityStop(this);
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}

package vazo.tsara.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.VideoSearchSortFragment;
import vazo.tsara.helpers.Config;

public class VideoSearchSortActivity extends BaseAppCompatActivity {

    Toolbar toolbar;
    String playlisteid;
    String search;
    String sortfields_label;
    String sortfields;
    String sprintf;
    static String TAG = Config.TAGKEY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_search_sort);
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }

        /**
         * Info de l'autre activity
         */
        Intent i = getIntent();

        if ( i.hasExtra("search") )
            search = i.getStringExtra("search");

        if ( i.hasExtra("sortfields") )
            sortfields = i.getStringExtra("sortfields");


        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);

        final ActionBar abar = getSupportActionBar();

        //abar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_tab));//line under the action bar
        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);

        switch (sortfields) {

            case "statistics_viewCount":
                //Nombre de vues
                sortfields_label = getString(R.string.sortopt_statistics_viewCount);
                break;
            case "statistics_likeCount":
                //Nombre de likes
                sortfields_label = getString(R.string.sortopt_statistics_likeCount);
                break;
            case "statistics_commentCount":
                //Nombre de commentaires
                sortfields_label = getString(R.string.sortopt_statistics_commentCount);
                break;
            case "contentDetails_duration":
                //Durée
                sortfields_label = getString(R.string.sortopt_contentDetails_duration);
                break;
            case "yesterday":
                //Durée
                sortfields_label = getString(R.string.sortopt_yesterday);
                break;
            default:
                sortfields_label = getString(R.string.sortopt_uploaded_date);
                break;
            case "publishedCommentLast":
                //Durée
                sortfields_label = getString(R.string.sortopt_publishedCommentLast);
                break;
        }

        if ( search !=null && !search.isEmpty() ) {
            sprintf  = getString(R.string.sprintf_search);
            textviewTitle.setText(String.format(sprintf, search));
        } else if(sortfields!=null && !sortfields.isEmpty()) {
            sprintf  = getString(R.string.sprintf_sortfields);

            textviewTitle.setText(String.format(sprintf, sortfields_label));
        } else {
            sprintf  = getString(R.string.sprintf_sortfields_search);
            textviewTitle.setText(String.format(sprintf, sortfields_label, search));
        }


        textviewTitle.setSelected(true);
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);

        //getSupportActionBar().setTitle(String.format(getString(R.string.sprintf_videos_in_playlists), titre) );



        // Replace the contents of the container with the fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.Content, new VideoSearchSortFragment().newInstance(sortfields, search));

        ft.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_video_search_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        /**
         * Retourner a la page history et non la page definie dans le manifest
         */
        if (item.getItemId() == android.R.id.home)
        {
            Log.d(TAG, "Clic HOME");
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

package vazo.tsara.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Iterator;
import java.util.Set;

import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.AddVideoFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;

public class AddVideoActivity extends BaseAppCompatActivity {

    static String TAG = Config.TAGKEY;
    ActionBar actionBar;
    Toolbar toolbar;

    String receivedAction;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);

        /*if ( !Useful.isAdmin(getApplicationContext()) ) {


            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte_close), this, ( (View) findViewById(R.id.content) ) );

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 3000);
        }*/

        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }

        /**
         * Info de l'autre activity
         */
        Intent i = getIntent();

        if ( i.hasExtra("url") )
            url = i.getStringExtra("url");


        //get the action
        receivedAction = i.getAction();
        Set<String> receivedCategory = i.getCategories();
        String receivedDataString = i.getDataString();
        String receivedScheme = i.getScheme();

        //find out what we are dealing with
        String receivedType = i.getType();

        //get the received text
        String receivedText = i.getStringExtra(Intent.EXTRA_TEXT);
        String allextras = "";
        StringBuilder str = new StringBuilder();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            while (it.hasNext()) {
                String key = it.next();
                str.append(key);
                str.append(":");
                str.append(bundle.get(key));
                str.append("\n\r");
            }
            allextras = str.toString();
        }

        Log.d(TAG,"-receivedScheme : "+receivedScheme+"\n-receivedDataString : "+receivedDataString+"\n- receivedAction : "+receivedAction+"\n- receivedType : "+receivedType+"\n- receivedText : "+receivedText+"\n- allextras \n========== "+allextras+"\n==========receivedCategory : "+receivedCategory+" - url : "+url);

        if (receivedAction!=null && url == null ) {

            if (receivedAction.equals("android.intent.action.VIEW") && !receivedDataString.isEmpty() && !Useful.isErrorUrl(receivedDataString, getApplicationContext()) )
                url = receivedDataString;
            else if(receivedAction.equals("android.intent.action.SEND") && !receivedText.isEmpty() && !Useful.isErrorUrl(receivedText, getApplicationContext()) )
                url = receivedText;

        }


        Log.d(TAG,"-receivedScheme - url : "+url);


        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        textviewTitle.setText(getString(R.string.title_activity_addvideo));
        textviewTitle.setSelected(true);
        actionBar.setCustomView(viewActionBar, params);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        String token = getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("token", "");
        Log.d(TAG, "LoginActivity token : " + token) ;


        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.Content, new AddVideoFragment().newInstance(url)).
                commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_video_search_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        /**
         * Retourner a la page history et non la page definie dans le manifest
         */
        if (item.getItemId() == android.R.id.home)
        {
            Log.d(TAG, "Clic HOME");

            if ( receivedAction==null  )
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        Log.d(TAG,"Activity AddVideoActivity onBackPressed");
        AddVideoFragment.onBackPressed();
    }
}

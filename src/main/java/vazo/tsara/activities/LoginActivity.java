package vazo.tsara.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.greenrobot.event.EventBus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.LoginFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.OptionBusEvent;

public class LoginActivity extends BaseAppCompatActivity {

    static String TAG = Config.TAGKEY;
    ActionBar actionBar;
    Toolbar toolbar;

    int resultCodeTwitter = 65539;

    String eventvar;
    String option = "";
    String videoid = "";



    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    //This object can receive a PleaseRefreshEvent
    public void onEvent(OptionBusEvent event){
        Log.d(TAG, "OptionBusEvent LoginActivity referer : " + event.getReferer());
        eventvar = event.getReferer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);



        setContentView(R.layout.activity_login);


        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }

        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        textviewTitle.setText(getString(R.string.title_activity_login));
        textviewTitle.setSelected(true);
        actionBar.setCustomView(viewActionBar, params);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        //String token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("token", "");
        String token = getSharedPreferences(Config.PREFERENCE_FILENAME, Context.MODE_PRIVATE).getString("token", "");
        Log.d(TAG, "LoginActivity token : " + token) ;


        printKeyHash();

        //EventBus.getDefault().post(new OptionBusEvent( eventvar ));

        Intent intent = getIntent();
        if(intent.hasExtra("option")){
            option = intent.getStringExtra("option");
            videoid = intent.getStringExtra("uid");
        }



        Fragment frag;


        if(videoid!=null && option!=null && !videoid.isEmpty() && !option.isEmpty()) {
            frag = new LoginFragment().newInstance(option, videoid, "af");
        } else if (option!=null &&  !option.isEmpty() ) {
            frag = new LoginFragment().newInstance(option);
        } else {
            frag = new LoginFragment().newInstance();
        }
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.Content, frag ).
                commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //LoginFragment.myOnActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "LoginActivity - onActivityResult - requestCode : " + requestCode + " - resultCode : " + resultCode );

        //twitter
        if (resultCode == RESULT_OK && requestCode == resultCodeTwitter) {

            String oauthVerifierParam = data.getStringExtra("oauth_verifier");

            Log.d(TAG, "LoginActivity - onActivityResult - requestCode : "+requestCode+" - resultCode : "+resultCode+" - oauthVerifierParam : "+oauthVerifierParam);
            if (oauthVerifierParam != null) {
                Twitter twitter;
                ConfigurationBuilder configurationBuilder;
                TwitterFactory twitterFactory;

                //Twitter
                configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.setOAuthConsumerKey(getString(R.string.consumer_key));
                configurationBuilder.setOAuthConsumerSecret(getString(R.string.consumer_secret));
                twitterFactory = new TwitterFactory(configurationBuilder.build());
                twitter = twitterFactory.getInstance();
                try {
                    twitter4j.auth.AccessToken accessToken = twitter.getOAuthAccessToken(oauthVerifierParam);
                    String name = accessToken.getScreenName();
                    long userid = accessToken.getUserId();


                } catch (TwitterException exception)
                {
                    exception.printStackTrace();
                }
            }
        }

        if (resultCode == 300) {
            setResult(300);
        }


        getSupportFragmentManager().findFragmentById(R.id.Content).onActivityResult(requestCode,resultCode,data);
    }

    private void printKeyHash(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "vazo.tsara",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }






}

package vazo.tsara.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.VideosByPlaylistsFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.PlaylisteSingle;
import vazo.tsara.models.TableFavoriplaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.Dbhandler;

public class VideosByArtistActivity extends BaseAppCompatActivity {

    Toolbar toolbar;
    String playlisteid;
    String opt, logoreference;
    String sprintf_videos_in_playlists, titre;
    static String TAG = Config.TAGKEY;

    Dbhandler db;
    String playlisteytid = "";

    Playlist p;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_by_artist);

        db = new Dbhandler(getApplicationContext());
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }

        /**
         * Info de l'autre activity
         */
        Intent i = getIntent();
        playlisteid = i.getStringExtra("playlisteid");
        titre = i.getStringExtra("titre");
        if ( i.hasExtra("opt") )
            opt = i.getStringExtra("opt");
        if ( i.hasExtra("logoreference") )
            logoreference = i.getStringExtra("logoreference");

        p = (Playlist) EventBus.getDefault().removeStickyEvent(Playlist.class);
        playlisteid = p.getId();
        titre = p.getObjetnom();



        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);

        final ActionBar abar = getSupportActionBar();

        //abar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_tab));//line under the action bar
        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);

        sprintf_videos_in_playlists = getString(R.string.sprintf_videos_de_artiste);


        textviewTitle.setText(String.format(sprintf_videos_in_playlists, titre));
        textviewTitle.setSelected(true);
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);

        //getSupportActionBar().setTitle(String.format(getString(R.string.sprintf_videos_in_playlists), titre) );

        getInfo(playlisteid);

        // Replace the contents of the container with the fragment
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.Content, new VideosByPlaylistsFragment().newInstance(playlisteid)).
                commit();




    }

   void getInfo( String idp )
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI)
                .build();
        ApiRetrofit apiRetrofit = restAdapter.create(ApiRetrofit.class);

         apiRetrofit.getSinglePlaylist(idp, new Callback<PlaylisteSingle>() {
            @Override
            public void success(PlaylisteSingle playlisteSingle, Response response) {
                Log.d(TAG, "playlisteSingle.getInfo().getPlaylisteid(): "+playlisteSingle.getInfo().getPlaylisteid() );
                if ( !playlisteSingle.getInfo().getPlaylisteid().isEmpty() )
                    playlisteytid = playlisteSingle.getInfo().getPlaylisteid();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "playlisteSingle error : "+(error.getMessage()!=null?error.getMessage():"") );
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_videos_by_artist, menu);

        String titlefavorite = getResources().getString(R.string.action_add_favorite);
        Drawable iconfavorite = getResources().getDrawable(R.drawable.ic_favorite_white_off);

        //if ( db.isArtistInFavori(Integer.parseInt(playlisteid)) ) {
        if ( !TableFavoriplaylists.isArtistInFavori( playlisteid ) ) {
            titlefavorite = getResources().getString(R.string.action_remove_favorite);
            iconfavorite = getResources().getDrawable(R.drawable.ic_favorite_white_off);
        }

        menu.findItem(R.id.action_favorite).setTitle(titlefavorite);
        menu.findItem(R.id.action_favorite).setIcon(iconfavorite);

        if ( !Useful.isAdmin(getApplicationContext())) {
            menu.removeItem(R.id.action_edit);
            menu.removeItem(R.id.action_rename);
            menu.removeItem(R.id.action_delete);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String msg_confirm;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_favorite) {
            //Si non connecte
            if (!Useful.isLoggedin(getApplicationContext())) {
                MaterialDialog dialogpl = new MaterialDialog.Builder(VideosByArtistActivity.this)
                        .title(R.string.dialog_information_title)
                        .content(R.string.error_favorite_nonconnecte)
                        .positiveText(R.string.btn_connecter)
                        .negativeText(android.R.string.cancel)
                        .negativeColor(Color.parseColor("#000000"))
                        .positiveColor(Color.parseColor("#000000"))
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                startActivity( new Intent( VideosByArtistActivity.this, LoginActivity.class) );
                            }


                            @Override
                            public void onNegative(MaterialDialog dialog) {

                            }
                        })
                        .build();
                dialogpl.show();
                return false;
            }
            //if ( !db.isArtistInFavori(Integer.parseInt(playlisteid)) ) {
            if ( !TableFavoriplaylists.isArtistInFavori( playlisteid ) ) {
                item.setTitle(getResources().getString(R.string.action_add_favorite));
                item.setIcon(getResources().getDrawable(R.drawable.ic_favorite_white));

                //db.addFavoriArtist(new FavoriPlaylistSQL(titre, Integer.parseInt(playlisteid), 0, 0, logoreference));
                TableFavoriplaylists.addArtistInFavori(playlisteid, getApplicationContext());
                msg_confirm = getString(R.string.msg_confirm_add_favorite_artist);
            } else {
                item.setTitle(getResources().getString(R.string.action_remove_favorite));
                item.setIcon( getResources().getDrawable(R.drawable.ic_favorite_white_off) );
                //db.deleteSingleFavoriArtist(Integer.parseInt(playlisteid));
                TableFavoriplaylists.deleteArtistInFavori(playlisteid, getApplicationContext());

                msg_confirm = getString(R.string.msg_confirm_remove_favorite_artist);


            }
            VazotsaraShareFunc.showSnackMsg(msg_confirm, this, ((View) findViewById(R.id.content)));
            return true;
        }

        if (id == R.id.play_list) {
            if (!playlisteytid.isEmpty() && YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(getApplicationContext()).equals(YouTubeInitializationResult.SUCCESS)) {
                startActivity(YouTubeStandalonePlayer.createPlaylistIntent(this, Config.DEVELOPER_KEY, playlisteytid, 0, 0, true, true));
            } else {
                VazotsaraShareFunc.showSnackMsg( getString(R.string.error_youtube_api), getApplicationContext(), ((View) findViewById(R.id.content)) );
            }
            return true;
        }
        /**
         * Retourner a la page history et non la page definie dans le manifest
         */
        if (item.getItemId() == android.R.id.home)
        {
            Log.d(TAG, "Clic HOME");
            finish();
        }

        //Admin
        if (id == R.id.action_rename) {
            VazotsaraShareFunc.showDialogEditPlaylist( p, this, null);
            return true;
        }

        if (id == R.id.action_edit) {
            Intent i = new Intent(this, PlaylistEditActivity.class);
            EventBus.getDefault().postSticky(p);
            startActivityForResult(i, 0);
            return true;
        }

        if (id == R.id.action_delete) {
            VazotsaraShareFunc.showDialogDeletePlaylist(this, p.getId(), 0, null, null);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}

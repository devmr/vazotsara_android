package vazo.tsara.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.greenrobot.event.EventBus;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.AboutFragment;
import vazo.tsara.fragments.ArtistsFragment;
import vazo.tsara.fragments.FavoriteFragment;
import vazo.tsara.fragments.GenreFragment;
import vazo.tsara.fragments.HomeFragment;
import vazo.tsara.fragments.PlaylistsFragment;
import vazo.tsara.fragments.RealisateurFragment;
import vazo.tsara.fragments.VideosFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.ActivityResultEvent;
import vazo.tsara.models.DataBusVideo;
import vazo.tsara.services.VazotsaraBCSaveBDD;

public class MainActivity extends BaseAppCompatActivity {

    private DrawerLayout drawerLayout;
    private View content;
    static String TAG = Config.TAGKEY;
    ActionBar actionBar;
    static TextView textviewTitle;

    private InterstitialAd mInterstitialAd;
    AdView mAdView;

    NavigationView view;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        if ( Useful.isAdmin(getApplicationContext()) )
            setTheme(R.style.MyNavigationDrawerThemeAdmin);

        //Afficher intro
        if ( !VazotsaraShareFunc.introIsShown(getApplicationContext()) ) {
            this.finish();
            Intent intent = new Intent(this, VazotsaraIntro.class);
            startActivity(intent);
        }

        String old_access = VazotsaraShareFunc.getLastAccessAppInPreferences(getApplicationContext());

        VazotsaraShareFunc.saveLastAccessAppInPreferences(String.valueOf(System.currentTimeMillis() / 1000L), getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);




        /*mAdView = (AdView) findViewById(R.id.ad_view);
        mAdView.setAdUnitId(Config.BANNER_AD_UNITID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);*/

        VazotsaraShareFunc.clearBadge(getApplicationContext());

        Log.d(TAG,"VazotsaraShareFunc.getLastAccessAppInPreferences : "+VazotsaraShareFunc.getLastAccessAppInPreferences(getApplicationContext() ));

        SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_LAST_CONNEXION);
        Date resultdate;
        if ( old_access.isEmpty() ) {
            resultdate = new Date( Long.parseLong(VazotsaraShareFunc.getLastAccessAppInPreferences(getApplicationContext()) )*1000L  );
        } else {
            resultdate = new Date( Long.parseLong( old_access )*1000L  );
        }

        //Ne pas afficher si premiere utilisation de l'appli
        if ( !old_access.isEmpty() ) {
            VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.sprintf_derniereconnexion), sdf.format(resultdate)), this, ((View) findViewById(R.id.content)));
        }
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }


        Log.d(TAG, "MainActivity Useful.isAdmin : " + Useful.isAdmin(getApplicationContext())) ;


        initToolbar();
        setupDrawerLayout();



        content = findViewById(R.id.content);

        //Afficher par defaut HomeFragment
        HomeFragment fragment = new HomeFragment();
        //HomeSQLFragment fragment = new HomeSQLFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.innercontent, fragment);
        fragmentTransaction.commit();
        setTitleActivity("Vazo::Tsara");
        //actionBar.setIcon(R.mipmap.ic_launcher);

        //setupAlarm();
    }



    void setupAlarm() {
        Log.d(TAG, "MainActivity setupAlarm");
        // BroadCase Receiver Intent Object
        Intent alarmIntent = new Intent(getApplicationContext(), VazotsaraBCSaveBDD.class);
        // Pending Intent Object
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Alarm Manager Object
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);

        // 15mn
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 5000, 10 * 1000, pendingIntent);

    }

    public static void setTitleActivity( String title ) {
        textviewTitle.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        /**
         * Retourner a la page history et non la page definie dans le manifest
         */
        if (item.getItemId() == android.R.id.home)
        {
            Log.d(TAG, "Clic HOME");
            //Snackbar.make(content, "Clic Home", Snackbar.LENGTH_LONG).show();
            drawerLayout.openDrawer(GravityCompat.START);

        }

        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);

        textviewTitle.setSelected(true);


        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(viewActionBar, params);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeButtonEnabled(true);

        }
    }
    void enableMenuAdmin(){
        if ( Useful.isAdmin(getApplicationContext()) ) {
            MenuItem item = view.getMenu().getItem(9);
            item.setVisible(false);
        } else {
            MenuItem item = view.getMenu().getItem(10);
            item.setVisible(false);

            view.getMenu().getItem(7).setVisible(false);
            view.getMenu().getItem(8).setVisible(false);
        }
    }

    void disableMenuAdmin(){
        if ( Useful.isAdmin(getApplicationContext()) ) {
            MenuItem item = view.getMenu().getItem(9);
            item.setVisible(true);
        } else {
            MenuItem item = view.getMenu().getItem(10);
            item.setVisible(true);

            view.getMenu().getItem(7).setVisible(true);
            view.getMenu().getItem(8).setVisible(true);
        }
    }

    private void setupDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        view = (NavigationView) findViewById(R.id.navigation_view);


        enableMenuAdmin();


        view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override public boolean onNavigationItemSelected(MenuItem menuItem) {



                //Checking if the item is in checked state or not, if not make it in checked state
                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()){

                    case R.id.drawer_home :

                        HomeFragment fragment = new HomeFragment();
                        //HomeSQLFragment fragment = new HomeSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.innercontent, fragment);
                        fragmentTransaction.commit();
                        //setTitle("Vazo::Tsara");
                        setTitleActivity("Vazo::Tsara");
                        //actionBar.setIcon(R.mipmap.ic_launcher);

                        return true;

                    case R.id.drawer_favorite :

                        FavoriteFragment fragmentf = new FavoriteFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionf = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionf.replace(R.id.innercontent,fragmentf);
                        fragmentTransactionf.commit();
                        //setTitle(getResources().getString(R.string.mnu_drawer_about));
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_favorite));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_videos :

                        VideosFragment fragmentv = new VideosFragment();
                        //VideosSQLFragment fragmentv = new VideosSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionv = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionv.replace(R.id.innercontent, fragmentv);
                        fragmentTransactionv.commit();
                        //setTitle(getResources().getString(R.string.mnu_drawer_videos));
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_videos));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_artists :

                        ArtistsFragment fragmentartist = new ArtistsFragment();
                        //ArtistsSQLFragment fragmentartist = new ArtistsSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionartist = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionartist.replace(R.id.innercontent,fragmentartist);
                        fragmentTransactionartist.commit();
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_artists));
                        actionBar.setIcon(null);

                        return true;



                    case R.id.drawer_playlists :

                        PlaylistsFragment fragmentp = new PlaylistsFragment();
                        //PlaylistsSQLFragment fragmentp = new PlaylistsSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionp = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionp.replace(R.id.innercontent, fragmentp);
                        fragmentTransactionp.commit();
                        //setTitle(getResources().getString(R.string.mnu_drawer_playlists));
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_playlists));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_genre :

                        GenreFragment fragmentgenre = new GenreFragment();
                       //GenreSQLFragment fragmentgenre = new GenreSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactiongenre = getSupportFragmentManager().beginTransaction();
                        fragmentTransactiongenre.replace(R.id.innercontent,fragmentgenre);
                        fragmentTransactiongenre.commit();
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_artists));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_realisateur :

                        RealisateurFragment fragmentrealisateur = new RealisateurFragment();
                        //RealisateurSQLFragment fragmentrealisateur = new RealisateurSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionrealisateur = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionrealisateur.replace(R.id.innercontent,fragmentrealisateur);
                        fragmentTransactionrealisateur.commit();
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_artists));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_about :

                        AboutFragment fragmenta = new AboutFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactiona = getSupportFragmentManager().beginTransaction();
                        fragmentTransactiona.replace(R.id.innercontent,fragmenta);
                        fragmentTransactiona.commit();
                        //setTitle(getResources().getString(R.string.mnu_drawer_about));
                        setTitleActivity(getResources().getString(R.string.mnu_drawer_about));
                        actionBar.setIcon(null);

                        return true;

                    case R.id.drawer_login :
                    case R.id.drawer_logout :
                        VazotsaraShareFunc.openLoginActivity(MainActivity.this);
                        return true;


                    case R.id.drawer_settings :

                        //Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));


                        return true;

                    case R.id.drawer_intro :

                        //Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, VazotsaraIntro.class).putExtra("fragsrc","1"));


                        return true;

                    case R.id.drawer_add_video :
                        VazotsaraShareFunc.showDialogAddVideo(MainActivity.this);
                        return true;

                    case R.id.drawer_add_playliste :
                        VazotsaraShareFunc.showDialogAddPlaylist(MainActivity.this);
                        return true;


                    /*case R.id.drawer_home_sql :

                        VideosSQLFragment fragmentsqlh = new VideosSQLFragment();
                        //HomeSQLFragment fragment = new HomeSQLFragment();
                        android.support.v4.app.FragmentTransaction fragmentTransactionsqlh = getSupportFragmentManager().beginTransaction();
                        fragmentTransactionsqlh.replace(R.id.innercontent, fragmentsqlh);
                        fragmentTransactionsqlh.commit();
                        //setTitle("Vazo::Tsara");
                        setTitleActivity("Vazo::Tsara");
                        //actionBar.setIcon(R.mipmap.ic_launcher);

                        return true;*/
                }




                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EventBus.getDefault().post(new ActivityResultEvent(requestCode, resultCode, data));
        /**
         * Retour du code apres ajout
         */
        if (resultCode == 100) {

            /**
             * Recharger cette activite en cours
             */
            Intent intent = getIntent();
            Log.d(TAG, "onActivityResult - infovideo_format : "+intent.getStringExtra("infovideo_format")+" - position : "+intent.getIntExtra("position",0));
            EventBus.getDefault().postSticky(new DataBusVideo(
                    intent.getStringExtra("infovideo_format"),
                    intent.getStringExtra("LikeCount"),
                    intent.getStringExtra("ViewCount"),
                    intent.getIntExtra("position",0)
            ) );
        }

        //Retour apres login admin
        if (resultCode == 300) {
            Log.d(TAG,"resultCode 300");
            enableMenuAdmin();
        }

    }
}

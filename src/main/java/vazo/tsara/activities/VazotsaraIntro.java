package vazo.tsara.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;

import vazo.tsara.R;
import vazo.tsara.fragments.SlideFragment;
import vazo.tsara.helpers.VazotsaraShareFunc;

/**
 * Created by rabehasy on 16/09/2015.
 */
public class VazotsaraIntro extends AppIntro {
    @Override
    public void init(Bundle bundle) {

        addSlide(SlideFragment.newInstance(R.layout.slide_intro1));
        addSlide(SlideFragment.newInstance(R.layout.slide_intro2));

        setSkipText(getString(R.string.txt_skipintro));
        setDoneText(getString(R.string.txt_done));
    }

    private void loadMainActivity(){
        //Maj Preferences
        VazotsaraShareFunc.setIntroIsShown(getApplicationContext());

        this.finish();
        Intent i = getIntent();

        if ( !i.hasExtra("fragsrc") || !i.getStringExtra("fragsrc").equals("1") ) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onSkipPressed() {
        loadMainActivity();
    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }
    public void getStarted(View v){
        loadMainActivity();
    }
}

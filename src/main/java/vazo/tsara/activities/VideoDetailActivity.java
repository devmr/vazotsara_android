package vazo.tsara.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.RemoteMediaPlayer;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.gson.Gson;
import com.inthecheesefactory.lib.fblike.widget.FBLikeView;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.fragments.VideoDetailFragment;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

public class VideoDetailActivity extends BaseAppCompatActivity {
//public class VideoDetailActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener  {

    static String TAG = "VAZOTSARA";
    ConnectionDetector detector;
    Boolean loaded = false;
    String uid;
    String videoytid = "";
    String contentDetails_definition = "";

    YouTubePlayerSupportFragment youTubePlayerFragment;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private CastDevice mSelectedDevice;
    private Cast.Listener mCastClientListener;
    private RemoteMediaPlayer mRemoteMediaPlayer;
    private boolean mWaitingForReconnect = false;
    private boolean mApplicationStarted = false;
    private boolean mVideoIsLoaded;
    private boolean mIsPlaying;
    private GoogleApiClient mApiClient;

    int position;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FBLikeView.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
        } catch(Exception  e) { }



        detector = new ConnectionDetector(this);
        setContentView(R.layout.activity_video_detail);

        Intent i = getIntent();
        uid = i.getStringExtra("uid");
        videoytid = i.getStringExtra("videoytid");
        contentDetails_definition = i.getStringExtra("contentDetails_definition");
        position = i.getIntExtra("position", 0);
        Log.d(TAG, "VideoDetailActivity - onCreate - uid : "+uid+" - videoytid :"+videoytid);

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Afficher ou non le titre de l'activity
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/

        /*if (videoytid != "") {
            final YouTubeFragment fragment = (YouTubeFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
            fragment.setVideoId(videoytid);
        }*/


        getInfo(Config.URLAPI+"/video/" + uid);

        // Begin the transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the fragment
        ft.replace(R.id.FragmentVideoDetail, new VideoDetailFragment().newInstance(uid, videoytid, contentDetails_definition, position));
        ft.commit();

        /*youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.youtube_fragment, youTubePlayerFragment).commit();*/



    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    void getInfo(String url ) {



        loaded = true;


        if ( !detector.isConnectingToInternet() )
            return;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .setConverter(new LenientGsonConverter(new Gson()))
                .build();

        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        vidapi.getSingleVideo(uid, new Callback<VideoSingleGson>() {


            @Override
            public void success(VideoSingleGson videopojo, retrofit.client.Response response) {
                Log.d(TAG, "response.getStatus() : " + response.getStatus());
                onSuccessResponse(videopojo);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });

    }

    void onSuccessResponse(final VideoSingleGson reponse) {
        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "handler.postDelayed - 500");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
                        youTubePlayerFragment.initialize(Config.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {

                            @Override
                            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                                if (!b) {

                                    //youTubePlayer.loadVideo(reponse.getInfo().getVideoytid());
                                    youTubePlayer.cueVideo(reponse.getInfo().getVideoytid());
                                }
                            }

                            @Override
                            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                                Log.d(TAG, "YouTubePlayer onInitializationFailure");

                            }
                        });


                    }
                });


            }
        }, 800);*/




    }
    public void onErrorResponse(RetrofitError volleyError) {
        String errorMessage = (volleyError.getMessage()==null)?"":volleyError.getMessage();
        Log.d(TAG, "onErrorResponse - Error:"+errorMessage);
        loaded = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu( menu );
        getMenuInflater().inflate(R.menu.menu_video_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }


}

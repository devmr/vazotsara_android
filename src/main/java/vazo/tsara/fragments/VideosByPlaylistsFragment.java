package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.VideoAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosByPlaylistsFragment extends Fragment {

    View rootView;
    View emptyView;

    String playlisteid;
    TextView textview;

    String sortfields = "uploadeddate_original";
    int currentPage = 0;
    int nb_video_par_page = 20;

    int nb_total, max_pages;

    String url = Config.URLAPI;

    boolean loaded;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = Config.TAGKEY;

    RecyclerView recyclerView;
    SparseItemRemoveAnimator mSparseAnimator;


    View viewstub_recyclerview_error;
    View viewstub_recyclerview_empty;
    View viewstub_recyclerview_content;
    View viewstub_recyclerview_loading;

    ViewStub vs_loading_default;
    ViewStub vs_content_errormessage;
    ViewStub vs_content_failureerrormessage;

    List<video> videoList = new ArrayList<video>();

    VideoAdapter adapter;


    public VideosByPlaylistsFragment() {
        // Required empty public constructor
    }

    public VideosByPlaylistsFragment newInstance(String playlisteid) {
        VideosByPlaylistsFragment frag = new VideosByPlaylistsFragment();
        Bundle args = new Bundle();
        args.putString("playlisteid", playlisteid);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        playlisteid = getArguments().getString("playlisteid", "");
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoFragment - onResume");
        adapter = new VideoAdapter( getActivity() , videoList, R.layout.adapter_videoitem, R.layout.adapter_videoloading, nb_video_par_page  );
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                if ( getActivity() != null ) {

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            recyclerView.setAdapter(adapter);
                        }
                    });

                }

            }
        });
        thread.start();
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "VideoFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Si connecte  Internet et adapter vide - Rafraichir
            if ( connecte.isConnectingToInternet() && adapter.getItemCount() == 0 ) {

                setLoadingView();
                getInfo(url, 0);
            }

            //Si non connecte  Internet et isLoading()
            if ( connecte.isConnectingToInternet() && adapter.getItemCount() > 0 ) {
                Log.d(TAG, "mRecycler.isLoadingMore - currentpage : " + currentPage);
                getInfo(url, currentPage);
                adapter.setDeconnecte(false);
            }

            //Si non connecte  Internet et isLoading()
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount() > 0 ) {
                Log.d(TAG, "mRecycler.isLoadingMore - NC currentpage : " + currentPage);
                showSnackError(getActivity().getString(R.string.error_non_connecte));
                adapter.setDeconnecte(true);
            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount() == 0 ) {
                Log.d(TAG, "Non connecte + adapter vide");
                setErrorView(getActivity().getString(R.string.error_non_connecte));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_videos_by_artist, container, false);
        return rootView;
    }
    void updateAdapter(){
        videoList.clear();
        adapter.notifyDataSetChanged();
        getInfo(url, 0);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewstub_recyclerview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_recyclerview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_recyclerview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_recyclerview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();


        recyclerView = (RecyclerView) viewstub_recyclerview_content.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.d(TAG, "STEP - recyclerView.addOnScrollListener - page " + current_page+" - max_pages: "+max_pages);
                if ( max_pages>0 && current_page<max_pages ) {
                    Log.d(TAG, "STEP - recyclerView.addOnScrollListener - Page suivante " + current_page+" - max_pages: "+max_pages);
                    getInfo(url, current_page);
                } else {
                    Log.d(TAG, "STEP - recyclerView.addOnScrollListener - Bas de page " + current_page+" - max_pages: "+max_pages);
                }
                currentPage = current_page;
            }
        });


    }
    public void setLoadingView()
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.VISIBLE);
    }
    public void setErrorView(final String error)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.VISIBLE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_error.findViewById(R.id.textView);
        Button btn_refresh = (Button) viewstub_recyclerview_error.findViewById(R.id.btn_refresh);

        textv.setText(error);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connecte.isConnectingToInternet()) {
                    showSnackError(error);
                } else {
                    setLoadingView();
                    getInfo(url, 0);
                }
            }
        });
    }
    void showSnackError(String error)
    {
        Snackbar snack = Snackbar.make(getActivity().findViewById(R.id.content), error, Snackbar.LENGTH_LONG);
        snack.show();

        //Color en blanc le text
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }
    public void setEmptyView(String textempty)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.VISIBLE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_recyclerview_content.setVisibility(View.VISIBLE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);
    }
    void getInfo(String url, int page )
    {
        if ( !connecte.isConnectingToInternet()  ) {
            if ( page > 0 ) {
                Log.d(TAG, "Loadmore NON CONNECTE");
                /*SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(getString(R.string.error_non_connecte))
                );*/

            }
            return;
        } else if ( page > 0 ) {

        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



        Log.d(TAG, "URL retrofit 608 : " + url + "/videos?limit=20&page=" + page + "&q=&sortfields=" + sortfields + "&plid=" + playlisteid);
        vidapi.getVideosInPlaylists(nb_video_par_page, page, playlisteid, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {

                setSuccessInfo(videoPojo);

            }

            @Override
            public void failure(RetrofitError error) {
                setFailureInfo(error);
            }
        });

    }
    void setSuccessInfo(VideoPojo videoPojo){

        nb_total = videoPojo.getTotal();
        max_pages = videoPojo.getShown();

        adapter.setServerListSize(nb_total);

        if (videoPojo.getVideos().size() > 0) {

            Log.d(TAG, "STEP - getInfo - L571 - success " + videoPojo.getVideos().get(0).getTitre());
            setListView();

            addToList(videoPojo.getVideos());
            adapter.notifyDataSetChanged();
            loaded = true;
        }
    }

    void setFailureInfo(RetrofitError error){
        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
        Log.d(TAG, "STEP - getInfo - L586 - fail " + this.getClass().getSimpleName());
        if (adapter.getItemCount() == 0)
            setErrorView(errorMessage);
        else {

            if ( getActivity() != null ) {
                Snackbar snack = Snackbar.make(getActivity().findViewById(R.id.content), errorMessage, Snackbar.LENGTH_LONG);
                snack.show();

                //Color en blanc le text
                View view = snack.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
            }
        }
    }
    void addToList(List<Videogson> videos) {
        for (int count = 0; count < videos.size(); count++) {
            videoList.add( fillItem(videos.get(count)) );
        }
    }

    video fillItem(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId( videogson.getId() );
        v.setVideoytid( videogson.getVideoytid()  );
        v.setStatistics_viewCount(videogson.getStatisticsViewCount() );
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount() );
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        return v;

    }

}

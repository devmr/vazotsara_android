package vazo.tsara.fragments;


import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.models.VideoStatPojo;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailTabStatFragment extends Fragment  implements
        DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {

    View rootView;
    static String TAG = Config.TAGKEY;
    String uid;

    private LineChart mChartViewCount;
    private LineChart mChartLikeCount;
    private LineChart mChartDislikeCount;
    private LineChart mChartCommentCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }
    public static VideoDetailTabStatFragment newInstance(String uid) {
        VideoDetailTabStatFragment frag = new VideoDetailTabStatFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        frag.setArguments(args);
        return frag;
    }

    public VideoDetailTabStatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail_tab_stat, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ( (Button) rootView.findViewById(R.id.date_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        VideoDetailTabStatFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        mChartViewCount = (LineChart) rootView.findViewById(R.id.chartViewCount);

        mChartViewCount.setNoDataText( getString(R.string.empty_stat_view) );
        Paint pv = mChartViewCount.getPaint(Chart.PAINT_INFO);
        pv.setColor(Color.parseColor("#999999"));

        setData(mChartViewCount, "viewCount");

        mChartLikeCount = (LineChart) rootView.findViewById(R.id.chartLikeCount);
        mChartLikeCount.setNoDataText( getString(R.string.empty_stat_like) );
        Paint pl = mChartLikeCount.getPaint(Chart.PAINT_INFO);
        pl.setColor(Color.parseColor("#999999"));

        setData(mChartLikeCount, "likeCount");

        mChartDislikeCount = (LineChart) rootView.findViewById(R.id.chartDislikeCount);
        mChartDislikeCount.setNoDataText( getString(R.string.empty_stat_dislike) );
        Paint pdl = mChartDislikeCount.getPaint(Chart.PAINT_INFO);
        pdl.setColor(Color.parseColor("#dddddd"));

        setData(mChartDislikeCount, "dislikeCount");

        mChartCommentCount = (LineChart) rootView.findViewById(R.id.chartCommentCount);
        mChartCommentCount.setNoDataText( getString(R.string.empty_stat_comment) );
        Paint pc = mChartCommentCount.getPaint(Chart.PAINT_INFO);
        pc.setColor(Color.parseColor("#999999"));

        setData(mChartCommentCount, "CommentCount");
    }

    private void setData(final LineChart mChart, final String affichageoption) {

        final ArrayList<String> xVals = new ArrayList<String>();
        final ArrayList<Entry> yVals = new ArrayList<Entry>();

        Log.d(TAG, "URL Stat : " + Config.URLAPI + "/statvideo/" + uid);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI+"/statvideo/"+uid)
                .setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.getStatVideo(uid, new Callback<VideoStatPojo>() {


            @Override
            public void success(VideoStatPojo videopojo, retrofit.client.Response response) {
                // count = videopojo.getVideosStatPojos().size();

                float val = 0;

                for (int i = 0; i < videopojo.getVideosStatPojos().size(); i++) {
                    xVals.add(Useful.usingDateFormatter(Long.parseLong(videopojo.getVideosStatPojos().get(i).getTstamp()) * 1000, "dd-M-yyyy") + "");

                    switch (affichageoption) {
                        case "viewCount":
                            val = Float.parseFloat(videopojo.getVideosStatPojos().get(i).getViewCount());
                            break;
                        case "likeCount":
                            val = Float.parseFloat(videopojo.getVideosStatPojos().get(i).getLikeCount());
                            break;
                        case "dislikeCount":
                            val = Float.parseFloat(videopojo.getVideosStatPojos().get(i).getDislikeCount());
                            break;
                        case "CommentCount":
                            val = Float.parseFloat(videopojo.getVideosStatPojos().get(i).getCommentCount());
                            break;
                    }


                    yVals.add(new Entry(val, i));
                    //Log.d("ANDYROHYHIDY","Stat - i: "+i+" - val : "+val);
                }

                // create a dataset and give it a type
                String label = "";
                switch (affichageoption) {
                    case "viewCount":
                        label = getString(R.string.lbl_stat_vue_du)+" ";
                        break;
                    case "likeCount":
                        label = getString(R.string.lbl_stat_like_du)+" ";
                        break;
                    case "dislikeCount":
                        label = getString(R.string.lbl_stat_dislike_du)+" ";
                        break;
                    case "CommentCount":
                        label = getString(R.string.lbl_stat_comment_du);
                        break;
                }
                if (videopojo.getStatGeneralinfo() != null) {
                    label += Useful.usingDateFormatter(Long.parseLong(videopojo.getStatGeneralinfo().getDateDebut()) * 1000, "dd-M-yyyy") + " au " + Useful.usingDateFormatter(Long.parseLong(videopojo.getStatGeneralinfo().getDateFin()) * 1000, "dd-M-yyyy");
                }

                LineDataSet set1 = new LineDataSet(yVals, label);
                // set1.setFillAlpha(110);
                // set1.setFillColor(Color.RED);

                // set the line to be drawn like this "- - - - - -"
                set1.enableDashedLine(10f, 0f, 0f);
                if (getActivity() != null) {
                    set1.setColor(getActivity().getResources().getColor(R.color.ColorPrimary));
                    set1.setCircleColor(getActivity().getResources().getColor(R.color.ColorPrimary));
                }

                set1.setLineWidth(2f);
                set1.setCircleSize(3f);
                set1.setDrawCircleHole(false);
                set1.setValueTextSize(9f);
                set1.setFillAlpha(65);
                set1.setFillColor(Color.BLACK);
                // set1.setDrawFilled(true);
                // set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
                // Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

                ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
                dataSets.add(set1); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(xVals, dataSets);

                // set data
                mChart.setData(data);

                // animate calls invalidate()...
                mChart.animateX(2500);


            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {

    }
}

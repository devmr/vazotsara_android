package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.FavoriVideoAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.FavoriVideoSQL;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.TableFavoriplaylists;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.Dbhandler;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabfavoriteVideosFragment extends Fragment {




    String url = Config.URLAPI;

    Boolean isLoading = true;


    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int nb_video_show = 20;

    View rootView;
    View emptyView;

    static View viewstub_listview_empty;
    static View viewstub_listview_content;
    static View viewstub_listview_loading;
    static View viewstub_listview_error;

    RecyclerView recyclerView;



    Button btnNewVideoAll, btnPopularVideoYesterday, btnPopularVideoAll;

    private SliderLayout mDemoSlider;



    List<video> videoList = new ArrayList<video>();



    boolean loaded;

    //PlaylistsAdapter adapter;
    FavoriVideoAdapter adapter;

    Parcelable state;

    Dbhandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        connecte = new ConnectionDetector(getActivity());

        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);

        super.onResume();
        Log.d(TAG, "TabhomeVideosFragment - onResume - loaded : "+loaded);

        if ( loaded )
            setListView();

        adapter = new FavoriVideoAdapter( getActivity() , videoList, R.layout.adapter_videoitem_favorite, R.layout.adapter_videoloading, recyclerView, TabfavoriteVideosFragment.this  );
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        recyclerView.setAdapter(adapter);

                    }
                });
            }
        });

        //adapter.setServerListSize(db.getFavoriVideoCount());
        adapter.setServerListSize(TableFavorivideos.getFavoriVideoCount());
        thread.start();


    }
    void showDetailIntent(video vid)
    {
        Log.d(TAG, "VideoAdapter - showDetailIntent - uid : " + vid.getId() + " - videoytid :" + vid.getVideoytid());
        Intent VDetailActivity = new Intent( getActivity(), VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId());
        VDetailActivity.putExtra("videoytid", vid.getVideoytid());
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition());
        getActivity().startActivityForResult(VDetailActivity, 100);

    }
    @Override
    public void onPause() {

        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "TabhomeVideosFragment - onPause");
    }



    public TabfavoriteVideosFragment() {
        // Required empty public constructor
    }

    public static TabfavoriteVideosFragment newInstance() {
        TabfavoriteVideosFragment frag = new TabfavoriteVideosFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tabfavorite_videos, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
    }



    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setErrorView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        viewstub_listview_error.setVisibility(View.VISIBLE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(textempty);

        Button btnv = (Button)  viewstub_listview_error.findViewById(R.id.btn_refresh);
        btnv.setText( getString(R.string.title_activity_login) );
        btnv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.openLoginActivity(getActivity(), "tabfavorite");
            }
        });
    }



    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(state != null) {
            Log.d(TAG, "TabhomeVideosFragment - Restore listview state..onViewCreated");

        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();



        recyclerView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        if (!Useful.isLoggedin(getActivity()) ) {
            setErrorView( getString(R.string.error_favorite_nonconnecte) );
            return;
        }

        /**
         * Asynctask pour charger les data
         */
        //db = new Dbhandler(getActivity().getApplicationContext());
        new loadFavoriVideoSQL().execute();


    }




    void addToListServer(List<Videogson> videos, List<video> videoListadd) {
        int added = 0;
        for (int count = 0; count < videos.size(); count++) {
                videoListadd.add(fillItemServer(videos.get(count)));
        }

    }

    void addToList(List<FavoriVideoSQL> favorivideolist, List<video> videoListadd) {
        for (int count = 0; count < favorivideolist.size(); count++) {
            videoListadd.add( fillItemDB(favorivideolist.get(count)) );
        }
    }

    video fillItemServer(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId(videogson.getId());

        v.setFormat(videogson.getContentDetailsDefinition());
        v.setThumburl(videogson.getThumburl());


        return v;

    }
    video fillItemDB(FavoriVideoSQL videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId(String.valueOf(videogson.getVideoid()));
        v.setFormat(videogson.getVideoformat());
        v.setThumburl( String.format(Config.THUMB_PATH, videogson.getVideoyt() ) );


        return v;

    }

    private class loadFavoriVideoSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");
        }
        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG,"loadFavoriVideoSQL  doInBackground - "+ TableFavorivideos.getFavoriVideoCount());


            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {



                    //List<FavoriVideoSQL> favorivideolist = db.getAllFavoriVideo();
                    List<FavoriVideoSQL> favorivideolist = TableFavorivideos.getAllFavoriVideo();

                    //Log.d(TAG,"db.getAllFavoriVideo().size(): "+db.getAllFavoriVideo().size());
                    Log.d(TAG,"db.getAllFavoriVideo().size(): "+TableFavorivideos.getAllFavoriVideo().size());

                    //if ( db.getAllFavoriVideo().size() == 0 ) {
                    if ( TableFavorivideos.getAllFavoriVideo().size() == 0 ) {
                        setEmptyView( getString(R.string.empty_favori_video));
                        return;
                    }
                    setListView();
                    videoList.clear();
                    //addToList(db.getAllFavoriVideo(), videoList);
                    addToList(TableFavorivideos.getAllFavoriVideo(), videoList);

                    adapter.notifyDataSetChanged();



                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute" );
        }
    }

    void getServerInfo() {
        Log.d(TAG,"getLoggedinID: "+Useful.getLoggedinID(getActivity()));

        if ( !Useful.getLoggedinID(getActivity()).isEmpty() ) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


            vidapi.getFavoritevideobyuser( Useful.getLoggedinID(getActivity()), new Callback<VideoPojo>() {

                @Override
                public void success(VideoPojo videoPojo, Response response) {
                    Log.d(TAG,"getServerInfo success " );
                    setSuccessInfo(videoPojo);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG,"getServerInfo failure " );
                }
            });

            vidapi.getFavoriteplaylistebyuser(Useful.getLoggedinID(getActivity()), new Callback<Playlists>() {
                @Override
                public void success(Playlists playlists, Response response) {
                    setSuccessFavoritePlayliste(playlists);
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });


        }
    }

    void setSuccessFavoritePlayliste(Playlists playlists) {

        if ( playlists.getPlaylists().size()>0 ) {
            saveBDDFavoriPlaylists(playlists.getPlaylists());
        }

    }
    void setSuccessInfo(VideoPojo videoPojo){
        if (videoPojo.getVideos().size() > 0) {

            saveBDDFavoriVideo(videoPojo.getVideos());

            setListView();

            addToListServer(videoPojo.getVideos(), videoList);
            adapter.notifyDataSetChanged();
            adapter.setServerListSize(videoPojo.getTotal());
            loaded = true;
        }
    }

    void saveBDDFavoriPlaylists(List<Playlist> playlist){
        Log.d(TAG, "saveBDDFavoriPlaylists - " + TableFavoriplaylists.getCountArtistInFavoriIsHidden(0) );
        new saveBDDFavoriPlaylists().execute(playlist);
    }

    void saveBDDFavoriVideo(List<Videogson> vgadded){
        Log.d(TAG, "saveBDDFavoriVideo - " + TableFavorivideos.getFavoriVideoCount());
        new saveBDDFavoriVideo().execute(vgadded);
    }
    private class saveBDDFavoriPlaylists extends AsyncTask<List<Playlist>, String, String>
    {

        @Override
        protected String doInBackground(List<Playlist>... params) {
            if ( params[0].size() > 0 ) {
                Log.d(TAG,"saveBDDFavoriPlaylists - "+params[0].size());
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInPlaylists(params[0]);

                    ActiveAndroid.beginTransaction();
                    try {
                        for (int i = 0; i < params[0].size(); i++) {


                            if ( !TableFavoriplaylists.isArtistInFavori(String.valueOf(params[0].get(i).getId())) ) {
                                Log.d(TAG,"saveBDDFavoriPlaylists - save "+params[0].get(i).getId());
                                TableFavoriplaylists fav = new TableFavoriplaylists();
                                fav.playlisteid = String.valueOf(params[0].get(i).getId());
                                fav.isdeleted = 0;
                                fav.ishidden = 0;
                                fav.save();
                            } else {
                                Log.d(TAG,"saveBDDFavoriPlaylists - (Exists) don't save "+params[0].get(i).getId());
                            }

                        }
                        ActiveAndroid.setTransactionSuccessful();
                    }
                    finally {
                        ActiveAndroid.endTransaction();
                    }
                }
            }
            return null;
        }
    }
    private class saveBDDFavoriVideo extends AsyncTask<List<Videogson>, String, String>
    {

        @Override
        protected String doInBackground(List<Videogson>... params) {

            if ( params[0].size() > 0 ) {
                Log.d(TAG,"saveBDDFavoriVideo - "+params[0].size());
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], "" );

                    ActiveAndroid.beginTransaction();
                    try {
                        for (int i = 0; i < params[0].size(); i++) {


                            if ( !TableFavorivideos.isVideoidInFavoris(String.valueOf(params[0].get(i).getId())) ) {
                                Log.d(TAG,"saveBDDFavoriVideo - save "+params[0].get(i).getId());
                                TableFavorivideos fav = new TableFavorivideos();
                                fav.videoid = String.valueOf(params[0].get(i).getId());
                                fav.isdeleted = 0;
                                fav.ishidden = 0;
                                fav.save();
                            } else {
                                Log.d(TAG,"saveBDDFavoriVideo - (Exists) don't save "+params[0].get(i).getId());
                            }

                        }
                        ActiveAndroid.setTransactionSuccessful();
                    }
                    finally {
                        ActiveAndroid.endTransaction();
                    }
                }
            }
            return null;
        }
    }

    BroadcastConnexion receiver;
    ConnectionDetector connecte;
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Si connecte  Internet et connecte en tant qu'user
            if ( connecte.isConnectingToInternet() && Useful.isLoggedin(getActivity())) {
                getServerInfo();
            }
        }
    }



}

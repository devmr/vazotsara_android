package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.MainActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.ArtistsSearchSuggestionsAdapter;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.PlaylistsRecyclerAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.SearchPlaylist;
import vazo.tsara.models.SingleResultSearchArtiste;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsFragment extends Fragment {


    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";
    String sortfields = "objetnom";
    String sortfieldsascdesc = "asc";
    int currentPage = 0;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    SearchView sv;

    //RecyclerView recyclerView;
    //ListView listView;
    RecyclerView listView;
    SparseItemRemoveAnimator mSparseAnimator;

    private SliderLayout mDemoSlider;



    List<Playlist> playList = new ArrayList<Playlist>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    PlaylistsRecyclerAdapter badapter;
    ArtistsSearchSuggestionsAdapter artistsSSAdapter;

    Parcelable state;

    String[] columns = new String[]{"_id", "NOM", "IMAGE", "PLAYLISTEID" };

    FloatingActionButton fab;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int lastFirstVisiblePosition;
    LinearLayoutManager linearLayoutManager;

    int rowCountInDb = 0;
    int pagerserver = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {




        inflater.inflate(R.menu.menu_fragment_artists, menu);
        final MenuItem item = menu.findItem(R.id.search);
        sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);

        sv.setSuggestionsAdapter(artistsSSAdapter);
        sv.setIconifiedByDefault(true);
        sv.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String objetnom = cursor.getString(1);
                String id = cursor.getString(3);
                String image = cursor.getString(2);

                sv.setQuery(objetnom, false);

                showListeVideoByPlaylists( new Playlist(id,"","",image,"","","",objetnom,"","","","","",""));
                //setSearch(objetnom);

                sv.clearFocus();


                //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();


                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String objetnom = cursor.getString(1);
                String id = cursor.getString(3);
                String image = cursor.getString(2);
                //sv.setQuery(objetnom, false);
                sv.clearFocus();

                showListeVideoByPlaylists( new Playlist(id,"","",image,"","","",objetnom,"","","","","","") );

                //getInfo(url, 0, objetnom );
                //setSearch(objetnom);
                //Toast.makeText(getActivity(), "titre: " + titre+" ("+id+")", Toast.LENGTH_SHORT).show();
                return false;
            }
        });






        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                if (query.length() > 2) {
                    loadData(query);
                    //setSearch(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                if (query.length() > 2) {
                    loadData(query);
                    //setSearch(query);
                }
                return false;
            }
        });




        //Clic sur Croix dans searchview
        ImageView closeButton = (ImageView) sv.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetSearch();
            }
        });

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    Log.d(TAG,"MenuItemCompat.setOnActionExpandListener onMenuItemActionExpand");
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    Log.d(TAG,"MenuItemCompat.setOnActionExpandListener onMenuItemActionCollapse");
                    resetSearch();
                    return true;
                }
            });
        }



        super.onCreateOptionsMenu(menu, inflater);

    }
    void resetSearch(){
        setSearch("");
        sv.setQuery("", false);
        sv.setIconified(false);
    }
    void setSearch(String s){
        playList.clear();
        getInfo(url,0,s);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sort:

                showDialogList( );
                break;
        }
        return true;
    }
    void showDialogList( ) {

        if ( !connecte.isConnectingToInternet()  ) {
            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte), getActivity());
            return;
        }



        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_sort)
                .items(R.array.sort_list_artistes)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        switch (which) {
                            case 0:
                                //Date de publication
                                sortfields = "created";
                                break;
                            case 1:
                                //Nombre de vues
                                sortfields = "nb_videos";
                                break;
                            case 2:
                                //Nombre de vues
                                sortfields = "objetnom";
                                break;

                        }

                        /*videoList.clear();
                        adapter.notifyDataSetChanged();
                        setLoadingInterface();
                        getInfo(url, 0, search);*/

                        //openSearchSortActivity();

                        String sortfields_label = "";


                        switch (sortfields) {

                            case "nb_videos":
                                //Nombre de vues
                                sortfields_label = getString(R.string.sortopt_nb_videos);
                                sortfieldsascdesc = "desc";
                                break;
                            case "created":
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_uploaded_date);
                                sortfieldsascdesc = "desc";
                                break;
                            default:
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_default);
                                sortfieldsascdesc = "asc";
                                break;

                        }

                        MainActivity.setTitleActivity(sortfields_label);

                        updateAdapter(sortfields,sortfieldsascdesc);

                    }
                })
                .show();
    }
    void updateAdapter(String orderby, String asc){
        playList.clear();
        badapter.notifyDataSetChanged();
        getInfo(url, 0, orderby, asc);
    }
    private void loadData(String searchText) {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getSearchPlaylist(searchText, "a", new Callback<SearchPlaylist>() {
            @Override
            public void success(SearchPlaylist searchResult, retrofit.client.Response response) {
                //setSuccessInfo(videoPojo);
                //    Log.d(TAG,"searchResult.getResults() : "+searchResult.getResults().size());
                if (searchResult != null) {
                    MatrixCursor matrixCursor = convertToCursor(searchResult.getResults());
                    artistsSSAdapter.changeCursor(matrixCursor);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d(TAG, "searchResult.getResults() error : " + error.getMessage());

            }
        });

    }

    private MatrixCursor convertToCursor(  List<SingleResultSearchArtiste> SingleResultSearchArtiste) {
        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;
        for (SingleResultSearchArtiste rowArtiste : SingleResultSearchArtiste) {
            String[] temp = new String[4];
            i = i + 1;
            temp[0] = Integer.toString(i);
            temp[1] = rowArtiste.getObjetnom();
            temp[2] = rowArtiste.getImage();
            temp[3] = rowArtiste.getId();


            Log.d(TAG,"temp[2] : "+temp[2]);
            cursor.addRow(temp);
        }



        return cursor;
    }
    public void onStart()
    {
        super.onStart();
        From from = new Select()
                .from(TablePlaylists.class)
                ;
        from.where("objetype = 'artiste'");

        rowCountInDb = from.count();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "TabhomeArtistsFragment - onResume");

        if ( loaded )
            setListView();

        //badapter = new PlaylistsBaseAdapter( getActivity() , playList, R.layout.adapter_artiste_item , 0, ArtistsFragment.this );
        badapter = new PlaylistsRecyclerAdapter( getActivity() , playList, R.layout.adapter_artiste_item , R.layout.adapter_videoloading );
        listView.setAdapter(badapter);

        linearLayoutManager.scrollToPosition(lastFirstVisiblePosition);
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "ArtistsFragment - onPause");

        lastFirstVisiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {



            Log.d(TAG, "ArtistsFragment - BroadcastConnexion - onReceive : "+badapter.getCount());

            if ( connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {
                Log.d(TAG, "ArtistsFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                setLoadingView();

                if (!sv.getQuery().toString().isEmpty())
                    getInfo(url,0,(sv.getQuery().toString().isEmpty()?"":sv.getQuery()));
                else
                    getInfo(url,0,sortfields,sortfieldsascdesc);
            }

            if ( connecte.isConnectingToInternet() && badapter.getItemCount() > 0 ) {
                mSwipeRefreshLayout.setRefreshing(true);
                Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte true - adapter.getItemCount() : " + badapter.getItemCount() + " - currentPage : " + currentPage);
                setListView();

                if ( rowCountInDb > 0 ) {
                    getSqlInfo(currentPage);
                }
                badapter.setDeconnecte(false);
            }

            //Si non connecte  Internet et isLoading()

            if ( !connecte.isConnectingToInternet()  ) {
                Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte false");

                if ( rowCountInDb > 0 ) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte false - rowCountInDb: "+rowCountInDb );
                    setListView();
                    getSqlInfo(currentPage);

                } else {
                    setEmptyView(getString(R.string.error_non_connecte));
                    badapter.setDeconnecte(true);
                }
            }


        }
    }

    void getSqlInfo(int page) {
        pagerserver = page;
        if ( page > playList.size() / Config.LIMIT_GENERAL  ) {
            pagerserver = playList.size() / Config.LIMIT_GENERAL;
        }

        Log.d(TAG, "ArtistsFragment - getSqlInfo - pageserver : "+pagerserver );
        new loadSqlInfo().execute( String.valueOf( page ) );
    }
    private class loadSqlInfo extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onPreExecute");
        }


        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground - sortfields " + sortfields);


            From from = new Select()
                    .from(TablePlaylists.class)
                    ;

            from.where("objetype = 'artiste'");


            Log.d(TAG, "ArtistsFragment STEP from.toSql(): " + from.toSql() );


            rowCountInDb = from.count();

            int offset = Integer.parseInt(params[0]) * Config.LIMIT_GENERAL;

            from = from.orderBy( sortfields )
                    .limit(Config.LIMIT_GENERAL)
                    .offset(offset);

            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground - rowCountInDb: "+rowCountInDb+" - SQL "+from.toSql());

            List<TablePlaylists> videos = from.execute();

            for (int count = 0; count < videos.size(); count++) {

                if (  count == 0 && offset == 0  ) {
                    Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground 1st Row : "+videos.get(count).objetnom+" - sortfields : "+sortfields );
                }

                if ( !badapter.idInAdapter(videos.get(count).serverid  ) ) {
                    playList.add( fillItemDb(videos.get(count)));

                    Log.d(TAG, "ArtistsFragment STEP loadSqlInfo badapter - Nom : " + videos.get(count).objetnom + " - ID : " + videos.get(count).serverid + " ajouté dans adapter via BDD à la position "+count);
                } else {
                    Log.d(TAG,"ArtistsFragment STEP loadSqlInfo badapter - Nom : "+videos.get(count).objetnom+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
                }
            }


            badapter.setServerListSize(rowCountInDb);
            badapter.setLoadedSize(playList.size());
            publishProgress("1");

            return params[0];
        }

        @Override
        public void onProgressUpdate(String... params) {
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onProgressUpdate");
            if ( !isAdded() ) {
                Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onProgressUpdate isAdded() false");
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    badapter.notifyDataSetChanged();
                    loaded = true;
                }
            });

        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(final String args) {
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onPostExecute");


        }
    }
    public ArtistsFragment() {
        // Required empty public constructor
    }

    public static ArtistsFragment newInstance() {
        ArtistsFragment frag = new ArtistsFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_playlists, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        artistsSSAdapter = new ArtistsSearchSuggestionsAdapter( getActivity(), R.layout.adapter_videosearchsuggestion_item, null, columns,null, -1000);


        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );


        listView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        listView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                current_page = pagerserver+1;

                Log.d(TAG, "ArtistsFragment STEP - recyclerView.addOnScrollListener - page " + current_page+" - playList.size() : "+playList.size());

                if ( !connecte.isConnectingToInternet()  ) {

                    if ( rowCountInDb > 0 ) {
                        getSqlInfo(currentPage);
                    }

                } else {
                    if (!sv.getQuery().toString().isEmpty()) {
                        getInfo(url, current_page, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                    } else {
                        getInfo(url, current_page, sortfields, sortfieldsascdesc);
                    }
                }


                currentPage = current_page;
            }
        });

        fab = (FloatingActionButton) viewstub_listview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabArtistsFragment(getActivity());
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_listview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastPlaylists();

            }
        });



    }

    void getLastPlaylists(){
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        final String lastplaylist = (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()):"");

        if ( !lastplaylist.isEmpty() && Integer.parseInt(lastplaylist)>0 ) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListPlaylisteBeforeId(lastplaylist, new Callback<Playlists>() {

                @Override
                public void success(Playlists serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if (serverinfo.getPlaylists().size()>0 && !serverinfo.getPlaylists().get(0).getId().equals(lastplaylist)) {
                        //Refresh
                        addToList(serverinfo.getPlaylists());
                        badapter.notifyDataSetChanged();

                        //Enregistrer dernière playlist dans Preferences
                        String lastid = serverinfo.getPlaylists().get(0).getId();
                        if ( VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());

                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.msg_confirm_playlists_added), serverinfo.getPlaylists().size()), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Error " + error);
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                }
            });


        }
        // Stop refresh animation dans tous les cas
        mSwipeRefreshLayout.setRefreshing(false);

    }

    void setTextView(){
        //badapter.getChildAt(position)
    }
    public static void updateItemText(String text, int i) {

    }
    void showListeVideoByPlaylists( Playlist p )
    {
        Intent VideoSearchActivity = new Intent(getActivity(), VideosByArtistActivity.class);
        EventBus.getDefault().postSticky(p);
        /*VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre );*/
        getActivity().startActivityForResult(VideoSearchActivity, 100);
    }

    void getInfo(String url, final int page, String orderby, String orderbascdesc ) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("artiste", 20, page, orderby, orderbascdesc, new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, retrofit.client.Response response) {
                Log.d(TAG, "ArtistsFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                if (plist.getPlaylists().size() == 0) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                setListView();
                addToList(plist.getPlaylists());

                //Enregistrer dernière playlist dans Preferences
                if (page <= 0 && isAdded()) {
                    String lastid = plist.getPlaylists().get(0).getId();
                    if (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()) != null)
                        VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());
                }
                badapter.setServerListSize(plist.getTotal());
                //adapter.notifyDataSetChanged();
                badapter.notifyDataSetChanged();
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "ArtistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });
    }
    void getInfo(String url, int page, CharSequence search) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        Log.d(TAG,"ArtistsFragment - getInfo - page : "+page+" - sv.getQuery() : "+sv.getQuery()+" - search : "+search);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("artiste", 20, page, search.toString(), new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, retrofit.client.Response response) {
                Log.d(TAG, "ArtistsFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                if ( plist.getPlaylists().size() == 0 ) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                setListView();
                addToList(plist.getPlaylists());
                badapter.setServerListSize(plist.getTotal());
                //adapter.notifyDataSetChanged();
                badapter.notifyDataSetChanged();
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "ArtistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

    }

    void addToList(List<Playlist> videos) {
        saveBDD(videos);
        for (int count = 0; count < videos.size(); count++) {
            if ( !badapter.idInAdapter(videos.get(count).getId()  ) ) {
                playList.add(fillItem(videos.get(count)));
            }
        }
    }
    void saveBDD(List<Playlist> liste){
        new saveBddCache().execute(liste);
    }
    private class saveBddCache extends AsyncTask<List<Playlist>, String, String>
    {

        @Override
        protected String doInBackground(List<Playlist>... params) {

            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInPlaylists(params[0]);
                    Log.d(TAG, "ArtistsFragment STEP saveBddCache doInBackground sortfields:" + sortfields );
                }
            }
            return null;
        }
    }
    Playlist fillItemDb(TablePlaylists item) {
        Playlist v = new Playlist();
        v.setNom(item.nom);
        v.setObjetnom(item.objetnom);
        v.setId(item.serverid);
        v.setNbVideos(item.nbVideos);
        v.setImage(item.image);
        v.setObjetype(item.objetype);
        v.setStatus(item.status);
        v.setDescriptionYt(item.descriptionYt);
        return v;
    }
    Playlist fillItem(Playlist videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getNom());
        v.setObjetnom(videogson.getObjetnom());
        v.setId(videogson.getId());
        v.setNbVideos(videogson.getNbVideos());
        v.setImage(videogson.getImage());
        v.setObjetype(videogson.getObjetype());
        v.setStatus(videogson.getStatus());
        v.setDescriptionYt(videogson.getDescriptionYt());
        return v;
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 20;
        private int currentPage = 0;
        private int previousTotal = 20;
        private boolean loading = true;

        public EndlessScrollListener() {
        }
        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            totalItemCount = playList.size();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                //new LoadGigsTask().execute(currentPage + 1);
                //getInfo(url,currentPage + 1);
                onLoadMore(currentPage , totalItemCount);
                loading = true;
            }
        }

        public abstract void onLoadMore(int page, int totalItemsCount);


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }
}

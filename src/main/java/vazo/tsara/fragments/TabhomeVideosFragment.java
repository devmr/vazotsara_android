package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Instant;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideoSearchSortActivity;
import vazo.tsara.adapters.VideosBaseAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.TwoWayAdapterView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabhomeVideosFragment extends Fragment {




    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int nb_video_show = 20;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    //RecyclerView recyclerView;
    TwoWayGridView grd_newvideo, grd_popularvideoyesterday, grd_popularvideoall, grd_recently_video_commented;
    SparseItemRemoveAnimator mSparseAnimator;

    Button btnNewVideoAll, btnPopularVideoYesterday, btnPopularVideoAll, btnHeaderRecentlyVideoComment;

    private SliderLayout mDemoSlider;



    List<video> videoList = new ArrayList<video>();
    List<video> videoListPeriode = new ArrayList<video>();
    List<video> videoListAllPopular = new ArrayList<video>();
    List<video> videoCommentedRecently = new ArrayList<video>();
    List<video> videoAll = new ArrayList<video>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    VideosBaseAdapter badapter;
    VideosBaseAdapter yesterday_adapter;
    VideosBaseAdapter allpopular_adapter;
    VideosBaseAdapter recently_video_commented_adapter;

    Parcelable state;

    ProgressWheel progress_wheel_grd_newvideo, progress_wheel_grd_popularvideoyesterday, progress_wheel_grd_popularvideoall, progress_wheel_grd_recently_video_commented;

    FloatingActionButton fab;

    ClipboardManager myClipboard;

    RelativeLayout bloc_clipboard;
    TextView askclip;

    Button btn_askclip_no;

    RestAdapter restAdapter ;

    AdView adView;

    int rowCountInDbNew = 0;
    int rowCountInDbLastcommented = 0;
    int rowCountInDbMustViewHier = 0;
    int rowCountInDbMustViewAll = 0;
    int rowCountInDb = 0;

    String datestr;
    DateTime dtstr;
    DateTime dt;

    boolean needUpdate = false;
    boolean onreceivepass = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

        //get timestamplastsave
        boolean notempty = TableLastcheck.TableIsNotEmpty();

        dt = DateTime.now();

        if ( notempty ) {
            datestr = TableLastcheck.getTimestamplastsave() ;
            Log.d(TAG,"getTimestamplastsave(): "+datestr);
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            dtstr = formatter.parseDateTime(datestr);
            Log.d(TAG,"getTimestamplastsave - Days.daysBetween(dtstr, dt).getDays(): "+Days.daysBetween(dtstr, dt).getDays());
            if ( Days.daysBetween(dtstr, dt).getDays() > 0 ) {
                needUpdate = true;
            }
        }

        if( !notempty ) {
            needUpdate = true;
        }

    }

    From getfromNew(){
        return new Select().from(TableVideos.class).where("listeNew = 1");
    }
    From getfromlisteCommentDate(){
        return new Select().from(TableVideos.class).where("listeCommentDate = 1");
    }
    From getfromlisteViewHier(){
        return new Select().from(TableVideos.class).where("listeViewHier = 1");
    }
    From getfromlisteView(){
        return new Select().from(TableVideos.class).where("listeView = 1");
    }
    public void onStart()
    {
        super.onStart();

        rowCountInDbNew = getfromNew().count();
        rowCountInDbLastcommented = getfromlisteCommentDate().count();
        rowCountInDbMustViewHier = getfromlisteViewHier().count();
        rowCountInDbMustViewAll = getfromlisteView().count();

        rowCountInDb = (rowCountInDbNew+rowCountInDbLastcommented+rowCountInDbMustViewHier+rowCountInDbMustViewAll);



        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "TabhomeVideosFragment - onResume - loaded : "+loaded+" - onreceivepass?"+onreceivepass);

        if ( !onreceivepass ) {
            checkFirst();
            return;
        }

        if ( loaded )
            setListView();






    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        //state = listView.onSaveInstanceState();
        super.onPause();
        Log.d(TAG, "TabhomeVideosFragment - onPause");
    }
    void checkFirst(){
        Log.d(TAG, "TabhomeVideosFragment - checkFirst" );
        boolean objectExists = false;
        onreceivepass = true;

        DateTime dt = DateTime.now();
        Instant now = Instant.now();

        String datestr = "2015-11-13 12:57:30";
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dtstr = formatter.parseDateTime(datestr);




        int anneedujour = dtstr.getYear();
        int moisdujour = dtstr.getMonthOfYear();
        int datedujour = dtstr.getDayOfMonth();

        int heure = dtstr.getHourOfDay();
        int minute = dtstr.getMinuteOfHour();
        int secondes = dtstr.getSecondOfMinute();

        Instant winter = new DateTime( anneedujour , moisdujour ,datedujour,heure,minute,secondes,0).toInstant();
        Interval skiTime = new Interval(winter, now.toInstant() );

        Instant loversDay = new DateTime(2013,2,14,0,0,0,0).toInstant();

        Log.d(TAG,"JODA now.toInstant(): "+now.toInstant() );
        Log.d(TAG,"JODA dt.toDateTime(): "+dt.toDateTime() );
        Log.d(TAG,"JODA dt.toDateTime().getMillis(): "+dt.toDateTime().getMillis() );
        Log.d(TAG,"JODA now.getMillis(): "+now.getMillis() );
        Log.d(TAG,"JODA skiTime: "+skiTime );
        Log.d(TAG,"JODA skiTime.contains(loversDay): "+skiTime.contains(loversDay) );
        Log.d(TAG,"JODA skiTime.toPeriod(): "+skiTime.toPeriod() );
        Log.d(TAG,"JODA Nb jours entre "+dtstr.toDateTime()+" et "+dt.toDateTime()+" : "+ Days.daysBetween(dtstr, dt).getDays() );
        Log.d(TAG,"JODA formatter.print(now) "+formatter.print(now)  );



        if ( connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {
              setLoadingView();

                if ( needUpdate ) {
                    Log.d(TAG, "TabhomeVideosFragment - needUpdate : "+needUpdate );
                    saveTimestamplastdate();
                }

            Runnable runnable = new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "TabhomeVideosFragment - checkFirst getInfo after 3000 - badapter.getCount() : "+(badapter.getCount()-1) );
                            getInfo(url, 0);
                        }
                    }, 3000);
                }
            };

            getActivity().runOnUiThread(runnable);


        }

        if ( connecte.isConnectingToInternet() && badapter.getCount() > 0) {

            if ( badapter.getCount()-1==0) {
                needUpdate = true;
            }
            if ( yesterday_adapter.getCount()-1==0) {
                needUpdate = true;
            }
            if ( allpopular_adapter.getCount()-1==0) {
                needUpdate = true;
            }
            if ( recently_video_commented_adapter.getCount()-1==0) {
                needUpdate = true;
            }

            if ( rowCountInDb>0) {
                setLoadingView();
                getInfoBDD();
            }

            //if ( needUpdate ) {
            Runnable runnable = new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "TabhomeVideosFragment - checkFirst getInfo after 3000 - badapter.getCount() : "+(badapter.getCount()-1) );
                            getInfo(url, 0);
                            //saveTimestamplastdate();
                        }
                    }, 3000);
                }
            };

            getActivity().runOnUiThread(runnable);
            //}
        }





        //Si non connecte  Internet et adapter vide - Rafraichir
        if ( !connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {

            if ( rowCountInDb>0) {
                Log.d(TAG, "TabhomeVideosFragment - checkFirst !connecte.isConnectingToInternet() - badapter.getCount() : "+(badapter.getCount()-1)+" - rowCountInDb: "+rowCountInDb );
                setListView();
                getInfoBDD();
            } else {
                setErrorView(getActivity().getString(R.string.error_non_connecte));
            }

        }
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {



            Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive" );

            if ( !onreceivepass ) {
                checkFirst();
            }


        }
    }

    private void saveTimestamplastdate() {
        Log.d(TAG,"saveTstamp ");
        new saveTstamp().execute();
    }

    void getInfoBDD(){
        new loadSqlNew().execute();
        new loadSqlLastcommented().execute();
        new loadSqlMustViewHier().execute();
        new loadSqlMustView().execute();
    }


    public TabhomeVideosFragment() {
        // Required empty public constructor
    }

    public static TabhomeVideosFragment newInstance() {
        TabhomeVideosFragment frag = new TabhomeVideosFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tabhome_videos, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(state != null) {
            Log.d(TAG, "TabhomeVideosFragment - Restore listview state..onViewCreated");

        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);




        myClipboard = (ClipboardManager) getActivity().getSystemService(getActivity().CLIPBOARD_SERVICE);

        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        adView = (AdView) viewstub_listview_content.findViewById(R.id.banner);
        adView.loadAd(new AdRequest.Builder().build());

        btnNewVideoAll = (Button) viewstub_listview_content.findViewById(R.id.btnNewVideoAll);
        btnPopularVideoYesterday = (Button) viewstub_listview_content.findViewById(R.id.btnPopularVideoYesterday);
        btnPopularVideoAll = (Button) viewstub_listview_content.findViewById(R.id.btnPopularVideoAll);
        btnHeaderRecentlyVideoComment = (Button) viewstub_listview_content.findViewById(R.id.btnHeaderRecentlyVideoComment);

        //listView = (ListView) viewstub_listview_content.findViewById(R.id.list);
        grd_newvideo = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_newvideo);
        badapter = new VideosBaseAdapter( getActivity() , videoList, R.layout.adapter_tabhome_videositem, "uploadeddate_original"  );
        grd_newvideo.setAdapter(badapter);

        grd_popularvideoyesterday = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_popularvideoyesterday);
        yesterday_adapter = new VideosBaseAdapter( getActivity() , videoListPeriode, R.layout.adapter_tabhome_videositem, "yesterday"  );
        grd_popularvideoyesterday.setAdapter(yesterday_adapter);

        grd_popularvideoall = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_popularvideoall);
        allpopular_adapter = new VideosBaseAdapter( getActivity() , videoListAllPopular, R.layout.adapter_tabhome_videositem, "statistics_viewCount"  );
        grd_popularvideoall.setAdapter(allpopular_adapter);

        grd_recently_video_commented = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_recently_video_commented);
        recently_video_commented_adapter = new VideosBaseAdapter( getActivity() , videoCommentedRecently, R.layout.adapter_tabhome_videositem, "publishedCommentLast"  );
        grd_recently_video_commented.setAdapter(recently_video_commented_adapter);

        progress_wheel_grd_newvideo = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_newvideo);
        progress_wheel_grd_popularvideoall = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_popularvideoall);
        progress_wheel_grd_popularvideoyesterday = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_popularvideoyesterday);
        progress_wheel_grd_recently_video_commented = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_recently_video_commented);

        fab = (FloatingActionButton) viewstub_listview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        //Clic sur Float action button (admin uniquement)
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabHome(getActivity());
            }
        });


        btnNewVideoAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "uploadeddate_original");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnPopularVideoYesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "yesterday");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnPopularVideoAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "statistics_viewCount");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnHeaderRecentlyVideoComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "publishedCommentLast");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        grd_newvideo.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid);
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition);
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_popularvideoall.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition ;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid );
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition );
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_popularvideoyesterday.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition ;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid );
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition );
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_recently_video_commented.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid);
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition);
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

    }

    void saveBDDNew(List<Videogson> vgadded ){
        new saveBddNew().execute(vgadded);
    }
    void saveBDDCommentLast(List<Videogson> vgadded ){
        new saveBddCommentLast().execute(vgadded);
    }
    void saveBDDstatistics_viewCount_hier(List<Videogson> vgadded ){
        new saveBddstatistics_viewCount_hier().execute(vgadded);
    }
    void saveBDDstatistics_viewCount(List<Videogson> vgadded ){
        new saveBddstatistics_viewCount().execute(vgadded);
    }

    void getInfo(String url, int page) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }



        Log.d(TAG, "TabhomeVideosFragment - getInfo - page : " + page);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getListVideos(nb_video_show, "uploadeddate_original", page , new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos uploadeddate_original success" );
                if (videoPojo.getVideos().size() > 0) {
                    Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos uploadeddate_original success - size:"+videoPojo.getVideos().size()  );

                    saveBDDNew(videoPojo.getVideos());
                    addToListServer(videoPojo.getVideos(), videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos uploadeddate_original failure" );
            }
        });

        vidapi.getListVideos(nb_video_show, "statistics_viewCount_hier", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos statistics_viewCount_hier success - size:"+videoPojo.getVideos().size() );
                saveBDDstatistics_viewCount_hier(videoPojo.getVideos());
                addToListServer(videoPojo.getVideos(), videoListPeriode, yesterday_adapter, grd_popularvideoyesterday, progress_wheel_grd_popularvideoyesterday);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos statistics_viewCount_hier failure" );
            }
        });

        vidapi.getListVideos(10, "statistics_viewCount", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos statistics_viewCount success - size:"+videoPojo.getVideos().size() );
                if (videoPojo.getVideos().size() > 0) {
                    saveBDDstatistics_viewCount(videoPojo.getVideos());
                    addToListServer(videoPojo.getVideos(), videoListAllPopular, allpopular_adapter, grd_popularvideoall, progress_wheel_grd_popularvideoall);
                }
        }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos statistics_viewCount failure" );
            }
        });

        vidapi.getListVideos(10, "publishedCommentLast", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos publishedCommentLast success - size:"+videoPojo.getVideos().size() );
                if (videoPojo.getVideos().size() > 0) {

                    saveBDDCommentLast(videoPojo.getVideos());
                    addToListServer(videoPojo.getVideos(), videoCommentedRecently, recently_video_commented_adapter, grd_recently_video_commented, progress_wheel_grd_recently_video_commented);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "TabhomeVideosFragment - getInfo - getListVideos publishedCommentLast failure" );
            }
        });



    }
    void fillCacheAdapter( String cacheName, final List<video> videoListadd,final VideosBaseAdapter adapter, final TwoWayGridView twrdv, final ProgressWheel prgwheel) {

        Type resultType = new TypeToken<List<Videogson>>() {}.getType();
        Log.d(TAG, "Reservoir fillCacheAdapter");



        Reservoir.getAsync( cacheName, resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG,"Reservoir fillCacheAdapter getAsync onSuccess");
                //success
                Log.d(TAG,"Cache getTitre : "+videos.get(0).getTitre() );

                if ( isAdded() )
                    setListView();

                if ( videos.size() <= 0  ) {
                    twrdv.setVisibility(View.GONE);
                    prgwheel.setVisibility(View.VISIBLE);
                }

                for (int count = 0; count < videos.size(); count++) {
                    if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                        videoListadd.add( fillItemServer(videos.get(count)) );
                    }
                }
                adapter.notifyDataSetChanged();
                loaded = true;

            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG,"Reservoir fillCacheAdapter getAsync onFailure "+e.getMessage() );
                twrdv.setVisibility(View.GONE);
                prgwheel.setVisibility(View.VISIBLE);
            }
        });
    }
    void addToListServer(List<Videogson> videos, List<video> videoListadd, VideosBaseAdapter adapter, TwoWayGridView twrdv, ProgressWheel prgwheel) {

        if ( isAdded() )
            setListView();



        if ( videos.size() <= 0  ) {
            twrdv.setVisibility(View.GONE);
            prgwheel.setVisibility(View.VISIBLE);
        }

        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                videoListadd.add( fillItemServer(videos.get(count)) );
                //videoAll.add( fillItemServer(videos.get(count)) );
            }
        }
        adapter.notifyDataSetChanged();
        loaded = true;
    }


    video fillItemServer(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId( videogson.getId() );
        v.setVideoytid( videogson.getVideoytid()  );
        v.setStatistics_viewCount(videogson.getStatisticsViewCount() );
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount() );
        v.setStatistics_dislikeCount(videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        v.setContentDetails_definition(videogson.getContentDetailsDefinition());

        return v;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_fragment_home, menu);

        super.onCreateOptionsMenu(menu, inflater);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.login:
                VazotsaraShareFunc.openLoginActivity(getActivity());
                break;
            /*case R.id.logout:
                setLogout();
                break;*/
        }
        return true;
    }


    private class loadSqlNew extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {

            From res = getfromNew().limit(20).orderBy("endtime_uploaded DESC,publishedAt DESC");

            final List<TableVideos> videos = res.execute();
            if ( isAdded() ) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addToListServer(VazotsaraShareFunc.convertModelToGson(videos), videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                    }
                });
            } else {
                Log.d(TAG,"Fragment isAdded false loadSqlNew");
            }
                    return null;

                }
            }


            private class loadSqlLastcommented extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {

            From res = getfromlisteCommentDate().limit(20).orderBy("publishedCommentLast DESC");

            final List<TableVideos> videos = res.execute();
            if ( isAdded() ) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addToListServer(VazotsaraShareFunc.convertModelToGson(videos), videoCommentedRecently, recently_video_commented_adapter, grd_recently_video_commented, progress_wheel_grd_recently_video_commented);
                    }
                });
            } else {
                Log.d(TAG,"Fragment isAdded false loadSqlLastcommented");
            }
            return null;
                }
            }
            private class loadSqlMustViewHier extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            From res = getfromlisteViewHier().limit(20).orderBy("statistics_viewCount_hier DESC");

            final List<TableVideos> videos = res.execute();
            if ( isAdded() ) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addToListServer(VazotsaraShareFunc.convertModelToGson(videos), videoListPeriode, yesterday_adapter, grd_popularvideoyesterday, progress_wheel_grd_popularvideoyesterday);
                    }
                });
            } else {
                Log.d(TAG,"Fragment isAdded false loadSqlMustViewHier");
            }
            return null;
                }
            }
            private class loadSqlMustView extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            From res = getfromlisteView().limit(20).orderBy("statistics_viewCount DESC");

            final List<TableVideos> videos = res.execute();
            if ( isAdded() ) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        addToListServer(VazotsaraShareFunc.convertModelToGson(videos), videoListAllPopular, allpopular_adapter, grd_popularvideoall, progress_wheel_grd_popularvideoall);
                    }
                });
            }  else {
                Log.d(TAG,"Fragment isAdded false loadSqlMustView");
            }

            return null;
        }
    }

    private class saveBddNew extends AsyncTask<List<Videogson>,String,String>
    {
        @Override
        protected String doInBackground(List<Videogson>... params) {
            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], "publishedAt" );
                    Log.d(TAG, "TabhomeVideosFragment STEP saveBddNew publishedAt " );
                }
            }
            return null;
        }
    }

    private class saveBddCommentLast extends AsyncTask<List<Videogson>,String,String>
    {
        @Override
        protected String doInBackground(List<Videogson>... params) {
            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], "publishedCommentLast" );
                    Log.d(TAG, "TabhomeVideosFragment STEP saveBddNew publishedCommentLast " );
                }
            }
            return null;
        }
    }

    private class saveBddstatistics_viewCount_hier extends AsyncTask<List<Videogson>,String,String>
    {
        @Override
        protected String doInBackground(List<Videogson>... params) {
            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], "statistics_viewCount_hier" );
                    Log.d(TAG, "TabhomeVideosFragment STEP saveBddNew statistics_viewCount_hier " );
                }
            }
            return null;
        }
    }

    private class saveBddstatistics_viewCount extends AsyncTask<List<Videogson>,String,String>
    {
        @Override
        protected String doInBackground(List<Videogson>... params) {
            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], "statistics_viewCount" );
                    Log.d(TAG, "TabhomeVideosFragment STEP saveBddNew statistics_viewCount " );
                }
            }
            return null;
        }
    }

    private class saveTstamp extends AsyncTask<String,String,String>{
        @Override
        protected String doInBackground(String... params) {

            Instant now = Instant.now();
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            String date = formatter.print(now);

            TableLastcheck.setTimestamplastsave(date);
            Log.d(TAG,"TabhomeVideosFragment saveTstamp doInBackground - date: "+date);

            return null;
        }
    }
}

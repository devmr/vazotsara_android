package vazo.tsara.fragments.SQL;



import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoEditActivity;
import vazo.tsara.adapters.SQL.VideodetailPagerSQLAdapter;
import vazo.tsara.adapters.VideodetailPagerAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailSQLFragment extends Fragment {


    View rootView;
    static String TAG = "VAZOTSARA";
    String uid;
    String videoytid;
    String contentDetails_definition;
    String sprintfformat;
    TabLayout tabs;
    ImageButton mnu_overflow;

    int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");
        videoytid = getArguments().getString("videoytid", "");
        contentDetails_definition = getArguments().getString("contentDetails_definition", "");
        position = getArguments().getInt("position");
    }

    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }

    public static VideoDetailSQLFragment newInstance(String uid, String videoytid, String contentDetails_definition, int position) {
        VideoDetailSQLFragment frag = new VideoDetailSQLFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        args.putString("videoytid", videoytid);
        args.putString("contentDetails_definition", contentDetails_definition);
        args.putInt("position", position);
        frag.setArguments(args);
        return frag;
    }

    public VideoDetailSQLFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(TAG, "VideoDetailFragment - onActivityCreated - uid : " + uid);

        ImageView thumbnail = (ImageView) rootView.findViewById(R.id.thumbnail);
        if ( videoytid != "" ) {

            if ( contentDetails_definition.equals("hd") )
                sprintfformat = Config.THUMB_PATH_MAX;
            else
                sprintfformat = Config.THUMB_PATH;

            Log.d(TAG,"String.format(sprintfformat, videoytid): "+String.format(sprintfformat, videoytid));
            Picasso.
                    with(getActivity()).
                    load(String.format(sprintfformat, videoytid))
                    .into(  thumbnail );
        }

        RelativeLayout blocthumbnail = (RelativeLayout) rootView.findViewById(R.id.blocthumbnail);
        blocthumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (YouTubeIntents.canResolvePlayVideoIntentWithOptions(getActivity())) {
                    //Opens in the StandAlonePlayer but in "Light box" mode
                    startActivity(YouTubeStandalonePlayer.createVideoIntent(getActivity(),
                            Config.DEVELOPER_KEY, videoytid, 0, true, true));
                }
            }
        });

        tabs = (TabLayout) rootView.findViewById(R.id.tabs);
        //PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.pager);

        final VideodetailPagerSQLAdapter adapter = new VideodetailPagerSQLAdapter(getChildFragmentManager(), uid, position, getActivity());
        pager.setAdapter(adapter);

        tabs.setupWithViewPager(pager);

        //Forcer la largeur des onglets equivalents
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setTabMode(TabLayout.MODE_FIXED);

        /*tabs.setViewPager(pager);

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( titlevideo+" - "+String.valueOf(adapter.getPageTitle(position)) ) );
            }

            @Override
            public void onPageSelected(int position) {
                //EventBus.getDefault().post(new BusUpdateActionBarTitleEvent( ((MyNavigationDrawer) getActivity() ).getSupportActionBar().getTitle()+" - "+adapter.getPageTitle(position) ) );
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/

        mnu_overflow = (ImageButton) rootView.findViewById(R.id.mnu_overflow);
        mnu_overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu( getActivity()  , view) {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {




                        switch (item.getItemId()) {
                            case R.id.mnu_admin_edit:
                                startActivity( new Intent(  getActivity(), VideoEditActivity.class ).putExtra("uid", uid ) );
                                return true;

                            case R.id.mnu_admin_delete:
                                VazotsaraShareFunc.showDialogDeleteVideo(uid, getActivity());
                                return true;


                            default:
                                return super.onMenuItemSelected(menu, item);
                        }
                    }
                };

                int menuRes;
                menuRes = R.menu.menu_popupvideo_detail;

                popupMenu.inflate(menuRes);
                popupMenu.getMenu().removeItem(R.id.album_overflow_delete);
                popupMenu.show();
            }
        });

        if ( Useful.isAdmin(getActivity())) {
            mnu_overflow.setVisibility(View.VISIBLE);
        }
    }






}

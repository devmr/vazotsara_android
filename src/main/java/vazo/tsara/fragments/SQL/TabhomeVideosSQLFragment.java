package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideoSearchSortActivity;
import vazo.tsara.adapters.VideosBaseAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.TwoWayAdapterView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabhomeVideosSQLFragment extends Fragment {




    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int nb_video_show = 20;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    //RecyclerView recyclerView;
    TwoWayGridView grd_newvideo, grd_popularvideoyesterday, grd_popularvideoall, grd_recently_video_commented;
    SparseItemRemoveAnimator mSparseAnimator;

    Button btnNewVideoAll, btnPopularVideoYesterday, btnPopularVideoAll, btnHeaderRecentlyVideoComment;

    private SliderLayout mDemoSlider;



    List<video> videoList = new ArrayList<video>();
    List<video> videoListPeriode = new ArrayList<video>();
    List<video> videoListAllPopular = new ArrayList<video>();
    List<video> videoCommentedRecently = new ArrayList<video>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    VideosBaseAdapter badapter;
    VideosBaseAdapter yesterday_adapter;
    VideosBaseAdapter allpopular_adapter;
    VideosBaseAdapter recently_video_commented_adapter;

    Parcelable state;

    ProgressWheel progress_wheel_grd_newvideo, progress_wheel_grd_popularvideoyesterday, progress_wheel_grd_popularvideoall, progress_wheel_grd_recently_video_commented;

    FloatingActionButton fab;

    ClipboardManager myClipboard;

    RelativeLayout bloc_clipboard;
    TextView askclip;

    Button btn_askclip_no;

    RestAdapter restAdapter ;

    AdView adView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "TabhomeVideosFragment - onResume - loaded : "+loaded);

        if ( loaded )
            setListView();

        //Si non connecte  Internet et isLoading()
        //if ( !connecte.isConnectingToInternet() ) {
            Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() > 0");
            //fillCacheAdapter("videoListCache", videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
            //fillCacheAdapter("videoListPeriodeCache", videoListPeriode, yesterday_adapter, grd_popularvideoyesterday, progress_wheel_grd_popularvideoyesterday);
            //fillCacheAdapter("videoListAllPopularCache",videoListAllPopular, allpopular_adapter, grd_popularvideoall, progress_wheel_grd_popularvideoall);
            //fillCacheAdapter("videoCommentedRecentlyCache", videoCommentedRecently, recently_video_commented_adapter, grd_recently_video_commented, progress_wheel_grd_recently_video_commented);
        //}


    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        //state = listView.onSaveInstanceState();
        super.onPause();
        Log.d(TAG, "TabhomeVideosFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {


            boolean objectExists = false;



            Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive : "+badapter.getCount()+" - objectExists ? "+objectExists);

            if ( connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {
                Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                setLoadingView();
                getInfo(url, 0);
            }





            //Si non connecte  Internet et adapter vide - Rafraichir
            /*if ( !connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {
                Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() == 0");

                setErrorView(getActivity().getString(R.string.error_non_connecte));

            }*/
        }
    }


    public TabhomeVideosSQLFragment() {
        // Required empty public constructor
    }

    public static TabhomeVideosSQLFragment newInstance() {
        TabhomeVideosSQLFragment frag = new TabhomeVideosSQLFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tabhome_videos, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(state != null) {
            Log.d(TAG, "TabhomeVideosFragment - Restore listview state..onViewCreated");

        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);




        myClipboard = (ClipboardManager) getActivity().getSystemService(getActivity().CLIPBOARD_SERVICE);

        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        adView = (AdView) viewstub_listview_content.findViewById(R.id.banner);
        adView.loadAd(new AdRequest.Builder().build());

        btnNewVideoAll = (Button) viewstub_listview_content.findViewById(R.id.btnNewVideoAll);
        btnPopularVideoYesterday = (Button) viewstub_listview_content.findViewById(R.id.btnPopularVideoYesterday);
        btnPopularVideoAll = (Button) viewstub_listview_content.findViewById(R.id.btnPopularVideoAll);
        btnHeaderRecentlyVideoComment = (Button) viewstub_listview_content.findViewById(R.id.btnHeaderRecentlyVideoComment);

        //listView = (ListView) viewstub_listview_content.findViewById(R.id.list);
        grd_newvideo = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_newvideo);
        badapter = new VideosBaseAdapter( getActivity() , videoList, R.layout.adapter_tabhome_videositem, "uploadeddate_original"  );
        grd_newvideo.setAdapter(badapter);

        grd_popularvideoyesterday = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_popularvideoyesterday);
        yesterday_adapter = new VideosBaseAdapter( getActivity() , videoListPeriode, R.layout.adapter_tabhome_videositem, "yesterday"  );
        grd_popularvideoyesterday.setAdapter(yesterday_adapter);

        grd_popularvideoall = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_popularvideoall);
        allpopular_adapter = new VideosBaseAdapter( getActivity() , videoListAllPopular, R.layout.adapter_tabhome_videositem, "statistics_viewCount"  );
        grd_popularvideoall.setAdapter(allpopular_adapter);

        grd_recently_video_commented = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_recently_video_commented);
        recently_video_commented_adapter = new VideosBaseAdapter( getActivity() , videoCommentedRecently, R.layout.adapter_tabhome_videositem, "publishedCommentLast"  );
        grd_recently_video_commented.setAdapter(recently_video_commented_adapter);

        progress_wheel_grd_newvideo = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_newvideo);
        progress_wheel_grd_popularvideoall = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_popularvideoall);
        progress_wheel_grd_popularvideoyesterday = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_popularvideoyesterday);
        progress_wheel_grd_recently_video_commented = (ProgressWheel) viewstub_listview_content.findViewById(R.id.progress_wheel_grd_recently_video_commented);

        fab = (FloatingActionButton) viewstub_listview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        //Clic sur Float action button (admin uniquement)
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabHome(getActivity());
            }
        });


        btnNewVideoAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "uploadeddate_original");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnPopularVideoYesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "yesterday");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnPopularVideoAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "statistics_viewCount");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        btnHeaderRecentlyVideoComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open activity
                Intent VideoSearchSortActivity = new Intent(getActivity(), VideoSearchSortActivity.class);
                VideoSearchSortActivity.putExtra("sortfields", "publishedCommentLast");
                VideoSearchSortActivity.putExtra("search", "" );
                getActivity().startActivityForResult(VideoSearchSortActivity, 100);
            }
        });

        grd_newvideo.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid);
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition);
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_popularvideoall.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition ;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid );
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition );
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_popularvideoyesterday.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition ;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid );
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition );
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

        grd_recently_video_commented.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        String uid, videoytid, contentDetails_definition;
                        uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                        videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                        contentDetails_definition = ((TextView) view.findViewById(R.id.contentDetails_definition)).getText().toString();


                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                        VDetailActivity.putExtra("uid", uid);
                        VDetailActivity.putExtra("videoytid", videoytid);
                        VDetailActivity.putExtra("contentDetails_definition", contentDetails_definition);
                        getActivity().startActivityForResult(VDetailActivity, 100);
                    }
                }
        );

    }



    void getInfo(String url, int page) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        Log.d(TAG, "TabhomeVideosFragment - getInfo - page : " + page);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getListVideos(nb_video_show, "uploadeddate_original", page , new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                if (videoPojo.getVideos().size() > 0) {
                    /*Reservoir.putAsync("videoListCache", videoPojo.getVideos(), new ReservoirPutCallback() {
                        @Override
                        public void onSuccess() {
                            //success
                            Log.d(TAG, "Reservoir putAsync onSuccess");
                            //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            //error
                            Log.d(TAG,"Reservoir putAsync onFailure "+(e.getMessage()!=null?e.getMessage():""));
                        }
                    });*/
                    addToListServer(videoPojo.getVideos(), videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                }
            }

            @Override
            public void failure(RetrofitError error) { }
        });

        vidapi.getListVideos(nb_video_show, "statistics_viewCount_hier", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                /*Reservoir.putAsync("videoListPeriodeCache", videoPojo.getVideos(), new ReservoirPutCallback() {
                    @Override
                    public void onSuccess() {
                        //success
                        Log.d(TAG, "Reservoir putAsync onSuccess");
                        //fillCacheAdapter("videoListPeriodeCache", videoListPeriode, yesterday_adapter, grd_popularvideoyesterday, progress_wheel_grd_popularvideoyesterday);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        //error
                        Log.d(TAG,"Reservoir putAsync onFailure "+(e.getMessage()!=null?e.getMessage():""));
                    }
                });*/
                addToListServer(videoPojo.getVideos(), videoListPeriode, yesterday_adapter, grd_popularvideoyesterday, progress_wheel_grd_popularvideoyesterday);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "getListVideosPeriode Error :" + error.getMessage());
            }
        });

        vidapi.getListVideos(10, "statistics_viewCount", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                if (videoPojo.getVideos().size() > 0) {
                    /*Reservoir.putAsync("videoListAllPopularCache", videoPojo.getVideos(), new ReservoirPutCallback() {
                        @Override
                        public void onSuccess() {
                            //success
                            Log.d(TAG, "Reservoir putAsync onSuccess");
                            //fillCacheAdapter("videoListAllPopularCache", videoListAllPopular, allpopular_adapter, grd_popularvideoall, progress_wheel_grd_popularvideoall);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            //error
                            Log.d(TAG, "Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                        }
                    });*/
                    addToListServer(videoPojo.getVideos(), videoListAllPopular, allpopular_adapter, grd_popularvideoall, progress_wheel_grd_popularvideoall);
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        vidapi.getListVideos(10, "publishedCommentLast", page, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                if (videoPojo.getVideos().size() > 0) {
                    /*Reservoir.putAsync("videoCommentedRecentlyCache", videoPojo.getVideos(), new ReservoirPutCallback() {
                        @Override
                        public void onSuccess() {
                            //success
                            Log.d(TAG, "Reservoir putAsync onSuccess");
                            //fillCacheAdapter("videoCommentedRecentlyCache", videoCommentedRecently, recently_video_commented_adapter, grd_recently_video_commented, progress_wheel_grd_recently_video_commented);
                        }

                        @Override
                        public void onFailure(Exception e) {
                            //error
                            Log.d(TAG, "Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                        }
                    });*/
                    addToListServer(videoPojo.getVideos(), videoCommentedRecently, recently_video_commented_adapter, grd_recently_video_commented, progress_wheel_grd_recently_video_commented);
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });



    }
    void fillCacheAdapter( String cacheName, final List<video> videoListadd,final VideosBaseAdapter adapter, final TwoWayGridView twrdv, final ProgressWheel prgwheel) {

        Type resultType = new TypeToken<List<Videogson>>() {}.getType();
        Log.d(TAG, "Reservoir fillCacheAdapter");



        Reservoir.getAsync( cacheName, resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG,"Reservoir fillCacheAdapter getAsync onSuccess");
                //success
                Log.d(TAG,"Cache getTitre : "+videos.get(0).getTitre() );

                if ( isAdded() )
                    setListView();

                if ( videos.size() <= 0  ) {
                    twrdv.setVisibility(View.GONE);
                    prgwheel.setVisibility(View.VISIBLE);
                }

                for (int count = 0; count < videos.size(); count++) {
                    if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                        videoListadd.add( fillItemServer(videos.get(count)) );
                    }
                }
                adapter.notifyDataSetChanged();
                loaded = true;

            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG,"Reservoir fillCacheAdapter getAsync onFailure "+e.getMessage() );
                twrdv.setVisibility(View.GONE);
                prgwheel.setVisibility(View.VISIBLE);
            }
        });
    }
    void addToListServer(List<Videogson> videos, List<video> videoListadd, VideosBaseAdapter adapter, TwoWayGridView twrdv, ProgressWheel prgwheel) {

        if ( isAdded() )
            setListView();

        if ( videos.size() <= 0  ) {
            twrdv.setVisibility(View.GONE);
            prgwheel.setVisibility(View.VISIBLE);
        }

        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                videoListadd.add( fillItemServer(videos.get(count)) );
            }
        }
        adapter.notifyDataSetChanged();
        loaded = true;
    }


    video fillItemServer(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId( videogson.getId() );
        v.setVideoytid( videogson.getVideoytid()  );
        v.setStatistics_viewCount(videogson.getStatisticsViewCount() );
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount() );
        v.setStatistics_dislikeCount(videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        v.setContentDetails_definition(videogson.getContentDetailsDefinition());

        return v;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_fragment_home, menu);

        super.onCreateOptionsMenu(menu, inflater);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.login:
                VazotsaraShareFunc.openLoginActivity(getActivity());
                break;
            /*case R.id.logout:
                setLogout();
                break;*/
        }
        return true;
    }




}

package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.adapters.VideoRelatedBaseAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.VideoSingleGsonPlaylist;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailTabRelatedSQLFragmentList extends Fragment {

    String uid;
    String url = Config.URLAPI;
    static String TAG = Config.TAGKEY;

    ListView listView;

    ConnectionDetector connecte;
    BroadcastConnexion receiver;

    View rootView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    VideoRelatedBaseAdapter adapter;

    List<video> videoArtistList = new ArrayList<video>();
    List<VideoSingleGsonPlaylist> plist = new ArrayList<VideoSingleGsonPlaylist>();

    boolean loaded;

    int nb_row;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");

        nb_row = TableVideos.getCountRelatedvideos(uid, "artiste");
    }
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoFragment - onResume");
        if ( loaded )
            setListView();
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "VideoFragment - onPause");
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            if ( connecte.isConnectingToInternet() && adapter.getCount() == 0 ) {
                if ( nb_row <= 0 ) {
                    setLoadingView();
                }
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                getInfoServer(url, 0);
            }
            //Si non connecte  Internet et isLoading()
            if ( !connecte.isConnectingToInternet() && nb_row > 0  ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() > 0");
                getInfoBDD();
            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if ( !connecte.isConnectingToInternet() && nb_row == 0 ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() == 0");
                setErrorView(getActivity().getString(R.string.error_non_connecte));
            }
        }
    }

    public static VideoDetailTabRelatedSQLFragmentList newInstance(String uid) {
        VideoDetailTabRelatedSQLFragmentList frag = new VideoDetailTabRelatedSQLFragmentList();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        frag.setArguments(args);
        return frag;
    }


    public VideoDetailTabRelatedSQLFragmentList() {
        // Required empty public constructor
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail_tab_related_fragment_list_bd, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );
        adapter = new VideoRelatedBaseAdapter( getActivity() , videoArtistList, R.layout.adapter_video_related  );

        listView = (ListView) viewstub_listview_content.findViewById(R.id.list);


        View headerListView = getActivity().getLayoutInflater().inflate(R.layout.adapter_video_related_header, null);
        listView.addHeaderView(headerListView);

        TextView txtheader = (TextView) headerListView.findViewById(R.id.headerlistview);
        txtheader.setText("Vidéos en relation");

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG,"onActivityCreated onItemClick - i : "+i+" - l: "+l);
                //System.exit(0);

                /**
                 * Si on ne clic pas sur le header
                 */
                if ( i>0) {

                    String uid;
                    uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                    String videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                    String contentDetailsDefinition = ((TextView) view.findViewById(R.id.contentDetailsDefinition)).getText().toString();

                    Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                    VDetailActivity.putExtra("uid", uid);
                    VDetailActivity.putExtra("videoytid", videoytid);
                    VDetailActivity.putExtra("contentDetails_definition", contentDetailsDefinition);
                    getActivity().startActivityForResult(VDetailActivity, 100);
                }

            }
        });
    }

    void getInfoServer(String url, int page) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        vidapi.getRelatedVideo(uid, "realisateur", new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                if (videoPojo.getVideos().size() == 0 && nb_row <= 0) {

                    loaded = true;
                    return;
                }
                setListView();
                //VazotsaraShareFunc.saveInVideos(videoPojo.getVideos());
                getInfoBDD();
                /*
                addToList(videoPojo.getVideos(), "realisateur");
                adapter.notifyDataSetChanged();*/
                loaded = true;

            }

            @Override
            public void failure(RetrofitError error) {
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

        vidapi.getRelatedVideo(uid, "", new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + videoPojo.getVideos().size());
                if (videoPojo.getVideos().size() == 0) {
                    loaded = true;
                    return;
                }
                setListView();
                //VazotsaraShareFunc.saveInVideos(videoPojo.getVideos());
                getInfoBDD();
                //adapter.notifyDataSetChanged();
                //adapter.notifyDataSetChanged();
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "PlaylistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });



    }

    void getInfoBDD() {
        new loadSQL().execute();
    }

    /*void addToList(List<Videogson> videos, String type ) {
        for (int count = 0; count < videos.size(); count++) {
            videoArtistList.add(fillItem(videos.get(count), type ));
        }
    }*/
    void addToList(List<TableVideos> videos, String type ) {
        int added = 0;
        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).serverid  ) ) {
                videoArtistList.add(fillItem(videos.get(count), type ));
                added++;
                Log.d(TAG, "badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " ajouté dans adapter - type "+type);
            } else {
                Log.d(TAG,"badapter - Nom : "+videos.get(count).titre+" - ID : "+videos.get(count).serverid+" existe deja dans adapter - type "+type);
            }
        }
        adapter.notifyDataSetChanged();

    }

    video fillItem(TableVideos field, String type ) {

        video v = new video();
        v.setTitre(field.titre);
        v.setId(field.serverid);
        v.setVideoytid(field.videoytid);
        v.setStatistics_viewCount(String.valueOf(field.statisticsViewCount));
        v.setStatistics_likeCount(String.valueOf(field.statisticsLikeCount));
        v.setStatistics_dislikeCount(String.valueOf(field.statisticsDislikeCount));
        v.setThumburl(field.thumburl);

        v.setDuree(String.valueOf(field.contentDetailsDuration));
        v.setFormat(field.contentDetailsDefinition);

        v.setContentDetails_definition(field.contentDetailsDefinition);
        v.setType(type);


        List<TablePlaylists> row = TableVideos.getAllPlaylistsByVideo(field.serverid);


        if ( row.size()>0 ) {
            for( int i = 0; i < row.size(); i++ ) {
                TablePlaylists item = row.get(i);
                VideoSingleGsonPlaylist e = new VideoSingleGsonPlaylist();
                e.setNom( item.nom  );
                e.setObjetid(item.objetid);
                e.setObjetnom(item.objetnom);
                e.setCreated(item.created);
                e.setDescriptionYt(item.descriptionYt);
                e.setFirstvideoid(item.firstvideoid);
                e.setId(item.serverid);
                e.setImage(item.image);
                e.setNbVideos(item.nbVideos);
                e.setCreatedAt(item.createdAt);
                e.setObjetype(item.objetype);
                e.setPlaylisteid(item.playlisteid);
                e.setStatus(item.status);

                plist.add(e);
            }
            v.setPlaylisteinfo( plist );
        }



        return v;
    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");

        }
        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {

                    if ( nb_row == 0 ) {
                        setEmptyView(getActivity().getString(R.string.error_no_item_video));
                        loaded = true;
                        return;
                    }


                    List<TableVideos> liste = TableVideos.getFromRelatedvideos(uid, "artiste").execute();
                    List<TableVideos> listerealisateur = TableVideos.getFromRelatedvideos(uid, "realisateur").execute();

                    Log.d(TAG, "VideosSQLFragment - SQL " + TableVideos.getFromRelatedvideos(uid, "artiste").toSql());

                    addToList(liste, "artiste");
                    addToList(listerealisateur, "realisateur");
                    loaded = true;
                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute");

        }
    }

}

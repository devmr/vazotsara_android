package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.VideoAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.TableVideosPlaylists;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosByPlaylistsSQLFragment extends Fragment {

    View rootView;
    View emptyView;

    String playlisteid;
    TextView textview;

    String sortfields = "uploadeddate_original";
    int currentPage = 0;
    int nb_video_par_page = 20;

    int nb_total, max_pages;

    String url = Config.URLAPI;

    boolean loaded;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = Config.TAGKEY;

    RecyclerView recyclerView;
    SparseItemRemoveAnimator mSparseAnimator;


    View viewstub_recyclerview_error;
    View viewstub_recyclerview_empty;
    View viewstub_recyclerview_content;
    View viewstub_recyclerview_loading;

    ViewStub vs_loading_default;
    ViewStub vs_content_errormessage;
    ViewStub vs_content_failureerrormessage;

    List<video> videoList = new ArrayList<video>();

    VideoAdapter adapter;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int lastFirstVisiblePosition;
    LinearLayoutManager linearLayoutManager;

    int nbtotal_row = 0;

    FloatingActionButton fab;


    public VideosByPlaylistsSQLFragment() {
        // Required empty public constructor
    }

    public VideosByPlaylistsSQLFragment newInstance(String playlisteid) {
        VideosByPlaylistsSQLFragment frag = new VideosByPlaylistsSQLFragment();
        Bundle args = new Bundle();
        args.putString("playlisteid", playlisteid);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        playlisteid = getArguments().getString("playlisteid", "");
        nbtotal_row = TableVideos.getAllVideosInPlaylistCount(playlisteid);
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoFragment - onResume");
        adapter = new VideoAdapter( getActivity() , videoList, R.layout.adapter_videoitem, R.layout.adapter_videoloading, nb_video_par_page  );
        recyclerView.setAdapter(adapter);

        linearLayoutManager.scrollToPosition(lastFirstVisiblePosition);
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "VideoFragment - onPause");
        lastFirstVisiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {



            if ( connecte.isConnectingToInternet() && adapter.getItemCount()-1 == 0 ) {
                Log.d(TAG, "VideosSQLFragment - Connecté et adapter vide");
                mSwipeRefreshLayout.setRefreshing(true);
                setLoadingView();

                if ( nbtotal_row >0 ) {
                    Log.d(TAG, "VideosSQLFragment - Connecté et adapter vide - nbrow : " + nbtotal_row);
                    getInfoBDD( 0, playlisteid );
                }

                getInfo(url, 0);
            }

            //Si non connecte  Internet et isLoading()
            if ( connecte.isConnectingToInternet() && adapter.getItemCount()-1 > 0 ) {
                Log.d(TAG, "mRecycler.isLoadingMore - currentpage : " + currentPage);
                mSwipeRefreshLayout.setRefreshing(true);
                if ( nbtotal_row >0 ) {
                    Log.d(TAG, "VideosSQLFragment - Connecté et adapter vide - nbrow : " + nbtotal_row);
                    getInfoBDD( currentPage , playlisteid);
                }
                getInfo(url, currentPage);
                adapter.setDeconnecte(false);
            }

            //Si non connecte  Internet et isLoading()
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount()-1 > 0 ) {
                Log.d(TAG, "mRecycler.isLoadingMore - NC currentpage : " + currentPage);

            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount()-1 == 0 ) {
                Log.d(TAG, "VideosSQLFragment - non Connecté et adapter vide");
                //Si nb row dans BDD >  0
                if ( nbtotal_row > 0 ) {
                    Log.d(TAG, "VideosSQLFragment - non Connecté et adapter vide - nbrow BDD > 0 ");
                    getInfoBDD( 0, playlisteid );
                } else {
                    Log.d(TAG, "VideosSQLFragment - non Connecté et adapter vide - nbrow BDD = 0 ");
                    setErrorView(getActivity().getString(R.string.error_non_connecte));
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_videos_by_artist_bdd, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewstub_recyclerview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_recyclerview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_recyclerview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_recyclerview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();


        recyclerView = (RecyclerView) viewstub_recyclerview_content.findViewById(R.id.list);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.d(TAG, "STEP - recyclerView.addOnScrollListener - page " + current_page+" - max_pages: "+max_pages);
                if ( max_pages>0 && current_page<max_pages ) {
                    Log.d(TAG, "STEP - recyclerView.addOnScrollListener - Page suivante " + current_page+" - max_pages: "+max_pages);

                    if ( connecte.isConnectingToInternet() ) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        getInfo(url, current_page);
                    } else {
                        getInfoBDD(current_page, playlisteid);
                    }
                } else {
                    Log.d(TAG, "STEP - recyclerView.addOnScrollListener - Bas de page " + current_page+" - max_pages: "+max_pages);
                }
                currentPage = current_page;
            }
        });

        fab = (FloatingActionButton) viewstub_recyclerview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabVideoFragment(getActivity());
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_recyclerview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                VazotsaraShareFunc.showSnackMsg( getString(R.string.error_swipe_non_fonctionnel), getActivity());
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.material_red_400, R.color.ColorPrimaryDark, R.color.material_red_400);



    }

    public void setLoadingView()
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.VISIBLE);
    }
    public void setErrorView(final String error)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.VISIBLE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_error.findViewById(R.id.textView);
        Button btn_refresh = (Button) viewstub_recyclerview_error.findViewById(R.id.btn_refresh);

        textv.setText(error);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connecte.isConnectingToInternet()) {
                    showSnackError(error);
                } else {
                    setLoadingView();
                    getInfo(url, 0);
                }
            }
        });
    }
    void showSnackError(String error)
    {
        Snackbar snack = Snackbar.make(getActivity().findViewById(R.id.content), error, Snackbar.LENGTH_LONG);
        snack.show();

        //Color en blanc le text
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }
    public void setEmptyView(String textempty)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.VISIBLE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_recyclerview_content.setVisibility(View.VISIBLE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);
    }
    void getInfoBDD(int page, String playlisteid) {
        new loadSQL().execute(String.valueOf(page), playlisteid);
    }
    void getInfo(String url, final int page )
    {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        Log.d(TAG, "URL retrofit 608 : " + url + "/videos?limit=20&page=" + page + "&q=&sortfields=" + sortfields + "&plid=" + playlisteid);
        vidapi.getVideosInPlaylists(nb_video_par_page, page, playlisteid, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {

                setSuccessInfo(videoPojo, page, playlisteid);

            }

            @Override
            public void failure(RetrofitError error) {
                setFailureInfo(error);
            }
        });

    }
    void setSuccessInfo(VideoPojo videoPojo, int page, String playlisteid){
        mSwipeRefreshLayout.setRefreshing(false);
        if (videoPojo.getVideos().size() == 0 && nbtotal_row <= 0) {
            if ( isAdded() )
                setEmptyView(getActivity().getString(R.string.error_no_item_video));
            loaded = true;
            return;
        } else {
            Log.d(TAG, "STEP - getInfo - L571 - success " + videoPojo.getVideos().get(0).getTitre());
            if ( isAdded() )
                setListView();
            //VazotsaraShareFunc.saveInVideos(videoPojo.getVideos() );
            //Afficher info BDD
            getInfoBDD( page, playlisteid );
            loaded = true;
        }
    }

    void setFailureInfo(RetrofitError error){
        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
        Log.d(TAG, "STEP - getInfo - L586 - fail " + this.getClass().getSimpleName());
        if (adapter.getItemCount() == 0)
            setErrorView(errorMessage);
        else {

            if ( getActivity() != null ) {
                Snackbar snack = Snackbar.make(getActivity().findViewById(R.id.content), errorMessage, Snackbar.LENGTH_LONG);
                snack.show();

                //Color en blanc le text
                View view = snack.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
            }
        }
    }
    void addToList(List<TableVideos> videos) {
        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).serverid  ) ) {
                videoList.add(fillItem(videos.get(count)));
            } else {
                Log.d(TAG,"badapter - Nom : "+videos.get(count).titre+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
            }
        }
        adapter.notifyDataSetChanged();
    }

    video fillItem(TableVideos field) {

        video v = new video();
        v.setTitre(field.titre);
        v.setId(field.serverid);
        v.setVideoytid( field.videoytid);
        v.setStatistics_viewCount(String.valueOf(field.statisticsViewCount));
        v.setStatistics_likeCount(String.valueOf(field.statisticsLikeCount));
        v.setStatistics_dislikeCount( String.valueOf( field.statisticsDislikeCount));
        v.setThumburl(field.thumburl);

        v.setDuree(String.valueOf(field.contentDetailsDuration));
        v.setFormat(field.contentDetailsDefinition);

        v.setContentDetails_definition(field.contentDetailsDefinition);

        return v;

    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");
            mSwipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {
                    int page = 0;
                    if ( params.length == 2 ) {
                        page = Integer.parseInt(params[0]);
                        playlisteid = params[1];
                    }

                    int nbrow = new Select()
                            .from(TableVideos.class).as("a")
                            .innerJoin(TableVideosPlaylists.class).as("b")
                            .on("b.videoid = a.serverid")
                            .innerJoin(TablePlaylists.class).as("c")
                            .on("c.serverid = b.playlisteid")
                            .where("b.playlisteid = ?", playlisteid )
                            .count();

                    Log.d(TAG, "loadFavoriVideoSQL  nbrow : "+nbrow );

                    if ( nbrow == 0 ) {
                        setEmptyView(getActivity().getString(R.string.error_no_item_video));
                        loaded = true;
                        return;
                    }



                    setListView();

                    List<TableVideos> liste = new Select()
                            .from(TableVideos.class).as("a")
                            .innerJoin(TableVideosPlaylists.class).as("b")
                            .on("b.videoid = a.serverid")
                            .innerJoin(TablePlaylists.class).as("c")
                            .on("c.serverid = b.playlisteid")
                            .orderBy("a." + sortfields + " DESC, a.serverid DESC")
                            .where("b.playlisteid = ?", playlisteid)
                            .limit(Config.LIMIT_GENERAL)
                            .offset(page * Config.LIMIT_GENERAL)
                            .execute();

                    Log.d(TAG, "VideosSQLFragment - SQL " + new Select()
                            .from(TableVideos.class).as("a")
                            .innerJoin(TableVideosPlaylists.class).as("b")
                            .on("b.videoid = a.serverid")
                            .innerJoin(TablePlaylists.class).as("c")
                            .on("c.serverid = b.playlisteid")
                            .orderBy("a." + sortfields + " DESC, a.serverid DESC")
                            .where("b.playlisteid = ?", playlisteid)
                            .limit(Config.LIMIT_GENERAL)
                            .offset(page * Config.LIMIT_GENERAL).toSql());

                    addToList(liste);

                    adapter.setServerListSize(nbrow);
                 }
             } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute" );
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

}

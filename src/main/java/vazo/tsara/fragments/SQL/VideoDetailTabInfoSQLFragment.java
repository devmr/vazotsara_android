package vazo.tsara.fragments.SQL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.IconButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideosByArtistActivitySQL;
import vazo.tsara.activities.VideosByRealisateurActivitySQL;
import vazo.tsara.adapters.ArtistsTWVAdapter;
import vazo.tsara.adapters.RealisateurTWVAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.TwoWayAdapterView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.FavoriVideoSQL;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.relatedvideomodel;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.Dbhandler;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailTabInfoSQLFragment extends Fragment {

    View rootView;
    static String TAG = "VAZOTSARA";
    String uid;
    boolean videoidInFavori = false;

    Dbhandler db;

    BroadcastConnexion receiver;
    ConnectionDetector detector;

    private TwoWayGridView artistes_list;
    private TwoWayGridView realisateur_list;

    private YouTubePlayerView youTubeView;
    private YouTubePlayer player;

    TextView titrevideo, nbview, nblike, nbdislike, nbcomment, hdformat, headerartists, headerrealisateur, action_favorite;
    //HtmlTextView html_text;
    RelativeTimeTextView v;
    IconButton btn_favori, btn_share;

    View vsload, vscontent_detail, vserror_nonconnecte;

    YouTubePlayerSupportFragment youTubePlayerFragment;

    VideoDetailActivity mActivity;

    Boolean loaded = false;

    List<relatedvideomodel> artistelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> realisationlist = new ArrayList<relatedvideomodel>();

    RelativeLayout bloc_addfavorite;

    //RecyclerView artistes_list;

    String infovideo_titre, infovideo_format, infovideo_videoyt, msg_confirm;

    Playlist plartiste;
    Playlist plrealisateur;

    int nb_row;

    int position;

    ArtistsTWVAdapter artisterecycleradapter;
    RealisateurTWVAdapter realisateuradapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        detector = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");
        position = getArguments().getInt("position");

        nb_row = new Select().from(TableVideos.class).where("serverid = ?",uid).count();


    }

    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }

    public static VideoDetailTabInfoSQLFragment newInstance(String uid, int position) {
        VideoDetailTabInfoSQLFragment frag = new VideoDetailTabInfoSQLFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        args.putInt("position", position);
        frag.setArguments(args);
        return frag;
    }

    public VideoDetailTabInfoSQLFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail_tab_info_bdd, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        db = new Dbhandler(getActivity());
        //videoidInFavori = db.VideoInFavoris( Integer.parseInt(uid) );
        videoidInFavori = TableFavorivideos.isVideoidInFavoris( uid );

        vsload = ((ViewStub) rootView.findViewById(R.id.pdload)).inflate();
        vscontent_detail = ((ViewStub) rootView.findViewById(R.id.content_detail)).inflate();
        vserror_nonconnecte = ((ViewStub) rootView.findViewById(R.id.error_nonconnecte)).inflate();

        titrevideo = (TextView) vscontent_detail.findViewById(R.id.titrevideo);
        nbview = (TextView) vscontent_detail.findViewById(R.id.nbview);
        nblike = (TextView) vscontent_detail.findViewById(R.id.nblike);
        nbdislike = (TextView) vscontent_detail.findViewById(R.id.nbdislike);
        nbcomment = (TextView) vscontent_detail.findViewById(R.id.nbcomment);
        hdformat = (TextView) vscontent_detail.findViewById(R.id.hdformat);
        headerartists = (TextView) vscontent_detail.findViewById(R.id.headerartists);
        headerrealisateur = (TextView) vscontent_detail.findViewById(R.id.headerrealisateur);
        action_favorite = (TextView) vscontent_detail.findViewById(R.id.action_favorite);
        v = (RelativeTimeTextView) vscontent_detail.findViewById(R.id.timestamp);

        //flowview = (GridView) vscontent_detail.findViewById(R.id.flowview);
        artistes_list = (TwoWayGridView) vscontent_detail.findViewById(R.id.artistes_list);
        realisateur_list = (TwoWayGridView) vscontent_detail.findViewById(R.id.realisateur_list);

        bloc_addfavorite = (RelativeLayout) vscontent_detail.findViewById(R.id.bloc_addfavorite);
        Iconify.addIcons(action_favorite);
        final TextView bloc_addfavorite_text = (TextView) vscontent_detail.findViewById(R.id.bloc_addfavorite_text);

        artisterecycleradapter = new ArtistsTWVAdapter(getActivity(),artistelist  );
        realisateuradapter = new RealisateurTWVAdapter(getActivity(),realisationlist  );


        //flowview.setAdapter(artisteadapter);
        artistes_list.setAdapter(artisterecycleradapter);
        realisateur_list.setAdapter(realisateuradapter);

        //Si la video est dans les favoris
        if ( videoidInFavori == true ) {
            action_favorite.setTextColor(Color.parseColor(Config.COLOR_FAVORI_IN));
            bloc_addfavorite_text.setTextColor(Color.parseColor(Config.COLOR_FAVORI_IN));
            bloc_addfavorite_text.setText(getString(R.string.action_remove_favorite));
        }

        bloc_addfavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG,"videoidInFavori - UID : "+Integer.parseInt(uid)+" - count : "+new Select().from(TableFavorivideos.class).count() );
                List<TableFavorivideos> items = new Select().from(TableFavorivideos.class).execute();
                for( int i = 0; i < new Select().from(TableFavorivideos.class).count(); i++ ) {
                    Log.d(TAG, "videoidInFavori videoid : " + items.get(i).videoid);
                }


                String color, text;
                if ( videoidInFavori == true ) {
                    color = Config.COLOR_FAVORI_OUT;
                    text = getString(R.string.action_add_favorite);
                    //Remove in DB
                    //db.deleteFavoriVideo(Integer.parseInt(uid), artistelist);
                    TableFavorivideos.deleteVideoidInFavori( uid, getActivity() );
                    msg_confirm = getString(R.string.msg_confirm_remove_favorite);
                    videoidInFavori = false;
                } else {
                    color = Config.COLOR_FAVORI_IN;
                    text = getString(R.string.action_remove_favorite);
                    msg_confirm = getString(R.string.msg_confirm_add_favorite);
                    //Add in DB
                    //db.addFavori(new FavoriVideoSQL(infovideo_titre,Integer.parseInt( uid ),0,infovideo_videoyt,infovideo_format), artistelist);
                    TableFavorivideos.addVideoInFavori(new FavoriVideoSQL( Integer.parseInt(uid) ), getActivity() );
                    videoidInFavori = true;
                }

                Log.d(TAG,"videoidInFavori : "+videoidInFavori);


                VazotsaraShareFunc.showSnackMsg(msg_confirm, getActivity());

                         action_favorite.setTextColor(Color.parseColor(color));
                bloc_addfavorite_text.setTextColor(Color.parseColor(color));
                bloc_addfavorite_text.setText(text);

            }
        });




        artistes_list.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                String uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                String titre = ((TextView) view.findViewById(R.id.nom)).getText().toString();

                String logoreference = ((TextView) view.findViewById(R.id.logoreference)).getText().toString();
                Log.d(TAG, "TwoWayAdapterView.OnItemClickListener : " + uid+" - "+logoreference);
                showListVideosByArtists(uid, titre, logoreference);


            }
        });

        realisateur_list.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                String uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                String titre = ((TextView) view.findViewById(R.id.nom)).getText().toString();
                Log.d(TAG, "TwoWayAdapterView.OnItemClickListener : " + uid);
                showListVideosByRealisateurs(uid, titre);
            }
        });

        vscontent_detail.setVisibility(View.GONE);
        vserror_nonconnecte.setVisibility(View.GONE);
        Log.d(TAG, "loaded : 196 " + loaded);

        getInfoServer(Config.URLAPI + "/video/" + uid);

    }
    void showListVideosByArtists(String uid, String titre, String logoreference)
    {

        Intent VideoSearchActivity = new Intent( getActivity(), VideosByArtistActivitySQL.class);
        EventBus.getDefault().postSticky(plartiste);
        /*VideoSearchActivity.putExtra("playlisteid", uid );
        VideoSearchActivity.putExtra("titre", titre );
        VideoSearchActivity.putExtra("opt", "artiste" );
        VideoSearchActivity.putExtra("logoreference", logoreference );*/
        getActivity().startActivity(VideoSearchActivity);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }
    void showListVideosByRealisateurs(String uid, String titre)
    {
        Log.d(TAG,"In showListVideosByRealisateurs");
        Intent VideoSearchActivity = new Intent( getActivity(), VideosByRealisateurActivitySQL.class);
        VideoSearchActivity.putExtra("playlisteid", uid );
        VideoSearchActivity.putExtra("titre", titre );
        VideoSearchActivity.putExtra("opt", "artiste");
        getActivity().startActivity(VideoSearchActivity);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    void getInfoBDD() {
        new loadSQL().execute();
    }
    void getInfoServer(String url ) {



        loaded = true;


        if ( !detector.isConnectingToInternet() )
            return;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .setConverter(new LenientGsonConverter(new Gson()))
                .build();

        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        vidapi.getSingleVideo(uid, new Callback<VideoSingleGson>() {


            @Override
            public void success(VideoSingleGson videopojo, retrofit.client.Response response) {
                Log.d(TAG, "response.getStatus() : "+response.getStatus() );
                onSuccessResponse(videopojo);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });

    }

    void onSuccessResponse(final VideoSingleGson reponse) {

        //VazotsaraShareFunc.saveUniqueVideo(reponse.getInfo());

        vsload.setVisibility(View.GONE);
        vserror_nonconnecte.setVisibility(View.GONE);
        vscontent_detail.setVisibility(View.VISIBLE);

        vscontent_detail.invalidate();
    }
    public void onErrorResponse(RetrofitError volleyError) {
        String errorMessage = (volleyError.getMessage()==null)?"":volleyError.getMessage();
        Log.d(TAG, "onErrorResponse - Error:"+errorMessage);
        loaded = false;
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoDetailTabInfoFragment - onResume - ID : " + uid);
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "PlaylistsFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG,"DEBUG BROADCAST - detector.isConnectingToInternet()? : "+detector.isConnectingToInternet()+" - loaded ? "+loaded+" - nb_row : "+nb_row);
            //Si connecte  Internet et adapter vide - Rafraichir
            if ( detector.isConnectingToInternet() && !loaded ) {

                if ( nb_row > 0 ) {
                    vsload.setVisibility(View.GONE);
                    vscontent_detail.setVisibility(View.VISIBLE);
                    vserror_nonconnecte.setVisibility(View.GONE);
                } else {
                    vsload.setVisibility(View.VISIBLE);
                    vscontent_detail.setVisibility(View.VISIBLE);
                    vserror_nonconnecte.setVisibility(View.GONE);
                }


                getInfoServer(Config.URLAPI + "/video/" + uid);
            }


            //Si non connecte  Internet et isLoading()
            if ( !detector.isConnectingToInternet()  ) {
                Log.d(TAG,"DEBUG BDD non connecte  Internet - nb_row : "+nb_row);
                if ( nb_row > 0 ) {
                    vsload.setVisibility(View.GONE);
                    vscontent_detail.setVisibility(View.VISIBLE);
                    vserror_nonconnecte.setVisibility(View.GONE);
                    getInfoBDD();
                } else {
                    vsload.setVisibility(View.GONE);
                    vscontent_detail.setVisibility(View.GONE);
                    vserror_nonconnecte.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");

        }
        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {

                    int nbrow=0;
                    List<TablePlaylists> liste = null;




                    if ( nb_row == 0 ) {
                        vsload.setVisibility(View.GONE);
                        vscontent_detail.setVisibility(View.GONE);
                        vserror_nonconnecte.setVisibility(View.VISIBLE);
                        loaded = true;
                        return;
                    }



                    artistelist.clear();
                    realisationlist.clear();

                    TableVideos item = new Select().from(TableVideos.class).where("serverid = ?", uid ).executeSingle();

                    //Remplir les champs
                    //Titre
                    titrevideo.setText( item.titre  );
                    infovideo_titre = item.titre;

                    nbview.setText("{fa-youtube_play} " + item.statisticsViewCount );
                    Iconify.addIcons(nbview);


                    nblike.setText("{fa_thumbs_up} " + item.statisticsLikeCount);
                    Iconify.addIcons(nblike);

                    nbdislike.setText("{fa_thumbs_down} " + item.statisticsDislikeCount );
                    Iconify.addIcons(nbdislike);

                    nbcomment.setText("{fa_comment} " + item.statisticsCommentCount);
                    Iconify.addIcons(nbcomment);

                    //html_text.setHtmlFromString( reponse.getInfo().getDescriptionYt(), true );
                    infovideo_format = item.contentDetailsDefinition;
                    infovideo_videoyt = item.videoytid;





                    //Cacher HD textview si format SD
                    if ( !item.contentDetailsDefinition.equals("hd") ) {
                        hdformat.setVisibility(View.GONE);
                    }
                    CharSequence du = DateUtils.getRelativeTimeSpanString(( Long.decode(item.publishedAt) * 1000), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
                    v.setReferenceTime((Long.decode(item.publishedAt) * 1000));
                    Iconify.addIcons(v);

                    Log.d(TAG, "DEBUG BDD - item.publishedAt :  " + item.publishedAt + " - Long.decode(item.publishedAt) : "+Long.decode(item.publishedAt) );



                    Date date = new Date();

                    //Log.d(TAG, "du : " + du + " - new Date().getTime(): " + date.getTime() + " - Timestamp(date.getTime()): " + new Timestamp(date.getTime()) + " - reponse.getInfo().getUploadeddateOriginal():" + (reponse.getInfo().getUploadeddateOriginal() * 1000) + " - System.currentTimeMillis(): " + String.valueOf(System.currentTimeMillis()));



                    Log.d(TAG, "DEBUG BDD - TableVideos.getAllPlaylistsByVideo : "+TableVideos.getAllPlaylistsByVideo(uid).size() );

                    //Playlists
                    if ( TableVideos.getAllPlaylistsByVideo(uid).size() > 0 ) {
                        for( int i = 0; i < TableVideos.getAllPlaylistsByVideo(uid).size(); i++ ) {

                            TablePlaylists row = TableVideos.getAllPlaylistsByVideo(uid).get(i);
                            if ( row.objetype!=null && row.objetype.equals("artiste") ) {

                                relatedvideomodel a = new relatedvideomodel();
                                a.setUid( row.serverid );
                                a.setObjetnom(row.objetnom);
                                a.setNbVideos(row.nbVideos);
                                a.setPathplayliste((row.image != "" ? String.format( Config.PATHPLAYLIST , row.image ) : ""));
                                a.setImage((row.image != "" ? row.image : ""));
                                artistelist.add(a);

                                plartiste = new Playlist(
                                        row.serverid,
                                        row.playlisteid,
                                        row.nom,
                                        (row.image != "" ? String.format( Config.PATHPLAYLIST , row.image ) : ""),
                                        row.created,
                                        row.descriptionYt,
                                        row.nbVideos,
                                        row.objetnom,
                                        row.objetype,
                                        row.objetid,
                                        row.firstvideoid,
                                        row.status,
                                        row.createdAt,
                                        row.updatedAt
                                );
                            }

                            if ( row.objetype!=null && row.objetype.equals("realisateur") ) {

                                relatedvideomodel a = new relatedvideomodel();
                                a.setUid( row.serverid );
                                a.setObjetnom(row.objetnom);
                                a.setNbVideos(row.nbVideos);
                                a.setPathplayliste((row.image != "" ? String.format( Config.PATHPLAYLIST , row.image ) : ""));
                                a.setImage((row.image != "" ? row.image : ""));
                                realisationlist.add(a);

                                plrealisateur = new Playlist(
                                        row.serverid,
                                        row.playlisteid,
                                        row.nom,
                                        (row.image != "" ? String.format( Config.PATHPLAYLIST , row.image ) : ""),
                                        row.created,
                                        row.descriptionYt,
                                        row.nbVideos,
                                        row.objetnom,
                                        row.objetype,
                                        row.objetid,
                                        row.firstvideoid,
                                        row.status,
                                        row.createdAt,
                                        row.updatedAt
                                );
                            }
                        }
                    }

                    if ( artisterecycleradapter.getCount() == 0 ) {
                        artistes_list.setVisibility(View.GONE);
                        headerartists.setVisibility(View.GONE);
                    }

                    if ( realisateuradapter.getCount() == 0 ) {
                        realisateur_list.setVisibility(View.GONE);
                        headerrealisateur.setVisibility(View.GONE);
                    }


                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute");

        }
    }




    }

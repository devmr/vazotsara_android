package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.MainActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.ArtistsSearchSuggestionsAdapter;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.SQL.PlaylistsRecyclerSQLAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.SearchPlaylist;
import vazo.tsara.models.SingleResultSearchArtiste;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsSQLFragment extends Fragment {


    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";
    String sortfields = "objetnom";
    String sortfieldsascdesc = "asc";
    int currentPage = 0;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    SearchView sv;

    //RecyclerView recyclerView;
    //ListView listView;
    RecyclerView listView;
    SparseItemRemoveAnimator mSparseAnimator;

    private SliderLayout mDemoSlider;



    List<Playlist> playList = new ArrayList<Playlist>();

    boolean loaded;
    boolean loaded0 = false;

    //PlaylistsAdapter adapter;
    PlaylistsRecyclerSQLAdapter badapter;
    ArtistsSearchSuggestionsAdapter artistsSSAdapter;

    Parcelable state;

    String[] columns = new String[]{"_id", "NOM", "IMAGE", "PLAYLISTEID" };

    FloatingActionButton fab;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int lastFirstVisiblePosition;
    LinearLayoutManager linearLayoutManager;

    int nbtotal_row = TablePlaylists.getAllCountType("artiste");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {




        inflater.inflate(R.menu.menu_fragment_artists, menu);
        final MenuItem item = menu.findItem(R.id.search);
        sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);

        sv.setSuggestionsAdapter(artistsSSAdapter);
        sv.setIconifiedByDefault(true);
        sv.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String objetnom = cursor.getString(1);
                String id = cursor.getString(3);
                String image = cursor.getString(2);

                sv.setQuery(objetnom, false);

                showListeVideoByPlaylists( new Playlist(id,"","",image,"","","",objetnom,"","","","","",""));
                //setSearch(objetnom);

                sv.clearFocus();


                //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();


                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String objetnom = cursor.getString(1);
                String id = cursor.getString(3);
                String image = cursor.getString(2);
                //sv.setQuery(objetnom, false);
                sv.clearFocus();

                showListeVideoByPlaylists( new Playlist(id,"","",image,"","","",objetnom,"","","","","","") );

                //getInfo(url, 0, objetnom );
                //setSearch(objetnom);
                //Toast.makeText(getActivity(), "titre: " + titre+" ("+id+")", Toast.LENGTH_SHORT).show();
                return false;
            }
        });






        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (query.length() > 2) {
                    loadData(query);
                    //setSearch(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                if (query.length() > 2) {
                    loadData(query);
                    //setSearch(query);
                }
                return false;
            }
        });




        //Clic sur Croix dans searchview
        ImageView closeButton = (ImageView) sv.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetSearch();
            }
        });

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    Log.d(TAG,"MenuItemCompat.setOnActionExpandListener onMenuItemActionExpand");
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    Log.d(TAG,"MenuItemCompat.setOnActionExpandListener onMenuItemActionCollapse");
                    resetSearch();
                    return true;
                }
            });
        }



        super.onCreateOptionsMenu(menu, inflater);

    }
    void resetSearch(){
        setSearch("");
        sv.setQuery("", false);
        sv.setIconified(false);
    }
    void setSearch(String s){
        playList.clear();
        getInfoBDD(0, s);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sort:


                showDialogList( );
                break;
        }
        return true;
    }
    void showDialogList( ) {

        if ( !connecte.isConnectingToInternet()  ) {

            return;
        }



        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_sort)
                .items(R.array.sort_list_artistes)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        switch (which) {
                            case 0:
                                //Date de publication
                                sortfields = "created";
                                break;
                            case 1:
                                //Nombre de vues
                                sortfields = "nb_videos";
                                break;
                            case 2:
                                //Nombre de vues
                                sortfields = "objetnom";
                                break;

                        }

                        /*videoList.clear();
                        adapter.notifyDataSetChanged();
                        setLoadingInterface();
                        getInfo(url, 0, search);*/

                        //openSearchSortActivity();

                        String sortfields_label = "";


                        switch (sortfields) {

                            case "nb_videos":
                                //Nombre de vues
                                sortfields_label = getString(R.string.sortopt_nb_videos);
                                sortfieldsascdesc = "desc";
                                break;
                            case "created":
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_uploaded_date);
                                sortfieldsascdesc = "desc";
                                break;
                            default:
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_default);
                                sortfieldsascdesc = "asc";
                                break;

                        }

                        MainActivity.setTitleActivity(sortfields_label);

                        updateAdapter(sortfields,sortfieldsascdesc);

                    }
                })
                .show();
    }
    void updateAdapter(String orderby, String asc){
        playList.clear();
        badapter.notifyDataSetChanged();
        getInfoBDD(0, orderby, asc);
    }
    private void loadData(String searchText) {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getSearchPlaylist(searchText, "a", new Callback<SearchPlaylist>() {
            @Override
            public void success(SearchPlaylist searchResult, Response response) {
                //setSuccessInfo(videoPojo);
                //    Log.d(TAG,"searchResult.getResults() : "+searchResult.getResults().size());
                if (searchResult != null) {
                    MatrixCursor matrixCursor = convertToCursor(searchResult.getResults());
                    artistsSSAdapter.changeCursor(matrixCursor);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d(TAG, "searchResult.getResults() error : " + error.getMessage());

            }
        });

    }

    private MatrixCursor convertToCursor(  List<SingleResultSearchArtiste> SingleResultSearchArtiste) {
        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;
        for (SingleResultSearchArtiste rowArtiste : SingleResultSearchArtiste) {
            String[] temp = new String[4];
            i = i + 1;
            temp[0] = Integer.toString(i);
            temp[1] = rowArtiste.getObjetnom();
            temp[2] = rowArtiste.getImage();
            temp[3] = rowArtiste.getId();


            Log.d(TAG,"temp[2] : "+temp[2]);
            cursor.addRow(temp);
        }



        return cursor;
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "TabhomeArtistsFragment - onResume");

        if ( loaded )
            setListView();

        //badapter = new PlaylistsBaseAdapter( getActivity() , playList, R.layout.adapter_artiste_item , 0, ArtistsFragment.this );
        badapter = new PlaylistsRecyclerSQLAdapter( getActivity() , playList, R.layout.adapter_artiste_item , R.layout.adapter_videoloading );
        listView.setAdapter(badapter);

        linearLayoutManager.scrollToPosition(lastFirstVisiblePosition);
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "ArtistsFragment - onPause");

        lastFirstVisiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            int adaptercount = badapter.getCount();



            Log.d(TAG, "ArtistsSQLFragment - BroadcastConnexion - onReceive : "+badapter.getCount());

            if ( adaptercount <= 1 ) {
                //Si Connecté et adapter vide
                if (connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "ArtistsSQLFragment - Connecté et adapter vide");
                    mSwipeRefreshLayout.setRefreshing(true);
                    setLoadingView();
                    if (!sv.getQuery().toString().isEmpty())
                        getInfoServer(0, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                    else
                        getInfoServer(0, sortfields, sortfieldsascdesc);

                }


                //Si non Connecté et adapter non vide
                if ( !connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter non vide");
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (nbtotal_row > 0) {
                        Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter vide - nbrow BDD > 0 ");
                        getInfoBDD(0, sortfields, sortfieldsascdesc);
                    } else {
                        Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter vide - nbrow BDD = 0 ");
                        setErrorView(getActivity().getString(R.string.error_non_connecte));
                    }
                }

            }
        }
    }


    public ArtistsSQLFragment() {
        // Required empty public constructor
    }

    public static ArtistsSQLFragment newInstance() {
        ArtistsSQLFragment frag = new ArtistsSQLFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_playlists_bdd, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        artistsSSAdapter = new ArtistsSearchSuggestionsAdapter( getActivity(), R.layout.adapter_videosearchsuggestion_item, null, columns,null, -1000);


        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );


        listView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        listView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loaded0 = false;
                if ( connecte.isConnectingToInternet() ) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    Log.d(TAG, "STEP - recyclerView.addOnScrollListener - page " + current_page + " - playList.size() : " + playList.size());
                    if (!sv.getQuery().toString().isEmpty()) {
                        //getInfoBDD(current_page, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                        getInfoServer(current_page, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                    } else {
                        //getInfoBDD(current_page, sortfields, sortfieldsascdesc);
                        getInfoServer(current_page, sortfields, sortfieldsascdesc);
                    }
                } else {

                    if (!sv.getQuery().toString().isEmpty()) {
                        //getInfoBDD(current_page, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                        getInfoBDD(current_page, (sv.getQuery().toString().isEmpty() ? "" : sv.getQuery()));
                    } else {
                        //getInfoBDD(current_page, sortfields, sortfieldsascdesc);
                        getInfoBDD(current_page, sortfields, sortfieldsascdesc);
                    }
                }
                currentPage = current_page;
            }
        });

        fab = (FloatingActionButton) viewstub_listview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabArtistsFragment(getActivity());
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_listview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastPlaylists();

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.material_red_400, R.color.ColorPrimaryDark, R.color.material_red_400);

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               showListeVideoByPlaylists( ((TextView) view.findViewById(R.id.uid)).getText().toString(), ((TextView) view.findViewById(R.id.titre)).getText().toString() );
            }
        });*/

        /*listView.setOnScrollListener( new EndlessScrollListener(){
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView

                if (!sv.getQuery().toString().isEmpty())
                    getInfo(url,page,(sv.getQuery().toString().isEmpty()?"":sv.getQuery()));
                else
                    getInfo(url,page,sortfields,sortfieldsascdesc);
                // or customLoadMoreDataFromApi(totalItemsCount);
            }


        });*/

        // Restore previous state (including selected item index and scroll position)



        /*mDemoSlider = (SliderLayout) viewstub_listview_content.findViewById(R.id.slider);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/Config.LIMIT_GENERAL13/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/Config.LIMIT_GENERAL43093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView baseSliderView) {

                        }
                    });

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);*/

    }

    void getLastPlaylists(){
        if ( !connecte.isConnectingToInternet()  ) {
            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte), getActivity() );
            return;
        }

        //final String lastplaylist = (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()):"");
        final String lastplaylist = TableLastcheck.getLastIdArtiste();

        if ( !lastplaylist.isEmpty() && Integer.parseInt(lastplaylist)>0 ) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListPlaylisteBeforeId(lastplaylist,"artiste", new Callback<Playlists>() {

                @Override
                public void success(Playlists serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if (serverinfo.getPlaylists().size()>0 && !serverinfo.getPlaylists().get(0).getId().equals(lastplaylist)) {
                        //Refresh
                        //addToList(serverinfo.getPlaylists());
                        //badapter.notifyDataSetChanged();

                        //Enregistrer dernière playlist dans Preferences
                        String lastid = serverinfo.getPlaylists().get(0).getId();
                        /*if ( VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());*/
                        TableLastcheck.setLastIdArtiste(lastid);

                        VazotsaraShareFunc.saveInPlaylists(serverinfo.getPlaylists());
                        addToListServer(serverinfo.getPlaylists(), true );

                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.msg_confirm_playlists_added), serverinfo.getPlaylists().size()), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Error " + error);
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                }
            });


        }
        // Stop refresh animation dans tous les cas
        mSwipeRefreshLayout.setRefreshing(false);

    }

    void setTextView(){
        //badapter.getChildAt(position)
    }
    public static void updateItemText(String text, int i) {

    }
    void showListeVideoByPlaylists( Playlist p )
    {
        Intent VideoSearchActivity = new Intent(getActivity(), VideosByArtistActivity.class);
        EventBus.getDefault().postSticky(p);
        /*VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre );*/
        getActivity().startActivityForResult(VideoSearchActivity, 100);
    }

    void getInfoServer( final int page, String orderby, String orderbascdesc ) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("artiste", Config.LIMIT_GENERAL, page, orderby, orderbascdesc, new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, retrofit.client.Response response) {
                Log.d(TAG, "ArtistsSQLFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                mSwipeRefreshLayout.setRefreshing(false);
                if (plist.getPlaylists().size() == 0) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                setListView();
                //addToList(plist.getPlaylists());
                VazotsaraShareFunc.saveInPlaylists(plist.getPlaylists());
                addToListServer(plist.getPlaylists());
                //Afficher BDD
                /*if ( !loaded0 ) {
                    getInfoBDD(page, sortfields, sortfieldsascdesc);
                }*/
                //Enregistrer dernière playlist dans Preferences
                if (page <= 0) {
                    String lastid = plist.getPlaylists().get(0).getId();
                    /*if (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()) != null)
                        VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());*/
                    TableLastcheck.setLastIdArtiste(lastid);
                }

                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "ArtistsSQLFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });
    }
    void getInfoServer(  final int page, final CharSequence search) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        Log.d(TAG, "ArtistsSQLFragment - getInfo - page : " + page + " - sv.getQuery() : " + sv.getQuery() + " - search : " + search);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("artiste", Config.LIMIT_GENERAL, page, search.toString(), new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, retrofit.client.Response response) {
                Log.d(TAG, "ArtistsSQLFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                if (plist.getPlaylists().size() == 0 && nbtotal_row <= 0) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                setListView();
                VazotsaraShareFunc.saveInPlaylists(plist.getPlaylists());
                addToListServer(plist.getPlaylists());

                //Afficher info BDD
                //getInfoBDD(page, search.toString());

                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "ArtistsSQLFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

    }

    void getInfoBDD(final int page, String orderby, String orderbascdesc ) {
         new loadSQL().execute(String.valueOf(page), orderby, orderbascdesc);
    }
    void getInfoBDD(int page, CharSequence search) {
        new loadSQL().execute( String.valueOf( page ), search.toString() );
    }

    void addToList(List<TablePlaylists> liste) {
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).serverid  ) ) {
                playList.add(fillItem(liste.get(count)));
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).objetnom+" - ID : "+liste.get(count).serverid+" existe deja dans adapter ");
            }
        }
    }
    void addToListServer(List<Playlist> liste, boolean lastadded) {
        int added = 0;
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).getId()  ) ) {
                playList.add(fillItemServer(liste.get(count)));
                added++;
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).getObjetnom()+" - ID : "+liste.get(count).getId()+" existe deja dans adapter ");
            }
        }
        if ( added > 0 && lastadded ) {
            badapter.notifyItemRangeChanged(0, added);
            linearLayoutManager.scrollToPosition(0);
        } else {
            badapter.notifyDataSetChanged();
        }
    }
    void addToListServer(List<Playlist> liste) {
        int added = 0;
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).getId()  ) ) {
                playList.add(fillItemServer(liste.get(count)));
                added++;
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).getObjetnom()+" - ID : "+liste.get(count).getId()+" existe deja dans adapter ");
            }
        }
        badapter.notifyDataSetChanged();
    }
    Playlist fillItemServer(Playlist videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getNom());
        v.setObjetnom(videogson.getObjetnom());
        v.setId(videogson.getId());
        v.setNbVideos(videogson.getNbVideos());
        v.setImage(videogson.getImage());
        v.setObjetype(videogson.getObjetype());
        v.setStatus(videogson.getStatus());
        v.setDescriptionYt(videogson.getDescriptionYt());
        return v;
    }
    Playlist fillItem(TablePlaylists field) {
        Playlist v = new Playlist();
        v.setNom( field.nom );
        v.setObjetnom( field.objetnom );
        v.setId( field.serverid );
        v.setNbVideos(field.nbVideos);
        v.setImage(field.image);
        v.setObjetype(field.objetype);
        v.setStatus(field.status);
        v.setDescriptionYt(field.descriptionYt);
        return v;
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = Config.LIMIT_GENERAL;
        private int currentPage = 0;
        private int previousTotal = Config.LIMIT_GENERAL;
        private boolean loading = true;

        public EndlessScrollListener() {
        }
        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            totalItemCount = playList.size();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                //new LoadGigsTask().execute(currentPage + 1);
                //getInfo(url,currentPage + 1);
                onLoadMore(currentPage , totalItemCount);
                loading = true;
            }
        }

        public abstract void onLoadMore(int page, int totalItemsCount);


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");
            mSwipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {

                    int nbrow=0;
                    List<TablePlaylists> liste = null;

                    if ( params.length == 3 ) {
                        String orderby = params[1];
                        String orderbascdesc = params[2];
                        int page = Integer.parseInt(params[0]);

                        nbrow = new Select()
                                .from(TablePlaylists.class)
                                .orderBy(orderby + " " + orderbascdesc)
                                .where("objetype = 'artiste'").count() ;

                        liste = new Select()
                                .from(TablePlaylists.class)
                                .orderBy(orderby + " " + orderbascdesc)
                                .where("objetype = 'artiste'")
                                .limit(Config.LIMIT_GENERAL)
                                .offset(page * Config.LIMIT_GENERAL)
                                .execute();

                        Log.d(TAG,"ArtistsSQLFragment - SQL "+new Select()
                                .from(TablePlaylists.class)
                                .orderBy(orderby + " " + orderbascdesc)
                                .where("objetype = 'artiste'")
                                .limit(Config.LIMIT_GENERAL)
                                .offset(page * Config.LIMIT_GENERAL).toSql());

                    } else if( params.length == 2 ) {
                        int page = Integer.parseInt(params[0]);
                        String search = params[1];

                        nbrow = new Select()
                                .from(TablePlaylists.class)
                                .where("objetype = 'artiste'")
                                .where("objetnom LIKE '%"+search+"%'").count() ;

                        liste = new Select()
                                .from(TablePlaylists.class)
                                .where("objetype = 'artiste'")
                                .where("objetnom LIKE '%"+search+"%'" )
                                .limit(Config.LIMIT_GENERAL)
                                .offset(page * Config.LIMIT_GENERAL)
                                .execute();

                        Log.d(TAG, "ArtistsSQLFragment - SQL " + new Select()
                                .from(TablePlaylists.class)
                                .where("objetype = 'artiste'")
                                .where("objetnom LIKE '%" + search + "%'")
                                .limit(Config.LIMIT_GENERAL)
                                .offset(page * Config.LIMIT_GENERAL).toSql());

                    }


                    if ( nbrow == 0 ) {
                        setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                        loaded = true;
                        return;
                    }



                    setListView();
                    addToList(liste);
                    badapter.setServerListSize(nbrow);
                    //adapter.notifyDataSetChanged();
                    badapter.notifyDataSetChanged();
                    loaded = true;
                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute" );
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}

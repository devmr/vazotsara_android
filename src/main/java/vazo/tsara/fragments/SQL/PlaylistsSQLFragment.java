package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.SQL.PlaylistsRecyclerSQLAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsSQLFragment extends Fragment {


    String url = Config.URLAPI;

    SwipeRefreshLayout mSwipeRefreshLayout;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int serverListSize = 0;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    //RecyclerView recyclerView;
    RecyclerView listView;
    SparseItemRemoveAnimator mSparseAnimator;

    private SliderLayout mDemoSlider;



    List<Playlist> playList = new ArrayList<Playlist>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    PlaylistsRecyclerSQLAdapter badapter;

    Parcelable state;

    int nbtotal_row = TablePlaylists.getAllCountType("autre");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "PlaylistsFragment - onResume");
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "PlaylistsFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            int adaptercount = badapter.getCount();

            if ( adaptercount <= 1 ) {
                //Si Connecté et adapter vide
                if (connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "ArtistsSQLFragment - Connecté et adapter vide");
                    mSwipeRefreshLayout.setRefreshing(true);
                    setLoadingView();
                    getInfoServer();

                }


                //Si non Connecté et adapter non vide
                if ( !connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter non vide");
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (nbtotal_row > 0) {
                        Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter vide - nbrow BDD > 0 ");
                        getInfoBDD();
                    } else {
                        Log.d(TAG, "ArtistsSQLFragment - non Connecté et adapter vide - nbrow BDD = 0 ");
                        setErrorView(getActivity().getString(R.string.error_non_connecte));
                    }
                }

            }
        }
    }


    public PlaylistsSQLFragment() {
        // Required empty public constructor
    }

    public static PlaylistsSQLFragment newInstance() {
        PlaylistsSQLFragment frag = new PlaylistsSQLFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "PlaylistsFragment - onCreateView");
        rootView = inflater.inflate(R.layout.fragment_playlists_bdd, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "PlaylistsFragment - onActivityCreated");
        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );


        listView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        badapter = new PlaylistsRecyclerSQLAdapter( getActivity() , playList, R.layout.adapter_artiste_item , R.layout.adapter_videoloading );
        listView.setAdapter(badapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_listview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastPlaylists();

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.material_red_400, R.color.ColorPrimaryDark, R.color.material_red_400);


        /*badapter = new PlaylistsBaseAdapter( getActivity() , playList, R.layout.adapter_playlistsitem, serverListSize );
        listView.setAdapter(badapter);*/


        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String uid;
                uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();

                Intent VideoSearchActivity = new Intent(getActivity(), VideosByArtistActivity.class);
                VideoSearchActivity.putExtra("playlisteid", uid);
                VideoSearchActivity.putExtra("titre", ((TextView) view.findViewById(R.id.titre)).getText());
                getActivity().startActivityForResult(VideoSearchActivity, 100);

            }
        });*/

        /*listView.setOnScrollListener( new EndlessScrollListener(){
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                //getInfo(url,page);
                // or customLoadMoreDataFromApi(totalItemsCount);
            }
        });*/

        // Restore previous state (including selected item index and scroll position)



        /*mDemoSlider = (SliderLayout) viewstub_listview_content.findViewById(R.id.slider);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView baseSliderView) {

                        }
                    });

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);*/

    }

    void getLastPlaylists(){
        if ( !connecte.isConnectingToInternet()  ) {
            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte), getActivity());
            return;
        }

        //final String lastplaylist = (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()):"");
        final String lastplaylist = TableLastcheck.getLastIdAutre();

        if ( !lastplaylist.isEmpty() && Integer.parseInt(lastplaylist)>0 ) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListPlaylisteBeforeId(lastplaylist, "autre",  new Callback<Playlists>() {

                @Override
                public void success(Playlists serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if (serverinfo.getPlaylists().size()>0 && !serverinfo.getPlaylists().get(0).getId().equals(lastplaylist)) {
                        //Refresh
                        //addToList(serverinfo.getPlaylists());
                        //badapter.notifyDataSetChanged();

                        //Enregistrer dernière playlist dans Preferences
                        String lastid = serverinfo.getPlaylists().get(0).getId();
                        /*if ( VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());*/
                        TableLastcheck.setLastIdAutre(lastid);

                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.msg_confirm_playlists_added), serverinfo.getPlaylists().size()), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Error " + error);
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                }
            });


        }
        // Stop refresh animation dans tous les cas
        mSwipeRefreshLayout.setRefreshing(false);

    }

    void getInfoServer() {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("autre", new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, retrofit.client.Response response) {
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                mSwipeRefreshLayout.setRefreshing(false);
                if (plist.getPlaylists().size() == 0) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                serverListSize = plist.getTotal();
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + serverListSize);
                setListView();
                //addToList(plist.getPlaylists());
                VazotsaraShareFunc.saveInPlaylists(plist.getPlaylists());
                //getInfoBDD();
                addToListServer(plist.getPlaylists());
                //adapter.notifyDataSetChanged();
                /*badapter.setServerListSize(serverListSize);
                badapter.notifyDataSetChanged();*/
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "PlaylistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

    }
    void getInfoBDD() {
        new loadSQL().execute();
    }
    void addToListServer(List<Playlist> liste) {
        int added = 0;
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).getId()  ) ) {
                playList.add(fillItemServer(liste.get(count)));
                added++;
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).getObjetnom()+" - ID : "+liste.get(count).getId()+" existe deja dans adapter ");
            }
        }
        badapter.notifyDataSetChanged();
    }
    Playlist fillItemServer(Playlist videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getNom());
        v.setObjetnom(videogson.getObjetnom());
        v.setId(videogson.getId());
        v.setNbVideos(videogson.getNbVideos());
        v.setImage(videogson.getImage());
        v.setObjetype(videogson.getObjetype());
        v.setStatus(videogson.getStatus());
        v.setDescriptionYt(videogson.getDescriptionYt());
        return v;
    }
    void addToList(List<TablePlaylists> liste) {
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).serverid  ) ) {
                playList.add(fillItem(liste.get(count)));
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).objetnom+" - ID : "+liste.get(count).serverid+" existe deja dans adapter ");
            }
        }
    }

    Playlist fillItem(TablePlaylists field) {
        Playlist v = new Playlist();
        v.setNom( field.nom );
        v.setObjetnom(field.objetnom);
        v.setId(field.serverid);
        v.setNbVideos(field.nbVideos);
        v.setImage(field.image);
        v.setObjetype(field.objetype);
        v.setStatus(field.status);
        v.setDescriptionYt(field.descriptionYt);
        return v;
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 0;
        private int previousTotal = 0;
        private boolean loading = true;

        public EndlessScrollListener() {
        }
        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                //new LoadGigsTask().execute(currentPage + 1);
                //getInfo(url,currentPage + 1);
                onLoadMore(currentPage + 1, totalItemCount);
                loading = true;
            }
        }

        public abstract void onLoadMore(int page, int totalItemsCount);

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");
            mSwipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {

                    int nbrow = new Select()
                            .from(TablePlaylists.class)
                            .where("objetype = 'autre'")
                            .count() ;

                    if ( nbrow == 0 ) {
                        setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                        loaded = true;
                        return;
                    }



                    setListView();

                    List<TablePlaylists> liste = new Select()
                            .from(TablePlaylists.class)
                            .orderBy("objetnom ASC")
                            .where("objetype = 'autre'")
                            .execute();

                    Log.d(TAG,"ArtistsSQLFragment - SQL "+new Select()
                            .from(TablePlaylists.class)
                            .orderBy("created DESC")
                            .where("objetype = 'autre'").toSql());
                    addToList(liste);

                    badapter.setServerListSize(nbrow);
                    //adapter.notifyDataSetChanged();
                    badapter.notifyDataSetChanged();
                    loaded = true;





                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute" );
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}

package vazo.tsara.fragments.SQL;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.MainActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideoSearchSortActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.SQL.VideoSQLAdapter;
import vazo.tsara.adapters.VideoSearchSuggestionsAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.helpers.RecyclerItemClickListener;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.DataBusVideo;
import vazo.tsara.models.Person;
import vazo.tsara.models.SearchVideo;
import vazo.tsara.models.SingleResultSearch;
import vazo.tsara.models.SingleResultSearchArtiste;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosSQLFragment extends Fragment {

    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";
    String search = "";
    String playlisteid = "";
    String sortfields = "publishedAt";
    int currentPage = 0;

    View rootView;
    View emptyView;
    View viewstub_recyclerview_error;
    View viewstub_recyclerview_empty;
    View viewstub_recyclerview_content;
    View viewstub_recyclerview_loading;

    VideoSearchSuggestionsAdapter mSearchViewAdapter;

    private SearchView mSearchView;
    private SearchView searchView;
    private MenuItem searchMenuItem;


    RecyclerView recyclerView;
    SparseItemRemoveAnimator mSparseAnimator;

    ViewStub vs_loading_default;
    ViewStub vs_content_errormessage;
    ViewStub vs_content_failureerrormessage;

    List<video> videoList = new ArrayList<video>();

    VideoSQLAdapter adapter;

    Person[] people;
    ArrayAdapter<Person> tadapter;

    boolean loaded;

    String[] columns = new String[]{"_id", "VIDEOYT", "TITRE", "VIDEOID", "contentDetails_definition"};
    String[] columnsArtiste = new String[]{"_id", "OBJETNOM", "ARTISTEID" };

    FloatingActionButton fab;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int lastFirstVisiblePosition;
    LinearLayoutManager linearLayoutManager;

    int nbtotal_row = 0;

    String lastvideoId;

    int pagerserver = 0;

    int listeNew = 0;
    int listeView = 0;
    int listeLike = 0;
    int listeComment = 0;
    int listeDuration = 0;
    int listeCommentDate = 0;
    int listeCommentlast = 0;
    int listeViewHier = 0;

    int clickpos = -1;

    From from;

    String option = "listeNew";
    String old_option = "";

    int addedserver = 0;

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    protected LayoutManagerType mCurrentLayoutManagerType;

    public VideosSQLFragment() {
        // Required empty public constructor
    }

    public static VideosSQLFragment newInstance(String search, String playlisteid) {

        VideosSQLFragment frag = new VideosSQLFragment();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putString("playlisteid", playlisteid);
        frag.setArguments(args);
        return frag;
    }
    public void onStop(){
        Log.d(TAG, "Fragment STEP onStop");
        super.onStop();
    }
    public void onDestroy(){
        Log.d(TAG, "Fragment STEP  onDestroy");
        super.onDestroy();

        /*Type resultType = new TypeToken<List<Videogson>>() {}.getType();
        Reservoir.getAsync("videoList", resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG, "Fragment STEP  onDestroy - nbvideos save : " + videos.size());
                VazotsaraShareFunc.saveInVideos(videos);

                try {
                    Reservoir.clearAsync(new ReservoirClearCallback() {
                        @Override
                        public void onSuccess() { }

                        @Override
                        public void onFailure(Exception e) { }
                    });
                } catch (Exception e) { }
            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG, "Fragment STEP  onDestroy getAsync onFailure " + e.getMessage());


            }
        });*/
        Log.d(TAG, "Fragment STEP  onDestroy - nbvideos loaded : " + videoList.size() + " - pageserver : " + pagerserver);
        saveBddOndetach();
    }
    public void onDetach(){
        Log.d(TAG, "Fragment STEP  onDetach");
        super.onDetach();
    }
    public void onAttach(Context context){
        Log.d(TAG, "Fragment STEP  onAttach" );
        super.onAttach(context);
    }
    public void onStart()
    {
        Log.d(TAG, "Fragment STEP  onStart" );
        Log.d(TAG, "Fragment STEP  - onStart " + this.getClass().getSimpleName());

        enableSortField("listeNew");


        from = new Select()
                .from(TableVideos.class)
                ;


        if ( listeNew == 1 ) {
            from.where("listeNew = 1");
        } else if ( listeView == 1 ) {
            from.where("listeView = 1");
        } else if ( listeComment == 1 ) {
            from.where("listeComment = 1");
        } else if ( listeDuration == 1 ) {
            from.where("listeDuration = 1");
        } else if ( listeCommentDate == 1 ) {
            from.where("listeCommentDate = 1");
        }

        nbtotal_row = from.count();

        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());

        } catch(Exception  e) { }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Fragment STEP - onCreate " + this.getClass().getSimpleName());
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

        // Get back arguments
        if (getArguments() != null) {
            search = getArguments().getString("search", "");
            playlisteid = getArguments().getString("playlisteid", "");
        }

    }


    public void onEvent(DataBusVideo videoEvent) {

        Log.d(TAG, "Fragment STEP  onEvent " + videoEvent.ViewCount);

        videoList.get(videoEvent.position).setStatistics_viewCount(videoEvent.ViewCount);
        videoList.get(videoEvent.position).setStatistics_likeCount(videoEvent.LikeCount);
        videoList.get(videoEvent.position).setContentDetails_definition(videoEvent.infovideo_format);

    }
    public void onResume() {
        Log.d(TAG, "Fragment STEP  onResume " );

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();

        if ( clickpos!=-1 && nbtotal_row > 0 ) {
            Log.d(TAG, "Fragment STEP  onResume " + System.currentTimeMillis());
            Runnable runnable = new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Log.d(TAG, "Fragment STEP  onResume " + System.currentTimeMillis() );
                            getInfoBDD(0);
                        }
                    }, 500);
                }
            };
            getActivity().runOnUiThread(runnable);

        }

        adapter = new VideoSQLAdapter( getActivity() , videoList, R.layout.adapter_videoitem, R.layout.adapter_videoloading  );
        recyclerView.setAdapter(adapter);

        Log.d(TAG, "Fragment STEP  onResume pagerserver: " + pagerserver);

        if ( clickpos != -1 ) {
            Log.d(TAG, "Fragment STEP  onResume clickpos: " + clickpos + " - ID : " + videoList.get(clickpos).getId());


            Runnable runnable = new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            VideoSingleGson v = TableVideos.getSingleVideo(videoList.get(clickpos).getId());
                            Log.d(TAG, "Fragment STEP  onResume postDelayed titre: " + v.getInfo().getTitre() );
                            Log.d(TAG, "Fragment STEP  onResume postDelayed view: " + v.getInfo().getStatisticsViewCount() );

                            videoList.get(clickpos).setStatistics_viewCount(v.getInfo().getStatisticsViewCount());
                            videoList.get(clickpos).setStatistics_likeCount(v.getInfo().getStatisticsLikeCount());
                            videoList.get(clickpos).setContentDetails_definition(v.getInfo().getContentDetailsDefinition());

                            adapter.notifyItemChanged(clickpos);
                        }
                    }, 1000);
                }
            };

            getActivity().runOnUiThread(runnable);
        }



        linearLayoutManager.scrollToPosition(lastFirstVisiblePosition);

    }
    @Override
    public void onPause() {
        Log.d(TAG, "Fragment STEP  onPause");
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "Fragment STEP  - onPause " + this.getClass().getSimpleName());

        lastFirstVisiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();

    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            int adaptercount = adapter.getItemCount();

            //Si connecte  Internet et adapter vide - Rafraichir
            Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adaptercount : "+adaptercount );

            //Si le nombre d'elements dans adapter n'existe pas
            if ( adaptercount <= 1 ) {
                Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adaptercount vide " );

                if (connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adaptercount vide - Connecté nbtotal_row: "+nbtotal_row);
                    mSwipeRefreshLayout.setRefreshing(true);
                    //Si aucune donnee dans BDD
                    if ( nbtotal_row <= 0 ) {
                        setLoadingView();
                        getInfoServer(url, 0);
                    } else {
                        setListView();
                        getInfoBDD(0);
                    }

                }

                //Si non Connecté et adapter non vide
                if (!connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adaptercount vide - Non Connecté" );
                    mSwipeRefreshLayout.setRefreshing(false);
                    if ( nbtotal_row > 0 ) {
                        Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adaptercount vide - Non Connecté - nbtotal_row : "+nbtotal_row );
                        getInfoBDD(0);
                    } else {
                        setErrorView(getActivity().getString(R.string.error_non_connecte));
                    }
                }
            } else {
                Log.d(TAG, "Fragment STEP  onResume - BroadcastConnexion - onReceive - adapter non vide " );
                //Des elements existent mais ne sont peut être pas à jour
                if (connecte.isConnectingToInternet() ) {
                    Log.d(TAG, "Fragment STEP  - BroadcastConnexion - onReceive - adaptercount non vide - Connecté");
                    mSwipeRefreshLayout.setRefreshing(true);
                    getInfoServer(url, 0);
                }
            }

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "Fragment STEP  - onCreateView - " + this.getClass().getSimpleName());
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_videos_bdd, container, false);
        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {




        inflater.inflate(R.menu.menu_fragment_videos, menu);
        MenuItem item = menu.findItem(R.id.search);
        final SearchView sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);

        sv.setSuggestionsAdapter(mSearchViewAdapter);
        sv.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String titre = cursor.getString(1);
                String id = cursor.getString(3);
                String videoyt = cursor.getString(2);
                String contentDetails_definition = cursor.getString(4);
                //sv.setQuery(titre, false);
                sv.clearFocus();

                if (!contentDetails_definition.isEmpty())
                    showDetailIntent(new video(id, videoyt, contentDetails_definition));
                else {
                    //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();
                    showListVideosArtisteIntent(id, titre);
                }

                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String titre = cursor.getString(1);
                String id = cursor.getString(3);
                String videoyt = cursor.getString(2);
                String contentDetails_definition = cursor.getString(4);
                //sv.setQuery(titre, false);
                sv.clearFocus();

                if (!contentDetails_definition.isEmpty())
                    showDetailIntent(new video(id, videoyt, contentDetails_definition));
                else {
                    //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();
                    showListVideosArtisteIntent(id, titre);
                }

                //Toast.makeText(getActivity(), "titre: " + titre+" ("+id+")", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (query.length() > 2) {
                    loadData(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.length() > 2) {
                    loadData(query);
                }
                return false;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);

    }
    void showDetailIntent(video vid)
    {
        Log.d(TAG, "Fragment STEP VideoAdapter - showDetailIntent - uid : " + vid.getId() + " - videoytid :" + vid.getVideoytid());
        Intent VDetailActivity = new Intent( getActivity(), VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId());
        VDetailActivity.putExtra("videoytid", vid.getVideoytid());
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition());
        getActivity().startActivityForResult(VDetailActivity, 100);

    }

    void showListVideosArtisteIntent(String uid, String titre)
    {
        Intent VideoSearchActivity = new Intent( getActivity(), VideosByArtistActivity.class);
        VideoSearchActivity.putExtra("playlisteid", uid );
        VideoSearchActivity.putExtra("titre", titre );
        VideoSearchActivity.putExtra("opt", "artiste" );
        getActivity().startActivity(VideoSearchActivity);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sort:


                showDialogList( );
                break;
        }
        return true;
    }
    void showDialogList( ) {





         new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_sort)
                .items(R.array.sort_list)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {


                        old_option = option;

                        switch (which) {
                            case 0:
                                //Les plus récentes
                                sortfields = "uploadeddate_original";
                                enableSortField("listeNew");
                                break;
                            case 1:
                                //Les commentées récemment
                                sortfields = "publishedCommentLast";
                                enableSortField("listeCommentlast");
                                break;
                            case 2:
                                //Les plus vues hier
                                sortfields = "statistics_viewCount_hier";
                                enableSortField("listeViewHier");
                                break;
                            case 3:
                                //Nombre de vues
                                sortfields = "statistics_viewCount";
                                enableSortField("listeView");
                                break;
                            case 4:
                                //Nombre de likes
                                sortfields = "statistics_likeCount";
                                enableSortField("listeLike");
                                break;
                            case 5:
                                //Nombre de commentaires
                                sortfields = "statistics_commentCount";
                                enableSortField("listeComment");
                                break;
                            case 6:
                                //Durée
                                sortfields = "contentDetails_duration";
                                enableSortField("listeDuration");
                                break;
                        }

                        /*videoList.clear();
                        adapter.notifyDataSetChanged();
                        setLoadingInterface();
                        getInfo(url, 0, search);*/

                        //openSearchSortActivity();

                        String sortfields_label = "";

                        switch (sortfields) {

                            case "statistics_viewCount":
                                //Nombre de vues
                                sortfields_label = getString(R.string.sortopt_statistics_viewCount);
                                break;
                            case "statistics_likeCount":
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_statistics_likeCount);
                                break;
                            case "statistics_commentCount":
                                //Nombre de commentaires
                                sortfields_label = getString(R.string.sortopt_statistics_commentCount);
                                break;
                            case "contentDetails_duration":
                                //Durée
                                sortfields_label = getString(R.string.sortopt_contentDetails_duration);
                                break;
                            default:
                                sortfields_label = getString(R.string.sortopt_uploaded_date);
                                break;
                        }

                        MainActivity.setTitleActivity(sortfields_label);
                        if (connecte.isConnectingToInternet()) {
                           // saveBddOndetach();
                        }
                        Runnable runnable = new Runnable() {
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateAdapter();
                                    }
                                }, 300);
                            }
                        };
                        getActivity().runOnUiThread(runnable);


                    }
                })
                .show();
    }

    private void enableSortField(String field) {
        Log.d(TAG,"Fragment STEP enableSortField: "+field);

        option = field;

        listeView = 0;
        listeNew = 0;
        listeLike = 0;
        listeComment = 0;
        listeDuration = 0;
        listeCommentDate = 0;
        listeCommentlast = 0;
        listeViewHier = 0;

        switch(field) {
            case "listeView" :
                listeView = 1;
                break;
            case "listeCommentlast" :
                listeCommentlast = 1;
                break;
            case "listeViewHier" :
                listeViewHier = 1;
                break;
            case "listeNew" :
                listeNew = 1;
                break;
            case "listeLike" :
                listeLike = 1;
                break;
            case "listeComment" :
                listeComment = 1;
                break;
            case "listeDuration" :
                listeDuration = 1;
                break;
            case "listeCommentDate" :
                listeCommentDate = 1;
                break;
        }

        Log.d(TAG, "Fragment STEP enableSortField - option: " + option + " - listeNew: " + listeNew + " - listeView:" + listeView + " - listeLike:" + listeLike + " - listeComment:" + listeComment + " - listeDuration:" + listeDuration + " - listeCommentDate:" + listeCommentDate);
    }




    void updateAdapter(){
        Log.d(TAG, "Fragment STEP updateAdapter nbtotal_row: "+nbtotal_row+" - getNbtotal_row():"+getNbtotal_row());
        videoList.clear();
        adapter.notifyDataSetChanged();
        //if (getNbtotal_row() <= 0) {
            Log.d(TAG,"Fragment STEP updateAdapter getInfoServer");
            if (!connecte.isConnectingToInternet() ) {
                mSwipeRefreshLayout.setRefreshing(false);
                setErrorView(getActivity().getString(R.string.error_non_connecte));
                return;
            }

            setLoadingView();
            getInfoServer(url, 0);

        /*} else {
            Log.d(TAG,"updateAdapter getInfoBDD");
            setListView();
            getInfoBDD(0);
        }*/
    }

    private void openSearchSortActivity() {

        Intent i = new Intent(getActivity(), VideoSearchSortActivity.class);

        if ( search!=null && !search.isEmpty() ) {
            i.putExtra("search", search);
        }

        if ( sortfields!=null && !sortfields.isEmpty() ) {
            i.putExtra("sortfields", sortfields);
        }


        getActivity().startActivity(i);
    }

    public void setLoadingView()
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(final String error)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.VISIBLE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_error.findViewById(R.id.textView);
        Button btn_refresh = (Button) viewstub_recyclerview_error.findViewById(R.id.btn_refresh);

        textv.setText(error);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connecte.isConnectingToInternet()) {
                    VazotsaraShareFunc.showSnackMsg(error, getActivity());
                } else {
                    setLoadingView();
                    getInfoServer(url, 0);
                }
            }
        });
    }

    public void setEmptyView(String textempty)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.VISIBLE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_recyclerview_content.setVisibility(View.VISIBLE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(TAG, "Fragment STEP  - onActivityCreated - " + this.getClass().getSimpleName());
        super.onActivityCreated(savedInstanceState);



        mSearchViewAdapter = new VideoSearchSuggestionsAdapter( getActivity(), R.layout.adapter_videosearchsuggestion_item, null, columns,null, -1000);


        viewstub_recyclerview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_recyclerview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_recyclerview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_recyclerview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();


        recyclerView = (RecyclerView) viewstub_recyclerview_content.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view,final int position) {
                        Log.d(TAG,"Fragment STEP recyclerView Clic item "+videoList.get(position).getTitre());
                        Runnable runnable = new Runnable() {
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        clickpos = position;
                                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                                        VDetailActivity.putExtra("uid", videoList.get(position).getId());
                                        VDetailActivity.putExtra("videoytid", videoList.get(position).getVideoytid());
                                        VDetailActivity.putExtra("contentDetails_definition", videoList.get(position).getContentDetails_definition());
                                        VDetailActivity.putExtra("position", position);
                                        getActivity().startActivityForResult(VDetailActivity, 100);
                                    }
                                }, 1000);
                            }
                        };
                        getActivity().runOnUiThread(runnable);



                    }
                })
        );


        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {




                 current_page = pagerserver+1;

                 Log.d(TAG, "Fragment STEP - recyclerView.addOnScrollListener - page " + current_page+" - nbtotal_row: "+nbtotal_row+" - Config.LIMIT_GENERAL: "+Config.LIMIT_GENERAL+" - Charge+limite: "+(videoList.size()+Config.LIMIT_GENERAL));
                if ( connecte.isConnectingToInternet() ) {
                    //Si des rows existent dans la BDD
                    if ( (videoList.size()+Config.LIMIT_GENERAL) < nbtotal_row ) {
                        Log.d(TAG,"Fragment STEP - recyclerView.addOnScrollListener getInfoBDD:"+current_page);
                        getInfoBDD(current_page);
                     } else {
                        Log.d(TAG,"Fragment STEP - recyclerView.addOnScrollListener getInfoServer:"+current_page);
                        mSwipeRefreshLayout.setRefreshing(true);
                        getInfoServer(url, current_page);
                    }
                } else {
                    getInfoBDD(current_page);
                }

                currentPage = current_page;
            }
        });

        fab = (FloatingActionButton) viewstub_recyclerview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabVideoFragment(getActivity());
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_recyclerview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastVideo();
                /*videoList.add(0, new video( "id",  "titre",  "thumburl",  "videoytid",  "statistics_viewCount",  "statistics_likeCount",  "statistics_dislikeCount",  "statistics_commentCount",  "format",  "0", "", null, ""));
                videoList.add(1, new video( "id",  "titre",  "thumburl",  "videoytid",  "statistics_viewCount",  "statistics_likeCount",  "statistics_dislikeCount",  "statistics_commentCount",  "format",  "0", "", null, ""));
                videoList.add(2, new video("id", "titre", "thumburl", "videoytid", "statistics_viewCount", "statistics_likeCount", "statistics_dislikeCount", "statistics_commentCount", "format", "0", "", null, ""));

                adapter.notifyItemRangeInserted(0, 3);
                linearLayoutManager.scrollToPosition(0);*/


            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.material_red_400, R.color.ColorPrimaryDark, R.color.material_red_400);



    }

    void getLastVideo(){
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        //final String lastvideo = (VazotsaraShareFunc.getLastVideoidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastVideoidInPreferences(getActivity()):"");
        lastvideoId = TableLastcheck.getLastIdVideo();
        //lastvideo = TableVideos.getLastVideoId();


        //Si aucune video prendre la derniere dans la table tx_videos
        if ( lastvideoId.isEmpty() || Integer.parseInt(lastvideoId)==0 ) {
            lastvideoId = TableVideos.getLastVideoId();
        }

        Log.d(TAG, "Fragment STEP  getLastVideo() - lastvideo : " + lastvideoId);



            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListVideosBeforeId(lastvideoId, new Callback<VideoPojo>() {

                @Override
                public void success(VideoPojo serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if (serverinfo.getVideos().size() > 0 && !serverinfo.getVideos().get(0).getId().equals(lastvideoId)) {

                        Log.d(TAG, "Fragment STEP  getLastVideo() - New videos : " + serverinfo.getVideos().size());


                        setSuccessInfo(serverinfo, 0, true);
                        //Refresh
                        //addToList(serverinfo.getVideos());
                        //adapter.notifyDataSetChanged();

                        //Enregistrer dernière video dans Preferences
                        TableLastcheck.setLastIdVideo(serverinfo.getVideos().get(0).getId());

                        Log.d(TAG, "Fragment STEP  getLastVideo() - New lastvideo : " + serverinfo.getVideos().get(0).getId());

                        /*if ( VazotsaraShareFunc.getLastVideoidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastVideoidInPreferences( lastvideoid , getActivity().getApplicationContext());
                        */
                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.msg_confirm_videos_added), serverinfo.getVideos().size()), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Fragment STEP Error " + error);
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                }
            });




        mSwipeRefreshLayout.setRefreshing(false);

    }
    private void loadData(String searchText) {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getSearchVideo(searchText, "v", new Callback<SearchVideo>() {
            @Override
            public void success(SearchVideo searchResult, Response response) {
                //setSuccessInfo(videoPojo);
                //    Log.d(TAG,"searchResult.getResults() : "+searchResult.getResults().size());
                if (searchResult != null) {
                    MatrixCursor matrixCursor = convertToCursor(searchResult.getResults(), searchResult.getArtiste());
                    mSearchViewAdapter.changeCursor(matrixCursor);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d(TAG, "Fragment STEP searchResult.getResults() error : " + error.getMessage());

            }
        });

    }

     private MatrixCursor convertToCursor(List<SingleResultSearch> SingleResultSearch, List<SingleResultSearchArtiste> SingleResultSearchArtiste) {
        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;
         for (SingleResultSearchArtiste rowArtiste : SingleResultSearchArtiste) {
             String[] temp = new String[5];
             i = i + 1;
             temp[0] = Integer.toString(i);
             temp[1] = rowArtiste.getObjetnom();
             temp[2] = rowArtiste.getImage();
             temp[3] = rowArtiste.getId();
             temp[4] = "";


             Log.d(TAG,"Fragment STEP temp[1] : "+temp[1]);
             cursor.addRow(temp);
         }
        for (SingleResultSearch rowVideo : SingleResultSearch) {
            String[] temp = new String[5];
            i = i + 1;
            temp[0] = Integer.toString(i);
            temp[1] = rowVideo.getTitre();
            temp[2] = rowVideo.getVideoytid();
            temp[3] = rowVideo.getId();
            temp[4] = rowVideo.getContentDetails_definition();


            Log.d(TAG,"Fragment STEP temp[1] : "+temp[1]);
            cursor.addRow(temp);
        }


        return cursor;
    }
    void getInfoBDD(int page) {
        pagerserver = page;
        if ( page < videoList.size() / Config.LIMIT_GENERAL  ) {
            pagerserver = videoList.size() / Config.LIMIT_GENERAL;
        }

        Log.d(TAG,"Fragment STEP onResume pagerserver (BDD) : "+pagerserver);
        new loadSQL().execute( String.valueOf( page ) );
    }

    void saveBddOndetach() {
        new saveBddOndetach().execute();
    }

    void getInfoServer(String url, final int page )
    {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        //setLoadingInterface();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



        Log.d(TAG, "Fragment STEP  URL retrofit 608 : " + url + "/videos?limit=20&page=" + page + "&q=&sortfields=" + sortfields);


        if ( !search.isEmpty() ) {
            vidapi.getListVideos(20, sortfields, page, search, new Callback<VideoPojo>() {
                @Override
                public void success(VideoPojo videoPojo, Response response) {

                    setSuccessInfo(videoPojo, page);
                }

                @Override
                public void failure(RetrofitError error) {

                    setFailureInfo(error);
                }
            });
        } else {
            vidapi.getListVideos(20, sortfields, page, new Callback<VideoPojo>() {
                @Override
                public void success(VideoPojo videoPojo, Response response) {
                    setSuccessInfo(videoPojo, page);
                }

                @Override
                public void failure(RetrofitError error) {
                    setFailureInfo(error);
                }
            });
        }

    }

    void setSuccessInfo(VideoPojo videoPojo, int page){
        mSwipeRefreshLayout.setRefreshing(false);

        if (videoPojo.getPage() >= pagerserver ) {
            pagerserver = videoPojo.getPage();
        }

        Log.d(TAG, "Fragment STEP  pagerserver (serveur) : " + pagerserver);

        if (videoPojo.getVideos().size() > 0) {


            //VazotsaraShareFunc.saveInVideos(videoPojo.getVideos() );
            //addAsync(videoPojo.getVideos());
            addToListServer( videoPojo.getVideos()  );
            //Afficher info BDD
            //getInfoBDD( page );

            loaded = true;
        }
    }
    void addAsync(final List<Videogson> vgadded){
        Type resultType = new TypeToken<List<Videogson>>() {}.getType();

        final List<Videogson> vg = new ArrayList<Videogson>();

        Reservoir.getAsync("videoList", resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG, "Fragment STEP Reservoir fillCacheAdapter getAsync onSuccess");
                //success


                for (int count = 0; count < videos.size(); count++) {
                    vg.add(videos.get(count));
                    Log.d(TAG, "Fragment STEP Cache getTitre : " + videos.get(count).getTitre());
                }

                for (int count = 0; count < vgadded.size(); count++) {
                    vg.add(vgadded.get(count));
                    Log.d(TAG, "Fragment STEP Cache getTitre : " + vgadded.get(count).getTitre());
                }

                Reservoir.putAsync("videoList", vg, new ReservoirPutCallback() {
                    @Override
                    public void onSuccess() {
                        //success
                        Log.d(TAG, "Fragment STEP Reservoir putAsync onSuccess");
                        //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                    }

                    @Override
                    public void onFailure(Exception e) {


                        //error
                        Log.d(TAG, "Fragment STEP Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                    }
                });

            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG, "Fragment STEP Reservoir fillCacheAdapter getAsync onFailure " + e.getMessage());
                Reservoir.putAsync("videoList", vgadded, new ReservoirPutCallback() {
                    @Override
                    public void onSuccess() {
                        //success
                        Log.d(TAG, "Fragment STEP Reservoir putAsync onSuccess");
                        //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        //error
                        Log.d(TAG, "Fragment STEP Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                    }
                });
            }
        });
    }
    void setSuccessInfo(VideoPojo videoPojo, int page, boolean lastadded){
        mSwipeRefreshLayout.setRefreshing(false);
        if (videoPojo.getVideos().size() == 0 && nbtotal_row <= 0) {
            setEmptyView(getActivity().getString(R.string.error_no_item_video));
            loaded = true;
            return;
        } else {
            Log.d(TAG, "Fragment STEP - getInfo - L571 - success " + videoPojo.getVideos().get(0).getTitre());
            setListView();

            addAsync(videoPojo.getVideos());


            //VazotsaraShareFunc.saveInVideos(videoPojo.getVideos());
            //Afficher info BDD
            //getInfoBDD(page, videoPojo.getVideos().size(), lastadded );
            addToListServer( videoPojo.getVideos(), lastadded  );

            loaded = true;
        }
    }

    void setFailureInfo(RetrofitError error){
        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
        Log.d(TAG, "Fragment STEP - getInfo - L586 - fail " + this.getClass().getSimpleName());
        if (adapter.getItemCount() == 0)
            setErrorView(errorMessage);
        else {

            if ( getActivity() != null ) {
                VazotsaraShareFunc.showSnackMsg( errorMessage, getActivity());
            }
        }
    }


    void addToList(List<TableVideos> videos, int nbrowadded, String lastadded ) {
        int added = 0;
        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).serverid  ) ) {
                videoList.add(fillItem(videos.get(count)));
                added++;
                Log.d(TAG, "Fragment STEP  badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " ajouté dans adapter ");
            } else {
                Log.d(TAG, "Fragment STEP  badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " existe deja dans adapter ");
            }
        }

        if ( nbrowadded > 0 && !lastadded.isEmpty() ) {
            adapter.notifyItemRangeChanged(0, nbrowadded);
            linearLayoutManager.scrollToPosition(0);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
    void addToList(List<TableVideos> videos) {
        int added = 0;
        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).serverid  ) ) {
                videoList.add(fillItem(videos.get(count)));
                added++;
                Log.d(TAG, "Fragment STEP  badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " ajouté dans adapter ");
            } else {
                Log.d(TAG,"Fragment STEP  badapter - Nom : "+videos.get(count).titre+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
            }
        }
        adapter.notifyDataSetChanged();

    }

    video fillItem(TableVideos field) {

        video v = new video();
        v.setTitre(field.titre);
        v.setId(field.serverid);
        v.setVideoytid( field.videoytid);
        v.setStatistics_viewCount(String.valueOf(field.statisticsViewCount));
        v.setStatistics_likeCount(String.valueOf(field.statisticsLikeCount));
        v.setStatistics_dislikeCount( String.valueOf( field.statisticsDislikeCount));
        v.setThumburl(field.thumburl);

        v.setDuree( String.valueOf(field.contentDetailsDuration) );
        v.setFormat( field.contentDetailsDefinition);

        v.setContentDetails_definition(field.contentDetailsDefinition);

        return v;

    }

    void addToListServer(List<Videogson> videos, boolean lastadded) {
        if ( isAdded() )
            setListView();


        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                videoList.add(fillItemServer(videos.get(count)));
                addedserver++;
                Log.d(TAG, "Fragment STEP  badapter - Nom : " + videos.get(count).getTitre() + " - ID : " + videos.get(count).getId() + " ajouté dans adapter ");
            } else {
                Log.d(TAG,"Fragment STEP  badapter - Nom : "+videos.get(count).getTitre()+" - ID : "+videos.get(count).getId()+" existe deja dans adapter ");
            }
        }
        if ( addedserver > 0 && lastadded ) {
            adapter.notifyItemRangeChanged(0, addedserver);
            linearLayoutManager.scrollToPosition(0);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    void addToListServer(List<Videogson> videos) {
        if ( isAdded() )
            setListView();

        int added = 0;

        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                videoList.add(count, fillItemServer(videos.get(count)));
                addedserver++;
                Log.d(TAG, "Fragment STEP  badapter - Nom : " + videos.get(count).getTitre() + " - ID : " + videos.get(count).getId() + " ajouté dans adapter à la position : "+count);
            } else {
                Log.d(TAG,"Fragment STEP  STEP t badapter - Nom : "+videos.get(count).getTitre()+" - ID : "+videos.get(count).getId()+" existe deja dans adapter ");
            }
        }
        adapter.notifyDataSetChanged();
    }

    video fillItemServer(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId(videogson.getId());
        v.setVideoytid(videogson.getVideoytid());
        v.setStatistics_viewCount(videogson.getStatisticsViewCount());
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount());
        v.setStatistics_dislikeCount(videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        v.setContentDetails_definition(videogson.getContentDetailsDefinition());

        return v;

    }
    private class majSQLVideo extends AsyncTask<String, String, String>
    {



        @Override
        protected String doInBackground(String... strings) {
            Log.d(TAG,"Fragment STEP onResume majSQLVideo doInBackground" );
            if (clickpos>=0) {
                Log.d(TAG, "Fragment STEP onResume doInBackground ID : " + videoList.get(clickpos).getId());
                VideoSingleGson v = TableVideos.getSingleVideo(videoList.get(clickpos).getId());
                //v.getInfo().
                Log.d(TAG,"Fragment STEP onResume doInBackground titreYt : "+v.getInfo().getTitre() );
                Log.d(TAG,"Fragment STEP onResume doInBackground statisticsViewCount : "+v.getInfo().getStatisticsViewCount() );
                Log.d(TAG,"Fragment STEP onResume doInBackground statisticsLikeCount : "+v.getInfo().getStatisticsLikeCount() );
                videoList.get(clickpos).setStatistics_viewCount( v.getInfo().getStatisticsViewCount() );
                videoList.get(clickpos).setStatistics_likeCount(v.getInfo().getStatisticsLikeCount());
                videoList.get(clickpos).setContentDetails_definition( v.getInfo().getContentDetailsDefinition() );
            }
            publishProgress("1");
            return null;
        }

        @Override
        public void onProgressUpdate(String... params) {
            Log.d(TAG, "Fragment STEP loadSQL  onProgressUpdate");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (clickpos>=0) {
                        Log.d(TAG,"Fragment STEP onResume majSQLVideo onProgressUpdate" );
                        adapter.notifyDataSetChanged();
                    }
                }
            });

        }
    }

    int getNbtotal_row() {
        From from = new Select()
                .from(TableVideos.class)
        ;


        if ( listeNew == 1 ) {
            from.where("listeNew = 1");
        } else if ( listeView == 1 ) {
            from.where("listeView = 1");
        } else if ( listeComment == 1 ) {
            from.where("listeComment = 1");
        } else if ( listeDuration == 1 ) {
            from.where("listeDuration = 1");
        } else if ( listeCommentDate == 1 ) {
            from.where("listeCommentDate = 1");
        } else if ( listeLike == 1 ) {
            from.where("listeLike = 1");
        } else if ( listeCommentlast == 1 ) {
            from.where("listeCommentlast = 1");
        } else if ( listeViewHier == 1 ) {
            from.where("listeViewHier = 1");
        }

        Log.d(TAG,"Fragment STEP updateAdapter getNbtotal_row : "+from.toSql() );

        return from.count();
    }

    private class saveBddOndetach extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... strings) {

            Log.d(TAG,"Fragment STEP saveBddOndetach doInBackground - videoList.size():"+videoList.size()+" - option: "+option );

            if ( videoList.size() > 0 ) {

                Log.d(TAG,"Fragment STEP saveBddOndetach doInBackground - videoList.size():"+videoList.size()+" - nbtotal_row:"+nbtotal_row +" - getNbtotal_row():"+getNbtotal_row()+" - addedserver:"+addedserver);

                if (addedserver>0 || getNbtotal_row() < videoList.size() ) {

                    List<Videogson> vgson = new ArrayList<Videogson>();
                    for (int i = 0; i < videoList.size(); i++) {

                        video v = videoList.get(i);


                        Videogson e = new Videogson();
                        e.setId(v.getId());
                        e.setTitre(v.getTitre());
                        e.setVideoytid(v.getVideoytid());
                        e.setThumburl(v.getThumburl());
                        e.setStatisticsViewCount(v.getStatistics_viewCount());
                        e.setStatisticsLikeCount(v.getStatistics_likeCount());
                        e.setStatisticsDislikeCount(v.getStatistics_dislikeCount());
                        e.setContentDetailsDuration( v.getDuree() );


                        vgson.add(e);
                    }


                    VazotsaraShareFunc.saveInVideos(vgson, (!old_option.isEmpty()?old_option: option) );

                    switch(option) {
                        case "listeNew":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteNew())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteNew());
                            }
                            TableLastcheck.setLastPagelisteNew(String.valueOf(pagerserver));
                            break;
                        case "listeView":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteView())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteView());
                            }
                            TableLastcheck.setLastPagelisteView(String.valueOf(pagerserver));
                            break;
                        case "listeComment":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteComment())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteComment());
                            }
                            TableLastcheck.setLastPagelisteComment(String.valueOf(pagerserver));
                            break;
                        case "listeCommentDate":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteCommentDate())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteCommentDate());
                            }
                            TableLastcheck.setLastPagelisteCommentDate(String.valueOf(pagerserver));
                            break;
                        case "listeDuration":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteDuration())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteDuration());
                            }
                            TableLastcheck.setLastPagelisteDuration(String.valueOf(pagerserver));
                            break;
                        case "listeLike":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteLike())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteLike());
                            }
                            TableLastcheck.setLastPagelisteLike(String.valueOf(pagerserver));
                            break;
                        case "listeCommentlast":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteCommentlast())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteCommentlast());
                            }
                            TableLastcheck.setLastPagelisteCommentlast(String.valueOf(pagerserver));
                            break;
                        case "listeViewHier":
                            if (TableLastcheck.TableIsNotEmpty() && pagerserver < Integer.parseInt(TableLastcheck.getLastPagelisteViewHier())) {
                                pagerserver = Integer.parseInt(TableLastcheck.getLastPagelisteViewHier());
                            }
                            TableLastcheck.setLastPagelisteViewHier(String.valueOf(pagerserver));
                            break;

                    }

                    Log.d(TAG, "Fragment STEP saveBddOndetach doInBackground pagerserver:" + pagerserver );

                }
            }
            return null;
        }
    }
    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "Fragment STEP onResume loadFavoriVideoSQL  onPreExecute");
            mSwipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "Fragment STEP onResume loadFavoriVideoSQL  doInBackground");
            Log.d(TAG, "Fragment STEP onResume loadFavoriVideoSQL enableSortField - listeNew: " + listeNew + " - listeView:" + listeView + " - listeLike:" + listeLike + " - listeComment:" + listeComment + " - listeDuration:" + listeDuration + " - listeCommentDate:" + listeCommentDate);


            from = new Select()
                    .from(TableVideos.class)
            ;


            if ( listeNew == 1 ) {
                from.where("listeNew = 1");
            } else if ( listeView == 1 ) {
                from.where("listeView = 1");
            } else if ( listeComment == 1 ) {
                from.where("listeComment = 1");
            } else if ( listeDuration == 1 ) {
                from.where("listeDuration = 1");
            } else if ( listeCommentDate == 1 ) {
                from.where("listeCommentDate = 1");
            } else if ( listeLike == 1 ) {
                from.where("listeLike = 1");
            } else if ( listeCommentlast == 1 ) {
                from.where("listeCommentlast = 1");
            } else if ( listeViewHier == 1 ) {
                from.where("listeViewHier = 1");
            }

            nbtotal_row = from.count();


            from = from.orderBy( (sortfields.equals("publishedAt")?"endtime_uploaded DESC, publishedAt ":sortfields) + " DESC")
                    .limit(Config.LIMIT_GENERAL)
                    .offset(Integer.parseInt(params[0]) * Config.LIMIT_GENERAL);

            Log.d(TAG, "Fragment STEP onResume loadFavoriVideoSQL  doInBackground SQL "+from.toSql());

            List<TableVideos> videos = from.execute();

            for (int count = 0; count < videos.size(); count++) {
                if ( !adapter.idInAdapter( videos.get(count).serverid  ) ) {
                    videoList.add(count, fillItem(videos.get(count)));

                    Log.d(TAG, "Fragment STEP onResume badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " ajouté dans adapter via BDD à la position "+count);
                } else {
                    Log.d(TAG,"Fragment STEP onResume badapter - Nom : "+videos.get(count).titre+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
                }
            }
            adapter.setServerListSize(nbtotal_row);
            publishProgress("1");

            return params[0];
        }

        @Override
        public void onProgressUpdate(String... params) {
            Log.d(TAG, "Fragment STEP onResume loadSQL  onProgressUpdate");
            if ( !isAdded() ) {
                Log.d(TAG, "Fragment STEP onResume loadSQL  onProgressUpdate isAdded() false");
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if ( isAdded() )
                        setListView();

                    adapter.notifyDataSetChanged();
                    loaded = true;
                }
            });

        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(final String args)
        {
            Log.d(TAG, "Fragment STEP onResume loadSQL  onPostExecute");
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }







}

package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.PlaylistsBaseAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.TwoWayAdapterView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabhomeArtistsFragment extends Fragment {


    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int nb_video_show = 20;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    //RecyclerView recyclerView;
    TwoWayGridView grd_newartists, grd_artistsmaxvideos, grd_artistspopularvideos;
    SparseItemRemoveAnimator mSparseAnimator;

    Button btnNewArtists, btnArtistsMaxVideos;

    private SliderLayout mDemoSlider;




    List<Playlist> playListadd = new ArrayList<Playlist>();
    List<Playlist> playListmaxvideos = new ArrayList<Playlist>();
    List<Playlist> playListpopularvideos = new ArrayList<Playlist>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    PlaylistsBaseAdapter adapter_new_artists;
    PlaylistsBaseAdapter adapter_artists_maxvideos;
    PlaylistsBaseAdapter adapter_artists_popularvideos;


    Parcelable state;

    FloatingActionButton fab;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_fragment_home, menu);

        super.onCreateOptionsMenu(menu, inflater);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.login:
                VazotsaraShareFunc.openLoginActivity(getActivity());
                break;
            /*case R.id.logout:
                setLogout();
                break;*/
        }
        return true;
    }
    public void onStart() {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName(this.getClass().getSimpleName());
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch (Exception e) {
        }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "TabhomeVideosFragment - onResume - loaded : " + loaded);

        if (loaded)
            setListView();


    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        //state = listView.onSaveInstanceState();
        super.onPause();
        Log.d(TAG, "TabhomeVideosFragment - onPause");
    }

    public class BroadcastConnexion extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive : " + adapter_new_artists.getCount());

            if (connecte.isConnectingToInternet() && adapter_new_artists.getCount() - 1 == 0) {
                Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                setLoadingView();
                getInfo(url, 0);
            }


            //Si non connecte  Internet et isLoading()
            if (!connecte.isConnectingToInternet() && adapter_new_artists.getCount() - 1 > 0) {
                Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() > 0");

            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if (!connecte.isConnectingToInternet() && adapter_new_artists.getCount() - 1 == 0) {
                Log.d(TAG, "TabhomeVideosFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() == 0");

                setErrorView(getActivity().getString(R.string.error_non_connecte));

            }
        }
    }


    public TabhomeArtistsFragment() {
        // Required empty public constructor
    }

    public static TabhomeArtistsFragment newInstance() {
        TabhomeArtistsFragment frag = new TabhomeArtistsFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tabhome_artists, container, false);
        return rootView;
    }

    public void setLoadingView() {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error) {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty) {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView() {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (state != null) {
            Log.d(TAG, "TabhomeVideosFragment - Restore listview state..onViewCreated");

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewstub_listview_content = ((ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ((ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ((ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ((ViewStub) rootView.findViewById(R.id.loading)).inflate();



        //listView = (ListView) viewstub_listview_content.findViewById(R.id.list);
        grd_newartists = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_newartists);
        adapter_new_artists = new PlaylistsBaseAdapter(getActivity(), playListadd, R.layout.adapter_tabhome_artistsitem);
        grd_newartists.setAdapter(adapter_new_artists);



        grd_artistsmaxvideos = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_artistsmaxvideos);
        adapter_artists_maxvideos = new PlaylistsBaseAdapter(getActivity(), playListmaxvideos, R.layout.adapter_tabhome_artistsitem);
        grd_artistsmaxvideos.setAdapter(adapter_artists_maxvideos);

        grd_artistspopularvideos = (TwoWayGridView) viewstub_listview_content.findViewById(R.id.grd_artistspopularvideos);
        adapter_artists_popularvideos = new PlaylistsBaseAdapter(getActivity(), playListpopularvideos, R.layout.adapter_tabhome_artistsitem);
        grd_artistspopularvideos.setAdapter(adapter_artists_popularvideos);

        fab = (FloatingActionButton) viewstub_listview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabHome(getActivity());
            }
        });




        grd_newartists.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        //showListeVideoByPlaylists( ((TextView) view.findViewById(R.id.uid)).getText().toString(), ((TextView) view.findViewById(R.id.titre)).getText().toString() );
                        showListeVideoByPlaylists( playListadd.get(position) );
                    }
                }
        );

        grd_artistsmaxvideos.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        //showListeVideoByPlaylists( ((TextView) view.findViewById(R.id.uid)).getText().toString(), ((TextView) view.findViewById(R.id.titre)).getText().toString() );
                        showListeVideoByPlaylists( playListmaxvideos.get(position) );
                    }
                }
        );

        grd_artistspopularvideos.setOnItemClickListener(
                new TwoWayAdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                        //showListeVideoByPlaylists( ((TextView) view.findViewById(R.id.uid)).getText().toString(), ((TextView) view.findViewById(R.id.titre)).getText().toString() );
                        showListeVideoByPlaylists( playListpopularvideos.get(position) );
                    }
                }
        );



    }

    void showListeVideoByPlaylists( Playlist p  )
    {
        Intent VideoSearchActivity = new Intent(getActivity(), VideosByArtistActivity.class);
        EventBus.getDefault().postSticky(p);
        /*VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre );*/
        getActivity().startActivityForResult(VideoSearchActivity, 100);
    }

    void getInfo(String url, int page) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        Log.d(TAG, "TabhomeVideosFragment - getInfo - page : " + page);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("artiste", nb_video_show, page, "created", "desc", new Callback<Playlists>() {
            @Override
            public void success(Playlists videoPojo, retrofit.client.Response response) {
                if (videoPojo.getPlaylists().size() > 0) {


                    setListView();
                    addToList(videoPojo.getPlaylists(), playListadd);
                    adapter_new_artists.notifyDataSetChanged();
                    loaded = true;


                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        vidapi.getPlaylists("artiste", nb_video_show, page, "nb_videos", "desc", new Callback<Playlists>() {
            @Override
            public void success(Playlists videoPojo, retrofit.client.Response response) {
                if (videoPojo.getPlaylists().size() > 0) {


                    setListView();
                    addToList(videoPojo.getPlaylists(), playListmaxvideos);
                    adapter_artists_maxvideos.notifyDataSetChanged();
                    loaded = true;
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        vidapi.getPlaylists("artiste", nb_video_show, page, "videopopular", "desc", new Callback<Playlists>() {
            @Override
            public void success(Playlists videoPojo, retrofit.client.Response response) {
                if (videoPojo.getPlaylists().size() > 0) {


                    setListView();
                    addToList(videoPojo.getPlaylists(), playListpopularvideos);
                    adapter_artists_popularvideos.notifyDataSetChanged();
                    loaded = true;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG,"Error vidapi.getPlaylists videopopular : "+error.getMessage());
            }
        });




    }

    void addToList(List<Playlist> videos, List<Playlist> playListadd) {
        for (int count = 0; count < videos.size(); count++) {
            playListadd.add(fillItem(videos.get(count)));
        }
    }


    Playlist fillItem(Playlist videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getNom());
        v.setObjetnom(videogson.getObjetnom());
        v.setId(videogson.getId());
        v.setNbVideos(videogson.getNbVideos());
        v.setImage(videogson.getImage());
        v.setObjetype(videogson.getObjetype());
        v.setStatus(videogson.getStatus());
        v.setDescriptionYt(videogson.getDescriptionYt());
        return v;
    }
}

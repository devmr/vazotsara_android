package vazo.tsara.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import vazo.tsara.R;
import vazo.tsara.adapters.ArtistsTWVAdapter;
import vazo.tsara.adapters.AutocompleteAdapterAddplaylist;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.DelayAutoCompleteTextView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.FieldAddPlaylist;
import vazo.tsara.models.FieldUpdateVideo;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.relatedvideomodel;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoEditFragment extends Fragment {

    String uid;
    String idartista;
    static String TAG = Config.TAGKEY;

    String o_titre;
    String o_description;
    String o_status;
    String o_category;
    String o_artist;
    String o_genre;
    String o_realisateur;
    String o_playlist;

    View rootView;

    MaterialSpinner spinn_status, spinn_category;


    TextView idartist;
    TextView thumburlpath;
    TextView nbvideos;

    TextView firstvideoyt;
    TextView playlisteyt;


    TwoWayGridView twgv_artists;
    TwoWayGridView twgv_genre;
    TwoWayGridView twgv_realisateur;
    TwoWayGridView twgv_playlists;



    Button btn_add_artist;
    Button btn_add_genre;
    Button btn_add_realisateur;
    Button btn_add_playlists;
    Button btn_source_hideshow;
    Button btn_fill;


    EditText edit_text_titre;
    EditText edit_text_description;



    View positiveAction;
    View neutralAction;

    DelayAutoCompleteTextView dialogautocomplete;


    private ArrayAdapter<CharSequence>  adapter_status, adapter_category;

    List<relatedvideomodel> artistelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> realisateurlist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> genrelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> playlistlist = new ArrayList<relatedvideomodel>();

    ArtistsTWVAdapter artisterecycleradapter ;
    ArtistsTWVAdapter genrerecycleradapter ;
    ArtistsTWVAdapter playlistsrecycleradapter ;
    ArtistsTWVAdapter realisateurrecycleradapter ;

    ActionProcessButton btnSignIn;

    String[] idartistsImplode;
    String[] idgenreImplode;
    String[] idplaylistsImplode;
    String[] idrealisateurImplode;

    String[] o_idartistsImplode;
    String[] o_idgenreImplode;
    String[] o_idplaylistsImplode;
    String[] o_idrealisateurImplode;



    RelativeLayout source;


    private String[] idCategoryYt = { "15","2","24", "27", "1", "23", "20", "10", "29", "22", "28", "17", "26", "19" };


    String videoytid;




    public VideoEditFragment() {
        // Required empty public constructor
    }

    public VideoEditFragment newInstance(String uid ) {
        VideoEditFragment frag = new VideoEditFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        frag.setArguments(args);
        return frag;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        uid = getArguments().getString("uid", "");



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_video, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        initView();





        //String[] ITEMS = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"};
        adapter_status = ArrayAdapter.createFromResource(getActivity(), R.array.status_video, android.R.layout.simple_spinner_item);

        //adapter_status = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, ITEMS  );
        adapter_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_status);

        //Selectionner "public" par defaut
        Log.d(TAG,"Arrays.asList :"+Arrays.asList( getResources().getStringArray(R.array.status_video) ).size());
        Log.d(TAG, "Arrays.asList : " + Arrays.asList(getResources().getStringArray(R.array.status_video)).indexOf("Public"));
        Log.d(TAG, "Arrays.asList :" + adapter_status.getPosition("Public"));




        adapter_category = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.status_category)  );
        adapter_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_category.setAdapter(adapter_category);




        getInfoVideo(uid);

        btn_add_artist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("a", artistelist, artisterecycleradapter);
            }
        });

        btn_add_playlists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("o", playlistlist, playlistsrecycleradapter);
            }
        });

        btn_add_genre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("g", genrelist, genrerecycleradapter);
            }
        });

        btn_add_realisateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("r", realisateurlist, realisateurrecycleradapter);
            }
        });



        twgv_artists.setAdapter(artisterecycleradapter);
        twgv_genre.setAdapter(genrerecycleradapter);
        twgv_playlists.setAdapter(playlistsrecycleradapter);
        twgv_realisateur.setAdapter(realisateurrecycleradapter);

        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);

        btn_fill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String titre = edit_text_titre.getText().toString();
                String[] nomartistsImplode = new String[artisterecycleradapter.getCount()];
                String[] descartistsImplode = new String[artisterecycleradapter.getCount()];

                if (titre.length() > 0) {
                    //Si le titre contient "::"
                    if (titre.contains("::"))
                        titre = titre.substring(0, titre.indexOf("::"));


                    for (int n = 0; n < artisterecycleradapter.getCount(); n = n + 1) {
                        nomartistsImplode[n] = artisterecycleradapter.getFieldAt(n, "nom");

                        if (!artisterecycleradapter.getFieldAt(n, "firstvideoid").isEmpty())
                            descartistsImplode[n] = "- Playliste " + artisterecycleradapter.getFieldAt(n, "nom") + " : https://www.youtube.com/watch?v=" + artisterecycleradapter.getFieldAt(n, "firstvideoid") + "&list=" + artisterecycleradapter.getFieldAt(n, "playlisteid");

                        titre = titre.replaceAll("(?i)" + artisterecycleradapter.getFieldAt(n, "nom").trim(), "");
                    }


                    //Prendre 100 caracteres
                    titre = titre.trim();

                    //Supprimer espace en debut
                    if (titre != null && titre.length() > 0 && titre.charAt(0) == '-')
                        titre = titre.substring(1).trim();


                    titre = (titre.length() > 100 ? titre.substring(0, 97) + "..." : titre);


                    edit_text_titre.setText(titre + "::" + TextUtils.join(",", nomartistsImplode));
                    edit_text_description.setText(TextUtils.join("\n", descartistsImplode));
                }


            }
        });

        //Reinitialiser le button signin si le texte a été modifié
        Log.d(TAG,"btnSignIn.getProgress(): "+btnSignIn.getProgress());

        edit_text_titre.addTextChangedListener(watcherBtnSignIn);
        edit_text_description.addTextChangedListener(watcherBtnSignIn);
        spinn_category.setOnItemSelectedListener(spinnChangeBtnSignin);
        spinn_status.setOnItemSelectedListener(spinnChangeBtnSignin);








        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressGenerator.start(btnSignIn);


                Log.d(TAG, "noErrorOnSubmit --- titre.isEmpty()? : " + edit_text_titre.getText().toString().isEmpty() + " - titre.contains(\"::\")? " + edit_text_titre.getText().toString().contains("::") + " - artisterecycleradapter.getCount() : " + artisterecycleradapter.getCount());

                if (noErrorOnSubmit()) {

                    saveForm();
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_connexion_submit), getActivity());
                }


            }
        });





    }



    void saveForm() {





        idartistsImplode = new String[artisterecycleradapter.getCount()];
        String[] nomartistsImplode = new String[artisterecycleradapter.getCount()];

        idgenreImplode = new String[genrerecycleradapter.getCount()];
        String[] nomgenreImplode = new String[artisterecycleradapter.getCount()];

        idrealisateurImplode = new String[realisateurrecycleradapter.getCount()];
        String[] nomrealisateurImplode = new String[artisterecycleradapter.getCount()];

        idplaylistsImplode = new String[playlistsrecycleradapter.getCount()];
        String[] nomplaylistsImplode = new String[artisterecycleradapter.getCount()];


        for (  int n = 0 ; n < artisterecycleradapter.getCount(); n = n+1 ) {
            idartistsImplode[n] = artisterecycleradapter.getFieldAt(n,"id");
            nomartistsImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < genrerecycleradapter.getCount(); n = n+1 ) {
            idgenreImplode[n] = genrerecycleradapter.getFieldAt(n,"id");
            nomgenreImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < realisateurrecycleradapter.getCount(); n = n+1 ) {
            idrealisateurImplode[n] = realisateurrecycleradapter.getFieldAt(n,"id");
            nomrealisateurImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < playlistsrecycleradapter.getCount(); n = n+1 ) {
            idplaylistsImplode[n] = playlistsrecycleradapter.getFieldAt(n,"id");
            nomplaylistsImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }

        Log.d(TAG, "Implode idartistsImplode : " + TextUtils.join(",", idartistsImplode));
        Log.d(TAG, "Implode idgenreImplode : " + TextUtils.join(",", idgenreImplode));
        Log.d(TAG, "Implode idrealisateurImplode : " + TextUtils.join(",", idrealisateurImplode));
        Log.d(TAG, "Implode idplaylistsImplode : " + TextUtils.join(",", idplaylistsImplode));

        String titre = edit_text_titre.getText().toString();
        String description = edit_text_description.getText().toString();
        String status = spinn_status.getSelectedItem().toString();
        String category = idCategoryYt[spinn_category.getSelectedItemPosition()-1];




                    /*if ( artisterecycleradapter.getCount() > 0 ) {
                        Log.d(TAG,"img 0 : "+artisterecycleradapter.getFieldAt(0,"id"));
                        Log.d(TAG,"img Lenght : "+artisterecycleradapter.getCount());
                    }*/
                    /*final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnSignIn.setProgress(100);
                        }
                    }, 5000);*/

        //Valider


        switch(status) {
            case "Public" :
                status = "public";
                break;
            default :
                status = "unlisted";
                break;
            case "Privé" :
                status = "private";
                break;
        }

        Log.d(TAG, "Is Admin? " + Useful.isAdmin(getActivity()));


        Log.d(TAG, "Form values // " +
                        "titre : " + titre + " - " +
                        "artist: " + TextUtils.join(",", idartistsImplode) + " - " +
                        "realisateur: " + TextUtils.join(",", idrealisateurImplode) + " - " +
                        "genre:" + TextUtils.join(",", idgenreImplode) + " - " +
                        "playlist:" + TextUtils.join(",", idplaylistsImplode) + " - " +
                        "status: " + status + " - " +
                        "category: " + category + " - " +
                        "description:" + description + " - " +
                        "o_idartistsImplode: " + o_artist + " - " +
                        "o_idrealisateurImplode: " + o_realisateur + " - " +
                        "o_idgenreImplode: " + o_genre + " - " +
                        "o_idplaylistsImplode: " + o_playlist + " - " +
                        "Config.CREATEDBY : " + Config.CREATEDBY
        );

        if ( isFieldEdited( titre, description, status, category, TextUtils.join(",", idartistsImplode), TextUtils.join(",", idgenreImplode), TextUtils.join(",", idrealisateurImplode),TextUtils.join(",", idplaylistsImplode)  ) ) {
            btnSignIn.setEnabled(false);
            btnSignIn.setProgress(1);

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            if ( Useful.isAdmin(getActivity()) ) {
                                request.addHeader("Authorization", " Bearer "+VazotsaraShareFunc.getAdminToken(getActivity()));
                            }
                        }
                    })
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
            vidapi.postUpdateVideo(
                    uid,
                    new FieldUpdateVideo(
                            titre,
                            TextUtils.join(",", idartistsImplode),
                            TextUtils.join(",", idrealisateurImplode),
                            TextUtils.join(",", idgenreImplode),
                            TextUtils.join(",", idplaylistsImplode),
                            status,
                            category,
                            description,
                            o_artist,
                            o_realisateur,
                            o_genre,
                            o_playlist,
                            Config.CREATEDBY,
                            videoytid
                    ), new Callback<TokenPojo>() {
                        @Override
                        public void success(TokenPojo sourceinfo, retrofit.client.Response response) {


                            VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), getActivity());
                            btnSignIn.setProgress(100);


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            btnSignIn.setProgress(-1);
                            btnSignIn.setEnabled(true);

                            String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                            Log.d(TAG, "errorMessage : " + errorMessage);


                            //Log.d(TAG, "postLogin - error : " + error.getMessage());
                            if (error.getResponse() != null) {
                                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                                try {
                                    JSONObject jsonObj = new JSONObject(json);
                                    Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));

                                    VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {

                                VazotsaraShareFunc.showSnackMsg(errorMessage, getActivity());
                            }
                        }
                    });
        } else {
            btnSignIn.setProgress(0);
            btnSignIn.setEnabled(true);

            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_noedit), getActivity());
        }



    }

    void initView(){
        spinn_status = (MaterialSpinner) rootView.findViewById(R.id.spinn_status);
        spinn_category = (MaterialSpinner) rootView.findViewById(R.id.spinn_category);

        edit_text_titre = (EditText) rootView.findViewById(R.id.edit_text_titre);
        edit_text_description = (EditText) rootView.findViewById(R.id.edit_text_description);





        twgv_artists = (TwoWayGridView) rootView.findViewById(R.id.twgv_artists);
        twgv_realisateur = (TwoWayGridView) rootView.findViewById(R.id.twgv_realisateur);
        twgv_genre = (TwoWayGridView) rootView.findViewById(R.id.twgv_genre);
        twgv_playlists = (TwoWayGridView) rootView.findViewById(R.id.twgv_playlists);



        btn_add_artist = (Button) rootView.findViewById(R.id.btn_add_artist);
        btn_add_genre = (Button) rootView.findViewById(R.id.btn_add_genre);
        btn_add_realisateur = (Button) rootView.findViewById(R.id.btn_add_realisateur);
        btn_add_playlists = (Button) rootView.findViewById(R.id.btn_add_playlists);
        btn_source_hideshow = (Button) rootView.findViewById(R.id.btn_source_hideshow);
        btn_fill = (Button) rootView.findViewById(R.id.btn_fill);


        artisterecycleradapter = new ArtistsTWVAdapter(getActivity(),artistelist, R.layout.adaper_artiste_item_add  );
        genrerecycleradapter = new ArtistsTWVAdapter(getActivity(),genrelist, R.layout.adaper_artiste_item_add  );
        playlistsrecycleradapter = new ArtistsTWVAdapter(getActivity(),playlistlist, R.layout.adaper_artiste_item_add  );
        realisateurrecycleradapter = new ArtistsTWVAdapter(getActivity(),realisateurlist, R.layout.adaper_artiste_item_add  );

        btnSignIn = (ActionProcessButton) rootView.findViewById(R.id.btnSignIn);

        source = (RelativeLayout) rootView.findViewById(R.id.source);



    }

    void showDialogAdd(final String type, final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter)
    {
        int intTitle = R.string.dialog_addartiste;
        switch (type) {
            case "a" :
                //artiste
                intTitle = R.string.dialog_addartiste;
                break;
            case "g" :
                //genre
                intTitle = R.string.dialog_addgenre;
                break;
            case "r" :
                //realisateur
                intTitle = R.string.dialog_addrealisateur;
                break;
            case "o" :
                //playlist
                intTitle = R.string.dialog_addplaylist;
                break;
        }
         MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(intTitle)
                .customView(R.layout.dialog_addplaylist_invideo, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .neutralText(R.string.dialog_addvideo_btn_create)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .neutralColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        idartista = idartist.getText().toString();



                        if (!dialogautocomplete.getText().toString().isEmpty() && !idartista.isEmpty() && Integer.parseInt(idartista)>0 ) {
                            addplaylist(liste, adapter);
                        } else {
                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_choice_playlist), getActivity());
                            return;
                        }






                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        //Créer l'item
                        if ( Integer.parseInt( idartist.getText().toString() ) > 0 ) {
                            addplaylist(liste, adapter);
                        } else if( !dialogautocomplete.getText().toString().isEmpty() ){
                            createplaylist(type, liste, adapter);
                        } else {
                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_create_playlist), getActivity());
                        }

                    }

                }).build();



        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        neutralAction = dialog.getActionButton(DialogAction.NEUTRAL);
        //noinspection ConstantConditions

        dialogautocomplete = (DelayAutoCompleteTextView) dialog.getCustomView().findViewById(R.id.dialogautocomplete);

        dialogautocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    neutralAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        idartist = (TextView) dialog.getCustomView().findViewById(R.id.idartist);
        thumburlpath = (TextView) dialog.getCustomView().findViewById(R.id.thumburlpath);
        nbvideos = (TextView) dialog.getCustomView().findViewById(R.id.nbvideos);

        firstvideoyt = (TextView) dialog.getCustomView().findViewById(R.id.firstvideoyt);
        playlisteyt = (TextView) dialog.getCustomView().findViewById(R.id.playlisteyt);

        dialogautocomplete.setThreshold(1);
        Log.d(TAG, "showDialogAdd type : " + type);
        dialogautocomplete.setAdapter(new AutocompleteAdapterAddplaylist(getActivity(), type));

        dialogautocomplete.setLoadingIndicator((ProgressBar) dialog.getCustomView().findViewById(R.id.progressBar));
        dialogautocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Log.d(TAG, "adapterView.getAdapter().getItem(i).toString()" + ((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setText(((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setSelection(dialogautocomplete.getText().length());

                idartist.setText(((TextView) view.findViewById(R.id.id)).getText().toString());
                thumburlpath.setText(((TextView) view.findViewById(R.id.thumburlpath)).getText().toString());
                nbvideos.setText(((TextView) view.findViewById(R.id.nbvideos)).getText().toString());

                firstvideoyt.setText(((TextView) view.findViewById(R.id.firstvideoyt)).getText().toString());
                playlisteyt.setText(((TextView) view.findViewById(R.id.playlisteyt)).getText().toString());

                if ( Integer.parseInt( idartist.getText().toString() ) > 0 )
                    positiveAction.setEnabled(true);



            }
        });

        dialog.show();

        positiveAction.setEnabled(false); // disabled by default
        neutralAction.setEnabled(false); // disabled by default
    }

    void addplaylist(final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter) {
        relatedvideomodel a = new relatedvideomodel();
        a.setUid(idartista);
        a.setObjetnom(dialogautocomplete.getText().toString());
        a.setNbVideos(nbvideos.getText().toString());
        a.setPathplayliste("");
        a.setPathplayliste(thumburlpath.getText().toString());
        a.setFirstvideoid(firstvideoyt.getText().toString());
        a.setPlaylisteid(playlisteyt.getText().toString());
        liste.add(a);
        adapter.notifyDataSetChanged();
    }

    void createplaylist(String type, final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter) {


        String objetype = "artiste";

        switch (type) {
            case "a" :
                //artiste
                objetype = "artiste";
                break;
            case "g" :
                //genre
                objetype = "genre";
                break;
            case "r" :
                //realisateur
                objetype = "realisateur";
                break;
            case "o" :
                //playlist
                objetype = "autre";
                break;
        }

        //Creer sur le serveur
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(getActivity()) ) {
                            request.addHeader("Authorization", " Bearer "+ VazotsaraShareFunc.getAdminToken(getActivity()));
                        }
                    }
                })
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.postCreatePlaylist(
                new FieldAddPlaylist(
                        dialogautocomplete.getText().toString(),
                        objetype,
                        "",
                        "public",
                        Config.CREATEDBY
                ), new Callback<TokenPojo>() {
                    @Override
                    public void success(TokenPojo sourceinfo, retrofit.client.Response response) {


                        VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), getActivity());
                        relatedvideomodel a = new relatedvideomodel();
                        a.setUid(sourceinfo.getId());
                        a.setObjetnom(dialogautocomplete.getText().toString());
                        a.setNbVideos("0");
                        a.setPathplayliste("");
                        a.setFirstvideoid("");
                        a.setPlaylisteid("");
                        liste.add(a);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                        Log.d(TAG, "errorMessage : " + errorMessage);


                        //Log.d(TAG, "postLogin - error : " + error.getMessage());
                        if (error.getResponse() != null) {
                            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                            try {
                                JSONObject jsonObj = new JSONObject(json);
                                Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));

                                VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                        }
                    }
                });
    }

    public boolean noErrorOnSubmit(){
        String titre = edit_text_titre.getText().toString();

        if( titre.isEmpty() )
            return false;

        if( !titre.contains("::") )
            return false;

        if ( artisterecycleradapter.getCount() == 0 )
            return false;



        return true;


    }




    void getInfoVideo(String id) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit apipl = restAdapter.create(ApiRetrofit.class);
        apipl.getSingleVideo(id, new Callback<VideoSingleGson>() {
            @Override
            public void success(VideoSingleGson servInfo, Response response) {
                String statut;

                Log.d(TAG, "getInfoVideo Success " + (servInfo.getInfo().getTitre() != null ? servInfo.getInfo().getTitre() : ""));

                o_titre = servInfo.getInfo().getTitre();
                o_description = servInfo.getInfo().getDescriptionYt();
                o_status = servInfo.getInfo().getStatusyt();
                o_category = servInfo.getInfo().getCategoryYt();
                videoytid = servInfo.getInfo().getVideoytid();

                edit_text_titre.setText(o_titre);

                edit_text_description.setText( o_description );

                switch ( o_status ) {
                    case "public" :
                        statut = "Public";
                        break;
                    case "unlisted" :
                        statut = "Non répertorié";
                        break;
                    default:
                        statut = "Privé";
                        break;
                }

                spinn_status.setSelection( adapter_status.getPosition( statut )+1 );
                spinn_category.setSelection( adapter_category.getPosition( "Musique" )+1 );

                //Remplir Twowaygridview
                if ( servInfo.getPlaylisteinfo().size()>0 ) {

                    //o_idartistsImplode = new String[artisterecycleradapter.getCount()];
                    ArrayList<String> o_idartistsImplode = new ArrayList<String>();
                    //o_idgenreImplode = new String[genrerecycleradapter.getCount()];
                    ArrayList<String> o_idgenreImplode = new ArrayList<String>();
                    //o_idrealisateurImplode = new String[realisateurrecycleradapter.getCount()];
                    ArrayList<String> o_idrealisateurImplode = new ArrayList<String>();
                    //o_idplaylistsImplode = new String[playlistsrecycleradapter.getCount()];
                    ArrayList<String> o_idplaylistsImplode = new ArrayList<String>();

                    for( int i = 0; i < servInfo.getPlaylisteinfo().size(); i++ ) {

                        relatedvideomodel a = new relatedvideomodel();
                        a.setUid(servInfo.getPlaylisteinfo().get(i).getId());
                        a.setObjetnom(servInfo.getPlaylisteinfo().get(i).getObjetnom());
                        a.setNbVideos(servInfo.getPlaylisteinfo().get(i).getNbVideos());
                        a.setPathplayliste( String.format( Config.PATHPLAYLIST, servInfo.getPlaylisteinfo().get(i).getImage() ) );
                        a.setFirstvideoid(servInfo.getPlaylisteinfo().get(i).getFirstvideoid());
                        a.setPlaylisteid(servInfo.getPlaylisteinfo().get(i).getPlaylisteid() );





                        if ( servInfo.getPlaylisteinfo().get(i).getObjetype().equals("artiste") ) {
                            artistelist.add(a);
                            artisterecycleradapter.notifyDataSetChanged();
                            o_idartistsImplode.add(servInfo.getPlaylisteinfo().get(i).getId() );
                            Log.d(TAG,"i: "+i+" - artisterecycleradapter.getCount():"+artisterecycleradapter.getCount()+" - "+artisterecycleradapter.getFieldAt(0,"id"));


                        } else if( servInfo.getPlaylisteinfo().get(i).getObjetype().equals("realisateur") ) {
                            realisateurlist.add(a);
                            realisateurrecycleradapter.notifyDataSetChanged();
                            Log.d(TAG, "i: " + i + " - artisterecycleradapter.getCount():" + realisateurrecycleradapter.getCount() + " - " + realisateurrecycleradapter.getFieldAt(0, "id"));
                            o_idrealisateurImplode.add(servInfo.getPlaylisteinfo().get(i).getId() );


                        } else if( servInfo.getPlaylisteinfo().get(i).getObjetype().equals("genre") ) {
                            genrelist.add(a);
                            genrerecycleradapter.notifyDataSetChanged();
                            Log.d(TAG, "i: " + i + " - artisterecycleradapter.getCount():" + genrerecycleradapter.getCount() + " - " + genrerecycleradapter.getFieldAt(0, "id"));
                            o_idgenreImplode.add(servInfo.getPlaylisteinfo().get(i).getId());

                        } else {
                            playlistlist.add(a);
                            playlistsrecycleradapter.notifyDataSetChanged();
                            Log.d(TAG, "i: " + i + " - artisterecycleradapter.getCount():" + playlistsrecycleradapter.getCount() + " - " + playlistsrecycleradapter.getFieldAt(0, "id"));
                            o_idplaylistsImplode.add(servInfo.getPlaylisteinfo().get(i).getId());

                        }

                    }

                    o_artist = TextUtils.join(",", o_idartistsImplode);
                    Log.d(TAG,"o_artist:"+o_artist);
                    o_genre = TextUtils.join(",", o_idgenreImplode);
                    o_realisateur = TextUtils.join(",", o_idrealisateurImplode);
                    o_playlist = TextUtils.join(",", o_idplaylistsImplode);

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG,"getInfoVideo failure "+(error.getMessage()!=null?error.getMessage():""));
            }
        });
    }

    boolean isFieldEdited(String titre, String description, String status, String category, String artist, String genre, String realisateur, String playlist) {
        if ( !o_titre.equals( titre ) )
            return true;

        if ( !o_description.equals( description ) )
            return true;

        if ( !o_status.equals( status ) )
            return true;

        if ( !o_category.equals( category ) )
            return true;

        if ( !o_artist.equals( artist ) )
            return true;

        if ( !o_genre.equals( genre ) )
            return true;

        if ( !o_realisateur.equals( realisateur ) )
            return true;

        if ( !o_playlist.equals( playlist ) )
            return true;

        return false;
    }

    TextWatcher watcherBtnSignIn = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            btnSignIn.setEnabled(true);
            btnSignIn.setProgress(0);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    AdapterView.OnItemSelectedListener spinnChangeBtnSignin = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            btnSignIn.setEnabled(true);
            btnSignIn.setProgress(0);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

}

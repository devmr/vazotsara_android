package vazo.tsara.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import vazo.tsara.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VazotsaraintroFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class VazotsaraintroFragment extends Fragment {

    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    int layoutResId;


    public VazotsaraintroFragment() {
        // Required empty public constructor
    }

    public static VazotsaraintroFragment newInstance(int layoutResId) {
        VazotsaraintroFragment sampleSlide = new VazotsaraintroFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID))
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.hello_blank_fragment);
        return textView;
    }



}

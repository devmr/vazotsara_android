package vazo.tsara.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.IconButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.facebook.share.widget.LikeView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.inthecheesefactory.lib.fblike.widget.FBLikeView;
import com.joanzapata.android.iconify.Iconify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.LoginActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.activities.VideosByRealisateurActivity;
import vazo.tsara.adapters.ArtistsTWVAdapter;
import vazo.tsara.adapters.RealisateurTWVAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.TwoWayAdapterView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.FavoriVideoSQL;
import vazo.tsara.models.OptionBusEvent;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.relatedvideomodel;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.Dbhandler;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailTabInfoFragment extends Fragment {

    View rootView;
    static String TAG = "VAZOTSARA";
    String uid;
    boolean videoidInFavori = false;

    Dbhandler db;

    BroadcastConnexion receiver;
    ConnectionDetector detector;

    private TwoWayGridView artistes_list;
    private TwoWayGridView realisateur_list;

    private YouTubePlayerView youTubeView;
    private YouTubePlayer player;

    TextView titrevideo, nbview, nblike, nbdislike, nbcomment, hdformat, headerartists, headerrealisateur, action_favorite;
    //HtmlTextView html_text;
    RelativeTimeTextView v;
    IconButton btn_favori, btn_share;

    View vsload, vscontent_detail, vserror_nonconnecte;

    YouTubePlayerSupportFragment youTubePlayerFragment;

    VideoDetailActivity mActivity;

    Boolean loaded = false;
    Boolean showsnack = false;

    List<relatedvideomodel> artistelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> realisationlist = new ArrayList<relatedvideomodel>();

    RelativeLayout bloc_addfavorite;

    //RecyclerView artistes_list;

    String infovideo_titre, infovideo_format, infovideo_videoyt, msg_confirm;

    Playlist plartiste;
    Playlist plrealisateur;

    int position;

    FBLikeView fbLikeView;
    LikeView likeView;

    TextView bloc_addfavorite_text;

    Videogson vgson;

    int nbrow = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        detector = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");
        nbrow =  TableVideos.getCountSingleVideo(uid);
        position = getArguments().getInt("position" );


    }
    public void onDetach(){
        Log.d(TAG, "Fragment STEP (detail)  onDetach");
        super.onDetach();
    }
    public void onAttach(Context context){
        Log.d(TAG, "Fragment STEP (detail)  onAttach" );
        super.onAttach(context);
    }
    public void onStop(){
        Log.d(TAG, "Fragment STEP (detail) onStop");
        super.onStop();
    }
    public void onDestroy(){
        Log.d(TAG, "Fragment STEP (detail)  onDestroy");
        super.onDestroy();
        if (vgson!=null ) {
            new saveCacheOndetach().execute();
        }
    }
    private class saveCacheOndetach extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... strings) {
            Log.d(TAG, "Fragment STEP (detail) onDestroy doInBackground");
            VazotsaraShareFunc.saveUniqueVideo(vgson, "");
            Reservoir.putAsync("single_"+uid, vgson, new ReservoirPutCallback() {
                @Override
                public void onSuccess() {
                    //success
                    Log.d(TAG, "Reservoir putAsync onSuccess");
                    //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                }

                @Override
                public void onFailure(Exception e) {
                    //error
                    Log.d(TAG, "Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                }
            });
            return null;
        }
    }

    public void onStart()
    {
        EventBus.getDefault().register(this);
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }

    public static VideoDetailTabInfoFragment newInstance(String uid, int position) {
        VideoDetailTabInfoFragment frag = new VideoDetailTabInfoFragment();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        args.putInt("position", position);
        frag.setArguments(args);
        return frag;
    }

    public VideoDetailTabInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail_tab_info, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        db = new Dbhandler(getActivity());
        //videoidInFavori = db.VideoInFavoris( Integer.parseInt(uid) );
        videoidInFavori = TableFavorivideos.isVideoidInFavoris( uid );

        vsload = ((ViewStub) rootView.findViewById(R.id.pdload)).inflate();
        vscontent_detail = ((ViewStub) rootView.findViewById(R.id.content_detail)).inflate();
        vserror_nonconnecte = ((ViewStub) rootView.findViewById(R.id.error_nonconnecte)).inflate();

        titrevideo = (TextView) vscontent_detail.findViewById(R.id.titrevideo);
        nbview = (TextView) vscontent_detail.findViewById(R.id.nbview);
        nblike = (TextView) vscontent_detail.findViewById(R.id.nblike);
        nbdislike = (TextView) vscontent_detail.findViewById(R.id.nbdislike);
        nbcomment = (TextView) vscontent_detail.findViewById(R.id.nbcomment);
        hdformat = (TextView) vscontent_detail.findViewById(R.id.hdformat);
        headerartists = (TextView) vscontent_detail.findViewById(R.id.headerartists);
        headerrealisateur = (TextView) vscontent_detail.findViewById(R.id.headerrealisateur);
        action_favorite = (TextView) vscontent_detail.findViewById(R.id.action_favorite);
        v = (RelativeTimeTextView) vscontent_detail.findViewById(R.id.timestamp);

        fbLikeView = (FBLikeView) vscontent_detail.findViewById(R.id.fbLikeView);
        fbLikeView.getLikeView().setObjectIdAndType(Config.URLDOMAIN, LikeView.ObjectType.OPEN_GRAPH);

        //flowview = (GridView) vscontent_detail.findViewById(R.id.flowview);
        artistes_list = (TwoWayGridView) vscontent_detail.findViewById(R.id.artistes_list);
        realisateur_list = (TwoWayGridView) vscontent_detail.findViewById(R.id.realisateur_list);

        bloc_addfavorite = (RelativeLayout) vscontent_detail.findViewById(R.id.bloc_addfavorite);
        Iconify.addIcons(action_favorite);
        bloc_addfavorite_text = (TextView) vscontent_detail.findViewById(R.id.bloc_addfavorite_text);



        //Si la video est dans les favoris
        if ( videoidInFavori == true ) {
            action_favorite.setTextColor(Color.parseColor(Config.COLOR_FAVORI_IN));
            bloc_addfavorite_text.setTextColor(Color.parseColor(Config.COLOR_FAVORI_IN));
            bloc_addfavorite_text.setText(getString(R.string.action_remove_favorite));
        }

        bloc_addfavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Si non connecte
                if (!Useful.isLoggedin(getActivity())) {
                    showsnack = true;
                    MaterialDialog dialogpl = new MaterialDialog.Builder(getActivity())
                            .title(R.string.dialog_information_title)
                            .content(R.string.error_favorite_nonconnecte)
                            .positiveText(R.string.btn_connecter)
                            .negativeText(android.R.string.cancel)
                            .negativeColor(Color.parseColor("#000000"))
                            .positiveColor(Color.parseColor("#000000"))
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    startActivityForResult(new Intent(getActivity(), LoginActivity.class).putExtra("option", "addfavorite").putExtra("uid",uid), 200 );
                                }


                                @Override
                                public void onNegative(MaterialDialog dialog) {

                                }
                            })
                            .build();
                    dialogpl.show();
                    return;
                }
                addfavoriteAction(uid);

            }
        });




        artistes_list.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                String uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                String titre = ((TextView) view.findViewById(R.id.nom)).getText().toString();

                String logoreference = ((TextView) view.findViewById(R.id.logoreference)).getText().toString();
                Log.d(TAG, "TwoWayAdapterView.OnItemClickListener : " + uid+" - "+logoreference);
                showListVideosByArtists(position);


            }
        });

        realisateur_list.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                String uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                String titre = ((TextView) view.findViewById(R.id.nom)).getText().toString();
                Log.d(TAG, "TwoWayAdapterView.OnItemClickListener : " + uid);
                showListVideosByRealisateurs(uid, titre);
            }
        });

        vscontent_detail.setVisibility(View.GONE);
        vserror_nonconnecte.setVisibility(View.GONE);
        Log.d(TAG, "loaded : 196 " + loaded);

        //getInfo(Config.URLAPI+"/video/" + uid);

    }

    void addfavoriteAction(String uid) {




        String color, text;
        if ( videoidInFavori == true ) {
            color = Config.COLOR_FAVORI_OUT;
            text = getString(R.string.action_add_favorite);
            //Remove in DB
            //db.deleteFavoriVideo(Integer.parseInt(uid), artistelist);
            TableFavorivideos.deleteVideoidInFavori( uid , getActivity());
            msg_confirm = getString(R.string.msg_confirm_remove_favorite);
            videoidInFavori = false;
        } else {
            color = Config.COLOR_FAVORI_IN;
            text = getString(R.string.action_remove_favorite);
            msg_confirm = getString(R.string.msg_confirm_add_favorite);
            //Add in DB
            //db.addFavori(new FavoriVideoSQL(infovideo_titre,Integer.parseInt( uid ),0,infovideo_videoyt,infovideo_format), artistelist);
            TableFavorivideos.addVideoInFavori(new FavoriVideoSQL( Integer.parseInt(uid) ), getActivity() );
            videoidInFavori = true;
        }

        Log.d(TAG,"videoidInFavori : "+videoidInFavori);


        VazotsaraShareFunc.showSnackMsg(msg_confirm, getActivity());

        action_favorite.setTextColor(Color.parseColor(color));
        bloc_addfavorite_text.setTextColor(Color.parseColor(color));
        bloc_addfavorite_text.setText(text);
    }

    void checkifIsFavorite(String uid){
        String color, text;
        videoidInFavori = TableFavorivideos.isVideoidInFavoris( uid );
        if ( !videoidInFavori  ) {
            color = Config.COLOR_FAVORI_OUT;
            text = getString(R.string.action_add_favorite);
            msg_confirm = getString(R.string.msg_confirm_remove_favorite);
            videoidInFavori = false;
        } else {
            color = Config.COLOR_FAVORI_IN;
            text = getString(R.string.action_remove_favorite);
            msg_confirm = getString(R.string.msg_confirm_add_favorite);
            videoidInFavori = true;
        }
        action_favorite.setTextColor(Color.parseColor(color));
        bloc_addfavorite_text.setTextColor(Color.parseColor(color));
        bloc_addfavorite_text.setText(text);

        if ( showsnack ) {
            VazotsaraShareFunc.showSnackMsg(msg_confirm, getActivity());
            showsnack = false;
        }

    }
    void showListVideosByArtists(int position )
    {

        Intent VideoSearchActivity = new Intent( getActivity(), VideosByArtistActivity.class);

        Playlist pl = new Playlist();
        pl.setId(artistelist.get(position).getUid());
        pl.setPlaylisteid(artistelist.get(position).getPlaylisteid());
        pl.setNom(artistelist.get(position).getObjetnom());
        pl.setObjetnom(artistelist.get(position).getObjetnom());
        pl.setImage(artistelist.get(position).getImage());
        pl.setNbVideos(artistelist.get(position).getNbVideos());
        pl.setFirstvideoid(artistelist.get(position).getFirstvideoid());


        EventBus.getDefault().postSticky(pl);
        /*VideoSearchActivity.putExtra("playlisteid", uid );
        VideoSearchActivity.putExtra("titre", titre );
        VideoSearchActivity.putExtra("opt", "artiste" );
        VideoSearchActivity.putExtra("logoreference", logoreference );*/
        getActivity().startActivity(VideoSearchActivity);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }
    void showListVideosByRealisateurs(String uid, String titre)
    {
        Log.d(TAG, "In showListVideosByRealisateurs");
        Intent VideoSearchActivity = new Intent( getActivity(), VideosByRealisateurActivity.class);
        VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre);
        VideoSearchActivity.putExtra("opt", "artiste");
        getActivity().startActivity(VideoSearchActivity);

        //activity.overridePendingTransition(R.animator.slide_in_left, R.animator.slide_out_left);
    }

    void getInfoBDD() {
        Log.d(TAG,"VideoDetailTabInfoFragment - getInfoBDD" );
        new loadSQL().execute();
    }
    void getInfo(String url ) {
        loaded = true;


        if ( !detector.isConnectingToInternet() )
            return;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .setConverter(new LenientGsonConverter(new Gson()))
                .build();

        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        vidapi.getSingleVideo(uid, new Callback<VideoSingleGson>() {


            @Override
            public void success(VideoSingleGson videopojo, retrofit.client.Response response) {
                Log.d(TAG, "response.getStatus() : " + response.getStatus());
                onSuccessResponse(videopojo);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });

    }

    private class loadSQL extends AsyncTask<VideoSingleGson,String,String> {

        @Override
        protected String doInBackground(VideoSingleGson... params) {

            Log.d(TAG,"VideoDetailTabInfoFragment - getInfoBDD loadSQL doInBackground" );

            final VideoSingleGson v = TableVideos.getSingleVideo(uid);

            Runnable runnable = new Runnable() {
                public void run() {
                    onSuccessResponse(v);
                }
            };
            getActivity().runOnUiThread(runnable);



            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(final String args) {
            Log.d(TAG, "Fragment STEP onResume loadSQL  onPostExecute");

            //onSuccessResponse(v);

        }

    }

    void onSuccessResponse(final VideoSingleGson reponse) {

        vgson = reponse.getInfo();

        if ( reponse.getInfo().getPlaylisteinfo().size() > 0 ) {


            Log.d(TAG, "Size: " + reponse.getInfo().getPlaylisteinfo().size());

            artistelist.clear();
            realisationlist.clear();

            for ( int i = 0; i < reponse.getInfo().getPlaylisteinfo().size(); i++ ) {
                Log.d(TAG,"Type: "+reponse.getInfo().getPlaylisteinfo().get(i).getObjetype()+" - Nom : "+reponse.getInfo().getPlaylisteinfo().get(i).getNom());
                if ( reponse.getInfo().getPlaylisteinfo().get(i).getObjetype()!=null && reponse.getInfo().getPlaylisteinfo().get(i).getObjetype().equals("artiste") ) {
                    //tagview.add(new Tag(reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom()));

                    relatedvideomodel a = new relatedvideomodel();
                    a.setUid(reponse.getInfo().getPlaylisteinfo().get(i).getId());
                    a.setObjetnom(reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom());
                    a.setNbVideos(reponse.getInfo().getPlaylisteinfo().get(i).getNbVideos());
                    a.setPathplayliste((reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ? reponse.getInfo().getPlaylisteinfo() + reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""));
                    a.setImage((reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ? reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""));
                    artistelist.add(a);

                    plartiste = new Playlist(
                            reponse.getInfo().getPlaylisteinfo().get(i).getId(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getPlaylisteid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getNom(),
                            (reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ? reponse.getInfo().getPlaylisteinfo() + reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""),
                            reponse.getInfo().getPlaylisteinfo().get(i).getCreated(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getDescriptionYt(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getNbVideos(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetype(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getFirstvideoid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getStatus(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getCreatedAt(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getUpdatedAt()
                    );

                }




                if ( reponse.getInfo().getPlaylisteinfo().get(i).getObjetype()!=null && reponse.getInfo().getPlaylisteinfo().get(i).getObjetype().equals("realisateur") ) {
                    //tagview.add(new Tag(reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom()));

                    relatedvideomodel a = new relatedvideomodel();
                    a.setUid(reponse.getInfo().getPlaylisteinfo().get(i).getId());
                    a.setObjetnom(reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom());
                    a.setNbVideos(reponse.getInfo().getPlaylisteinfo().get(i).getNbVideos());
                    a.setPathplayliste((reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ?  reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""));
                    a.setImage((reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ? reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""));
                    realisationlist.add(a);

                    plrealisateur = new Playlist(
                            reponse.getInfo().getPlaylisteinfo().get(i).getId(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getPlaylisteid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getNom(),
                            (reponse.getInfo().getPlaylisteinfo().get(i).getImage() != "" ?  reponse.getInfo().getPlaylisteinfo().get(i).getImage() : ""),
                            reponse.getInfo().getPlaylisteinfo().get(i).getCreated(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getDescriptionYt(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getNbVideos(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetnom(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetype(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getObjetid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getFirstvideoid(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getStatus(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getCreatedAt(),
                            reponse.getInfo().getPlaylisteinfo().get(i).getUpdatedAt()
                    );

                }


            }


        }

        //ArtistsAdapter artisteadapter = new ArtistsAdapter(getActivity(),artistelist);
        //ArtistsAdapter artisterecycleradapter = new ArtistsAdapter(getActivity(),artistelist  );
        ArtistsTWVAdapter artisterecycleradapter = new ArtistsTWVAdapter(getActivity(),artistelist  );
        RealisateurTWVAdapter realisateuradapter = new RealisateurTWVAdapter(getActivity(),realisationlist  );

        //flowview.setAdapter(artisteadapter);
        artistes_list.setAdapter(artisterecycleradapter);
        realisateur_list.setAdapter(realisateuradapter);

        //Titre
        titrevideo.setText( reponse.getInfo().getTitre()  );
        infovideo_titre = reponse.getInfo().getTitre();

        nbview.setText("{fa-youtube_play} " + reponse.getInfo().getStatisticsViewCount());
        Iconify.addIcons(nbview);


        nblike.setText("{fa_thumbs_up} " + reponse.getInfo().getStatisticsLikeCount());
        Iconify.addIcons(nblike);

        nbdislike.setText("{fa_thumbs_down} " + reponse.getInfo().getStatisticsDislikeCount());
        Iconify.addIcons(nbdislike);

        nbcomment.setText("{fa_comment} " + reponse.getInfo().getStatisticsCommentCount());
        Iconify.addIcons(nbcomment);





        //html_text.setHtmlFromString( reponse.getInfo().getDescriptionYt(), true );
        infovideo_format = reponse.getInfo().getContentDetailsDefinition();
        infovideo_videoyt = reponse.getInfo().getVideoytid();





        //Cacher HD textview si format SD
        if (reponse.getInfo().getContentDetailsDefinition()!=null && !reponse.getInfo().getContentDetailsDefinition().equals("hd") ) {
            hdformat.setVisibility(View.GONE);
        }

        Long temps = null;
        if ( reponse.getInfo().getEndtimeUploaded() != null ) {
            temps = Long.decode( reponse.getInfo().getEndtimeUploaded() );
        } else if(reponse.getInfo().getPublishedAt() != null ){
            temps = Long.decode( reponse.getInfo().getPublishedAt() );
        }

        if ( temps != null ) {
            CharSequence du = DateUtils.getRelativeTimeSpanString((temps * 1000), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
            v.setReferenceTime((temps * 1000));
            Iconify.addIcons(v);
        }





        Date date = new Date();

        //Log.d(TAG, "du : " + du + " - new Date().getTime(): " + date.getTime() + " - Timestamp(date.getTime()): " + new Timestamp(date.getTime()) + " - reponse.getInfo().getUploadeddateOriginal():" + (reponse.getInfo().getUploadeddateOriginal() * 1000) + " - System.currentTimeMillis(): " + String.valueOf(System.currentTimeMillis()));

        if ( realisateuradapter.getCount()==0) {
            realisateur_list.setVisibility(View.GONE);
            headerrealisateur.setVisibility(View.GONE);
        }

        if ( artisterecycleradapter.getCount() == 0 ) {
            artistes_list.setVisibility(View.GONE);
            headerartists.setVisibility(View.GONE);
        }

        setContentView();

        vscontent_detail.invalidate();
    }
    public void onErrorResponse(RetrofitError volleyError) {
        String errorMessage = (volleyError.getMessage()==null)?"":volleyError.getMessage();
        Log.d(TAG, "onErrorResponse - Error:" + errorMessage);
        loaded = false;
    }
    public void onEvent(OptionBusEvent event){
        Log.d(TAG, "OptionBusEvent " + event.getReferer());
        //Maj Favori
        if ( !event.getReferer().isEmpty() && Integer.parseInt( event.getReferer() )>0 ) {
            addfavoriteAction(event.getReferer());
        }

    }
    @Override
    public void onResume() {
        Log.d(TAG, "Fragment STEP (detail)  onResume");
        EventBus.getDefault().unregister(this);
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoDetailTabInfoFragment - onResume - ID : " + uid);
        checkifIsFavorite(uid);
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "PlaylistsFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "BroadcastConnexion - loaded: "+loaded );
            //Si connecte  Internet et adapter vide - Rafraichir
            if ( detector.isConnectingToInternet() && !loaded ) {
                Log.d(TAG,"BroadcastConnexion - detector.isConnectingToInternet() true"  );
                //setLoadingView();
                if ( nbrow > 0 ) {
                    getInfoBDD();
                }

                getInfo(Config.URLAPI+"/video/" + uid);
            }


            //Si non connecte  Internet et isLoading()
            if ( !detector.isConnectingToInternet() && !loaded ) {
                Log.d(TAG,"BroadcastConnexion - detector.isConnectingToInternet() false && loaded false - nbrow : "+nbrow  );
                 if ( nbrow > 0 ) {
                     //setLoadingView();
                    getInfoBDD();
                } else {
                    setErrorView();
                 }
            }
        }
    }



    public void setLoadingView()
    {
        vsload.setVisibility(View.VISIBLE);
        vscontent_detail.setVisibility(View.VISIBLE);
        vserror_nonconnecte.setVisibility(View.GONE);
    }

    public void setErrorView()
    {
        vsload.setVisibility(View.GONE);
        vscontent_detail.setVisibility(View.GONE);
        vserror_nonconnecte.setVisibility(View.VISIBLE);
    }



    public void setContentView()
    {
        vsload.setVisibility(View.GONE);
        vscontent_detail.setVisibility(View.VISIBLE);
        vserror_nonconnecte.setVisibility(View.GONE);
    }




    }

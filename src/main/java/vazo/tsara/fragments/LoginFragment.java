package vazo.tsara.fragments;


import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.ProgressGenerator;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.FavoriVideoSQL;
import vazo.tsara.models.LoginPassword;
import vazo.tsara.models.OptionBusEvent;
import vazo.tsara.models.TableFavorivideos;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.profileModel;
import vazo.tsara.services.ApiRetrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment  implements ProgressGenerator.OnCompleteListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    View rootView;

    String TAG = Config.TAGKEY;
    String url = Config.URLAPI;

    TextInputLayout textInputLayoutUsername;
    TextInputLayout textInputLayoutPassword;

    EditText edit_text_username, edit_text_password;

    TextView txtsuccess;
    TextView txtsuccess_guest;
    TextView txtv_login_admin;
    TextView txtv_login_guest;

    ActionProcessButton btnSignIn;

    private AdView mAdView;

    View view_admin;
    View view_guest;

    LoginButton fblogin_button;
    //Button ggplus_login;
    Button ggplus_logout;
    ConnectionResult mConnectionResult;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    static GoogleApiClient mGoogleApiClient;
    boolean signedInUserGG;
    static boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;

    private static final int GOOGLE_SIGN_IN = 9000;

    //Twitter
    Button twitterConnect;
    Button twitterDisconnect;
    ConfigurationBuilder configurationBuilder;
    TwitterFactory twitterFactory;
    Twitter twitter;

    //Facebook
    Button facebookConnect;

    String twitter_name;
    String twitter_id;
    String option;
    String option2;
    String videoid;

    TwitterLoginButton twitter_login_button;

    SignInButton ggplus_login;

    AdView adView;



    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            //displayMessage(profile);
            if (isAdded()) {
                VazotsaraShareFunc.createOrUpdateUser(getProfile("facebook"), getActivity());
                Log.d(TAG, "ConnectRS - Facebook connect onSuccess ");

                //Ouvrir mainActivity et fermer login+
                setSuccess("Facebook");
            }
        }

        @Override
        public void onCancel() {
            Log.d(TAG,"ConnectRS - Facebook connect onCancel ");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d(TAG,"ConnectRS - Facebook connect onError ");
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        // Get back arguments
        if (getArguments() != null) {
            twitter_name = getArguments().getString("twitter_name", "");
            twitter_id = getArguments().getString("twitter_id", "");
            option = getArguments().getString("option", "");
            videoid = getArguments().getString("videoid", "");
            option2 = getArguments().getString("option2", "");
        }

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                //displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        TwitterAuthConfig authConfig =
                new TwitterAuthConfig(getString(R.string.consumer_key),
                        getString(R.string.consumer_secret));
        Fabric.with(getActivity(), new TwitterCore(authConfig));


    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
        if (mGoogleApiClient.isConnected()) {
             mGoogleApiClient.disconnect();
        }
    }


    public LoginFragment() {
        // Required empty public constructor
    }
    public LoginFragment newInstance() {
        LoginFragment frag = new LoginFragment();
        return frag;
    }
    public LoginFragment newInstance(String option ) {
        LoginFragment frag = new LoginFragment();
        Bundle args = new Bundle();
        args.putString("option", option);
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment newInstance(String option, String videoid, String option2 ) {
        LoginFragment frag = new LoginFragment();
        Bundle args = new Bundle();
        args.putString("option", option);
        args.putString("videoid", videoid);
        args.putString("option2", option2);
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment newInstance(String twitter_name, String twitter_id) {
        LoginFragment frag = new LoginFragment();
        Bundle args = new Bundle();
        args.putString("twitter_name", twitter_name);
        args.putString("twitter_id", twitter_id);
        frag.setArguments(args);
        return frag;
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }

        if ( mGoogleApiClient != null ) {
            Log.d(TAG, "mGoogleApiClient Connect" );
            mGoogleApiClient.connect();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Google+
        final GoogleApiClient.Builder googleApiClientBuilder = new GoogleApiClient.Builder(getActivity());
        googleApiClientBuilder.addConnectionCallbacks(this);
        googleApiClientBuilder.addOnConnectionFailedListener(this);
        googleApiClientBuilder.addApi(Plus.API);
        googleApiClientBuilder.addScope(Plus.SCOPE_PLUS_LOGIN);
        mGoogleApiClient = googleApiClientBuilder.build();

        //Twitter
        configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(getString(R.string.consumer_key));
        configurationBuilder.setOAuthConsumerSecret(getString(R.string.consumer_secret));
        twitterFactory = new TwitterFactory(configurationBuilder.build());
        twitter = twitterFactory.getInstance();

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_login, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        view_admin = ( (ViewStub) rootView.findViewById(R.id.view_admin)).inflate();
        view_guest = ( (ViewStub) rootView.findViewById(R.id.view_guest)).inflate();

        //View Guest

        adView = (AdView) view_guest.findViewById(R.id.banner);
        adView.loadAd(new AdRequest.Builder().build());

        txtv_login_admin = (TextView) view_guest.findViewById(R.id.txtv_login_admin);
        txtv_login_admin.setPaintFlags(txtv_login_admin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtv_login_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableViewAdmin();
            }
        });

        fblogin_button = (LoginButton) view_guest.findViewById(R.id.fblogin_button);
        // If using in a fragment
        fblogin_button.setReadPermissions("user_friends");
        fblogin_button.setFragment(this);
        // Callback registration
        fblogin_button.registerCallback(callbackManager, callback);



        ggplus_login = (SignInButton) view_guest.findViewById(R.id.ggplus_login);
        /*ggplus_logout = (Button) view_guest.findViewById(R.id.ggplus_logout);
        Iconify.addIcons(ggplus_login);
        Iconify.addIcons(ggplus_logout);*/
        ggplus_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mGoogleApiClient.isConnecting()) {
                    signedInUserGG = true;
                    googleConnect();
                }
            }
        });

        /*ggplus_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( mGoogleApiClient.isConnecting()) {
                    signedInUserGG = true;
                    googleDisconnect();
                }
            }
        });*/

        /*twitterConnect = (Button) view_guest.findViewById(R.id.twitterConnect);
        twitterDisconnect = (Button) view_guest.findViewById(R.id.twitterDisconnect);
        Iconify.addIcons(twitterConnect);
        Iconify.addIcons(twitterDisconnect);

        twitterConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final RequestToken requestToken = twitter.getOAuthRequestToken("twitter-callback:///");
                            final String url = requestToken.getAuthenticationURL();
                            final Intent intent = new Intent(getActivity(), LoginTwitterActivity.class);
                            intent.putExtra("AuthenticationURL", url);

                            startActivityForResult(intent, 3);
                        } catch (TwitterException exception) {
                            exception.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        twitterDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.disconnectTwitterInfoInPreferences(getActivity());
                showButtonConnectTwitter();
            }
        });

        if (VazotsaraShareFunc.isTwitterLoggedin(getActivity())) {
            hideButtonConnectTwitter();
        }*/

        twitter_login_button = (TwitterLoginButton) view_guest.findViewById(R.id.twitter_login_button);
        twitter_login_button.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                if ( isAdded() ) {
                    VazotsaraShareFunc.createOrUpdateUser(new profileModel(
                            String.valueOf(result.data.getId()),
                            result.data.getUserName(),
                            "",
                            "twitter"
                    ), getActivity());
                    //Toast.makeText(getActivity(),"Username : "+result.data.getUserId()+" - "+result.data.getUserName(), Toast.LENGTH_LONG).show();
                    //Ouvrir mainActivity et fermer login+
                    setSuccess("Twitter");
                }
            }

            @Override
            public void failure(com.twitter.sdk.android.core.TwitterException e) {

            }


        });

        txtsuccess_guest = (TextView) view_guest.findViewById(R.id.txtsuccess_guest);

        //View Admin
        txtv_login_guest = (TextView) view_admin.findViewById(R.id.txtv_login_guest);
        txtv_login_guest.setPaintFlags(txtv_login_guest.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtv_login_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableViewGuest();
            }
        });

        textInputLayoutUsername = (TextInputLayout) view_admin.findViewById(R.id.textInputLayoutUsername);
        textInputLayoutPassword = (TextInputLayout) view_admin.findViewById(R.id.textInputLayoutPassword);

        edit_text_username = (EditText) view_admin.findViewById(R.id.edit_text_username);
        edit_text_password = (EditText) view_admin.findViewById(R.id.edit_text_password);



        txtsuccess = (TextView) view_admin.findViewById(R.id.txtsuccess);

        setupFloatingLabelError();

        final ProgressGenerator progressGenerator = new ProgressGenerator(LoginFragment.this);
        btnSignIn = (ActionProcessButton) view_admin.findViewById(R.id.btnSignIn);

        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressGenerator.start(btnSignIn);

                if (noErrorOnSubmit()) {

                    btnSignIn.setEnabled(false);
                    btnSignIn.setProgress(1);

                    edit_text_username.setEnabled(false);
                    edit_text_password.setEnabled(false);

                    String login = edit_text_username.getText().toString();
                    String password = edit_text_password.getText().toString();

                    //Api Login
                    postLogin(login, password);



                    /*final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnSignIn.setProgress(100);
                        }
                    }, 5000);*/


                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_connexion_submit), getActivity());


                }


            }
        });

        enableViewGuest();
    }

    void showButtonConnectTwitter() {
        twitter_login_button.setVisibility(View.VISIBLE);
    }

    void hideButtonConnectTwitter() {
        twitter_login_button.setVisibility(View.GONE);
    }

    void showButtonConnectGoogle() {
        ggplus_login.setVisibility(View.VISIBLE);
    }

    void hideButtonConnectGoogle() {
        ggplus_login.setVisibility(View.GONE);
    }

    void showButtonConnectFacebook() {
        fblogin_button.setVisibility(View.VISIBLE);
    }

    void hideButtonConnectFacebook() {
        fblogin_button.setVisibility(View.GONE);
    }


    private void googleConnect() {
        Log.d(TAG, "mGoogleApiClient - googleConnect");
        if (mConnectionResult!=null && mConnectionResult.hasResolution()) {
            Log.d(TAG, "mGoogleApiClient - googleConnect - mConnectionResult.hasResolution()");
            try {
                Log.d(TAG,"mGoogleApiClient - googleConnect - mConnectionResult.hasResolution() try");
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(getActivity(), LoginFragment.RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private void googleDisconnect()
    {
        if (mGoogleApiClient.isConnected() == true)
        {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();

            Log.d(TAG, "mGoogleApiClient - googleDisconnect");

            //showButtonConnectGoogle();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "LoginFragment - onActivityResult - requestCode : " + requestCode + " - resultCode : " + resultCode);
        //Facebook
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //Google
        if (requestCode == LoginFragment.RC_SIGN_IN) {
            Log.d(TAG,"LoginFragment - onActivityResult google "+requestCode);
            signedInUserGG = false;
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                Log.d(TAG,"mGoogleApiClient - onActivityResult - mGoogleApiClient.isConnecting() false");
                mGoogleApiClient.connect();
            }
            checkResult();
        }

        //Twitter core kit
        twitter_login_button.onActivityResult(requestCode, resultCode, data);

        if ( requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode() ) {
            Log.d(TAG,"LoginFragment - onActivityResult facebook "+requestCode);
            checkResult();
        }

        if ( requestCode == 140 ) {
            Log.d(TAG,"LoginFragment - onActivityResult twitter "+requestCode);
            checkResult();
        }

    }
    void checkResult() {
        Log.d(TAG,"LoginFragment - checkResult" );

        if (option.isEmpty() || videoid.isEmpty())
            return;

        Log.d(TAG, "LoginFragment - checkResult tabfavorite.isempty false" );

        if ( !videoid.isEmpty() ) {
            boolean videoidInFavori = TableFavorivideos.isVideoidInFavoris(videoid);
            if ( videoidInFavori == true ) {
                TableFavorivideos.deleteVideoidInFavori( videoid , getActivity());
            } else {
               TableFavorivideos.addVideoInFavori(new FavoriVideoSQL( Integer.parseInt(videoid) ), getActivity() );
            }
        }



        EventBus.getDefault().post(new OptionBusEvent(videoid));

        //Fermer la fenetre
        getActivity().finish();


    }
    public void enableViewAdmin(){
        view_guest.setVisibility(View.GONE);
        view_admin.setVisibility(View.VISIBLE);

        edit_text_username.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

    }

    public void enableViewGuest(){
        view_guest.setVisibility(View.VISIBLE);
        view_admin.setVisibility(View.GONE);


    }

    void postLogin(String login, String password) {

        Log.d(TAG, "postLogin - login : " + login + " - password : " + password);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .build();
        ApiRetrofit api = restAdapter.create(ApiRetrofit.class);
        api.postLogin(new LoginPassword(login, password), new Callback<TokenPojo>() {
            //api.postLogin( login,password , new Callback<TokenPojo>() {
            @Override
            public void success(TokenPojo tokenresult, retrofit.client.Response response) {
                Log.d(TAG, "postLogin - tokenresult : " + tokenresult.getToken());
                if (isAdded()) {
                    btnSignIn.setProgress(100);

                    //Sauvegarder le token dans les preferences
                    VazotsaraShareFunc.saveTokenInPreferences(tokenresult.getToken(), getActivity());

                    txtsuccess.setVisibility(View.VISIBLE);

                    //Ouvrir mainActivity et fermer login
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().setResult(300);
                            getActivity().finish();
                        }
                    }, 1000);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                //Log.d(TAG, "postLogin - error : " + error.getMessage());
                if (error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());

                        if (jsonObj.get("error") != null) {
                            textInputLayoutUsername.setError(jsonObj.get("error").toString());
                            textInputLayoutPassword.setError(jsonObj.get("error").toString());

                            textInputLayoutUsername.setErrorEnabled(true);
                            textInputLayoutPassword.setErrorEnabled(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());

                }


                initFields();


            }
        });
    }


    void initFields(){
        edit_text_username.setEnabled(true);
        edit_text_password.setEnabled(true);
        btnSignIn.setProgress(0);
        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);
        btnSignIn.setEnabled(true);
    }
    public boolean noErrorOnSubmit(){

        if ( edit_text_username.getText().toString().isEmpty() || edit_text_username.getText().toString().length() < 3 )
            return false;

        if ( edit_text_password.getText().toString().isEmpty() || edit_text_password.getText().toString().length() < 3 )
            return false;

        return true;


    }

    private void setupFloatingLabelError() {


        textInputLayoutUsername.getEditText().addTextChangedListener(new TextWatcher() {
            // ...
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if ( s.length() > 0 && s.length() <= 4 || !android.util.Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches() ) {
                    textInputLayoutUsername.setError(getString(R.string.error_username));
                    textInputLayoutUsername.setErrorEnabled(true);
                } else {
                    textInputLayoutUsername.setErrorEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }


        });

        textInputLayoutPassword.getEditText().addTextChangedListener(new TextWatcher() {
            // ...
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.length() <= 4) {
                    textInputLayoutPassword.setError(getString(R.string.error_password));
                    textInputLayoutPassword.setErrorEnabled(true);
                } else {
                    textInputLayoutPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }


        });
    }


    @Override
    public void onComplete() {

    }

    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {

        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
        Profile profile = Profile.getCurrentProfile();

        }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {

        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }



    @Override
    public void onConnected(Bundle bundle) {
        signedInUserGG = false;
        Log.d(TAG, "mGoogleApiClient - onConnected");
        //Toast.makeText(getActivity(), "Connected GGplus", Toast.LENGTH_LONG).show();
        if (isAdded()) {
            VazotsaraShareFunc.createOrUpdateUser(getProfile("google"), getActivity());

            //Ouvrir mainActivity et fermer login+
            setSuccess("Google");
        }


        //hideButtonConnectGoogle();
    }

    void setSuccess(String provider) {
        txtsuccess_guest.setVisibility(View.VISIBLE);
        txtsuccess_guest.setText( String.format( getString(R.string.txt_successlogin_sprintf), provider) );
        //Cacher les boutons
        hideButtonConnectTwitter();
        hideButtonConnectFacebook();
        hideButtonConnectGoogle();

        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        }, 1000);*/
    }

    profileModel getProfile(String provider) {
        if ( provider.isEmpty() )
            return null;

        switch( provider ) {
            case "google" :
                try {
                    if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                        Person profile = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                        //profile.getId();

                        return new profileModel(
                                profile.getId(),
                                profile.getDisplayName(),
                                profile.getImage().getUrl(),
                                provider
                        );

                        //return Plus.AccountApi.getAccountName(mGoogleApiClient);
                    }
                } catch (Exception exception) {

                }
                break;
            case "facebook":
                Profile profile = Profile.getCurrentProfile();
                //profile.getId();
                return new profileModel(
                        profile.getId(),
                        profile.getName(),
                        String.format("https://graph.facebook.com/v2.5/%s/picture?type=normal", profile.getId()),
                        provider
                );


            case "twitter":
                final String[] idv = new String[1];
                final String[] name = new String[1];
                final String[] avatar = new String[1];

                TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
                twitterApiClient.getAccountService().verifyCredentials(false,false, new com.twitter.sdk.android.core.Callback<User>() {


                    @Override
                    public void success(Result<User> result) {
                        idv[0] = String.valueOf(result.data.id);
                        name[0] = String.valueOf(result.data.name);
                        avatar[0] = String.valueOf(result.data.profileImageUrl);

                    }

                    @Override
                    public void failure(com.twitter.sdk.android.core.TwitterException e) {

                    }
                });

                return new profileModel(
                        idv[0],
                        name[0],
                        avatar[0],
                        provider
                );



        }
        return null;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"mGoogleApiClient - onConnectionSuspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (result!=null && !result.hasResolution()) {
            Log.d(TAG,"mGoogleApiClient onConnectionFailed");
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(), 0).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;
            Log.d(TAG,"mGoogleApiClient mIntentInProgress false");
            if (signedInUserGG) {
                googleConnect();
            }
        }

    }
}

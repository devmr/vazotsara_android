package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.TextView;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.daimajia.slider.library.SliderLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.PlaylistsRecyclerAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.TableLastcheck;
import vazo.tsara.models.TablePlaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RealisateurFragment extends Fragment {


    String url = Config.URLAPI;

    SwipeRefreshLayout mSwipeRefreshLayout;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";

    int currentPage = 0;
    int serverListSize = 0;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    //RecyclerView recyclerView;
    RecyclerView listView;
    SparseItemRemoveAnimator mSparseAnimator;

    private SliderLayout mDemoSlider;



    List<Playlist> playList = new ArrayList<Playlist>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    PlaylistsRecyclerAdapter badapter;

    Parcelable state;

    int nbtotal_row = TablePlaylists.getAllCountType("genre");

    int rowCountInDb = 0;
    int pagerserver = 0;


    String sortfields = "objetnom";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

    }
    public void onStart()
    {
        super.onStart();

        rowCountInDb = TablePlaylists.getAllCountType("realisateur");
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "PlaylistsFragment - onResume");
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "PlaylistsFragment - onPause");
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            if ( connecte.isConnectingToInternet() && badapter.getCount()-1 == 0 ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                setLoadingView();
                getInfoServer();
            }

            if ( connecte.isConnectingToInternet() && badapter.getItemCount() > 0 ) {
                mSwipeRefreshLayout.setRefreshing(true);
                Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte true - adapter.getItemCount() : " + badapter.getItemCount() + " - currentPage : " + currentPage);
                setListView();

                if ( rowCountInDb > 0 ) {
                    getSqlInfo(currentPage);
                }
                badapter.setDeconnecte(false);
            }

            //Si non connecte  Internet et isLoading()

            if ( !connecte.isConnectingToInternet()  ) {
                Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte false");

                if ( rowCountInDb > 0 ) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    Log.d(TAG, "ArtistsFragment STEP - BroadcastConnexion - onReceive - connecte false - rowCountInDb: "+rowCountInDb );
                    setListView();
                    getSqlInfo(currentPage);

                } else {
                    setEmptyView(getString(R.string.error_non_connecte));
                    badapter.setDeconnecte(true);
                }
            }
        }
    }
    void getSqlInfo(int page) {
        pagerserver = page;
        if ( page > playList.size() / Config.LIMIT_GENERAL  ) {
            pagerserver = playList.size() / Config.LIMIT_GENERAL;
        }

        Log.d(TAG, "ArtistsFragment - getSqlInfo - pageserver : " + pagerserver);
        new loadSqlInfo().execute(String.valueOf(page));
    }
    private class loadSqlInfo extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onPreExecute");
        }


        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground - sortfields " + sortfields);


            From from = new Select()
                    .from(TablePlaylists.class)
                    ;

            from.where("objetype = 'realisateur'");


            Log.d(TAG, "ArtistsFragment STEP from.toSql(): " + from.toSql() );


            rowCountInDb = from.count();

            int offset = Integer.parseInt(params[0]) * Config.LIMIT_GENERAL;

            from = from.orderBy( sortfields )
                    .limit(Config.LIMIT_GENERAL)
                    .offset(offset);

            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground - rowCountInDb: "+rowCountInDb+" - SQL "+from.toSql());

            List<TablePlaylists> videos = from.execute();

            for (int count = 0; count < videos.size(); count++) {

                if (  count == 0 && offset == 0  ) {
                    Log.d(TAG, "ArtistsFragment STEP loadSqlInfo doInBackground 1st Row : "+videos.get(count).objetnom+" - sortfields : "+sortfields );
                }

                if ( !badapter.idInAdapter(videos.get(count).serverid  ) ) {
                    playList.add( fillItemDb(videos.get(count)));

                    Log.d(TAG, "ArtistsFragment STEP loadSqlInfo badapter - Nom : " + videos.get(count).objetnom + " - ID : " + videos.get(count).serverid + " ajouté dans adapter via BDD à la position "+count);
                } else {
                    Log.d(TAG,"ArtistsFragment STEP loadSqlInfo badapter - Nom : "+videos.get(count).objetnom+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
                }
            }


            badapter.setServerListSize(rowCountInDb);
            badapter.setLoadedSize(playList.size());
            publishProgress("1");

            return params[0];
        }

        @Override
        public void onProgressUpdate(String... params) {
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onProgressUpdate");
            if ( !isAdded() ) {
                Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onProgressUpdate isAdded() false");
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    badapter.notifyDataSetChanged();
                    loaded = true;
                }
            });

        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(final String args) {
            Log.d(TAG, "ArtistsFragment STEP loadSqlInfo onPostExecute");


        }
    }

    public RealisateurFragment() {
        // Required empty public constructor
    }

    public static RealisateurFragment newInstance() {
        RealisateurFragment frag = new RealisateurFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "PlaylistsFragment - onCreateView");
        rootView = inflater.inflate(R.layout.fragment_playlists, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "PlaylistsFragment - onActivityCreated");
        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );


        listView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        badapter = new PlaylistsRecyclerAdapter( getActivity() , playList, R.layout.adapter_artiste_item , R.layout.adapter_videoloading );
        listView.setAdapter(badapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_listview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastPlaylists();

            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.ColorPrimaryDark, R.color.material_red_400, R.color.ColorPrimaryDark, R.color.material_red_400);



    }

    void getLastPlaylists(){
        if ( !connecte.isConnectingToInternet()  ) {
            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte), getActivity());
            return;
        }

        //final String lastplaylist = (VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity()):"");
        final String lastplaylist = TableLastcheck.getLastIdGenre();

        if ( !lastplaylist.isEmpty() && Integer.parseInt(lastplaylist)>0 ) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListPlaylisteBeforeId(lastplaylist, "genre",  new Callback<Playlists>() {

                @Override
                public void success(Playlists serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if (serverinfo.getPlaylists().size()>0 && !serverinfo.getPlaylists().get(0).getId().equals(lastplaylist)) {
                        //Refresh
                        //addToList(serverinfo.getPlaylists());
                        //badapter.notifyDataSetChanged();

                        //Enregistrer dernière playlist dans Preferences
                        String lastid = serverinfo.getPlaylists().get(0).getId();
                        /*if ( VazotsaraShareFunc.getLastPlaylistidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastPlaylistidInPreferences(lastid, getActivity());*/
                        TableLastcheck.setLastIdGenre(lastid);

                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.msg_confirm_playlists_added), serverinfo.getPlaylists().size()), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Error " + error);
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                }
            });


        }
        // Stop refresh animation dans tous les cas
        mSwipeRefreshLayout.setRefreshing(false);

    }

    void getInfoServer() {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);


        vidapi.getPlaylists("realisateur", new Callback<Playlists>() {
            @Override
            public void success(Playlists plist, Response response) {
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + plist.getPlaylists().size());
                mSwipeRefreshLayout.setRefreshing(false);
                if (plist.getPlaylists().size() == 0) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                    loaded = true;
                    return;
                }
                serverListSize = plist.getTotal();
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + serverListSize);
                setListView();
                //addToList(plist.getPlaylists());
                //VazotsaraShareFunc.saveInPlaylists(plist.getPlaylists());
                addToListServer(plist.getPlaylists());
                //getInfoBDD();
                //adapter.notifyDataSetChanged();
                /*badapter.setServerListSize(serverListSize);
                badapter.notifyDataSetChanged();*/
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "PlaylistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

    }
    void getInfoBDD() {
        new loadSQL().execute();
    }
    void addToListServer(List<Playlist> liste) {
        saveBDD(liste);
        int added = 0;
        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).getId()  ) ) {
                playList.add(fillItemServer(liste.get(count)));
                added++;
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).getObjetnom()+" - ID : "+liste.get(count).getId()+" existe deja dans adapter ");
            }
        }
        badapter.notifyDataSetChanged();
    }
    void saveBDD(List<Playlist> liste){
        new saveBddCache().execute(liste);
    }
    private class saveBddCache extends AsyncTask<List<Playlist>, String, String>
    {

        @Override
        protected String doInBackground(List<Playlist>... params) {

            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInPlaylists(params[0]);
                    Log.d(TAG, "ArtistsFragment STEP saveBddCache doInBackground sortfields:" + sortfields );
                }
            }
            return null;
        }
    }
    Playlist fillItemDb(TablePlaylists item) {
        Playlist v = new Playlist();
        v.setNom(item.nom);
        v.setObjetnom(item.objetnom);
        v.setId(item.serverid);
        v.setNbVideos(item.nbVideos);
        v.setImage(item.image);
        v.setObjetype(item.objetype);
        v.setStatus(item.status);
        v.setDescriptionYt(item.descriptionYt);
        return v;
    }
    Playlist fillItemServer(Playlist videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getNom());
        v.setObjetnom(videogson.getObjetnom());
        v.setId(videogson.getId());
        v.setNbVideos(videogson.getNbVideos());
        v.setImage(videogson.getImage());
        v.setObjetype(videogson.getObjetype());
        v.setStatus(videogson.getStatus());
        v.setDescriptionYt(videogson.getDescriptionYt());
        return v;
    }
    void addToList(List<TablePlaylists> liste) {

        for (int count = 0; count < liste.size(); count++) {
            if ( !badapter.idInAdapter( liste.get(count).serverid  ) ) {
                playList.add(fillItem(liste.get(count)));
            } else {
                Log.d(TAG,"badapter - Nom : "+liste.get(count).objetnom+" - ID : "+liste.get(count).serverid+" existe deja dans adapter ");
            }
        }
    }

    Playlist fillItem(TablePlaylists field) {
        Playlist v = new Playlist();
        v.setNom( field.nom );
        v.setObjetnom(field.objetnom);
        v.setId(field.serverid);
        v.setNbVideos(field.nbVideos);
        v.setImage(field.image);
        v.setObjetype(field.objetype);
        v.setStatus(field.status);
        v.setDescriptionYt(field.descriptionYt);
        return v;
    }

    public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 0;
        private int previousTotal = 0;
        private boolean loading = true;

        public EndlessScrollListener() {
        }
        public EndlessScrollListener(int visibleThreshold) {
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                //new LoadGigsTask().execute(currentPage + 1);
                //getInfo(url,currentPage + 1);
                onLoadMore(currentPage + 1, totalItemCount);
                loading = true;
            }
        }

        public abstract void onLoadMore(int page, int totalItemsCount);

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }

    private class loadSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriVideoSQL  onPreExecute");
            mSwipeRefreshLayout.setRefreshing(true);
        }
        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "loadFavoriVideoSQL  doInBackground" );

            if (!isAdded() )
                return "";

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {

                    int nbrow = new Select()
                            .from(TablePlaylists.class)
                            .where("objetype = 'genre'")
                            .count() ;

                    if ( nbrow == 0 ) {
                        setEmptyView(getActivity().getString(R.string.error_no_item_playlist));
                        loaded = true;
                        return;
                    }



                    setListView();

                    List<TablePlaylists> liste = new Select()
                            .from(TablePlaylists.class)
                            .orderBy("objetnom ASC")
                            .where("objetype = 'genre'")
                            .execute();

                    Log.d(TAG,"ArtistsSQLFragment - SQL "+new Select()
                            .from(TablePlaylists.class)
                            .orderBy("created DESC")
                            .where("objetype = 'autre'").toSql());
                    addToList(liste);

                    badapter.setServerListSize(nbrow);
                    //adapter.notifyDataSetChanged();
                    badapter.notifyDataSetChanged();
                    loaded = true;





                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriVideoSQL  onPostExecute" );
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}

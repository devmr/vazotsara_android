package vazo.tsara.fragments;


import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.HomePagerAdapter;
import vazo.tsara.helpers.Config;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    String TAG = Config.TAGKEY;
    View rootView;
    //PagerSlidingTabStrip tabs;
    TabLayout tabs;
    ViewPager pager;
    HomePagerAdapter adapter;



    public HomeFragment() {
        // Required empty public constructor
    }

    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs = (TabLayout) rootView.findViewById(R.id.tabs);
        pager = (ViewPager) rootView.findViewById(R.id.pager);

    }

    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        //receiver = new BroadcastConnexion();
        //getActivity().registerReceiver(receiver, filter);
        super.onResume();

        adapter = new HomePagerAdapter( getChildFragmentManager(), getActivity() );
        pager.setAdapter(adapter);
        //tabs.setViewPager(pager);
        tabs.setupWithViewPager(pager);

        //Forcer la largeur des onglets equivalents
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setTabMode(TabLayout.MODE_FIXED);
    }


}

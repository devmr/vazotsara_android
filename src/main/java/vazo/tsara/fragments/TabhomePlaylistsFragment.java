package vazo.tsara.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vazo.tsara.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabhomePlaylistsFragment extends Fragment {


    public TabhomePlaylistsFragment() {
        // Required empty public constructor
    }

    public static TabhomePlaylistsFragment newInstance() {
        TabhomePlaylistsFragment frag = new TabhomePlaylistsFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tabhome_playlists, container, false);
    }


}

package vazo.tsara.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import vazo.tsara.R;
import vazo.tsara.activities.LoginActivity;
import vazo.tsara.adapters.ArtistsTWVAdapter;
import vazo.tsara.adapters.AutocompleteAdapterAddplaylist;
import vazo.tsara.adapters.AutocompleteAdapterSearchVideo;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.DMWebVideoView;
import vazo.tsara.libraries.DelayAutoCompleteTextView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.Embedly;
import vazo.tsara.models.FieldAddPlaylist;
import vazo.tsara.models.FieldAddVideo;
import vazo.tsara.models.FormatResults;
import vazo.tsara.models.LoginPassword;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.relatedvideomodel;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddVideoFragment extends Fragment {

    String url;
    String idartista;
    static String TAG = Config.TAGKEY;

    View rootView;

    MaterialSpinner spinn_status, spinn_category;

    TextView txtv_url;
    TextView idartist;
    TextView thumburlpath;
    TextView nbvideos;
    TextView source_titre;
    TextView source_description;
    TextView source_provider;
    TextView source_author;
    TextView firstvideoyt;
    TextView playlisteyt;
    TextView txt_choose_format;
    TextView formattype_choosen;

    TwoWayGridView twgv_artists;
    TwoWayGridView twgv_genre;
    TwoWayGridView twgv_realisateur;
    TwoWayGridView twgv_playlists;

    CheckBox chk_removefile;

    Button btn_add_artist;
    Button btn_add_genre;
    Button btn_add_realisateur;
    Button btn_add_playlists;
    Button btn_source_hideshow;
    Button btn_fill;
    Button btn_choose_format;
    Button btn_source_play;

    EditText edit_text_titre;
    EditText edit_text_description;

    TextInputLayout textInputLayoutUrl;

    View positiveAction;
    View neutralAction;
    View viewstub_error;
    View viewstub_content;

    DelayAutoCompleteTextView dialogautocomplete;
    DelayAutoCompleteTextView textview_autocomplete;

    private ArrayAdapter<CharSequence>  adapter_status, adapter_category;

    List<relatedvideomodel> artistelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> realisateurlist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> genrelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> playlistlist = new ArrayList<relatedvideomodel>();

    ArtistsTWVAdapter artisterecycleradapter ;
    ArtistsTWVAdapter genrerecycleradapter ;
    ArtistsTWVAdapter playlistsrecycleradapter ;
    ArtistsTWVAdapter realisateurrecycleradapter ;

    ActionProcessButton btnSignIn;
    ActionProcessButton btnSignInDialog;

    String[] idartistsImplode;
    String[] idgenreImplode;
    String[] idplaylistsImplode;
    String[] idrealisateurImplode;

    CharSequence[] itemsformat;
    List<String> listItemsformat = new ArrayList<String>();
    List<String> listItemsformatType = new ArrayList<String>();
    List<String> listItemsformatFormat = new ArrayList<String>();

    RelativeLayout source;

    ImageView source_miniature;

    DMWebVideoView dailymotionplayer;
    TableRow trow_dm;

    ProgressBar pbar_autoc;

    EditText edit_text_username;
    EditText edit_text_password;

    static boolean fieldchanged = false;

    static FragmentActivity activity;


    public static void onBackPressed() {
        Log.d(TAG,"Fragment AddVideoFragment onBackPressed - fieldchanged: "+fieldchanged);
        if ( !fieldchanged ) {
            activity.finish();
        } else {

            new MaterialDialog.Builder(activity)
                    .title(R.string.dialog_supprfavori_title)
                    .content(R.string.dialog_field_changed)
                    .positiveText(R.string.dialog_supprfavori_positivebutton)
                    .negativeText(R.string.dialog_supprfavori_negativebutton)
                    .negativeColor(Color.parseColor("#000000"))
                    .positiveColor(Color.parseColor("#000000"))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            activity.finish();
                        }


                        @Override
                        public void onNegative(MaterialDialog dialog) {

                        }
                    })
                    .show();


            //VazotsaraShareFunc.showSnackMsg("Erreur lors de la validation", activity);
        }
    }

    public void setErrorView(final String error)
    {
        viewstub_content.setVisibility(View.GONE);
        viewstub_error.setVisibility(View.VISIBLE);


        TextView textv = (TextView) viewstub_error.findViewById(R.id.textView);
        Button btn_login = (Button) viewstub_error.findViewById(R.id.btn_login);

        textv.setText(error);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showDialogConnexion();
                Intent loginActivity = new Intent(getActivity(), LoginActivity.class).putExtra("url", url);
                getActivity().startActivityForResult(loginActivity, 100);
            }
        });
    }

    public void setContentView()
    {
        viewstub_content.setVisibility(View.VISIBLE);
        viewstub_error.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();

        if ( Useful.isAdmin( getActivity() ) ) {
            setContentView();
        }


    }

    public AddVideoFragment() {
        // Required empty public constructor
    }

    public AddVideoFragment newInstance(String url ) {
        AddVideoFragment frag = new AddVideoFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        frag.setArguments(args);
        return frag;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        activity = getActivity();
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url", "");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_video, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        initView();





        //String[] ITEMS = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"};
        adapter_status = ArrayAdapter.createFromResource(getActivity(), R.array.status_video, android.R.layout.simple_spinner_item);

        //adapter_status = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, ITEMS  );
        adapter_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_status);

        //Selectionner "public" par defaut
        Log.d(TAG,"Arrays.asList :"+Arrays.asList( getResources().getStringArray(R.array.status_video) ).size());
        Log.d(TAG, "Arrays.asList : " + Arrays.asList(getResources().getStringArray(R.array.status_video)).indexOf("Public"));
        Log.d(TAG, "Arrays.asList :" + adapter_status.getPosition("Public"));

        spinn_status.setSelection(adapter_status.getPosition("Public") + 1);


        adapter_category = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.status_category)  );
        adapter_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_category.setAdapter(adapter_category);

        //Selectionner "Musique" par defaut
        spinn_category.setSelection(adapter_category.getPosition("Musique") + 1);

        txtv_url.setText(url);

        if ( !Useful.isErrorUrl(url, getActivity() ) ) {
            getEmbedly(url);
            getFormatVideo(url);
        }

        btn_add_artist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("a", artistelist, artisterecycleradapter);
            }
        });

        btn_add_playlists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("o", playlistlist, playlistsrecycleradapter);
            }
        });

        btn_add_genre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("g", genrelist, genrerecycleradapter);
            }
        });

        btn_add_realisateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd("r", realisateurlist, realisateurrecycleradapter);
            }
        });

        btn_source_hideshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (source.getVisibility() == View.GONE)
                    source.setVisibility(View.VISIBLE);
                else
                    source.setVisibility(View.GONE);
            }
        });




        btn_choose_format.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "listItemsformat.size() : " + listItemsformat.size());

                if (listItemsformat.size() == 0) {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_choice_format), getActivity());

                    return;
                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title(R.string.dialog_chooseformat_title)
                            .items(listItemsformat.toArray(new CharSequence[listItemsformat.size()]))
                            .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {


                                    //Remplir le textView formattype_choosen
                                    formattype_choosen.setText(listItemsformatType.get(which).toString());

                                    Spannable btnText = new SpannableString(String.format(getString(R.string.btn_format_choosen), text + " - " + listItemsformatFormat.get(which).toString()));
                                    btnText.setSpan(new StyleSpan(Typeface.BOLD), 15, 20, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                    btn_choose_format.setText(btnText);

                                    return true; // allow selection
                                }
                            })
                            .positiveText("Choisir")
                            .positiveColor(Color.parseColor("#000000"))
                            .show();
                }
            }
        });

        twgv_artists.setAdapter(artisterecycleradapter);
        twgv_genre.setAdapter(genrerecycleradapter);
        twgv_playlists.setAdapter(playlistsrecycleradapter);
        twgv_realisateur.setAdapter(realisateurrecycleradapter);

        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);

        btn_fill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String titre = edit_text_titre.getText().toString();
                String[] nomartistsImplode = new String[artisterecycleradapter.getCount()];
                String[] descartistsImplode = new String[artisterecycleradapter.getCount()];

                if (titre.length() > 0) {

                    //Convertir en ucfirst


                    //Si le titre contient "::"
                    if (titre.contains("::"))
                        titre = titre.substring(0, titre.indexOf("::"));


                    for (int n = 0; n < artisterecycleradapter.getCount(); n = n + 1) {
                        nomartistsImplode[n] = artisterecycleradapter.getFieldAt(n, "nom");

                        if (!artisterecycleradapter.getFieldAt(n, "firstvideoid").isEmpty())
                            descartistsImplode[n] = "- Playliste " + artisterecycleradapter.getFieldAt(n, "nom") + " : https://www.youtube.com/watch?v=" + artisterecycleradapter.getFieldAt(n, "firstvideoid") + "&list=" + artisterecycleradapter.getFieldAt(n, "playlisteid");

                        titre = titre.replaceAll("(?i)" + artisterecycleradapter.getFieldAt(n, "nom").trim(), "");
                    }


                    //Prendre 100 caracteres
                    titre = titre.trim();

                    //Supprimer espace en debut
                    if (titre!=null && titre.length()>0 && titre.charAt(0) == '-')
                        titre = titre.substring(1).trim();

                    //Supprimer tiret à la fin
                    if (titre!=null && titre.length()>0 && titre.charAt(titre.length()-1) == '-')
                        titre = titre.substring(0,titre.length()-1).trim();

                    //Limiter a 100 car refuse par Youtube
                    titre = (titre.length() > 100 ? titre.substring(0, 97) + "..." : titre);

                    //ucfirst
                    titre = Useful.ucfirst(titre);

                    fieldchanged = true;


                    edit_text_titre.setText(titre + "::" + TextUtils.join(",", nomartistsImplode));
                    edit_text_description.setText( (TextUtils.join("\n", descartistsImplode)!=null?TextUtils.join("\n", descartistsImplode):"" ) );
                }


            }
        });

        Log.d(TAG, "URLSOURCE : " + url + " - ID YT : " + Useful.getIdYoutubeVideo(url, getActivity()) + " - IDDM : " + Useful.getIdDailymotionVideo(url, getActivity()));

        btn_source_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Useful.isYoutubeUrl(url, getActivity()) && YouTubeIntents.canResolvePlayVideoIntentWithOptions(getActivity())) {

                    startActivity(YouTubeStandalonePlayer.createVideoIntent(getActivity(), Config.DEVELOPER_KEY, Useful.getIdYoutubeVideo(url, getActivity()), 0, true, true));

                } else if (Useful.isDailymotionUrl(url, getActivity())) {
                    trow_dm.setVisibility(View.VISIBLE);
                    dailymotionplayer.setVideoId(Useful.getIdDailymotionVideo(url, getActivity()));
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_clic_source_play), getActivity());

                }
            }
        });


        textview_autocomplete.setAdapter(new AutocompleteAdapterSearchVideo(getActivity()));
        textview_autocomplete.setLoadingIndicator(pbar_autoc);
        textview_autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                VazotsaraShareFunc.showSnackMsg(getString(R.string.error_clicautocomplete_noaction), getActivity());

                textview_autocomplete.setText("");
            }
        });


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressGenerator.start(btnSignIn);


                Log.d(TAG, "noErrorOnSubmit --- titre.isEmpty()? : " + edit_text_titre.getText().toString().isEmpty() + " - titre.contains(\"::\")? " + edit_text_titre.getText().toString().contains("::") + " - artisterecycleradapter.getCount() : " + artisterecycleradapter.getCount());

                if (noErrorOnSubmit()) {
                    saveForm();
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_connexion_submit), getActivity());

                }


            }
        });





    }

    void saveForm() {


        btnSignIn.setEnabled(false);
        btnSignIn.setProgress(1);

        idartistsImplode = new String[artisterecycleradapter.getCount()];
        String[] nomartistsImplode = new String[artisterecycleradapter.getCount()];

        idgenreImplode = new String[genrerecycleradapter.getCount()];
        String[] nomgenreImplode = new String[artisterecycleradapter.getCount()];

        idrealisateurImplode = new String[realisateurrecycleradapter.getCount()];
        String[] nomrealisateurImplode = new String[artisterecycleradapter.getCount()];

        idplaylistsImplode = new String[playlistsrecycleradapter.getCount()];
        String[] nomplaylistsImplode = new String[artisterecycleradapter.getCount()];


        for (  int n = 0 ; n < artisterecycleradapter.getCount(); n = n+1 ) {
            idartistsImplode[n] = artisterecycleradapter.getFieldAt(n,"id");
            nomartistsImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < genrerecycleradapter.getCount(); n = n+1 ) {
            idgenreImplode[n] = genrerecycleradapter.getFieldAt(n,"id");
            nomgenreImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < realisateurrecycleradapter.getCount(); n = n+1 ) {
            idrealisateurImplode[n] = realisateurrecycleradapter.getFieldAt(n,"id");
            nomrealisateurImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }
        for (  int n = 0 ; n < playlistsrecycleradapter.getCount(); n = n+1 ) {
            idplaylistsImplode[n] = playlistsrecycleradapter.getFieldAt(n,"id");
            nomplaylistsImplode[n] = artisterecycleradapter.getFieldAt(n,"nom");
        }

        Log.d(TAG, "Implode idartistsImplode : " + TextUtils.join(",", idartistsImplode));
        Log.d(TAG, "Implode idgenreImplode : " + TextUtils.join(",", idgenreImplode));
        Log.d(TAG, "Implode idrealisateurImplode : " + TextUtils.join(",", idrealisateurImplode));
        Log.d(TAG, "Implode idplaylistsImplode : " + TextUtils.join(",", idplaylistsImplode));

        String titre = edit_text_titre.getText().toString();
        String description = edit_text_description.getText().toString();
        String status = spinn_status.getSelectedItem().toString();
        String category = spinn_category.getSelectedItem().toString();
        String removef = (chk_removefile.isChecked()?"1":"0");

        String type_dl = formattype_choosen.getText().toString();
        String url_dl = url;
        String title_dl = source_titre.getText().toString();
        String description_dl = source_description.getText().toString();
        String author_dl = source_author.getText().toString();

                    /*if ( artisterecycleradapter.getCount() > 0 ) {
                        Log.d(TAG,"img 0 : "+artisterecycleradapter.getFieldAt(0,"id"));
                        Log.d(TAG,"img Lenght : "+artisterecycleradapter.getCount());
                    }*/
                    /*final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnSignIn.setProgress(100);
                        }
                    }, 5000);*/

        //Valider
        if ( category.equals("Musique") )
            category = "10";

        switch(status) {
            case "Public" :
                status = "public";
                break;
            default :
                status = "unlisted";
                break;
            case "Privé" :
                status = "private";
                break;
        }

        Log.d(TAG, "Is Admin? " + Useful.isAdmin(getActivity()));

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(getActivity()) ) {
                            request.addHeader("Authorization", " Bearer "+VazotsaraShareFunc.getAdminToken(getActivity()));
                        }
                    }
                })
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.postCreateVideo(
                new FieldAddVideo(
                        titre,
                        TextUtils.join(",", idartistsImplode),
                        TextUtils.join(",", idrealisateurImplode),
                        TextUtils.join(",", idgenreImplode),
                        TextUtils.join(",", idplaylistsImplode),
                        status,
                        category,
                        removef,
                        description,
                        type_dl,
                        url_dl,
                        title_dl,
                        description_dl,
                        author_dl,
                        Config.CREATEDBY
                ), new Callback<TokenPojo>() {
                    @Override
                    public void success(TokenPojo sourceinfo, retrofit.client.Response response) {


                        VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), getActivity());
                        btnSignIn.setProgress(100);

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().finish();
                            }
                        }, 2000);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        btnSignIn.setEnabled(true);
                        btnSignIn.setProgress(0);

                        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                        Log.d(TAG, "errorMessage : " + errorMessage);


                        //Log.d(TAG, "postLogin - error : " + error.getMessage());
                        if (error.getResponse() != null) {
                            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                            try {
                                JSONObject jsonObj = new JSONObject(json);
                                Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                                VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                        }
                    }
                });
    }

    void initView(){

        viewstub_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();

        if ( !Useful.isAdmin( getActivity() ) ) {
            setErrorView(getString(R.string.error_connexion_action));
        } else {
            setContentView();
        }

        spinn_status = (MaterialSpinner) viewstub_content.findViewById(R.id.spinn_status);
        spinn_category = (MaterialSpinner) viewstub_content.findViewById(R.id.spinn_category);

        edit_text_titre = (EditText) viewstub_content.findViewById(R.id.edit_text_titre);
        edit_text_description = (EditText) viewstub_content.findViewById(R.id.edit_text_description);

        txtv_url = (TextView) viewstub_content.findViewById(R.id.txtv_url);
        source_titre = (TextView) viewstub_content.findViewById(R.id.source_titre);
        source_description = (TextView) viewstub_content.findViewById(R.id.source_description);
        source_author = (TextView) viewstub_content.findViewById(R.id.source_author);
        source_provider = (TextView) viewstub_content.findViewById(R.id.source_provider);
        txt_choose_format = (TextView) viewstub_content.findViewById(R.id.txt_choose_format);
        formattype_choosen = (TextView) viewstub_content.findViewById(R.id.formattype_choosen);

        twgv_artists = (TwoWayGridView) viewstub_content.findViewById(R.id.twgv_artists);
        twgv_realisateur = (TwoWayGridView) viewstub_content.findViewById(R.id.twgv_realisateur);
        twgv_genre = (TwoWayGridView) viewstub_content.findViewById(R.id.twgv_genre);
        twgv_playlists = (TwoWayGridView) viewstub_content.findViewById(R.id.twgv_playlists);

        chk_removefile = (CheckBox) viewstub_content.findViewById(R.id.chk_removefile);

        btn_add_artist = (Button) viewstub_content.findViewById(R.id.btn_add_artist);
        btn_add_genre = (Button) viewstub_content.findViewById(R.id.btn_add_genre);
        btn_add_realisateur = (Button) viewstub_content.findViewById(R.id.btn_add_realisateur);
        btn_add_playlists = (Button) viewstub_content.findViewById(R.id.btn_add_playlists);
        btn_source_hideshow = (Button) viewstub_content.findViewById(R.id.btn_source_hideshow);
        btn_fill = (Button) viewstub_content.findViewById(R.id.btn_fill);
        btn_choose_format = (Button) viewstub_content.findViewById(R.id.btn_choose_format);
        btn_source_play = (Button) viewstub_content.findViewById(R.id.btn_source_play);

        artisterecycleradapter = new ArtistsTWVAdapter(getActivity(),artistelist, R.layout.adaper_artiste_item_add  );
        genrerecycleradapter = new ArtistsTWVAdapter(getActivity(),genrelist, R.layout.adaper_artiste_item_add  );
        playlistsrecycleradapter = new ArtistsTWVAdapter(getActivity(),playlistlist, R.layout.adaper_artiste_item_add  );
        realisateurrecycleradapter = new ArtistsTWVAdapter(getActivity(),realisateurlist, R.layout.adaper_artiste_item_add  );

        btnSignIn = (ActionProcessButton) viewstub_content.findViewById(R.id.btnSignIn);

        source = (RelativeLayout) viewstub_content.findViewById(R.id.source);

        dailymotionplayer = (DMWebVideoView) viewstub_content.findViewById(R.id.dailymotionplayer);
        trow_dm = (TableRow) viewstub_content.findViewById(R.id.trow_dm);


        textview_autocomplete = (DelayAutoCompleteTextView) viewstub_content.findViewById(R.id.txtv_autoc);
        pbar_autoc = (ProgressBar) viewstub_content.findViewById(R.id.pbar_autoc);

    }
    void showDialogConnexion()
    {
        int intTitle = R.string.dialog_connexion;


        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(intTitle)
                .customView(R.layout.dialog_add_connexion, true)
                .positiveText(R.string.dialog_connexion)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        String  login = edit_text_username.getText().toString();
                        String  password = edit_text_password.getText().toString();

                        postLogin(login, password);

                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }


                }).build();



        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);


        edit_text_username = (EditText) dialog.getCustomView().findViewById(R.id.edit_text_username);
        edit_text_password = (EditText) dialog.getCustomView().findViewById(R.id.edit_text_password);
        btnSignInDialog = (ActionProcessButton) dialog.getCustomView().findViewById(R.id.btnSignInDialog);

        edit_text_username.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_text_password.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                positiveAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.show();

        positiveAction.setEnabled(false); // disabled by default

    }

    void postLogin(String login, String password) {

        Log.d(TAG, "postLogin - login : " + login + " - password : " + password);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url)
                .build();
        ApiRetrofit api = restAdapter.create(ApiRetrofit.class);
        api.postLogin(new LoginPassword(login, password), new Callback<TokenPojo>() {
            //api.postLogin( login,password , new Callback<TokenPojo>() {
            @Override
            public void success(TokenPojo tokenresult, retrofit.client.Response response) {
                Log.d(TAG, "postLogin - tokenresult : " + tokenresult.getToken());

                btnSignInDialog.setProgress(100);

                //Sauvegarder le token dans les preferences
                VazotsaraShareFunc.saveTokenInPreferences(tokenresult.getToken(), getActivity());
            }

            @Override
            public void failure(RetrofitError error) {

                    btnSignInDialog.setProgress(-1);
                    btnSignInDialog.setEnabled(true);

                if (error.getResponse() != null) {
                    String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                    try {
                        JSONObject jsonObj = new JSONObject(json);
                        Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));
                        VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());

                }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnSignInDialog.setProgress(0);
                        }
                    }, 500);

            }
        });
    }

    void showDialogAdd(final String type, final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter)
    {
        int intTitle = R.string.dialog_addartiste;
        switch (type) {
            case "a" :
                //artiste
                intTitle = R.string.dialog_addartiste;
                break;
            case "g" :
                //genre
                intTitle = R.string.dialog_addgenre;
                break;
            case "r" :
                //realisateur
                intTitle = R.string.dialog_addrealisateur;
                break;
            case "o" :
                //playlist
                intTitle = R.string.dialog_addplaylist;
                break;
        }
         MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(intTitle)
                .customView(R.layout.dialog_addplaylist_invideo, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .neutralText(R.string.dialog_addvideo_btn_create)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .neutralColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        idartista = idartist.getText().toString();



                        if (!dialogautocomplete.getText().toString().isEmpty() && !idartista.isEmpty() && Integer.parseInt(idartista)>0 ) {
                            addplaylist(liste, adapter);
                        } else {

                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_choice_playlist), getActivity());
                            return;
                        }






                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        //Créer l'item
                        if ( Integer.parseInt( idartist.getText().toString() ) > 0 ) {
                            addplaylist(liste, adapter);
                        } else if( !dialogautocomplete.getText().toString().isEmpty() ){
                            createplaylist(type, liste, adapter);
                        } else {
                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_create_playlist), getActivity());

                        }

                    }

                }).build();



        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        neutralAction = dialog.getActionButton(DialogAction.NEUTRAL);
        //noinspection ConstantConditions

        dialogautocomplete = (DelayAutoCompleteTextView) dialog.getCustomView().findViewById(R.id.dialogautocomplete);

        dialogautocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    neutralAction.setEnabled(s.toString().trim().length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        idartist = (TextView) dialog.getCustomView().findViewById(R.id.idartist);
        thumburlpath = (TextView) dialog.getCustomView().findViewById(R.id.thumburlpath);
        nbvideos = (TextView) dialog.getCustomView().findViewById(R.id.nbvideos);

        firstvideoyt = (TextView) dialog.getCustomView().findViewById(R.id.firstvideoyt);
        playlisteyt = (TextView) dialog.getCustomView().findViewById(R.id.playlisteyt);

        dialogautocomplete.setThreshold(1);
        Log.d(TAG, "showDialogAdd type : " + type);
        dialogautocomplete.setAdapter(new AutocompleteAdapterAddplaylist(getActivity(), type));

        dialogautocomplete.setLoadingIndicator((ProgressBar) dialog.getCustomView().findViewById(R.id.progressBar));
        dialogautocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Log.d(TAG, "adapterView.getAdapter().getItem(i).toString()" + ((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setText(((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setSelection(dialogautocomplete.getText().length());

                idartist.setText(((TextView) view.findViewById(R.id.id)).getText().toString());
                thumburlpath.setText(((TextView) view.findViewById(R.id.thumburlpath)).getText().toString());
                nbvideos.setText(((TextView) view.findViewById(R.id.nbvideos)).getText().toString());

                firstvideoyt.setText(((TextView) view.findViewById(R.id.firstvideoyt)).getText().toString());
                playlisteyt.setText(((TextView) view.findViewById(R.id.playlisteyt)).getText().toString());

                if ( Integer.parseInt( idartist.getText().toString() ) > 0 )
                    positiveAction.setEnabled(true);



            }
        });

        dialog.show();

        positiveAction.setEnabled(false); // disabled by default
        neutralAction.setEnabled(false); // disabled by default
    }

    void addplaylist(final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter) {
        relatedvideomodel a = new relatedvideomodel();
        a.setUid(idartista);
        a.setObjetnom(dialogautocomplete.getText().toString());
        a.setNbVideos(nbvideos.getText().toString());
        a.setPathplayliste("");
        a.setPathplayliste(thumburlpath.getText().toString());
        a.setFirstvideoid(firstvideoyt.getText().toString());
        a.setPlaylisteid(playlisteyt.getText().toString());
        liste.add(a);
        adapter.notifyDataSetChanged();
        fieldchanged = true;
    }

    void createplaylist(String type, final List<relatedvideomodel> liste, final ArtistsTWVAdapter adapter) {


        String objetype = "artiste";

        switch (type) {
            case "a" :
                //artiste
                objetype = "artiste";
                break;
            case "g" :
                //genre
                objetype = "genre";
                break;
            case "r" :
                //realisateur
                objetype = "realisateur";
                break;
            case "o" :
                //playlist
                objetype = "autre";
                break;
        }

        //Creer sur le serveur
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        if ( Useful.isAdmin(getActivity()) ) {
                            request.addHeader("Authorization", " Bearer "+ VazotsaraShareFunc.getAdminToken(getActivity()));
                        }
                    }
                })
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.postCreatePlaylist(
                new FieldAddPlaylist(
                        dialogautocomplete.getText().toString(),
                        objetype,
                        "",
                        "public",
                        Config.CREATEDBY
                ), new Callback<TokenPojo>() {
                    @Override
                    public void success(TokenPojo sourceinfo, retrofit.client.Response response) {


                        VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), getActivity());
                        relatedvideomodel a = new relatedvideomodel();
                        a.setUid(sourceinfo.getId());
                        a.setObjetnom(dialogautocomplete.getText().toString());
                        a.setNbVideos("0");
                        a.setPathplayliste("");
                        a.setFirstvideoid("");
                        a.setPlaylisteid("");
                        liste.add(a);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                        Log.d(TAG, "errorMessage : " + errorMessage);


                        //Log.d(TAG, "postLogin - error : " + error.getMessage());
                        if (error.getResponse() != null) {
                            String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                            try {
                                JSONObject jsonObj = new JSONObject(json);
                                Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));

                                VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_failure_retrofit), getActivity());
                        }
                    }
                });
    }

    public boolean noErrorOnSubmit(){
        String titre = edit_text_titre.getText().toString();

        if( titre.isEmpty() )
            return false;

        if( !titre.contains("::") )
            return false;

        if ( artisterecycleradapter.getCount() == 0 )
            return false;

        if ( formattype_choosen.getText().toString().trim().isEmpty() )
            return false;

        return true;


    }
    void getFormatVideo(String urlsource ) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
        vidapi.getFormatVideo(urlsource, new Callback<FormatResults>() {
            public void success(FormatResults sourceinfo, retrofit.client.Response response) {
                Log.d(TAG,"sourceinfo.getFormat().size(): "+sourceinfo.getFormat().size() );

                if (sourceinfo.getFormat()!=null && sourceinfo.getFormat().get(0)!=null && sourceinfo.getFormat().get(0).getType()!=null && sourceinfo.getFormat().get(0).getInfo()!=null && sourceinfo.getFormat().get(0).getFormat()!=null && sourceinfo.getFormat().size() > 0 ) {
                    //btn_choose_format.setText(String.format(getString(R.string.btn_choose_format_data),sourceinfo.getFormat().size()) );


                    //Remplir le textView formattype_choosen
                    formattype_choosen.setText(sourceinfo.getFormat().get(0).getType());

                    //Spannable btnText = new SpannableString(  String.format( getActivity().getString(R.string.btn_format_default) , sourceinfo.getFormat().get(0).getInfo() + " - "+sourceinfo.getFormat().get(0).getFormat())  );
                    //btnText.setSpan(new StyleSpan(Typeface.BOLD), 0, 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    btn_choose_format.setText(String.format( getActivity().getString(R.string.btn_format_default) , (sourceinfo.getFormat() !=null?sourceinfo.getFormat().get(0).getInfo():"") + " - "+(sourceinfo.getFormat() !=null?sourceinfo.getFormat().get(0).getFormat():"")));
                }


                for( int i = 0; i < sourceinfo.getFormat().size(); i++ ) {
                    //itemsformat[i] = sourceinfo.getFormat().get(i).getInfo()+" - "+sourceinfo.getFormat().get(i).getFormat();
                    listItemsformat.add( sourceinfo.getFormat().get(i).getInfo() );
                    listItemsformatType.add( sourceinfo.getFormat().get(i).getType() );
                    listItemsformatFormat.add(sourceinfo.getFormat().get(i).getFormat() );
                }

            }
            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);

                VazotsaraShareFunc.showSnackMsg(errorMessage, getActivity());
            }
        });
    }

    void getEmbedly( String urlsource ) {



        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        Log.d(TAG,"url "+url+" - urlsource : "+urlsource);
        vidapi.getEmbedly(urlsource, new Callback<Embedly>() {
            @Override
            public void success(Embedly sourceinfo, retrofit.client.Response response) {



                if ( sourceinfo.getTitle() != null && !sourceinfo.getTitle().isEmpty() ) {

                    if ( edit_text_titre.getText().toString().isEmpty() )
                        edit_text_titre.setText( Useful.ucfirst( sourceinfo.getTitle() ) );

                    source_titre.setText(sourceinfo.getTitle());


                }



                if ( sourceinfo.getAuthorName() != null && !sourceinfo.getAuthorName().isEmpty() )
                    source_author.setText( sourceinfo.getAuthorName() );

                if ( sourceinfo.getDescription() != null && !sourceinfo.getDescription().isEmpty() )
                    source_description.setText( sourceinfo.getDescription() );

                if ( sourceinfo.getProviderName() != null && !sourceinfo.getProviderName().isEmpty() )
                    source_provider.setText( sourceinfo.getProviderName() );
            }

            @Override
            public void failure(RetrofitError error) {

                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                Log.d(TAG, "errorMessage : " + errorMessage);
                VazotsaraShareFunc.showSnackMsg(errorMessage, getActivity());

            }
        });

    }

}

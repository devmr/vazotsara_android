package vazo.tsara.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;
import com.malinskiy.superrecyclerview.swipe.SwipeDismissRecyclerViewTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.adapters.VideoAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchVideoActivityFragment extends Fragment {

    View rootView;
    View emptyView;

    String playlisteid;
    TextView textview;

    String sortfields = "uploadeddate_original";
    int currentPage = 0;

    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = Config.TAGKEY;

    SuperRecyclerView recyclerView;
    SparseItemRemoveAnimator mSparseAnimator;

    ViewStub vs_loading_default;
    ViewStub vs_content_errormessage;
    ViewStub vs_content_failureerrormessage;

    List<video> videoList = new ArrayList<video>();

    VideoAdapter adapter;


    public SearchVideoActivityFragment() {
    }
    public static SearchVideoActivityFragment newInstance(String playlisteid) {
        SearchVideoActivityFragment frag = new SearchVideoActivityFragment();
        Bundle args = new Bundle();
        args.putString("playlisteid", playlisteid);
        frag.setArguments(args);
        return frag;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        playlisteid = getArguments().getString("playlisteid", "");
    }
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoFragment - onResume");
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "VideoFragment - onPause");
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Si connecte  Internet et adapter vide - Rafraichir
            if ( connecte.isConnectingToInternet() && adapter.getItemCount() == 0 ) {

                setLoadingInterface();
                getInfo(url, 0);
            }

            //Si non connecte  Internet et isLoading()
            if ( connecte.isConnectingToInternet() && adapter.getItemCount() > 0 && recyclerView.isLoadingMore()) {
                Log.d(TAG, "mRecycler.isLoadingMore - currentpage : " + currentPage);
                recyclerView.getSwipeToRefresh().setRefreshing(true);
                if ( !sortfields.equals("uploadeddate_original"))
                    currentPage = 0;

                getInfo(url, currentPage);
            }

            //Si non connecte  Internet et isLoading()
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount() > 0 && recyclerView.isLoadingMore()) {
                Log.d(TAG, "mRecycler.isLoadingMore - NC currentpage : " + currentPage);
                /*SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(getString(R.string.error_non_connecte))
                );*/
            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if ( !connecte.isConnectingToInternet() && adapter.getItemCount() == 0 ) {
                Log.d(TAG, "Non connecte + adapter vide");
                setTextEmpty(getString(R.string.error_non_connecte));
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_search_video, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*textview = (TextView) rootView.findViewById(R.id.textview);
        textview.setText("playlisteid: "+playlisteid);*/

        adapter = new VideoAdapter( getActivity() , videoList, R.layout.adapter_videoitem  );

        recyclerView = (SuperRecyclerView) rootView.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        boolean dismissEnabled = true;
        if (dismissEnabled) {
            recyclerView.setupSwipeToDismiss(new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
                @Override
                public boolean canDismiss(int i) {
                    return false;
                }

                @Override
                public void onDismiss(RecyclerView recyclerView, int[] ints) {

                }
            });
            mSparseAnimator = new SparseItemRemoveAnimator();
            recyclerView.getRecyclerView().setItemAnimator(mSparseAnimator);
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                if ( getActivity() != null ) {

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            recyclerView.setAdapter(adapter);
                        }
                    });

                }

            }
        });
        thread.start();

        //refresh
        recyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG, "recyclerView.setRefreshListener onRefresh");
            }
        });

        recyclerView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
                currentPage += 1;
                Log.d(TAG, "recyclerView.setRefreshListener onMoreAsked - numberOfItems : " + numberOfItems + " - numberBeforeMore : " + numberBeforeMore + " - currentItemPos:  " + currentItemPos + " - urlmore : " + url);
                getInfo(url, currentPage);
            }
        }, 1);


    }

    void getInfo(String url, int page )
    {
        if ( !connecte.isConnectingToInternet()  ) {
            if ( page > 0 ) {
                Log.d(TAG, "Loadmore NON CONNECTE");
                /*SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(getString(R.string.error_non_connecte))
                );*/
                recyclerView.getMoreProgressView().setVisibility(View.GONE);
            }
            return;
        } else if ( page > 0 ) {
            recyclerView.getMoreProgressView().setVisibility(View.VISIBLE);
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



        Log.d(TAG, "URL retrofit 608 : " + url + "/videos?limit=20&page=" + page + "&q=&sortfields=" + sortfields+"&playlisteid="+playlisteid);
        vidapi.getVideosInPlaylists(20, page, playlisteid, new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {

                addToList(videoPojo.getVideos());
                adapter.notifyDataSetChanged();
                recyclerView.hideMoreProgress();
                isLoading = false;
                Log.d(TAG, "isLoading after getInfoGson success : " + isLoading);
                Log.d(TAG, "mRecycler.getSwipeToRefresh().isRefreshing() : " + recyclerView.getSwipeToRefresh().isRefreshing());

                if (recyclerView.getSwipeToRefresh().isRefreshing())
                    recyclerView.getSwipeToRefresh().setRefreshing(false);

            }

            @Override
            public void failure(RetrofitError error) {
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();

                Log.d(TAG, "loadmore - Error:" + errorMessage);

                setTextFailure(errorMessage);

                isLoading = false;

                Log.d(TAG, "isLoading after getInfoGson - failure : " + errorMessage);
            }
        });

    }

    void addToList(List<Videogson> videos) {
        for (int count = 0; count < videos.size(); count++) {
            videoList.add( fillItem(videos.get(count)) );
        }
    }

    video fillItem(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId( videogson.getId() );
        v.setVideoytid( videogson.getVideoytid()  );
        v.setStatistics_viewCount(videogson.getStatisticsViewCount() );
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount() );
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        return v;

    }

    void setTextFailure(String errorMessage) {
        emptyView = recyclerView.getEmptyView();


        vs_loading_default = (ViewStub) emptyView.findViewById(R.id.loading_default);
        vs_content_errormessage = (ViewStub) emptyView.findViewById(R.id.content_errormessage);
        vs_content_failureerrormessage = (ViewStub) emptyView.findViewById(R.id.content_failureerrormessage);

        if (vs_loading_default!=null )
            vs_loading_default.setVisibility(View.GONE);
        if ( vs_content_errormessage!=null )
            vs_content_errormessage.setVisibility(View.GONE);
        if (vs_content_failureerrormessage!=null) {
            TextView tv = (TextView) vs_content_failureerrormessage.inflate().findViewById(R.id.textViewFailure);
            tv.setText(errorMessage);
        }

    }

    void setTextEmpty(String text)
    {
        emptyView = recyclerView.getEmptyView();

        vs_loading_default = (ViewStub) emptyView.findViewById(R.id.loading_default);
        vs_content_errormessage = (ViewStub) emptyView.findViewById(R.id.content_errormessage);

        if (vs_loading_default!=null)
            vs_loading_default.setVisibility(View.GONE);

        if ( vs_content_errormessage != null ) {
            TextView tv = (TextView) vs_content_errormessage.inflate().findViewById(R.id.textView);
            tv.setText(text);
        }

    }
    void setLoadingInterface()
    {
        //recyclerView.getSwipeToRefresh().setRefreshing(true);

        emptyView = recyclerView.getEmptyView();





        vs_loading_default = (ViewStub) emptyView.findViewById(R.id.loading_default);
        vs_content_errormessage = (ViewStub) emptyView.findViewById(R.id.content_errormessage);
        vs_content_failureerrormessage = (ViewStub) emptyView.findViewById(R.id.content_failureerrormessage);

        if ( vs_loading_default != null )
            vs_loading_default.setVisibility(View.VISIBLE);
        if ( vs_content_errormessage != null )
            vs_content_errormessage.setVisibility(View.GONE);
        if ( vs_content_failureerrormessage != null )
            vs_content_failureerrormessage.setVisibility(View.GONE);



    }
}

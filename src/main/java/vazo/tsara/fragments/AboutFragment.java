package vazo.tsara.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.helpers.Config;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    View rootView;
    WebView wbv_text;
    String TAG = Config.TAGKEY;


    public AboutFragment() {
        // Required empty public constructor
    }

    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_about, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        wbv_text = (WebView) rootView.findViewById(R.id.wbv_text);
        wbv_text.loadUrl("file:///android_asset/about.html");
        wbv_text.setBackgroundColor(Color.TRANSPARENT);



    }

}

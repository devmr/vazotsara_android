package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;
import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.MainActivity;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.activities.VideoSearchSortActivity;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.VideoAdapter;
import vazo.tsara.adapters.VideoSearchSuggestionsAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.EndlessRecyclerOnScrollListener;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.Person;
import vazo.tsara.models.SearchVideo;
import vazo.tsara.models.SingleResultSearch;
import vazo.tsara.models.SingleResultSearchArtiste;
import vazo.tsara.models.TableVideos;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.VideoSingleGson;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosFragment extends Fragment {

    String url = Config.URLAPI;

    Boolean isLoading = true;

    BroadcastConnexion receiver;
    ConnectionDetector connecte;

    String TAG = "VAZOTSARA";
    String search = "";
    String playlisteid = "";
    String sortfields = "publishedAt";
    int currentPage = 0;

    View rootView;
    View emptyView;
    View viewstub_recyclerview_error;
    View viewstub_recyclerview_empty;
    View viewstub_recyclerview_content;
    View viewstub_recyclerview_loading;

    VideoSearchSuggestionsAdapter mSearchViewAdapter;

    private SearchView mSearchView;
    private SearchView searchView;
    private MenuItem searchMenuItem;


    RecyclerView recyclerView;
    SparseItemRemoveAnimator mSparseAnimator;

    ViewStub vs_loading_default;
    ViewStub vs_content_errormessage;
    ViewStub vs_content_failureerrormessage;

    List<video> videoList = new ArrayList<video>();

    VideoAdapter adapter;

    Person[] people;
    ArrayAdapter<Person> tadapter;

    boolean loaded;

    String[] columns = new String[]{"_id", "VIDEOYT", "TITRE", "VIDEOID", "contentDetails_definition"};
    String[] columnsArtiste = new String[]{"_id", "OBJETNOM", "ARTISTEID" };

    FloatingActionButton fab;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int lastFirstVisiblePosition;
    LinearLayoutManager linearLayoutManager;

    int rowCountInDb = 0;

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    int clickpos = -1;
    int pagerserver = 0;

    protected LayoutManagerType mCurrentLayoutManagerType;

    public VideosFragment() {
        // Required empty public constructor
    }

    public static VideosFragment newInstance(String search, String playlisteid) {

        VideosFragment frag = new VideosFragment();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putString("playlisteid", playlisteid);
        frag.setArguments(args);
        return frag;
    }
    public void onStart()
    {
        Log.d(TAG, "STEP - onStart " + this.getClass().getSimpleName());
        super.onStart();

        From from = new Select()
                .from(TableVideos.class)
        ;

        rowCountInDb = from.count();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());

        } catch(Exception  e) { }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "STEP - onCreate " + this.getClass().getSimpleName());
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

        // Get back arguments
        if (getArguments() != null) {
            search = getArguments().getString("search", "");
            playlisteid = getArguments().getString("playlisteid", "");
        }

    }



    public void onResume() {
        Log.d(TAG, "VideosFragment STEP - onResume " );
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();



        adapter = new VideoAdapter( getActivity() , videoList, R.layout.adapter_videoitem, R.layout.adapter_videoloading, Config.LIMIT_GENERAL   );
        recyclerView.setAdapter(adapter);




        linearLayoutManager.scrollToPosition(lastFirstVisiblePosition);

        if ( clickpos != -1 ) {
            Log.d(TAG, "VideosFragment STEP  onResume clickpos: " + clickpos + " - ID : " + videoList.get(clickpos).getId());


            Runnable runnable = new Runnable() {
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //fillItemCacheAdapter(clickpos);
                            if ( videoList.get(clickpos).getId()!=null && Integer.parseInt(videoList.get(clickpos).getId()) > 0 ) {
                                VideoSingleGson v = TableVideos.getSingleVideo(videoList.get(clickpos).getId());
                                Log.d(TAG, "VideosFragment STEP  onResume postDelayed titre: " + v.getInfo().getTitre());
                                Log.d(TAG, "VideosFragment STEP  onResume postDelayed view: " + v.getInfo().getStatisticsViewCount());

                                videoList.get(clickpos).setStatistics_viewCount(v.getInfo().getStatisticsViewCount());
                                videoList.get(clickpos).setStatistics_likeCount(v.getInfo().getStatisticsLikeCount());
                                videoList.get(clickpos).setContentDetails_definition(v.getInfo().getContentDetailsDefinition());

                                adapter.notifyItemChanged(clickpos);
                            } else {
                                Log.d(TAG, "VideosFragment STEP  onResume postDelayed null " );
                            }

                        }
                    }, 2000);
                }
            };

            getActivity().runOnUiThread(runnable);
        }

    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "STEP - onPause " + this.getClass().getSimpleName());

        lastFirstVisiblePosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();

    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Si connecte  Internet et adapter vide - Rafraichir
            if ( connecte.isConnectingToInternet() && adapter.getItemCount() == 0 ) {
                mSwipeRefreshLayout.setRefreshing(true);
                Log.d(TAG, "VideosFragment STEP - BroadcastConnexion - onReceive " );
                setLoadingView();
                Runnable runnable = new Runnable() {
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "VideosFragment - BroadcastConnexion - onReceive connecte true - badapter.getCount() == 0 getServerInfo apres 5sec");
                                getServerInfo(url, 0);
                            }
                        }, 2000);
                    }
                };
                getActivity().runOnUiThread(runnable);

            }

            //Si non connecte  Internet et isLoading()

            if ( connecte.isConnectingToInternet() && adapter.getItemCount() > 0 ) {
                mSwipeRefreshLayout.setRefreshing(true);
                Log.d(TAG, "VideosFragment STEP - BroadcastConnexion - onReceive - connecte true - adapter.getItemCount() : " + adapter.getItemCount() + " - currentPage : " + currentPage);
                setListView();

                if ( rowCountInDb > 0 ) {
                    getSqlInfo(currentPage);
                }
                adapter.setDeconnecte(false );
            }

            //Si non connecte  Internet et isLoading()

            if ( !connecte.isConnectingToInternet()  ) {
                Log.d(TAG, "VideosFragment STEP - BroadcastConnexion - onReceive - connecte false" );

                if ( rowCountInDb > 0 ) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    Log.d(TAG, "VideosFragment STEP - BroadcastConnexion - onReceive - connecte false - rowCountInDb: "+rowCountInDb );
                    setListView();
                    getSqlInfo(currentPage);

                } else {
                    setEmptyView(getString(R.string.error_non_connecte));
                    adapter.setDeconnecte(true);
                }
            }


        }
    }
    void getSqlInfo(int page) {
        pagerserver = page;
        if ( page > videoList.size() / Config.LIMIT_GENERAL  ) {
            pagerserver = videoList.size() / Config.LIMIT_GENERAL;
        }

        Log.d(TAG, "VideosFragment - getSqlInfo - pageserver : "+pagerserver );
        new loadSqlInfo().execute( String.valueOf( page ) );
    }
    Integer getCountRow(){
        From from = new Select()
                .from(TableVideos.class)
                ;

        if ( sortfields.equals("publishedAt")  ) {
            from.where("listeNew = 1");
        }  else if (  sortfields.equals("statistics_viewCount")  ) {
            from.where("listeView = 1");
        } else if (  sortfields.equals("statistics_commentCount")  ) {
            from.where("listeComment = 1");
        } else if (  sortfields.equals("contentDetails_duration")  ) {
            from.where("listeDuration = 1");
        } else if (  sortfields.equals("publishedCommentLast")  ) {
            from.where("listeCommentDate = 1");
        } else if (  sortfields.equals("statistics_viewCount_hier")  ) {
            from.where("listeViewHier = 1");
        } else if (  sortfields.equals("statistics_likeCount")  ) {
            from.where("listeLike = 1");
        }

        return from.count();
    }
    private class loadSqlInfo extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "VideosFragment STEP loadSqlInfo onPreExecute");
        }


        @Override
        protected String doInBackground(final String... params) {

            Log.d(TAG, "VideosFragment STEP loadSqlInfo doInBackground - sortfields " +sortfields);


            From from = new Select()
                    .from(TableVideos.class)
            ;

            if ( sortfields.equals("publishedAt")  ) {
                from.where("listeNew = 1");
            }  else if (  sortfields.equals("statistics_viewCount")  ) {
                from.where("listeView = 1");
            } else if (  sortfields.equals("statistics_commentCount")  ) {
                from.where("listeComment = 1");
            } else if (  sortfields.equals("contentDetails_duration")  ) {
                from.where("listeDuration = 1");
            } else if (  sortfields.equals("publishedCommentLast")  ) {
                from.where("listeCommentDate = 1");
            } else if (  sortfields.equals("statistics_viewCount_hier")  ) {
                from.where("listeViewHier = 1");
            } else if (  sortfields.equals("statistics_likeCount")  ) {
                from.where("listeLike = 1");
            }


            rowCountInDb = from.count();

            int offset = Integer.parseInt(params[0]) * Config.LIMIT_GENERAL;

            from = from.orderBy( (sortfields.equals("publishedAt")?"endtime_uploaded DESC, publishedAt ":sortfields) + " DESC")
                    .limit(Config.LIMIT_GENERAL)
                    .offset(offset);

            Log.d(TAG, "VideosFragment STEP loadSqlInfo doInBackground - rowCountInDb: "+rowCountInDb+" - SQL "+from.toSql());

            List<TableVideos> videos = from.execute();

            for (int count = 0; count < videos.size(); count++) {

                if (  count == 0 && offset == 0  ) {
                    Log.d(TAG, "VideosFragment STEP loadSqlInfo doInBackground 1st Row : "+videos.get(count).titre+" - sortfields : "+sortfields );
                }

                if ( !adapter.idInAdapter(videos.get(count).serverid  ) ) {
                    videoList.add( fillItemDb(videos.get(count)));

                    Log.d(TAG, "VideosFragment STEP loadSqlInfo badapter - Nom : " + videos.get(count).titre + " - ID : " + videos.get(count).serverid + " ajouté dans adapter via BDD à la position "+count);
                } else {
                    Log.d(TAG,"VideosFragment STEP loadSqlInfo badapter - Nom : "+videos.get(count).titre+" - ID : "+videos.get(count).serverid+" existe deja dans adapter ");
                }
            }


            adapter.setServerListSize(rowCountInDb);
            adapter.setLoadedSize(videoList.size());
            publishProgress("1");

            return params[0];
        }

        @Override
        public void onProgressUpdate(String... params) {
            Log.d(TAG, "VideosFragment STEP loadSqlInfo onProgressUpdate");
            if ( !isAdded() ) {
                Log.d(TAG, "VideosFragment STEP loadSqlInfo onProgressUpdate isAdded() false");
                return;
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    adapter.notifyDataSetChanged();
                    loaded = true;
                }
            });

        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(final String args) {
            Log.d(TAG, "VideosFragment STEP loadSqlInfo onPostExecute");
            

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "VideosFragment STEP - onCreateView " );
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_videos, container, false);
        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {




        inflater.inflate(R.menu.menu_fragment_videos, menu);
        MenuItem item = menu.findItem(R.id.search);
        final SearchView sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);

        sv.setSuggestionsAdapter(mSearchViewAdapter);
        sv.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String titre = cursor.getString(1);
                String id = cursor.getString(3);
                String videoyt = cursor.getString(2);
                String contentDetails_definition = cursor.getString(4);
                //sv.setQuery(titre, false);
                sv.clearFocus();

                if ( !contentDetails_definition.isEmpty() )
                    showDetailIntent( new video(id, videoyt, contentDetails_definition ) );
                else {
                    //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();
                    showListVideosArtisteIntent(id, titre);
                }

                return true;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                Cursor cursor = (Cursor) sv.getSuggestionsAdapter().getItem(position);
                String titre = cursor.getString(1);
                String id = cursor.getString(3);
                String videoyt = cursor.getString(2);
                String contentDetails_definition = cursor.getString(4);
                //sv.setQuery(titre, false);
                sv.clearFocus();

                if ( !contentDetails_definition.isEmpty() )
                    showDetailIntent( new video(id, videoyt, contentDetails_definition ) );
                else {
                    //Toast.makeText(getActivity(), "titre: " + titre + " (" + id + ")", Toast.LENGTH_SHORT).show();
                    showListVideosArtisteIntent(id,titre);
                }

                //Toast.makeText(getActivity(), "titre: " + titre+" ("+id+")", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                VazotsaraShareFunc.showSnackMsg(String.format(getString(R.string.sprintf_search), query), getActivity());

                 if (query.length() > 2) {
                    loadData(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                 if (query.length() > 2) {
                    loadData(query);
                }
                return false;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);

    }
    void showDetailIntent(video vid)
    {
        Log.d(TAG, "VideosFragment - showDetailIntent - uid : " + vid.getId() + " - videoytid :" + vid.getVideoytid());
        Intent VDetailActivity = new Intent( getActivity(), VideoDetailActivity.class);

        VDetailActivity.putExtra("uid", vid.getId());
        VDetailActivity.putExtra("videoytid", vid.getVideoytid());
        VDetailActivity.putExtra("contentDetails_definition", vid.getContentDetails_definition());
        getActivity().startActivityForResult(VDetailActivity, 100);

    }

    void showListVideosArtisteIntent(String uid, String titre)
    {
        Intent VideoSearchActivity = new Intent( getActivity(), VideosByArtistActivity.class);
        VideoSearchActivity.putExtra("playlisteid", uid );
        VideoSearchActivity.putExtra("titre", titre );
        VideoSearchActivity.putExtra("opt", "artiste" );
        getActivity().startActivity(VideoSearchActivity);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_sort:
                showDialogList( );
                break;
        }
        return true;
    }
    void showDialogList( ) {

        if ( !connecte.isConnectingToInternet()  ) {

            //VazotsaraShareFunc.showSnackMsg(getString(R.string.error_non_connecte), getActivity());
            //return;
        }



         new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_sort)
                .items(R.array.sort_list)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                        switch (which) {
                            case 0:
                                //Les plus récentes
                                sortfields = "publishedAt";

                                break;
                            case 1:
                                //Les commentées récemment
                                sortfields = "publishedCommentLast";

                                break;
                            case 2:
                                //Les plus vues hier
                                sortfields = "statistics_viewCount_hier";

                                break;
                            case 3:
                                //Nombre de vues
                                sortfields = "statistics_viewCount";

                                break;
                            case 4:
                                //Nombre de likes
                                sortfields = "statistics_likeCount";

                                break;
                            case 5:
                                //Nombre de commentaires
                                sortfields = "statistics_commentCount";

                                break;
                            case 6:
                                //Durée
                                sortfields = "contentDetails_duration";

                                break;
                        }



                        String sortfields_label = "";

                        switch (sortfields) {

                            case "statistics_viewCount":
                                //Nombre de vues
                                sortfields_label = getString(R.string.sortopt_statistics_viewCount);
                                break;
                            case "publishedCommentLast":
                                sortfields_label = getString(R.string.sortopt_publishedCommentLast);
                                break;
                            case "statistics_viewCount_hier":
                                sortfields_label = getString(R.string.sortopt_statistics_viewCount_hier);
                                break;
                            case "statistics_likeCount":
                                //Nombre de likes
                                sortfields_label = getString(R.string.sortopt_statistics_likeCount);
                                break;
                            case "statistics_commentCount":
                                //Nombre de commentaires
                                sortfields_label = getString(R.string.sortopt_statistics_commentCount);
                                break;
                            case "contentDetails_duration":
                                //Durée
                                sortfields_label = getString(R.string.sortopt_contentDetails_duration);
                                break;
                            default:
                                sortfields_label = getString(R.string.sortopt_uploaded_date);
                                break;
                        }

                        MainActivity.setTitleActivity(sortfields_label);

                        updateAdapter();

                    }
                })
                .show();
    }




    void updateAdapter(){
        if ( !connecte.isConnectingToInternet()  ) {
            Log.d(TAG,"VideosFragment updateAdapter - connecte false - getCountRow(): "+getCountRow()+" - rowCountInDb: "+rowCountInDb);
           if ( rowCountInDb > 0 || getCountRow() > 0  ) {
               if ( getCountRow() == 0  ) {
                   setEmptyView(getString(R.string.empty_video));
               } else {
                   setListView();
                   getSqlInfo(0);
               }
            }
        } else {
            Log.d(TAG, "VideosFragment updateAdapter - connecte true getServerInfo");
            getServerInfo(url, 0);
        }
        videoList.clear();
        adapter.notifyDataSetChanged();

    }

    private void openSearchSortActivity() {

        Intent i = new Intent(getActivity(), VideoSearchSortActivity.class);

        if ( search!=null && !search.isEmpty() ) {
            i.putExtra("search", search);
        }

        if ( sortfields!=null && !sortfields.isEmpty() ) {
            i.putExtra("sortfields", sortfields);
        }


        getActivity().startActivity(i);
    }

    public void setLoadingView()
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(final String error)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.VISIBLE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_error.findViewById(R.id.textView);
        Button btn_refresh = (Button) viewstub_recyclerview_error.findViewById(R.id.btn_refresh);

        textv.setText(error);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connecte.isConnectingToInternet()) {
                    VazotsaraShareFunc.showSnackMsg(error, getActivity());
                } else {
                    setLoadingView();
                    getServerInfo(url, 0);
                }
            }
        });
    }

    public void setEmptyView(String textempty)
    {
        viewstub_recyclerview_content.setVisibility(View.GONE);
        viewstub_recyclerview_empty.setVisibility(View.VISIBLE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_recyclerview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_recyclerview_content.setVisibility(View.VISIBLE);
        viewstub_recyclerview_empty.setVisibility(View.GONE);
        viewstub_recyclerview_error.setVisibility(View.GONE);
        viewstub_recyclerview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(TAG, "VideosFragment STEP onActivityCreated");
        super.onActivityCreated(savedInstanceState);



        mSearchViewAdapter = new VideoSearchSuggestionsAdapter( getActivity(), R.layout.adapter_videosearchsuggestion_item, null, columns,null, -1000);


        viewstub_recyclerview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_recyclerview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_recyclerview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_recyclerview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();


        recyclerView = (RecyclerView) viewstub_recyclerview_content.findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        /*recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        Log.d(TAG, "VideosFragment STEP recyclerView Clic item " + videoList.get(position).getTitre());
                        Runnable runnable = new Runnable() {
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        clickpos = position;
                                        Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                                        VDetailActivity.putExtra("uid", videoList.get(position).getId());
                                        VDetailActivity.putExtra("videoytid", videoList.get(position).getVideoytid());
                                        VDetailActivity.putExtra("contentDetails_definition", videoList.get(position).getContentDetails_definition());
                                        VDetailActivity.putExtra("position", position);
                                        getActivity().startActivityForResult(VDetailActivity, 100);
                                    }
                                }, 1000);
                            }
                        };
                        getActivity().runOnUiThread(runnable);


                    }
                })
        );*/



        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager, getActivity().getActionBar()) {
            @Override
            public void onLoadMore(int current_page) {
                current_page = pagerserver+1;

                Log.d(TAG, "VideosFragment STEP - recyclerView.addOnScrollListener - page " + current_page);
                if ( !connecte.isConnectingToInternet()  ) {
                    Log.d(TAG, "VideosFragment STEP - recyclerView.addOnScrollListener - !connecte.isConnectingToInternet() -  page " + current_page+" - rowCountInDb: "+rowCountInDb+" - getCountRow():"+getCountRow()+" SQL Count:"+new Select().from(TableVideos.class).where("listeNew = 1").count());

                    if ( rowCountInDb > 0 ) {
                        getSqlInfo(currentPage);
                    }
                } else {
                    getServerInfo(url, current_page);
                }
                currentPage = current_page;
            }
        });

        fab = (FloatingActionButton) viewstub_recyclerview_content.findViewById(R.id.fab);
        //Cacher si non admin
        if (Useful.isAdmin(getActivity())) {
            fab.setVisibility(View.VISIBLE);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VazotsaraShareFunc.clickFabVideoFragment(getActivity());
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) viewstub_recyclerview_content.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //refresh ici
                getLastVideo();

            }
        });



    }

    void getLastVideo(){
        if ( !connecte.isConnectingToInternet()  ) {
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        }

        final String lastvideo = (VazotsaraShareFunc.getLastVideoidInPreferences(getActivity())!=null?VazotsaraShareFunc.getLastVideoidInPreferences(getActivity()):"");

        if ( !lastvideo.isEmpty() && Integer.parseInt(lastvideo)>0 ) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

            vidapi.getListVideosBeforeId( lastvideo, new Callback<VideoPojo>() {

                @Override
                public void success(VideoPojo serverinfo, Response response) {
                    //Si des nouvelles videos existent?
                    if ( serverinfo.getVideos().size()>0 && !serverinfo.getVideos().get(0).getId().equals(lastvideo) ) {
                        //Refresh
                        addToList(serverinfo.getVideos());
                        adapter.notifyDataSetChanged();

                        //Enregistrer dernière video dans Preferences
                        String lastvideoid = serverinfo.getVideos().get(0).getId();
                        if ( VazotsaraShareFunc.getLastVideoidInPreferences(getActivity())!=null  )
                            VazotsaraShareFunc.setLastVideoidInPreferences( lastvideoid , getActivity().getApplicationContext());

                        //Afficher Notif
                        VazotsaraShareFunc.showSnackMsg( String.format(getString(R.string.msg_confirm_videos_added), serverinfo.getVideos().size() ), getActivity());
                    }

                    // Stop refresh animation dans tous les cas
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG, "Error "+error);
                    VazotsaraShareFunc.showSnackMsg( getString(R.string.error_failure_retrofit), getActivity());
                }
            });


        }

    }
    private void loadData(String searchText) {
        if ( !connecte.isConnectingToInternet()  ) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);




        vidapi.getSearchVideo(searchText, "v", new Callback<SearchVideo>() {
            @Override
            public void success(SearchVideo searchResult, retrofit.client.Response response) {
                //setSuccessInfo(videoPojo);
                //    Log.d(TAG,"searchResult.getResults() : "+searchResult.getResults().size());
                if (searchResult != null) {
                    MatrixCursor matrixCursor = convertToCursor(searchResult.getResults(), searchResult.getArtiste());
                    mSearchViewAdapter.changeCursor(matrixCursor);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.d(TAG, "VideosFragment searchResult.getResults() error : " + error.getMessage());

            }
        });

    }

     private MatrixCursor convertToCursor(List<SingleResultSearch> SingleResultSearch, List<SingleResultSearchArtiste> SingleResultSearchArtiste) {
        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;
         for (SingleResultSearchArtiste rowArtiste : SingleResultSearchArtiste) {
             String[] temp = new String[5];
             i = i + 1;
             temp[0] = Integer.toString(i);
             temp[1] = rowArtiste.getObjetnom();
             temp[2] = rowArtiste.getImage();
             temp[3] = rowArtiste.getId();
             temp[4] = "";


             Log.d(TAG,"temp[1] : "+temp[1]);
             cursor.addRow(temp);
         }
        for (SingleResultSearch rowVideo : SingleResultSearch) {
            String[] temp = new String[5];
            i = i + 1;
            temp[0] = Integer.toString(i);
            temp[1] = rowVideo.getTitre();
            temp[2] = rowVideo.getVideoytid();
            temp[3] = rowVideo.getId();
            temp[4] = rowVideo.getContentDetails_definition();


            Log.d(TAG,"temp[1] : "+temp[1]);
            cursor.addRow(temp);
        }


        return cursor;
    }
    void getServerInfo(String url, int page )
    {
        if ( !connecte.isConnectingToInternet()  ) {
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        }

        mSwipeRefreshLayout.setRefreshing(true);

        //setLoadingInterface();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



        Log.d(TAG, "VideosFragment URL retrofit 608 : " + url + "/videos?limit="+Config.LIMIT_GENERAL+"&page=" + page + "&q=&sortfields=" + sortfields);


        if ( !search.isEmpty() ) {
            vidapi.getListVideos(Config.LIMIT_GENERAL, sortfields, page, search, new Callback<VideoPojo>() {
                @Override
                public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                    setSuccessInfo(videoPojo);
                }

                @Override
                public void failure(RetrofitError error) {
                    setFailureInfo(error);
                }
            });
        } else {
            vidapi.getListVideos(Config.LIMIT_GENERAL, sortfields, page, new Callback<VideoPojo>() {
                @Override
                public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                    setSuccessInfo(videoPojo);
                }

                @Override
                public void failure(RetrofitError error) {
                    setFailureInfo(error);
                }
            });
        }

    }

    void setSuccessInfo(VideoPojo videoPojo){

        mSwipeRefreshLayout.setRefreshing(false);
        if (videoPojo.getPage() >= pagerserver ) {
            pagerserver = videoPojo.getPage();
        }
        Log.d(TAG, "VideosFragment STEP  pagerserver (serveur) : " + pagerserver);


        if (videoPojo.getVideos().size() > 0) {
            Log.d(TAG, "STEP - getInfo - L571 - success " + videoPojo.getVideos().get(0).getTitre());
            saveBDD(videoPojo.getVideos());

            setListView();

            addToList(videoPojo.getVideos());
            adapter.notifyDataSetChanged();
            adapter.setServerListSize(videoPojo.getTotal());
            loaded = true;
        }
    }

    void setFailureInfo(RetrofitError error){
        mSwipeRefreshLayout.setRefreshing(false);
        String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
        Log.d(TAG, "VideosFragment STEP - getInfo - L586 - fail " + this.getClass().getSimpleName());
        if (adapter.getItemCount() == 0) {
            setErrorView(errorMessage);
        } else {
            if ( getActivity() != null ) {
                VazotsaraShareFunc.showSnackMsg( errorMessage, getActivity());
            }
        }
    }
    void saveBDD(List<Videogson> vgadded){
        new saveBddCache().execute(vgadded);
    }
    void addAsync(final List<Videogson> vgadded){
        Type resultType = new TypeToken<List<Videogson>>() {}.getType();

        final List<Videogson> vg = new ArrayList<Videogson>();

        Reservoir.getAsync("videoList", resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG, "VideosFragment Reservoir fillCacheAdapter getAsync onSuccess");
                //success


                for (int count = 0; count < videos.size(); count++) {
                    vg.add(videos.get(count));
                    Log.d(TAG, " VideosFragment Cache getTitre : " + videos.get(count).getTitre());
                }

                for (int count = 0; count < vgadded.size(); count++) {
                    vg.add(vgadded.get(count));
                    Log.d(TAG, "VideosFragment Cache getTitre : " + vgadded.get(count).getTitre());
                }

                Reservoir.putAsync("videoList", vg, new ReservoirPutCallback() {
                    @Override
                    public void onSuccess() {
                        //success
                        Log.d(TAG, "VideosFragment Reservoir putAsync onSuccess");
                        //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        //error
                        Log.d(TAG, "VideosFragment Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                    }
                });

            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG, "VideosFragment Reservoir fillCacheAdapter getAsync onFailure " + e.getMessage());
                Reservoir.putAsync("videoList", vgadded, new ReservoirPutCallback() {
                    @Override
                    public void onSuccess() {
                        //success
                        Log.d(TAG, "VideosFragment Reservoir putAsync onSuccess");
                        //fillCacheAdapter("videoListCache",videoList, badapter, grd_newvideo, progress_wheel_grd_newvideo);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        //error
                        Log.d(TAG, "VideosFragment Reservoir putAsync onFailure " + (e.getMessage() != null ? e.getMessage() : ""));
                    }
                });
            }
        });
    }

    private class saveBddCache extends AsyncTask<List<Videogson>, String, String>
    {

        @Override
        protected String doInBackground(List<Videogson>... params) {

            if ( params[0].size() > 0 ) {
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInVideos(params[0], sortfields );
                    Log.d(TAG, "VideosFragment STEP saveBddCache doInBackground sortfields:" + sortfields );
                }
            }
            return null;
        }
    }
    void fillItemCacheAdapter(int clicpos) {

        Type resultType = new TypeToken<Videogson>() {}.getType();
        Log.d(TAG, "VideosFragment Reservoir fillItemCacheAdapter");

        if (clickpos!=-1 && !videoList.get(clickpos).getId().equals("") ) {
            Reservoir.getAsync("single_"+videoList.get(clickpos).getId(), resultType, new ReservoirGetCallback<Videogson>() {
                @Override
                public void onSuccess(Videogson videos) {
                    Log.d(TAG, "VideosFragment Reservoir fillItemCacheAdapter getAsync onSuccess");

                    videoList.get(clickpos).setStatistics_viewCount( videos.getStatisticsViewCount() );
                    videoList.get(clickpos).setStatistics_likeCount( videos.getStatisticsLikeCount() );
                    videoList.get(clickpos).setContentDetails_definition( videos.getContentDetailsDefinition() );
                    adapter.notifyDataSetChanged();


                }

                @Override
                public void onFailure(Exception e) {
                    //error
                    Log.d(TAG, "VideosFragment Reservoir fillItemCacheAdapter getAsync onFailure " + e.getMessage());

                }
            });
        }




    }
    void fillCacheAdapter() {

        Type resultType = new TypeToken<List<Videogson>>() {}.getType();
        Log.d(TAG, "VideosFragment Reservoir fillCacheAdapter");



        Reservoir.getAsync("videoList", resultType, new ReservoirGetCallback<List<Videogson>>() {
            @Override
            public void onSuccess(List<Videogson> videos) {
                Log.d(TAG, "VideosFragment Reservoir fillCacheAdapter getAsync onSuccess");
                //success
                Log.d(TAG, "VideosFragment Cache getTitre : " + videos.get(0).getTitre());

                if (isAdded())
                    setListView();


                for (int count = 0; count < videos.size(); count++) {
                    if (!adapter.idInAdapter(videos.get(count).getId())) {
                        videoList.add(fillItem(videos.get(count)));
                    }
                }
                adapter.notifyDataSetChanged();
                loaded = true;

            }

            @Override
            public void onFailure(Exception e) {
                //error
                Log.d(TAG, "VideosFragment Reservoir fillCacheAdapter getAsync onFailure " + e.getMessage());
                if (!connecte.isConnectingToInternet()) {
                    setEmptyView(getString(R.string.error_non_connecte));
                } else {
                    setLoadingView();
                }

            }
        });
    }
    void addToList(List<Videogson> videos) {
        int added = 0;
        for (int count = 0; count < videos.size(); count++) {
            if ( !adapter.idInAdapter( videos.get(count).getId()  ) ) {
                videoList.add(fillItem(videos.get(count)));
                added++;
                Log.d(TAG, "VideosFragment badapter - Nom : " + videos.get(count).getTitre() + " - ID : " + videos.get(count).getId() + " ajouté dans adapter ");
            } else {
                Log.d(TAG, "VideosFragment badapter - Nom : " + videos.get(count).getTitre() + " - ID : " + videos.get(count).getId() + " existe deja dans adapter ");
            }
        }
        adapter.setLoadedSize(videoList.size());
    }
    video fillItemDb(TableVideos field) {

        video v = new video();
        v.setTitre(field.titre);
        v.setId(field.serverid);
        v.setVideoytid( field.videoytid);
        v.setStatistics_viewCount(String.valueOf(field.statisticsViewCount ));
        v.setStatistics_likeCount(String.valueOf(field.statisticsLikeCount ) );
        v.setStatistics_dislikeCount( String.valueOf( field.statisticsDislikeCount ) );
        v.setThumburl(field.thumburl);

        v.setDuree( String.valueOf( field.contentDetailsDuration) );
        v.setFormat( field.contentDetailsDefinition);

        v.setContentDetails_definition(field.contentDetailsDefinition);

        return v;

    }
    video fillItem(Videogson videogson) {

        video v = new video();
        v.setTitre(videogson.getTitre());
        v.setId( videogson.getId() );
        v.setVideoytid( videogson.getVideoytid()  );
        v.setStatistics_viewCount(videogson.getStatisticsViewCount() );
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount() );
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount( videogson.getStatisticsDislikeCount() );
        v.setThumburl(videogson.getThumburl());

        v.setDuree(videogson.getContentDetailsDuration());
        v.setFormat(videogson.getContentDetailsDefinition());

        v.setContentDetails_definition(videogson.getContentDetailsDefinition());

        return v;

    }

}

package vazo.tsara.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;
import com.rey.material.widget.CheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import vazo.tsara.R;
import vazo.tsara.adapters.AutocompleteAdapterSearchVideo;
import vazo.tsara.adapters.VideosTWVAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.libraries.DelayAutoCompleteTextView;
import vazo.tsara.libraries.TwoWayGridView;
import vazo.tsara.models.FieldUpdatePlaylist;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.TokenPojo;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.relatedvideomodel;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistEditFragment extends Fragment {

    String uid;
    String idartista;
    static String TAG = Config.TAGKEY;

    String o_titre;
    String o_titreyt;
    String o_description;
    String o_status;
    String o_type;
    String o_videos;

    View rootView;

    MaterialSpinner spinn_status, spinn_type;


    TextView idartist;
    TextView thumburlpath;
    TextView nbvideos;

    TextView firstvideoyt;
    TextView playlisteyt;


    TwoWayGridView twgv_videos;




    Button btn_add_videos;



    EditText edit_text_titre;
    EditText edit_text_titre_yt;
    EditText edit_text_description;



    View positiveAction;
    View neutralAction;

    DelayAutoCompleteTextView dialogautocomplete;


    private ArrayAdapter<CharSequence>  adapter_status, adapter_category;

    List<relatedvideomodel> artistelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> realisateurlist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> genrelist = new ArrayList<relatedvideomodel>();
    List<relatedvideomodel> playlistlist = new ArrayList<relatedvideomodel>();

    VideosTWVAdapter videotwadapter ;


    ActionProcessButton btnSignIn;

    String[] idartistsImplode;
    String[] idgenreImplode;
    String[] idplaylistsImplode;
    String[] idrealisateurImplode;

    String[] o_idartistsImplode;
    String[] o_idgenreImplode;
    String[] o_idplaylistsImplode;
    String[] o_idrealisateurImplode;



    RelativeLayout source;

    Playlist plinfo;


    private String[] idCategoryYt = { "15","2","24", "27", "1", "23", "20", "10", "29", "22", "28", "17", "26", "19" };


    String videoytid;

    List<Videogson> videolistinTwv = new ArrayList<Videogson>();

    CheckBox chk_rename_all_videos;
    CheckBox chk_remove_doublons;


    public PlaylistEditFragment() {
        // Required empty public constructor
    }

    public PlaylistEditFragment newInstance(Playlist p ) {
        PlaylistEditFragment frag = new PlaylistEditFragment();
        Bundle args = new Bundle();
        args.putSerializable("plinfo", p );
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        plinfo = (Playlist) getArguments().getSerializable("plinfo");


        o_titre = plinfo.getObjetnom();
        o_titreyt = plinfo.getNom();
        o_status = plinfo.getStatus();
        o_type = plinfo.getObjetype();
        o_description = plinfo.getDescriptionYt();
        uid = plinfo.getId();

        Log.d(TAG, "Playlist \n plinfo - " +
                "o_titre " + o_titre + " - " +
                "titreyt : " + o_titreyt  + " - " +
                "o_status : " + o_status  + " - " +
                "o_type : " + o_type  + " - " +
                "o_description : " + o_description  + " - "
        );






    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_playlist, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        initView();





        //String[] ITEMS = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"};
        adapter_status = ArrayAdapter.createFromResource(getActivity(), R.array.status_playliste, android.R.layout.simple_spinner_item);

        //adapter_status = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, ITEMS  );
        adapter_status.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_status.setAdapter(adapter_status);






        adapter_category = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.playliste_type)  );
        adapter_category.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinn_type.setAdapter(adapter_category);






        btn_add_videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAdd( videolistinTwv, videotwadapter);
            }
        });





        twgv_videos.setAdapter(videotwadapter);


        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);



        //Reinitialiser le button signin si le texte a été modifié
        Log.d(TAG, "btnSignIn.getProgress(): " + btnSignIn.getProgress());

        edit_text_titre.addTextChangedListener(watcherBtnSignIn);
        edit_text_titre_yt.addTextChangedListener(watcherBtnSignIn);
        edit_text_description.addTextChangedListener(watcherBtnSignIn);
        spinn_type.setOnItemSelectedListener(spinnChangeBtnSignin);
        spinn_status.setOnItemSelectedListener(spinnChangeBtnSignin);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressGenerator.start(btnSignIn);


                Log.d(TAG, "noErrorOnSubmit --- titre.isEmpty()? : " + edit_text_titre.getText().toString().isEmpty() + " - titre.contains(\"::\")? " + edit_text_titre.getText().toString().contains("::") + " - artisterecycleradapter.getCount() : " + videotwadapter.getCount());

                if (noErrorOnSubmit()) {

                    saveForm();
                } else {
                    VazotsaraShareFunc.showSnackMsg(getString(R.string.error_connexion_submit), getActivity());
                }


            }
        });

        //Remplir les champs
        edit_text_titre.setText(o_titre);
        edit_text_titre_yt.setText(o_titreyt);
        edit_text_description.setText(o_description);


        String objetype = "Artiste";
        switch (o_type) {
            case "artiste" :
                //artiste
                objetype = "Artiste";
                break;
            case "genre" :
                //genre
                objetype = "Genre";
                break;
            case "realisateur" :
                //realisateur
                objetype = "Réalisateur";
                break;
            case "Autre" :
                //playlist
                objetype = "autre";
                break;
        }
        spinn_type.setSelection( adapter_category.getPosition(objetype)+1);

        String statuspl = "public";
        switch (o_status) {
            case "public" :
                //artiste
                statuspl = "Public";
                break;
            case "private" :
                //genre
                statuspl = "Privé";
                break;

        }
        spinn_status.setSelection( adapter_status.getPosition(statuspl)+1);

        getVideoInServer();



    }

    void getVideoInServer()
    {


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



        vidapi.getAllVideosInPlaylists(plinfo.getId(), new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo servInfo, retrofit.client.Response response) {
                //o_idartistsImplode = new String[artisterecycleradapter.getCount()];
                ArrayList<String> videosinpl = new ArrayList<String>();
                for (int i = 0; i < servInfo.getVideos().size(); i++) {
                    Videogson vid = new Videogson();
                    vid.setId(servInfo.getVideos().get(i).getId());
                    vid.setTitre((servInfo.getVideos().get(i).getTitre().length() > 50 ? servInfo.getVideos().get(i).getTitre().substring(0, 50) + "..." : servInfo.getVideos().get(i).getTitre()));
                    vid.setThumburl(servInfo.getVideos().get(i).getThumburl());

                    videosinpl.add(servInfo.getVideos().get(i).getId());

                    videolistinTwv.add(vid);
                    videotwadapter.notifyDataSetChanged();

                }

                o_videos = TextUtils.join(",", videosinpl);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "Error : " + (error.getMessage() != null ? error.getMessage() : ""));
            }
        });

    }

    void saveForm() {

        String[] idvideosImplode = new String[videotwadapter.getCount()];


        for (  int n = 0 ; n < videotwadapter.getCount(); n = n+1 ) {
            idvideosImplode[n] = videotwadapter.getFieldAt(n,"id");
        }

        Log.d(TAG, "Implode idvideosImplode : " + TextUtils.join(",", idvideosImplode));


        String titre = edit_text_titre.getText().toString();
        String titreyt = edit_text_titre_yt.getText().toString();
        String description = edit_text_description.getText().toString();
        String status = spinn_status.getSelectedItem().toString();
        String type = spinn_type.getSelectedItem().toString();

        switch(status) {
            case "Public" :
                status = "public";
                break;
            case "Privé" :
                status = "private";
                break;
        }

        switch(type) {
            case "Artiste" :
                type = "artiste";
                break;
            case "Réalisateur" :
                type = "realisateur";
                break;
            case "Genre" :
                type = "genre";
                break;
            case "Autre" :
                type = "autre";
                break;
        }

        Log.d(TAG, "Is Admin? " + Useful.isAdmin(getActivity()));


        Log.d(TAG, "Form values // " +
                        "titre : " + titre + " - " +
                        "titreyt : " + titreyt + " - " +
                        "videos: " + TextUtils.join(",", idvideosImplode) + " - " +
                        "status: " + status + " - " +
                        "type: " + type + " - " +
                        "description:" + description + " - " +
                        "o_titre: " + o_titre + " - " +
                        "o_titreyt: " + o_titreyt + " - " +
                        "o_videos: " + o_videos + " - " +
                        "o_status: " + o_status + " - " +
                        "o_type: " + o_type + " - " +
                        "eviter_doublons: " + chk_rename_all_videos.isChecked() + " - " +
                        "eviter_doublons: " + chk_remove_doublons.isChecked() + " - " +

                        "Config.CREATEDBY : " + Config.CREATEDBY
        );





        if ( isFieldEdited( titre, titreyt, description, status, type, TextUtils.join(",", idvideosImplode)  ) ) {

            //Changer les valeurs initiales
            o_titre = titre;
            o_titreyt = titreyt;
            o_status = status;
            o_type = type;
            o_description = description;

            btnSignIn.setEnabled(false);
            btnSignIn.setProgress(1);

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Config.URLAPI).setConverter(new LenientGsonConverter(new Gson()))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestInterceptor.RequestFacade request) {
                            if ( Useful.isAdmin(getActivity()) ) {
                                request.addHeader("Authorization", " Bearer "+VazotsaraShareFunc.getAdminToken(getActivity()));
                            }
                        }
                    })
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);
            vidapi.postUpdatePlaylist(
                    uid,
                    new FieldUpdatePlaylist(
                            titre,
                            titreyt,
                            description,
                            type,
                            TextUtils.join(",", idvideosImplode),
                            status,
                            Config.CREATEDBY,
                            (chk_rename_all_videos.isChecked()?"1":"0"),
                            (chk_rename_all_videos.isChecked()?"1":"0"),
                            o_videos
                    )
                    , new Callback<TokenPojo>() {
                        @Override
                        public void success(TokenPojo sourceinfo, Response response) {


                            VazotsaraShareFunc.showSnackMsg((sourceinfo.getToken() != null ? sourceinfo.getToken() : ""), getActivity());
                            btnSignIn.setProgress(100);


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            btnSignIn.setProgress(-1);
                            btnSignIn.setEnabled(true);

                            String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                            Log.d(TAG, "errorMessage : " + errorMessage);


                            //Log.d(TAG, "postLogin - error : " + error.getMessage());
                            if (error.getResponse() != null) {
                                String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                                try {
                                    JSONObject jsonObj = new JSONObject(json);
                                    Log.d(TAG, "postLogin - error : " + jsonObj.get("error"));

                                    VazotsaraShareFunc.showSnackMsg("Erreur : " + (jsonObj.get("error") != null ? jsonObj.get("error") : ""), getActivity());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {

                                VazotsaraShareFunc.showSnackMsg(errorMessage, getActivity());
                            }


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    btnSignIn.setProgress(0);
                                }
                            }, 500);
                        }
                    });
        } else {
            btnSignIn.setProgress(0);
            btnSignIn.setEnabled(true);

            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_noedit), getActivity());
        }



    }

    void initView(){
        spinn_status = (MaterialSpinner) rootView.findViewById(R.id.spinn_status);
        spinn_type = (MaterialSpinner) rootView.findViewById(R.id.spinn_type);

        edit_text_titre = (EditText) rootView.findViewById(R.id.edit_text_titre);
        edit_text_titre_yt = (EditText) rootView.findViewById(R.id.edit_text_titre_yt);
        edit_text_description = (EditText) rootView.findViewById(R.id.edit_text_description);

        twgv_videos = (TwoWayGridView) rootView.findViewById(R.id.twgv_videos);
        btn_add_videos = (Button) rootView.findViewById(R.id.btn_add_videos);
        videotwadapter = new VideosTWVAdapter(getActivity(), videolistinTwv, R.layout.adapter_videos_item_add  );
        btnSignIn = (ActionProcessButton) rootView.findViewById(R.id.btnSignIn);

        chk_rename_all_videos = (CheckBox) rootView.findViewById(R.id.chk_rename_all_videos);
        chk_remove_doublons = (CheckBox) rootView.findViewById(R.id.chk_remove_doublons);

    }

    void showDialogAdd( final List<Videogson> liste, final VideosTWVAdapter adapter)
    {
        int intTitle = R.string.dialog_addvideo;

         MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(intTitle)
                .customView(R.layout.dialog_addplaylist_invideo, true)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .positiveText(R.string.dialog_addvideo_btn_ok)
                .negativeText(android.R.string.cancel)
                .negativeColor(Color.parseColor("#000000"))
                .neutralColor(Color.parseColor("#000000"))
                .positiveColor(Color.parseColor("#000000"))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        idartista = idartist.getText().toString();



                        if (!dialogautocomplete.getText().toString().isEmpty() && !idartista.isEmpty() && Integer.parseInt(idartista)>0 ) {
                            addvideos(liste, adapter);
                        } else {
                            VazotsaraShareFunc.showSnackMsg(getString(R.string.error_choice_playlist), getActivity());
                            return;
                        }






                    }


                    @Override
                    public void onNegative(MaterialDialog dialog) {

                    }



                }).build();



        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        neutralAction = dialog.getActionButton(DialogAction.NEUTRAL);
        //noinspection ConstantConditions

        dialogautocomplete = (DelayAutoCompleteTextView) dialog.getCustomView().findViewById(R.id.dialogautocomplete);

        dialogautocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        idartist = (TextView) dialog.getCustomView().findViewById(R.id.idartist);
        thumburlpath = (TextView) dialog.getCustomView().findViewById(R.id.thumburlpath);
        nbvideos = (TextView) dialog.getCustomView().findViewById(R.id.nbvideos);

        firstvideoyt = (TextView) dialog.getCustomView().findViewById(R.id.firstvideoyt);
        playlisteyt = (TextView) dialog.getCustomView().findViewById(R.id.playlisteyt);

        dialogautocomplete.setThreshold(1);
        dialogautocomplete.setAdapter(new AutocompleteAdapterSearchVideo(getActivity()));

        dialogautocomplete.setLoadingIndicator((ProgressBar) dialog.getCustomView().findViewById(R.id.progressBar));
        dialogautocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Log.d(TAG, "adapterView.getAdapter().getItem(i).toString()" + ((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setText(((TextView) view.findViewById(R.id.titre)).getText().toString());
                dialogautocomplete.setSelection(dialogautocomplete.getText().length());

                idartist.setText(((TextView) view.findViewById(R.id.id)).getText().toString());
                thumburlpath.setText(((TextView) view.findViewById(R.id.thumburlpath)).getText().toString());
                nbvideos.setText(((TextView) view.findViewById(R.id.nbvideos)).getText().toString());

                firstvideoyt.setText(((TextView) view.findViewById(R.id.firstvideoyt)).getText().toString());
                playlisteyt.setText(((TextView) view.findViewById(R.id.playlisteyt)).getText().toString());

                if (Integer.parseInt(idartist.getText().toString()) > 0)
                    positiveAction.setEnabled(true);


            }
        });

        dialog.show();

        positiveAction.setEnabled(false); // disabled by default

    }

    void addvideos(final List<Videogson> liste, final VideosTWVAdapter adapter) {
        Videogson a = new Videogson();
        a.setId(idartista);
        a.setTitre(dialogautocomplete.getText().toString());
        a.setThumburl(nbvideos.getText().toString());

        liste.add(a);
        adapter.notifyDataSetChanged();
    }



    public boolean noErrorOnSubmit(){
        String titre = edit_text_titre.getText().toString();

        if( titre.isEmpty() )
            return false;


        if ( videotwadapter.getCount() == 0 )
            return false;



        return true;


    }

    @Override
    public void onResume() {
        super.onResume();
    }



    boolean isFieldEdited(String titre, String titreyt, String description, String status, String type, String videos ) {
        if ( !o_titre.equals( titre ) )
            return true;

        if ( !o_titreyt.equals( titreyt ) )
            return true;


        if ( !o_description.equals( description ) )
            return true;

        if ( !o_status.equals( status ) )
            return true;

        if ( !o_type.equals( type ) )
            return true;

        if ( !o_videos.equals( videos ) )
            return true;



        return false;
    }

    TextWatcher watcherBtnSignIn = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            btnSignIn.setEnabled(true);
            btnSignIn.setProgress(0);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    AdapterView.OnItemSelectedListener spinnChangeBtnSignin = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            btnSignIn.setEnabled(true);
            btnSignIn.setProgress(0);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

}

package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideosByArtistActivity;
import vazo.tsara.adapters.DividerItemDecoration;
import vazo.tsara.adapters.FavoriPlaylistsRecyclerAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.Useful;
import vazo.tsara.helpers.VazotsaraShareFunc;
import vazo.tsara.models.FavoriPlaylistSQL;
import vazo.tsara.models.Playlist;
import vazo.tsara.models.Playlists;
import vazo.tsara.models.TableFavoriplaylists;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.Dbhandler;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabfavoriteArtistsFragment extends Fragment {


    String url = Config.URLAPI;

    Boolean isLoading = true;



    String TAG = "VAZOTSARA";
    String sortfields = "objetnom";
    String sortfieldsascdesc = "asc";
    int currentPage = 0;

    View rootView;
    View emptyView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;



    //RecyclerView recyclerView;
    RecyclerView listView;
    SparseItemRemoveAnimator mSparseAnimator;




    List<Playlist> playList = new ArrayList<Playlist>();

    boolean loaded;

    //PlaylistsAdapter adapter;
    //FavoriPlaylistsBaseAdapter badapter;
    FavoriPlaylistsRecyclerAdapter badapter;

    Dbhandler db;


    Parcelable state;

    String[] columns = new String[]{"_id", "NOM", "IMAGE", "PLAYLISTEID" };

    @Override
    public void onCreate(Bundle savedInstanceState) {

        connecte = new ConnectionDetector(getActivity());

        super.onCreate(savedInstanceState);
        //Menu fragment
        setHasOptionsMenu(true);

        //db = new Dbhandler(getActivity().getApplicationContext());

    }

    void updateAdapter(String orderby, String asc){
        playList.clear();
        badapter.notifyDataSetChanged();

    }


    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG, "Analytics - onStart : " + this.getClass().getSimpleName());
        } catch(Exception  e) { }
    }

    public void onResume() {

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);

        super.onResume();
        Log.d(TAG, "TabhomeArtistsFragment - onResume");

        if ( loaded )
            setListView();



        //badapter = new FavoriPlaylistsBaseAdapter( getActivity() , playList, R.layout.adapter_artiste_item, R.layout.adapter_videoloading  );
        badapter = new FavoriPlaylistsRecyclerAdapter( getActivity() , playList, R.layout.adapter_artiste_item, R.layout.adapter_videoloading  );
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        listView.setAdapter(badapter);

                    }
                });
            }
        });
        thread.start();

        //badapter.setServerListSize(((db.getCountFavoriArtist() - db.getCountFavoriArtistHidden(1)) > 0 ?(db.getCountFavoriArtist()-db.getCountFavoriArtistHidden(1)):0 ) );
        badapter.setServerListSize(((TableFavoriplaylists.getCountArtistInFavori() - TableFavoriplaylists.getCountArtistInFavoriIsHidden(1)) > 0 ?(TableFavoriplaylists.getCountArtistInFavori()-TableFavoriplaylists.getCountArtistInFavoriIsHidden(1)):0 ) );

    }
    @Override
    public void onPause() {

        getActivity().unregisterReceiver(receiver);

        super.onPause();
        Log.d(TAG, "ArtistsFragment - onPause");
    }



    public TabfavoriteArtistsFragment() {
        // Required empty public constructor
    }

    public static TabfavoriteArtistsFragment newInstance() {
        TabfavoriteArtistsFragment frag = new TabfavoriteArtistsFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_playlists, container, false);
        return rootView;
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );


        listView = (RecyclerView) viewstub_listview_content.findViewById(R.id.list);
        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "adapterView.getAdapter().getItemViewType(i): " + adapterView.getAdapter().getItemViewType(i));
                if ( adapterView.getAdapter().getItemViewType(i) == 1 )
                    showListeVideoByPlaylists(((TextView) view.findViewById(R.id.uid)).getText().toString(), ((TextView) view.findViewById(R.id.titre)).getText().toString());
            }
        });*/

        /**
         * Asynctask pour charger les data
         */
        new loadFavoriPlaylistSQL().execute();



    }

    void showListeVideoByPlaylists(String uid, String titre )
    {
        Intent VideoSearchActivity = new Intent(getActivity(), VideosByArtistActivity.class);
        VideoSearchActivity.putExtra("playlisteid", uid);
        VideoSearchActivity.putExtra("titre", titre );
        getActivity().startActivityForResult(VideoSearchActivity, 100);
    }


    void addToList(List<FavoriPlaylistSQL> videos, List<Playlist> playList) {
        for (int count = 0; count < videos.size(); count++) {
            playList.add(fillItem(videos.get(count)));
        }
    }

    Playlist fillItem(FavoriPlaylistSQL videogson) {
        Playlist v = new Playlist();
        v.setNom(videogson.getTitre());
        v.setId(String.valueOf(videogson.getVideoid() )  );
        v.setNbVideos("0");
        v.setImage("");
        //v.setNbVideos(videogson.getNbVideos());
        //v.setImage(videogson.getImage());
        return v;
    }

    private class loadFavoriPlaylistSQL extends AsyncTask<String, String, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "loadFavoriPlaylistSQL  onPreExecute");
        }
        @Override
        protected String doInBackground(String... params) {

            Log.d(TAG, "loadFavoriPlaylistSQL  doInBackground" );

            getActivity().runOnUiThread( new Runnable() {
                @Override
                public void run() {



                    //List<FavoriPlaylistSQL> favorivideolist = db.getAllFavoriArtist(0);
                    List<FavoriPlaylistSQL> favorivideolist = TableFavoriplaylists.getAllFavoriArtist(0);

                    //Log.d(TAG,"db.getAllFavoriVideo().size(): "+db.getAllFavoriArtist(0).size());
                    Log.d(TAG,"db.getAllFavoriVideo().size(): "+TableFavoriplaylists.getAllFavoriArtist(0).size());

                    //if ( db.getAllFavoriArtist(0).size() == 0 ) {
                    if ( TableFavoriplaylists.getAllFavoriArtist(0).size() == 0 ) {
                        setEmptyView( getString(R.string.empty_favori_artiste));
                        return;
                    }
                    setListView();
                    playList.clear();
                    //addToList(db.getAllFavoriArtist(0), playList);
                    addToList(TableFavoriplaylists.getAllFavoriArtist(0), playList);

                    badapter.notifyDataSetChanged();



                }
            } );

            return null;
        }
        /**
         * Masquer la boite de dialog
         */
        protected void onPostExecute(String args)
        {
            Log.d(TAG, "loadFavoriPlaylistSQL  onPostExecute" );
        }
    }

    void saveBDDFavoriPlaylists(List<Playlist> playlist){
        Log.d(TAG, "saveBDDFavoriPlaylists - " + TableFavoriplaylists.getCountArtistInFavoriIsHidden(0) );
        new saveBDDFavoriPlaylists().execute(playlist);
    }

    void setSuccessFavoritePlayliste(Playlists playlists) {

        if ( playlists.getPlaylists().size()>0 ) {
            saveBDDFavoriPlaylists(playlists.getPlaylists());
        }

    }

    void getServerInfo() {
        Log.d(TAG,"getLoggedinID: "+Useful.getLoggedinID(getActivity()));

        if ( !Useful.getLoggedinID(getActivity()).isEmpty() ) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                    .build();
            ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);



            vidapi.getFavoriteplaylistebyuser(Useful.getLoggedinID(getActivity()), new Callback<Playlists>() {
                @Override
                public void success(Playlists playlists, Response response) {
                    setSuccessFavoritePlayliste(playlists);
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });


        }
    }

    private class saveBDDFavoriPlaylists extends AsyncTask<List<Playlist>, String, String>
    {

        @Override
        protected String doInBackground(List<Playlist>... params) {
            if ( params[0].size() > 0 ) {
                Log.d(TAG,"saveBDDFavoriPlaylists - "+params[0].size());
                if ( 0<params[0].size() ) {
                    VazotsaraShareFunc.saveInPlaylists(params[0]);

                    ActiveAndroid.beginTransaction();
                    try {
                        for (int i = 0; i < params[0].size(); i++) {


                            if ( !TableFavoriplaylists.isArtistInFavori(String.valueOf(params[0].get(i).getId())) ) {
                                Log.d(TAG,"saveBDDFavoriPlaylists - save "+params[0].get(i).getId());
                                TableFavoriplaylists fav = new TableFavoriplaylists();
                                fav.playlisteid = String.valueOf(params[0].get(i).getId());
                                fav.isdeleted = 0;
                                fav.ishidden = 0;
                                fav.save();
                            } else {
                                Log.d(TAG,"saveBDDFavoriPlaylists - (Exists) don't save "+params[0].get(i).getId());
                            }

                        }
                        ActiveAndroid.setTransactionSuccessful();
                    }
                    finally {
                        ActiveAndroid.endTransaction();
                    }
                }
            }
            return null;
        }
    }

    BroadcastConnexion receiver;
    ConnectionDetector connecte;
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Si connecte  Internet et connecte en tant qu'user
            if ( connecte.isConnectingToInternet() && Useful.isLoggedin(getActivity())) {
                getServerInfo();
            }
        }
    }


}

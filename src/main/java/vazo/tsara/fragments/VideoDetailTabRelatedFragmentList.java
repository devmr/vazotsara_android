package vazo.tsara.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import vazo.tsara.R;
import vazo.tsara.VztApplication;
import vazo.tsara.activities.VideoDetailActivity;
import vazo.tsara.adapters.VideoRelatedBaseAdapter;
import vazo.tsara.helpers.Config;
import vazo.tsara.models.VideoPojo;
import vazo.tsara.models.Videogson;
import vazo.tsara.models.video;
import vazo.tsara.services.ApiRetrofit;
import vazo.tsara.services.ConnectionDetector;
import vazo.tsara.services.LenientGsonConverter;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoDetailTabRelatedFragmentList extends Fragment {

    String uid;
    String url = Config.URLAPI;
    static String TAG = Config.TAGKEY;

    ListView listView;

    ConnectionDetector connecte;
    BroadcastConnexion receiver;

    View rootView;
    View viewstub_listview_error;
    View viewstub_listview_empty;
    View viewstub_listview_content;
    View viewstub_listview_loading;

    VideoRelatedBaseAdapter adapter;

    List<video> videoArtistList = new ArrayList<video>();

    boolean loaded;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        connecte = new ConnectionDetector(getActivity());
        super.onCreate(savedInstanceState);
        // Get back arguments
        uid = getArguments().getString("uid", "");
    }
    public void onResume() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new BroadcastConnexion();
        getActivity().registerReceiver(receiver, filter);
        super.onResume();
        Log.d(TAG, "VideoFragment - onResume");
        if ( loaded )
            setListView();
    }
    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
        Log.d(TAG, "VideoFragment - onPause");
    }
    public void onStart()
    {
        super.onStart();
        /**
         * Analytics
         */
        //OLD - Analytics
        //easyTracker = EasyTracker.getInstance(MainActivity.this);
        try
        {
            Tracker t = ((VztApplication) getActivity().getApplication()).getTracker(
                    VztApplication.TrackerName.APP_TRACKER);
            t.setScreenName( this.getClass().getSimpleName() );
            t.send(new HitBuilders.AppViewBuilder().build());
            Log.d(TAG,"Analytics - onStart : "+this.getClass().getSimpleName() );
        } catch(Exception  e) { }
    }
    public class BroadcastConnexion extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent) {
            if ( connecte.isConnectingToInternet() && adapter.getCount() == 0 ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive badapter.getCount() == 0");
                setLoadingView();
                getInfo(url, 0);
            }
            //Si non connecte  Internet et isLoading()
            if ( !connecte.isConnectingToInternet() && adapter.getCount() > 0  ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() > 0");
                 /*SnackbarManager.show(
                         Snackbar.with(getActivity())
                                 .text(getString(R.string.error_non_connecte))
                 );*/

            }

            //Si non connecte  Internet et adapter vide - Rafraichir
            if ( !connecte.isConnectingToInternet() && adapter.getCount() == 0 ) {
                Log.d(TAG, "PlaylistsFragment - BroadcastConnexion - onReceive !connecte - badapter.getCount() == 0");
                /*SnackbarManager.show(
                        Snackbar.with(getActivity())
                                .text(getString(R.string.error_non_connecte))
                );*/
                setErrorView(getActivity().getString(R.string.error_non_connecte));
            }
        }
    }

    public static VideoDetailTabRelatedFragmentList newInstance(String uid) {
        VideoDetailTabRelatedFragmentList frag = new VideoDetailTabRelatedFragmentList();
        Bundle args = new Bundle();
        args.putString("uid", uid);
        frag.setArguments(args);
        return frag;
    }


    public VideoDetailTabRelatedFragmentList() {
        // Required empty public constructor
    }

    public void setLoadingView()
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.VISIBLE);
    }

    public void setErrorView(String error)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.VISIBLE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_error.findViewById(R.id.textView);
        textv.setText(error);
    }

    public void setEmptyView(String textempty)
    {
        viewstub_listview_content.setVisibility(View.GONE);
        viewstub_listview_empty.setVisibility(View.VISIBLE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);

        TextView textv = (TextView) viewstub_listview_empty.findViewById(R.id.textView);
        textv.setText(textempty);
    }

    public void setListView()
    {
        viewstub_listview_content.setVisibility(View.VISIBLE);
        viewstub_listview_empty.setVisibility(View.GONE);
        viewstub_listview_error.setVisibility(View.GONE);
        viewstub_listview_loading.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_video_detail_tab_related_fragment_list, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        viewstub_listview_content = ( (ViewStub) rootView.findViewById(R.id.content)).inflate();
        viewstub_listview_empty = ( (ViewStub) rootView.findViewById(R.id.empty)).inflate();
        viewstub_listview_error = ( (ViewStub) rootView.findViewById(R.id.error)).inflate();
        viewstub_listview_loading = ( (ViewStub) rootView.findViewById(R.id.loading)).inflate();

        //adapter = new PlaylistsAdapter( getActivity() , playList, R.layout.adapter_playlistsitem  );
        adapter = new VideoRelatedBaseAdapter( getActivity() , videoArtistList, R.layout.adapter_video_related  );

        listView = (ListView) viewstub_listview_content.findViewById(R.id.list);


        View headerListView = getActivity().getLayoutInflater().inflate(R.layout.adapter_video_related_header, null);
        listView.addHeaderView(headerListView);

        TextView txtheader = (TextView) headerListView.findViewById(R.id.headerlistview);
        txtheader.setText("Vidéos en relation");

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.d(TAG,"onActivityCreated onItemClick - i : "+i+" - l: "+l);
                //System.exit(0);

                /**
                 * Si on ne clic pas sur le header
                 */
                if ( i>0) {

                    String uid;
                    uid = ((TextView) view.findViewById(R.id.uid)).getText().toString();
                    String videoytid = ((TextView) view.findViewById(R.id.videoytid)).getText().toString();
                    String contentDetailsDefinition = ((TextView) view.findViewById(R.id.contentDetailsDefinition)).getText().toString();

                    Intent VDetailActivity = new Intent(getActivity(), VideoDetailActivity.class);
                    VDetailActivity.putExtra("uid", uid);
                    VDetailActivity.putExtra("videoytid", videoytid);
                    VDetailActivity.putExtra("contentDetails_definition", contentDetailsDefinition);
                    getActivity().startActivityForResult(VDetailActivity, 100);
                }

            }
        });
    }

    void getInfo(String url, int page) {
        if (!connecte.isConnectingToInternet()) {
            return;
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(url).setConverter(new LenientGsonConverter(new Gson()))
                .build();
        ApiRetrofit vidapi = restAdapter.create(ApiRetrofit.class);

        vidapi.getRelatedVideo(uid, "realisateur", new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                if ( videoPojo.getVideos().size() == 0) {
                    loaded = true;
                    return;
                }
                setListView();
                addToList(videoPojo.getVideos(), "realisateur");
                adapter.notifyDataSetChanged();
                loaded = true;

            }

            @Override
            public void failure(RetrofitError error) {
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });

        vidapi.getRelatedVideo(uid, "",  new Callback<VideoPojo>() {
            @Override
            public void success(VideoPojo videoPojo, retrofit.client.Response response) {
                Log.d(TAG, "PlaylistsFragment - getInfo success - plist length : " + videoPojo.getVideos().size());
                if ( videoPojo.getVideos().size() == 0 && isAdded()) {
                    setEmptyView(getActivity().getString(R.string.error_no_item_video));
                    loaded = true;
                    return;
                }
                setListView();
                addToList(videoPojo.getVideos(), "artiste");
                //adapter.notifyDataSetChanged();
                adapter.notifyDataSetChanged();
                loaded = true;
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "PlaylistsFragment - getInfo failure");
                String errorMessage = (error.getMessage() == null) ? "Message is empty" : error.getMessage();
                setErrorView(errorMessage);
            }
        });



    }

    void addToList(List<Videogson> videos, String type ) {
        for (int count = 0; count < videos.size(); count++) {
            videoArtistList.add(fillItem(videos.get(count), type ));
        }
    }

    video fillItem(Videogson videogson, String type ) {
        video v = new video();
        v.setTitre(videogson.getTitre() );
        v.setId(videogson.getId());
        v.setStatistics_viewCount(videogson.getStatisticsViewCount());
        v.setStatistics_likeCount(videogson.getStatisticsLikeCount());
        v.setStatistics_dislikeCount(videogson.getStatisticsDislikeCount());
        v.setStatistics_dislikeCount(videogson.getStatisticsDislikeCount());
        v.setThumburl(videogson.getThumburl());
        v.setType(type);
        v.setPlaylisteinfo(videogson.getPlaylisteinfo());

        v.setContentDetails_definition(videogson.getContentDetailsDefinition());
        v.setVideoytid( videogson.getVideoytid()  );

        return v;
    }

}

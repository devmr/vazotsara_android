package vazo.tsara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Videogson {

    @Expose
    private String id;
    @Expose
    private String videoytid;
    @SerializedName("is_downloaded")
    @Expose
    private String isDownloaded;
    @SerializedName("starttime_downloaded")
    @Expose
    private String starttimeDownloaded;
    @SerializedName("endtime_downloaded")
    @Expose
    private String endtimeDownloaded;
    @Expose
    private String titre;
    @SerializedName("starttime_uploaded")
    @Expose
    private String starttimeUploaded;
    @SerializedName("endtime_uploaded")
    @Expose
    private String endtimeUploaded;
    @SerializedName("is_uploaded")
    @Expose
    private String isUploaded;
    @SerializedName("titre_yt")
    @Expose
    private String titreYt;
    @SerializedName("description_yt")
    @Expose
    private String descriptionYt;
    @SerializedName("uploadeddate_original")
    @Expose
    private String uploadeddateOriginal;
    @SerializedName("publishedAt")
    @Expose
    private String publishedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @Expose
    private String removefile;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @Expose
    private String statusyt;
    @SerializedName("category_yt")
    @Expose
    private String categoryYt;
    @Expose
    private String filesize;
    @Expose
    private String filename;
    @SerializedName("contentDetails_duration")
    @Expose
    private String contentDetailsDuration;
    @SerializedName("contentDetails_definition")
    @Expose
    private String contentDetailsDefinition;
    @SerializedName("status_uploadStatus")
    @Expose
    private String statusUploadStatus;
    @SerializedName("status_privacyStatus")
    @Expose
    private String statusPrivacyStatus;
    @SerializedName("status_license")
    @Expose
    private String statusLicense;
    @SerializedName("status_embeddable")
    @Expose
    private String statusEmbeddable;
    @SerializedName("status_publicStatsViewable")
    @Expose
    private String statusPublicStatsViewable;
    @SerializedName("statistics_viewCount")
    @Expose
    private String statisticsViewCount;
    @SerializedName("statistics_likeCount")
    @Expose
    private String statisticsLikeCount;
    @SerializedName("statistics_dislikeCount")
    @Expose
    private String statisticsDislikeCount;
    @SerializedName("statistics_favoriteCount")
    @Expose
    private String statisticsFavoriteCount;
    @SerializedName("statistics_commentCount")
    @Expose
    private String statisticsCommentCount;
    @SerializedName("fileDetails_container")
    @Expose
    private String fileDetailsContainer;
    @SerializedName("fileDetails_videoStreams_frameRateFps")
    @Expose
    private String fileDetailsVideoStreamsFrameRateFps;
    @SerializedName("fileDetails_videoStreams_aspectRatio")
    @Expose
    private String fileDetailsVideoStreamsAspectRatio;
    @SerializedName("fileDetails_videoStreams_codec")
    @Expose
    private String fileDetailsVideoStreamsCodec;
    @SerializedName("fileDetails_videoStreams_widthPixels")
    @Expose
    private String fileDetailsVideoStreamsWidthPixels;
    @SerializedName("fileDetails_videoStreams_heightPixels")
    @Expose
    private String fileDetailsVideoStreamsHeightPixels;
    @Expose
    private String source;
    @Expose
    private String thumburl;
    @Expose
    private String publishedCommentLast;

    @SerializedName("statistics_viewCount_hier")
    @Expose
    private String statisticsViewCountHier;

    @SerializedName("playlists")
    @Expose
    private List<VideoSingleGsonPlaylist> playlisteinfo = new ArrayList<VideoSingleGsonPlaylist>();


    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The videoytid
     */
    public String getVideoytid() {
        return videoytid;
    }

    /**
     *
     * @param videoytid
     * The videoytid
     */
    public void setVideoytid(String videoytid) {
        this.videoytid = videoytid;
    }

    /**
     *
     * @return
     * The isDownloaded
     */
    public String getIsDownloaded() {
        return isDownloaded;
    }

    /**
     *
     * @param isDownloaded
     * The is_downloaded
     */
    public void setIsDownloaded(String isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    /**
     *
     * @return
     * The starttimeDownloaded
     */
    public String getStarttimeDownloaded() {
        return starttimeDownloaded;
    }

    /**
     *
     * @param starttimeDownloaded
     * The starttime_downloaded
     */
    public void setStarttimeDownloaded(String starttimeDownloaded) {
        this.starttimeDownloaded = starttimeDownloaded;
    }

    /**
     *
     * @return
     * The endtimeDownloaded
     */
    public String getEndtimeDownloaded() {
        return endtimeDownloaded;
    }

    /**
     *
     * @param endtimeDownloaded
     * The endtime_downloaded
     */
    public void setEndtimeDownloaded(String endtimeDownloaded) {
        this.endtimeDownloaded = endtimeDownloaded;
    }

    /**
     *
     * @return
     * The titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     *
     * @param titre
     * The titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     *
     * @return
     * The starttimeUploaded
     */
    public String getStarttimeUploaded() {
        return starttimeUploaded;
    }

    /**
     *
     * @param starttimeUploaded
     * The starttime_uploaded
     */
    public void setStarttimeUploaded(String starttimeUploaded) {
        this.starttimeUploaded = starttimeUploaded;
    }

    /**
     *
     * @return
     * The endtimeUploaded
     */
    public String getEndtimeUploaded() {
        return endtimeUploaded;
    }

    /**
     *
     * @param endtimeUploaded
     * The endtime_uploaded
     */
    public void setEndtimeUploaded(String endtimeUploaded) {
        this.endtimeUploaded = endtimeUploaded;
    }

    /**
     *
     * @return
     * The isUploaded
     */
    public String getIsUploaded() {
        return isUploaded;
    }

    /**
     *
     * @param isUploaded
     * The is_uploaded
     */
    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    /**
     *
     * @return
     * The titreYt
     */
    public String getTitreYt() {
        return titreYt;
    }

    /**
     *
     * @param titreYt
     * The titre_yt
     */
    public void setTitreYt(String titreYt) {
        this.titreYt = titreYt;
    }

    /**
     *
     * @return
     * The descriptionYt
     */
    public String getDescriptionYt() {
        return descriptionYt;
    }

    /**
     *
     * @param descriptionYt
     * The description_yt
     */
    public void setDescriptionYt(String descriptionYt) {
        this.descriptionYt = descriptionYt;
    }

    /**
     *
     * @return
     * The uploadeddateOriginal
     */
    public String getUploadeddateOriginal() {
        return uploadeddateOriginal;
    }

    /**
     *
     * @param uploadeddateOriginal
     * The uploadeddate_original
     */
    public void setUploadeddateOriginal(String uploadeddateOriginal) {
        this.uploadeddateOriginal = uploadeddateOriginal;
    }

    /**
     *
     * @return
     * The publishedAt
     */
    public String getPublishedAt() {
        return publishedAt;
    }

    /**
     *
     * @param publishedAt
     * The publishedAt
     */
    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The removefile
     */
    public String getRemovefile() {
        return removefile;
    }

    /**
     *
     * @param removefile
     * The removefile
     */
    public void setRemovefile(String removefile) {
        this.removefile = removefile;
    }

    /**
     *
     * @return
     * The deletedAt
     */
    public String getDeletedAt() {
        return deletedAt;
    }

    /**
     *
     * @param deletedAt
     * The deleted_at
     */
    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     *
     * @return
     * The statusyt
     */
    public String getStatusyt() {
        return statusyt;
    }

    /**
     *
     * @param statusyt
     * The statusyt
     */
    public void setStatusyt(String statusyt) {
        this.statusyt = statusyt;
    }

    /**
     *
     * @return
     * The categoryYt
     */
    public String getCategoryYt() {
        return categoryYt;
    }

    /**
     *
     * @param categoryYt
     * The category_yt
     */
    public void setCategoryYt(String categoryYt) {
        this.categoryYt = categoryYt;
    }

    /**
     *
     * @return
     * The filesize
     */
    public String getFilesize() {
        return filesize;
    }

    /**
     *
     * @param filesize
     * The filesize
     */
    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    /**
     *
     * @return
     * The filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     *
     * @param filename
     * The filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     *
     * @return
     * The contentDetailsDuration
     */
    public String getContentDetailsDuration() {
        return contentDetailsDuration;
    }

    /**
     *
     * @param contentDetailsDuration
     * The contentDetails_duration
     */
    public void setContentDetailsDuration(String contentDetailsDuration) {
        this.contentDetailsDuration = contentDetailsDuration;
    }

    /**
     *
     * @return
     * The contentDetailsDefinition
     */
    public String getContentDetailsDefinition() {
        return contentDetailsDefinition;
    }

    /**
     *
     * @param contentDetailsDefinition
     * The contentDetails_definition
     */
    public void setContentDetailsDefinition(String contentDetailsDefinition) {
        this.contentDetailsDefinition = contentDetailsDefinition;
    }

    /**
     *
     * @return
     * The statusUploadStatus
     */
    public String getStatusUploadStatus() {
        return statusUploadStatus;
    }

    /**
     *
     * @param statusUploadStatus
     * The status_uploadStatus
     */
    public void setStatusUploadStatus(String statusUploadStatus) {
        this.statusUploadStatus = statusUploadStatus;
    }

    /**
     *
     * @return
     * The statusPrivacyStatus
     */
    public String getStatusPrivacyStatus() {
        return statusPrivacyStatus;
    }

    /**
     *
     * @param statusPrivacyStatus
     * The status_privacyStatus
     */
    public void setStatusPrivacyStatus(String statusPrivacyStatus) {
        this.statusPrivacyStatus = statusPrivacyStatus;
    }

    /**
     *
     * @return
     * The statusLicense
     */
    public String getStatusLicense() {
        return statusLicense;
    }

    /**
     *
     * @param statusLicense
     * The status_license
     */
    public void setStatusLicense(String statusLicense) {
        this.statusLicense = statusLicense;
    }

    /**
     *
     * @return
     * The statusEmbeddable
     */
    public String getStatusEmbeddable() {
        return statusEmbeddable;
    }

    /**
     *
     * @param statusEmbeddable
     * The status_embeddable
     */
    public void setStatusEmbeddable(String statusEmbeddable) {
        this.statusEmbeddable = statusEmbeddable;
    }

    /**
     *
     * @return
     * The statusPublicStatsViewable
     */
    public String getStatusPublicStatsViewable() {
        return statusPublicStatsViewable;
    }

    /**
     *
     * @param statusPublicStatsViewable
     * The status_publicStatsViewable
     */
    public void setStatusPublicStatsViewable(String statusPublicStatsViewable) {
        this.statusPublicStatsViewable = statusPublicStatsViewable;
    }

    /**
     *
     * @return
     * The statisticsViewCount
     */
    public String getStatisticsViewCount() {
        return statisticsViewCount;
    }

    /**
     *
     * @param statisticsViewCount
     * The statistics_viewCount
     */
    public void setStatisticsViewCount(String statisticsViewCount) {
        this.statisticsViewCount = statisticsViewCount;
    }

    /**
     *
     * @return
     * The statisticsViewCount
     */
    public String getStatisticsViewCountHier() {
        return statisticsViewCountHier;
    }

    /**
     *
     * @param statisticsViewCount
     * The statistics_viewCount
     */
    public void setStatisticsViewCountHier(String statisticsViewCountHier) {
        this.statisticsViewCountHier = statisticsViewCountHier;
    }

    /**
     *
     * @return
     * The statisticsLikeCount
     */
    public String getStatisticsLikeCount() {
        return statisticsLikeCount;
    }

    /**
     *
     * @param statisticsLikeCount
     * The statistics_likeCount
     */
    public void setStatisticsLikeCount(String statisticsLikeCount) {
        this.statisticsLikeCount = statisticsLikeCount;
    }

    /**
     *
     * @return
     * The statisticsDislikeCount
     */
    public String getStatisticsDislikeCount() {
        return statisticsDislikeCount;
    }

    /**
     *
     * @param statisticsDislikeCount
     * The statistics_dislikeCount
     */
    public void setStatisticsDislikeCount(String statisticsDislikeCount) {
        this.statisticsDislikeCount = statisticsDislikeCount;
    }

    /**
     *
     * @return
     * The statisticsFavoriteCount
     */
    public String getStatisticsFavoriteCount() {
        return statisticsFavoriteCount;
    }

    /**
     *
     * @param statisticsFavoriteCount
     * The statistics_favoriteCount
     */
    public void setStatisticsFavoriteCount(String statisticsFavoriteCount) {
        this.statisticsFavoriteCount = statisticsFavoriteCount;
    }

    /**
     *
     * @return
     * The statisticsCommentCount
     */
    public String getStatisticsCommentCount() {
        return statisticsCommentCount;
    }

    /**
     *
     * @param statisticsCommentCount
     * The statistics_commentCount
     */
    public void setStatisticsCommentCount(String statisticsCommentCount) {
        this.statisticsCommentCount = statisticsCommentCount;
    }

    /**
     *
     * @return
     * The fileDetailsContainer
     */
    public String getFileDetailsContainer() {
        return fileDetailsContainer;
    }

    /**
     *
     * @param fileDetailsContainer
     * The fileDetails_container
     */
    public void setFileDetailsContainer(String fileDetailsContainer) {
        this.fileDetailsContainer = fileDetailsContainer;
    }

    /**
     *
     * @return
     * The fileDetailsVideoStreamsFrameRateFps
     */
    public String getFileDetailsVideoStreamsFrameRateFps() {
        return fileDetailsVideoStreamsFrameRateFps;
    }

    /**
     *
     * @param fileDetailsVideoStreamsFrameRateFps
     * The fileDetails_videoStreams_frameRateFps
     */
    public void setFileDetailsVideoStreamsFrameRateFps(String fileDetailsVideoStreamsFrameRateFps) {
        this.fileDetailsVideoStreamsFrameRateFps = fileDetailsVideoStreamsFrameRateFps;
    }

    /**
     *
     * @return
     * The fileDetailsVideoStreamsAspectRatio
     */
    public String getFileDetailsVideoStreamsAspectRatio() {
        return fileDetailsVideoStreamsAspectRatio;
    }

    /**
     *
     * @param fileDetailsVideoStreamsAspectRatio
     * The fileDetails_videoStreams_aspectRatio
     */
    public void setFileDetailsVideoStreamsAspectRatio(String fileDetailsVideoStreamsAspectRatio) {
        this.fileDetailsVideoStreamsAspectRatio = fileDetailsVideoStreamsAspectRatio;
    }

    /**
     *
     * @return
     * The fileDetailsVideoStreamsCodec
     */
    public String getFileDetailsVideoStreamsCodec() {
        return fileDetailsVideoStreamsCodec;
    }

    /**
     *
     * @param fileDetailsVideoStreamsCodec
     * The fileDetails_videoStreams_codec
     */
    public void setFileDetailsVideoStreamsCodec(String fileDetailsVideoStreamsCodec) {
        this.fileDetailsVideoStreamsCodec = fileDetailsVideoStreamsCodec;
    }

    /**
     *
     * @return
     * The fileDetailsVideoStreamsWidthPixels
     */
    public String getFileDetailsVideoStreamsWidthPixels() {
        return fileDetailsVideoStreamsWidthPixels;
    }

    /**
     *
     * @param fileDetailsVideoStreamsWidthPixels
     * The fileDetails_videoStreams_widthPixels
     */
    public void setFileDetailsVideoStreamsWidthPixels(String fileDetailsVideoStreamsWidthPixels) {
        this.fileDetailsVideoStreamsWidthPixels = fileDetailsVideoStreamsWidthPixels;
    }

    /**
     *
     * @return
     * The fileDetailsVideoStreamsHeightPixels
     */
    public String getFileDetailsVideoStreamsHeightPixels() {
        return fileDetailsVideoStreamsHeightPixels;
    }

    /**
     *
     * @param fileDetailsVideoStreamsHeightPixels
     * The fileDetails_videoStreams_heightPixels
     */
    public void setFileDetailsVideoStreamsHeightPixels(String fileDetailsVideoStreamsHeightPixels) {
        this.fileDetailsVideoStreamsHeightPixels = fileDetailsVideoStreamsHeightPixels;
    }

    /**
     *
     * @return
     * The source
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     * The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return
     * The thumburl
     */
    public String getThumburl() {
        return thumburl;
    }

    /**
     *
     * @param thumburl
     * The thumburl
     */
    public void setThumburl(String thumburl) {
        this.thumburl = thumburl;
    }

    /**
     *
     * @return
     * The playlisteinfo
     */
    public List<VideoSingleGsonPlaylist> getPlaylisteinfo() {
        return playlisteinfo;
    }

    /**
     *
     * @param playlisteinfo
     * The playlisteinfo
     */
    public void setPlaylisteinfo(List<VideoSingleGsonPlaylist> playlisteinfo) {
        this.playlisteinfo = playlisteinfo;
    }

    /**
     *
     * @return
     * The publishedCommentLast
     */
    public String getPublishedCommentLast() {
        return publishedCommentLast;
    }

    /**
     *
     * @param publishedCommentLast
     * The publishedCommentLast
     */
    public void setPublishedCommentLast(String publishedCommentLast) {
        this.publishedCommentLast = publishedCommentLast;
    }

}

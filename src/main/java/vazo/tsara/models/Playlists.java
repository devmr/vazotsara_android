package vazo.tsara.models;

/**
 * Created by rabehasy on 22/06/2015.
 */
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;



public class Playlists {

    @Expose
    private Integer total;

    private List<Playlist> playlists = new ArrayList<Playlist>();

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     *
     * @return
     * The playlists
     */
    public List<Playlist> getPlaylists() {
        return playlists;
    }

    /**
     *
     * @param playlists
     * The playlists
     */
    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }

}

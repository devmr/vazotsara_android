package vazo.tsara.models;
import com.google.gson.annotations.Expose;


/**
 * Created by rabehasy on 07/09/2015.
 */
public class ServerMsgPojo {
    @Expose
    private String message;

    /**
     *
     * @return
     * The token
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param msg
     * The token
     */
    public void setMessage(String message) {
        this.message = message;
    }


}

package vazo.tsara.models;
import com.google.gson.annotations.Expose;
/**
 * Created by rabehasy on 07/09/2015.
 */
public class UserinfoPojo {
    @Expose
    private UserPojo user;

    /**
     *
     * @return
     * The user
     */
    public UserPojo getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(UserPojo user) {
        this.user = user;
    }
}

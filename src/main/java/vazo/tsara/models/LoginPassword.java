package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class LoginPassword {
    final String email;
    final String password;

    public LoginPassword(String email, String password) {
        this.email = email;
        this.password = password;
    }
}

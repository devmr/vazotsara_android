package vazo.tsara.models;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
/**
 * Created by rabehasy on 11/09/2015.
 */
public class FormatResults {
    @Expose
    private List<FormatVideo> format = new ArrayList<FormatVideo>();

    /**
     *
     * @return
     * The format
     */
    public List<FormatVideo> getFormat() {
        return format;
    }

    /**
     *
     * @param format
     * The format
     */
    public void setFormat(List<FormatVideo> format) {
        this.format = format;
    }
}

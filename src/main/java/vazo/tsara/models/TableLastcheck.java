package vazo.tsara.models;


import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import vazo.tsara.helpers.Config;

@Table(name = "LAST_CHECK")
public class TableLastcheck extends Model {

    static String TAG = Config.TAGKEY;

    @Column(name = "date")
    public String date;

    @Column(name = "lastvideoid")
    public String lastvideoid;

    @Column(name = "lastplaylisteartisteid")
    public String lastplaylisteartisteid;

    @Column(name = "lastplaylistegenreid")
    public String lastplaylistegenreid;

    @Column(name = "lastplaylisterealisateurid")
    public String lastplaylisterealisateurid;

    @Column(name = "lastplaylisteautreid")
    public String lastplaylisteautreid;

    @Column(name = "lastpagelisteNew")
    public String lastpagelisteNew;

    @Column(name = "lastpagelisteView")
    public String lastpagelisteView;

    @Column(name = "lastpagelisteLike")
    public String lastpagelisteLike;

    @Column(name = "lastpagelisteComment")
    public String lastpagelisteComment;

    @Column(name = "lastpagelisteDuration")
    public String lastpagelisteDuration;

    @Column(name = "lastpagelisteCommentDate")
    public String lastpagelisteCommentDate;

    @Column(name = "lastpagelisteCommentlast")
    public String lastpagelisteCommentlast;

    @Column(name = "lastpagelisteViewHier")
    public String lastpagelisteViewHier;

    @Column(name = "totalvideosinserver")
    public int totalvideosinserver;

    @Column(name = "timestamplastsave")
    public String timestamplastsave;

    public TableLastcheck() {
        super();
    }
    public static String getLastIdVideo() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.lastvideoid;
        }
        return "0";
    }

    public static void setLastIdVideo(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastvideoid = id;
        item.save();
    }

    public static String getLastIdArtiste() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.lastplaylisteartisteid;
        }
        return "0";
    }

    public static void setLastIdArtiste(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastplaylisteartisteid = id;
        item.save();
    }

    public static String getLastIdGenre() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item = new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.lastplaylistegenreid;
        }
        return "0";
    }

    public static void setLastIdGenre(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastplaylistegenreid = id;
        item.save();
    }

    public static String getLastIdRealisateur() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item = new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.lastplaylisterealisateurid;
        }
        return "0";
    }

    public static void setLastIdRealisateur(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastplaylisterealisateurid = id;
        item.save();
    }

    public static String getLastIdAutre() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item = new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.lastplaylisterealisateurid;
        }
        return "0";
    }

    public static void setLastIdAutre(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastplaylisteautreid = id;
        item.save();
    }

    public static boolean TableIsNotEmpty() {
        Log.d(TAG, "TableLastcheck Count : " + (new Select().from(TableLastcheck.class).count()>0) );
        return new Select().from(TableLastcheck.class).count()>0;
    }

    public static long getIdFirstTableRow() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item = new Select().from(TableLastcheck.class).limit(1).executeSingle();
            return item.getId();
        }
        return 0;
    }

    public static String getLastPagelisteView() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteView!=null?item.lastpagelisteView:"0");
        }
        return "0";
    }

    public static void setLastPagelisteView(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteView = id;
        item.save();
    }

    public static String getLastPagelisteNew() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteNew!=null?item.lastpagelisteNew:"0");
        }
        return "0";
    }

    public static void setLastPagelisteNew(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteNew = id;
        item.save();
    }

    public static String getLastPagelisteLike() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteLike!=null?item.lastpagelisteLike:"0");
        }
        return "0";
    }

    public static void setLastPagelisteLike(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteLike = id;
        item.save();
    }

    public static String getLastPagelisteComment() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteComment!=null?item.lastpagelisteComment:"0");
        }
        return "0";
    }

    public static void setLastPagelisteComment(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteComment = id;
        item.save();
    }

    public static String getLastPagelisteDuration() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteDuration!=null?item.lastpagelisteDuration:"0");
        }
        return "0";
    }

    public static void setLastPagelisteDuration(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteDuration = id;
        item.save();
    }

    public static String getLastPagelisteCommentDate() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteCommentDate!=null?item.lastpagelisteCommentDate:"0");
        }
        return "0";
    }

    public static void setLastPagelisteCommentDate(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteCommentDate = id;
        item.save();
    }

    public static String getLastPagelisteCommentlast() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteCommentlast!=null?item.lastpagelisteCommentlast:"0");
        }
        return "0";
    }

    public static void setLastPagelisteCommentlast(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteCommentlast = id;
        item.save();
    }

    public static String getLastPagelisteViewHier() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return (item.lastpagelisteViewHier!=null?item.lastpagelisteViewHier:"0");
        }
        return "0";
    }

    public static void setLastPagelisteViewHier(String id) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.lastpagelisteViewHier = id;
        item.save();
    }

    public static int getTotalvideosinserver() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.totalvideosinserver;
        }
        return 0;
    }

    public static void setTotalvideosinserver(int count) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.totalvideosinserver = count;
        item.save();
    }

    public static String getTimestamplastsave() {
        if ( TableIsNotEmpty() ) {
            TableLastcheck item =  new Select().from(TableLastcheck.class).where("id = 1").executeSingle();
            return item.timestamplastsave;
        }
        return "";
    }

    public static void setTimestamplastsave(String timestamplastsave) {
        TableLastcheck item;
        if ( TableIsNotEmpty() ) {
            item = TableLastcheck.load(TableLastcheck.class, getIdFirstTableRow() );
        } else {
            item = new TableLastcheck();
        }
        item.timestamplastsave = timestamplastsave;
        item.save();
    }


}

package vazo.tsara.models;

/**
 * Created by rabehasy on 03/11/2015.
 */
public class OptionBusEvent {
    String referer;

    public OptionBusEvent(String referer) {
        this.referer = referer;
    }

    public String getReferer(){
        return referer;
    }
}

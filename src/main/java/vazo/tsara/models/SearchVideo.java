package vazo.tsara.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rabehasy on 24/08/2015.
 */
public class SearchVideo {

    @Expose
    private List<SingleResultSearch> results = new ArrayList<SingleResultSearch>();
    @Expose
    private List<SingleResultSearchArtiste> artiste = new ArrayList<SingleResultSearchArtiste>();

    /**
     *
     * @return
     * The results
     */
    public List<SingleResultSearch> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<SingleResultSearch> results) {
        this.results = results;
    }

    /**
     *
     * @return
     * The artiste
     */
    public List<SingleResultSearchArtiste> getArtiste() {
        return artiste;
    }

    /**
     *
     * @param artiste
     * The artiste
     */
    public void setArtiste(List<SingleResultSearchArtiste> artiste) {
        this.artiste = artiste;
    }

}

package vazo.tsara.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class UserPojo {
    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String email;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    @Expose
    private Object permissions;
    @Expose
    private String activated;
    @SerializedName("activation_code")
    @Expose
    private Object activationCode;
    @SerializedName("activated_at")
    @Expose
    private Object activatedAt;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("persist_code")
    @Expose
    private String persistCode;
    @SerializedName("reset_password_code")
    @Expose
    private Object resetPasswordCode;
    @SerializedName("first_name")
    @Expose
    private Object firstName;
    @SerializedName("last_name")
    @Expose
    private Object lastName;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The apiToken
     */
    public String getApiToken() {
        return apiToken;
    }

    /**
     *
     * @param apiToken
     * The api_token
     */
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    /**
     *
     * @return
     * The permissions
     */
    public Object getPermissions() {
        return permissions;
    }

    /**
     *
     * @param permissions
     * The permissions
     */
    public void setPermissions(Object permissions) {
        this.permissions = permissions;
    }

    /**
     *
     * @return
     * The activated
     */
    public String getActivated() {
        return activated;
    }

    /**
     *
     * @param activated
     * The activated
     */
    public void setActivated(String activated) {
        this.activated = activated;
    }

    /**
     *
     * @return
     * The activationCode
     */
    public Object getActivationCode() {
        return activationCode;
    }

    /**
     *
     * @param activationCode
     * The activation_code
     */
    public void setActivationCode(Object activationCode) {
        this.activationCode = activationCode;
    }

    /**
     *
     * @return
     * The activatedAt
     */
    public Object getActivatedAt() {
        return activatedAt;
    }

    /**
     *
     * @param activatedAt
     * The activated_at
     */
    public void setActivatedAt(Object activatedAt) {
        this.activatedAt = activatedAt;
    }

    /**
     *
     * @return
     * The lastLogin
     */
    public String getLastLogin() {
        return lastLogin;
    }

    /**
     *
     * @param lastLogin
     * The last_login
     */
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     *
     * @return
     * The persistCode
     */
    public String getPersistCode() {
        return persistCode;
    }

    /**
     *
     * @param persistCode
     * The persist_code
     */
    public void setPersistCode(String persistCode) {
        this.persistCode = persistCode;
    }

    /**
     *
     * @return
     * The resetPasswordCode
     */
    public Object getResetPasswordCode() {
        return resetPasswordCode;
    }

    /**
     *
     * @param resetPasswordCode
     * The reset_password_code
     */
    public void setResetPasswordCode(Object resetPasswordCode) {
        this.resetPasswordCode = resetPasswordCode;
    }

    /**
     *
     * @return
     * The firstName
     */
    public Object getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public Object getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

}


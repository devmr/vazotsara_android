package vazo.tsara.models;

/**
 * Created by rabehasy on 31/08/2015.
 */
/**
 * Created by Miary on 28/08/2015.
 */
public class FavoriPlaylistSQL {

    /**
     * Variables
     */
    int id;
    String titre;
    String image;

    int videoid;
    int isdeleted;
    int ishidden;

    public FavoriPlaylistSQL() {}

    public FavoriPlaylistSQL(int id, String titre, int videoid, int isdeleted, int ishidden, String image ) {
        this.id = id;
        this.titre = titre;
        this.videoid = videoid;
        this.isdeleted = isdeleted;
        this.ishidden = ishidden;
        this.image = image;

    }
    public FavoriPlaylistSQL( String titre, int videoid, int isdeleted, int ishidden, String image ) {

        this.titre = titre;
        this.videoid = videoid;
        this.isdeleted = isdeleted;
        this.ishidden = ishidden;
        this.image = image;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getVideoid() {
        return this.videoid;
    }

    public void setVideoid(int videoid) {
        this.videoid = videoid;
    }

    public int getIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(int isdeleted) {
        this.isdeleted = isdeleted;
    }

    public int getIshidden() {
        return this.ishidden;
    }

    public void setIshidden(int ishidden) {
        this.ishidden = ishidden;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }




}
package vazo.tsara.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class VideoSingleGson {
    @Expose
    private Videogson info;
    @Expose
    private List<VideoSingleGsonPlaylisteinfo> playlisteinfo = new ArrayList<VideoSingleGsonPlaylisteinfo>();

    /**
     *
     * @return
     * The info
     */
    /*public VideoSingleGsonInfo getInfo() {
        return info;
    }*/
    public Videogson getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    /*public void setInfo(VideoSingleGsonInfo info) {
        this.info = info;
    }*/
    public void setInfo(Videogson info) {
        this.info = info;
    }

    /**
     *
     * @return
     * The playlisteinfo
     */
    public List<VideoSingleGsonPlaylisteinfo> getPlaylisteinfo() {
        return playlisteinfo;
    }

    /**
     *
     * @param playlisteinfo
     * The playlisteinfo
     */
    public void setPlaylisteinfo(List<VideoSingleGsonPlaylisteinfo> playlisteinfo) {
        this.playlisteinfo = playlisteinfo;
    }

}

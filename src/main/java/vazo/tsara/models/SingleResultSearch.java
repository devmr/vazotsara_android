package vazo.tsara.models;

/**
 * Created by rabehasy on 24/08/2015.
 */

import com.google.gson.annotations.Expose;
public class SingleResultSearch {
    @Expose
    private String id;
    @Expose
    private String titre;
    @Expose
    private String videoytid;

    @Expose
    private String contentDetails_definition;

    public String getContentDetails_definition() { return this.contentDetails_definition; }

    public void setContentDetails_definition(String contentDetails_definition) { this.contentDetails_definition = contentDetails_definition; }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     *
     * @param titre
     * The titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     *
     * @return
     * The videoytid
     */
    public String getVideoytid() {
        return videoytid;
    }

    /**
     *
     * @param videoytid
     * The videoytid
     */
    public void setVideoytid(String videoytid) {
        this.videoytid = videoytid;
    }
}

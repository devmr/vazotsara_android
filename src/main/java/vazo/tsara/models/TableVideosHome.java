package vazo.tsara.models;

/**
 * Created by rabehasy on 22/06/2015.
 */

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "tx_videos_home")
public class TableVideosHome extends Model {

    @Column(name = "videoid")
    public String videoid;

    @Column(name = "rubrique")
    public String rubrique;

    public TableVideosHome(){
        super();
    }



    public static void saveRelation(String videoid, String rubrique) {
        TableVideosHome item = new TableVideosHome();
        item.videoid = videoid;
        item.rubrique = rubrique;
        item.save();
    }


}

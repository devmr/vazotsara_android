package vazo.tsara.models;
import com.google.gson.annotations.Expose;


/**
 * Created by rabehasy on 07/09/2015.
 */
public class TokenPojo {
    @Expose
    private String token;
    private String id;

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     * The token
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setId(String id) {
        this.id = id;
    }
}

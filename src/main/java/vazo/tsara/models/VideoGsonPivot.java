package vazo.tsara.models;

/**
 * Created by Miary on 16/04/2015.
 */
import com.google.gson.annotations.Expose;

public class VideoGsonPivot {

        @Expose
        private String videoid;
        @Expose
        private String playlisteid;

        /**
         *
         * @return
         * The videoid
         */
        public String getVideoid() {
            return videoid;
        }

        /**
         *
         * @param videoid
         * The videoid
         */
        public void setVideoid(String videoid) {
            this.videoid = videoid;
        }

        /**
         *
         * @return
         * The playlisteid
         */
        public String getPlaylisteid() {
            return playlisteid;
        }

        /**
         *
         * @param playlisteid
         * The playlisteid
         */
        public void setPlaylisteid(String playlisteid) {
            this.playlisteid = playlisteid;
        }

    }

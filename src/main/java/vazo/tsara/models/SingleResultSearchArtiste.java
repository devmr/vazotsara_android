package vazo.tsara.models;

/**
 * Created by Miary on 25/08/2015.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleResultSearchArtiste {
    @Expose
    private String id;
    @Expose
    private String objetnom;
    @Expose
    private String image;

    @SerializedName("nb_videos")
    @Expose
    private String nbvideos;

    @Expose
    private String playlisteid;

    @Expose
    private String firstvideoid;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The objetnom
     */
    public String getObjetnom() {
        return objetnom;
    }

    /**
     *
     * @param objetnom
     * The objetnom
     */
    public void setObjetnom(String objetnom) {
        this.objetnom = objetnom;
    }

    /**
     *
     * @return
     * The objetnom
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param objetnom
     * The objetnom
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The objetnom
     */
    public String getNbvideos() {
        return nbvideos;
    }

    /**
     *
     * @param objetnom
     * The objetnom
     */
    public void setNbvideos(String nbvideos) {
        this.nbvideos = nbvideos;
    }

    /**
     *
     * @return
     * The firstvideoid
     */
    public String getFirstvideoid() {
        return firstvideoid;
    }

    /**
     *
     * @param firstvideoid
     * The firstvideoid
     */
    public void setFirstvideoid(String firstvideoid) {
        this.firstvideoid = firstvideoid;
    }

    /**
     *
     * @return
     * The playlisteid
     */
    public String getPlaylisteid() {
        return playlisteid;
    }

    /**
     *
     * @param playlisteid
     * The playlisteid
     */
    public void setPlaylisteid(String playlisteid) {
        this.playlisteid = playlisteid;
    }

}

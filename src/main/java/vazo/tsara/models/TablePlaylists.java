package vazo.tsara.models;

/**
 * Created by rabehasy on 22/06/2015.
 */

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import vazo.tsara.helpers.Config;

@Table(name = "tx_playlists")
public class TablePlaylists extends Model {

    @Column(name = "serverid")
    public  String serverid;

    @Column(name = "playlisteid")
    public  String playlisteid;

    @Column(name = "nom")
    public  String nom;

    @Column(name = "image")
    public  String image;

    @Column(name = "created")
    public  String created;

    @Column(name = "description_yt")
    public  String descriptionYt;

    @Column(name = "nb_videos")
    public  String nbVideos;

    @Column(name = "objetnom")
    public  String objetnom;

    @Column(name = "objetype")
    public  String objetype;

    @Column(name = "objetid")
    public  String objetid;

    @Column(name = "firstvideoid")
    public  String firstvideoid;

    @Column(name = "status")
    public  String status;

    @Column(name = "created_at")
    public  String createdAt;

    @Column(name = "updated_at")
    public  String updatedAt;

    @Column(name = "createdby")
    public  String createdby;

    public TablePlaylists() {
        super();
    }

    public static int getAllCount(String serverid){
        return new Select().from(TablePlaylists.class).where("serverid = ?", serverid).count();
    }
    public static int getAllCountType(String objetype){
        return new Select().from(TablePlaylists.class).where("objetype = ?", objetype).count();
    }

    public static void saveOrNew(VideoSingleGsonPlaylist itemserver ){
        String serverid = itemserver.getId();
        TablePlaylists item;
        if ( getAllCount(itemserver.getId()) > 0 ) {
            item = new Select().from(TablePlaylists.class).where("serverid = ?", serverid).executeSingle();
        } else {
            item = new TablePlaylists();
        }

        item.objetnom = itemserver.getObjetnom();
        item.nbVideos = itemserver.getNbVideos();
        item.nom = itemserver.getNom();
        item.descriptionYt = itemserver.getDescriptionYt();
        item.created = itemserver.getCreated();
        item.createdAt = itemserver.getCreatedAt();
        item.createdby = Config.CREATEDBY;
        item.playlisteid = itemserver.getPlaylisteid();
        item.image = itemserver.getImage();
        item.objetype = itemserver.getObjetype();
        item.serverid = itemserver.getId();
        item.objetid = itemserver.getObjetid();
        item.updatedAt = itemserver.getUpdatedAt();
        item.status = itemserver.getStatus();
        item.save();
    }

}

package vazo.tsara.models;

/**
 * Created by Miary on 02/05/2015.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatGeneralinfo {

    @SerializedName("max_ViewCount")
    @Expose
    private String maxViewCount;
    @SerializedName("max_LikeCount")
    @Expose
    private String maxLikeCount;
    @SerializedName("max_DislikeCount")
    @Expose
    private String maxDislikeCount;
    @SerializedName("max_commentCount")
    @Expose
    private String maxCommentCount;
    @SerializedName("date_debut")
    @Expose
    private String dateDebut;
    @SerializedName("date_fin")
    @Expose
    private String dateFin;
    @SerializedName("date_maxview")
    @Expose
    private String dateMaxview;
    @SerializedName("date_maxlike")
    @Expose
    private String dateMaxlike;
    @SerializedName("date_maxcomment")
    @Expose
    private String dateMaxcomment;

    /**
     *
     * @return
     * The maxViewCount
     */
    public String getMaxViewCount() {
        return maxViewCount;
    }

    /**
     *
     * @param maxViewCount
     * The max_ViewCount
     */
    public void setMaxViewCount(String maxViewCount) {
        this.maxViewCount = maxViewCount;
    }

    /**
     *
     * @return
     * The maxLikeCount
     */
    public String getMaxLikeCount() {
        return maxLikeCount;
    }

    /**
     *
     * @param maxLikeCount
     * The max_LikeCount
     */
    public void setMaxLikeCount(String maxLikeCount) {
        this.maxLikeCount = maxLikeCount;
    }

    /**
     *
     * @return
     * The maxDislikeCount
     */
    public String getMaxDislikeCount() {
        return maxDislikeCount;
    }

    /**
     *
     * @param maxDislikeCount
     * The max_DislikeCount
     */
    public void setMaxDislikeCount(String maxDislikeCount) {
        this.maxDislikeCount = maxDislikeCount;
    }

    /**
     *
     * @return
     * The maxCommentCount
     */
    public String getMaxCommentCount() {
        return maxCommentCount;
    }

    /**
     *
     * @param maxCommentCount
     * The max_commentCount
     */
    public void setMaxCommentCount(String maxCommentCount) {
        this.maxCommentCount = maxCommentCount;
    }

    /**
     *
     * @return
     * The dateDebut
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     *
     * @param dateDebut
     * The date_debut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     *
     * @return
     * The dateFin
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     *
     * @param dateFin
     * The date_fin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     *
     * @return
     * The dateMaxview
     */
    public String getDateMaxview() {
        return dateMaxview;
    }

    /**
     *
     * @param dateMaxview
     * The date_maxview
     */
    public void setDateMaxview(String dateMaxview) {
        this.dateMaxview = dateMaxview;
    }

    /**
     *
     * @return
     * The dateMaxlike
     */
    public String getDateMaxlike() {
        return dateMaxlike;
    }

    /**
     *
     * @param dateMaxlike
     * The date_maxlike
     */
    public void setDateMaxlike(String dateMaxlike) {
        this.dateMaxlike = dateMaxlike;
    }

    /**
     *
     * @return
     * The dateMaxcomment
     */
    public String getDateMaxcomment() {
        return dateMaxcomment;
    }

    /**
     *
     * @param dateMaxcomment
     * The date_maxcomment
     */
    public void setDateMaxcomment(String dateMaxcomment) {
        this.dateMaxcomment = dateMaxcomment;
    }
}

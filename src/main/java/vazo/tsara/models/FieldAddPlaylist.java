package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class FieldAddPlaylist {
    final String objetnom;

    final String objetype;
    final String videosinpl;
    final String status;
    final String createdby;
    String o_videosinpl;

    public FieldAddPlaylist(String objetnom, String objetype, String videosinpl, String status, String createdby) {
        this.objetnom = objetnom;
        this.objetype = objetype;
        this.videosinpl = videosinpl;
        this.status = status;
        this.createdby = createdby;
    }

    public FieldAddPlaylist(String objetnom ,String objetype, String videosinpl, String o_videosinpl, String status, String createdby) {
        this.objetnom = objetnom;

        this.objetype = objetype;
        this.videosinpl = videosinpl;
        this.o_videosinpl = o_videosinpl;
        this.status = status;
        this.createdby = createdby;
    }
}

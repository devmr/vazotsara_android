package vazo.tsara.models;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class relatedvideomodel {

    String objetnom;
    String nbVideos;
    String pathplayliste;
    String image;
    String uid;
    String playlisteid;
    String firstvideoid;

    public relatedvideomodel(){}

    public relatedvideomodel(String objetnom)
    {
        this.objetnom = objetnom;
    }

    public String getObjetnom()
    {
        return this.objetnom;
    }

    public void setObjetnom(String objetnom)
    {
        this.objetnom = objetnom;
    }

    public void setNbVideos(String nbVideos)
    {
        this.nbVideos = nbVideos;
    }
    public void setPathplayliste(String pathplayliste)
    {
        this.pathplayliste = pathplayliste;
    }

    public String getNbVideos()
    {
        return this.nbVideos;
    }
    public String getPathplayliste()
    {
        return this.pathplayliste;
    }

    public String getUid()
    {
        return this.uid;
    }
    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getImage()
    {
        return this.image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    /**
     *
     * @return
     * The firstvideoid
     */
    public String getFirstvideoid() {
        return firstvideoid;
    }

    /**
     *
     * @param firstvideoid
     * The firstvideoid
     */
    public void setFirstvideoid(String firstvideoid) {
        this.firstvideoid = firstvideoid;
    }

    /**
     *
     * @return
     * The playlisteid
     */
    public String getPlaylisteid() {
        return playlisteid;
    }

    /**
     *
     * @param playlisteid
     * The playlisteid
     */
    public void setPlaylisteid(String playlisteid) {
        this.playlisteid = playlisteid;
    }
}

package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class FieldUpdateVideo {
    final String titre;
    final String artiste;
    final String realisation;
    final String rythme;
    final String playlisteyt;
    final String statusyt;
    final String category_yt;
    final String description_yt;

    final String o_artiste;
    final String o_realisation;
    final String o_rythme;
    final String o_playlisteyt;

    final String createdby;
    final String videoytid;

    public FieldUpdateVideo(String titre, String artiste, String realisation, String rythme, String playlisteyt, String statusyt, String category_yt, String description_yt, String o_artiste, String o_realisation, String o_rythme, String o_playlisteyt , String createdby, String videoytid) {
        this.titre = titre;
        this.artiste = artiste;
        this.realisation = realisation;
        this.rythme = rythme;
        this.playlisteyt = playlisteyt;
        this.statusyt = statusyt;
        this.category_yt = category_yt;

        this.description_yt = description_yt;

        this.createdby = createdby;

        this.o_artiste = o_artiste;
        this.o_realisation = o_realisation;
        this.o_rythme = o_rythme;
        this.o_playlisteyt = o_playlisteyt;
        this.videoytid = videoytid;
    }
}

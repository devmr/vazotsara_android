package vazo.tsara.models;


import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import vazo.tsara.helpers.Config;
import vazo.tsara.helpers.VazotsaraShareFunc;

@Table(name = "tx_favorivideos")
public class TableFavorivideos extends Model {

    public String TAG = Config.TAGKEY;

    @Column(name = "videoid")
    public String videoid;

    @Column(name = "ishidden")
    public int ishidden;

    @Column(name = "isdeleted")
    public int isdeleted;


    public TableFavorivideos() {
        super();
    }

    public static void deleteVideoInFavori(String videoid ) {
        new Delete().from(TableFavorivideos.class).where("videoid = ?", videoid).execute();
    }

    public static int getFavoriVideoCount(){
        return new Select().from(TableFavorivideos.class).count();
    }

    public static void deleteVideoidInFavori(String videoid, Context context ) {
        //Sauvegarder sur le serveur
        VazotsaraShareFunc.deleteVideoidInFavori(videoid, context);

        new Delete().from(TableFavorivideos.class).where("videoid = ?", videoid).execute();
    }

    public static void addVideoInFavori(FavoriVideoSQL video, Context context){

        Log.d(Config.TAGKEY,"addVideoInFavori : "+String.valueOf( video.getVideoid() ));

        TableFavorivideos fav = new TableFavorivideos();
        fav.videoid = String.valueOf( video.getVideoid() );
        fav.isdeleted = 0;
        fav.ishidden = 0;
        fav.save();

        //Sauvegarder sur le serveur
        VazotsaraShareFunc.addVideoidInFavori( String.valueOf( video.getVideoid() ), context );

    }

    public static boolean isVideoidInFavoris(String videoid){
        Log.d(Config.TAGKEY, "SQL isVideoidInFavoris : " + videoid + " - " + new Select().from(TableFavorivideos.class).where("videoid = ?", videoid).toCountSql());
        return new Select().from(TableFavorivideos.class).where("videoid = ?", videoid).count()>0;
    }

    public static List<FavoriVideoSQL> getAllVideoinPlaylistfavori() {
        String listid = null;
        List<FavoriPlaylistSQL> favorivideolist = TableFavoriplaylists.getAllFavoriArtist(0);

        List<Integer> tabid = new ArrayList<Integer>();
        for( int i=0; i < favorivideolist.size(); i++ ) {
            tabid.add(favorivideolist.get(i).getId());
        }

        listid = TextUtils.join(",", tabid );

        From query = new Select("a.Id, a.titre, b.videoid,  a.videoytid, a.contentDetails_definition")
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.videoid = a.serverid")
                .where("b.videoid IN(" + listid +")")
                .groupBy("b.videoid")
                ;
        Log.d(Config.TAGKEY, "query.toSql() count : " + query.count());
        Log.d(Config.TAGKEY, "query.toSql() : " + query.toSql());

        Cursor cursor = Cache.openDatabase().rawQuery(query.toSql(), query.getArguments());
        List<FavoriVideoSQL> favorivideoList = new ArrayList<FavoriVideoSQL>();
        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {
            Log.d(Config.TAGKEY, "query setTitre : "+cursor.getString(1) );
            do {
                Log.d(Config.TAGKEY, "query cursor.moveToFirst() true" );
                FavoriVideoSQL favoriRow = new FavoriVideoSQL();
                favoriRow.setId(Integer.parseInt(cursor.getString(0)));
                favoriRow.setTitre(cursor.getString(1));
                favoriRow.setVideoid(Integer.parseInt(cursor.getString(2)));
                favoriRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                favoriRow.setVideoyt(cursor.getString(4));
                favoriRow.setVideoformat(cursor.getString(5));


                /**
                 * Ajouter la ligne à la liste
                 */
                favorivideoList.add(favoriRow);
            } while( cursor.moveToNext() );
        }

        return favorivideoList;
    }

    public static List<FavoriVideoSQL> getAllFavoriVideo() {

        List<FavoriVideoSQL> favorivideoList = new ArrayList<FavoriVideoSQL>();

        From query = new Select("a.Id, b.titre, a.videoid,  a.isdeleted, b.videoytid, b.contentDetails_definition")
                .from(TableFavorivideos.class)
                .as("a")
                .innerJoin(TableVideos.class)
                .as("b")
                .on("b.serverid = a.videoid")
                .groupBy("b.serverid")
                ;

        Log.d(Config.TAGKEY, "query.toSql() count : " + query.count());
        Log.d(Config.TAGKEY, "query.toSql() : " + query.toSql());

        Cursor cursor = Cache.openDatabase().rawQuery(query.toSql(), query.getArguments());
        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {
            Log.d(Config.TAGKEY, "query setTitre : "+cursor.getString(1) );
            do {
                Log.d(Config.TAGKEY, "query cursor.moveToFirst() true" );
                FavoriVideoSQL favoriRow = new FavoriVideoSQL();
                favoriRow.setId(Integer.parseInt(cursor.getString(0)));
                favoriRow.setTitre(cursor.getString(1));
                favoriRow.setVideoid(Integer.parseInt(cursor.getString(2)));
                favoriRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                favoriRow.setVideoyt(cursor.getString(4));
                favoriRow.setVideoformat(cursor.getString(5));


                /**
                 * Ajouter la ligne à la liste
                 */
                favorivideoList.add(favoriRow);
            } while( cursor.moveToNext() );
        }

        return favorivideoList;

    }

}

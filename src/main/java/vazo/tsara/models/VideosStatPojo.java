package vazo.tsara.models;

import com.google.gson.annotations.Expose;

public class VideosStatPojo {

    @Expose
    private String viewCount;
    @Expose
    private String likeCount;
    @Expose
    private String dislikeCount;
    @Expose
    private String commentCount;
    @Expose
    private String tstamp;

    /**
     *
     * @return
     * The viewCount
     */
    public String getViewCount() {
        return viewCount;
    }

    /**
     *
     * @param viewCount
     * The viewCount
     */
    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    /**
     *
     * @return
     * The likeCount
     */
    public String getLikeCount() {
        return likeCount;
    }

    /**
     *
     * @param likeCount
     * The likeCount
     */
    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    /**
     *
     * @return
     * The dislikeCount
     */
    public String getDislikeCount() {
        return dislikeCount;
    }

    /**
     *
     * @param dislikeCount
     * The dislikeCount
     */
    public void setDislikeCount(String dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    /**
     *
     * @return
     * The commentCount
     */
    public String getCommentCount() {
        return commentCount;
    }

    /**
     *
     * @param commentCount
     * The commentCount
     */
    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    /**
     *
     * @return
     * The tstamp
     */
    public String getTstamp() {
        return tstamp;
    }

    /**
     *
     * @param tstamp
     * The tstamp
     */
    public void setTstamp(String tstamp) {
        this.tstamp = tstamp;
    }

}

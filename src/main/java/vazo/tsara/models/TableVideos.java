package vazo.tsara.models;


import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import vazo.tsara.helpers.Config;

@Table(name = "tx_videos")
public class TableVideos extends Model {
    @Expose
    @Column(name = "serverid")
    public String serverid;

    @Expose
    @Column(name = "titre")
    public String titre;

    @Expose
    @Column(name = "videoytid")
    public String videoytid;

    @Expose
    @Column(name = "is_downloaded")
    public String isDownloaded;

    @Expose
    @Column(name = "starttime_downloaded")
    public String starttimeDownloaded;

    @Expose
    @Column(name = "endtime_downloaded")
    public String endtimeDownloaded;

    @Expose
    @Column(name = "starttime_uploaded")
    public String starttimeUploaded;

    @Expose
    @Column(name = "endtime_uploaded")
    public String endtimeUploaded;

    @Expose
    @Column(name = "is_uploaded")
    public String isUploaded;

    @Expose
    @Column(name = "titre_yt")
    public String titreYt;

    @Expose
    @Column(name = "description_yt")
    public String descriptionYt;

    @Expose
    @Column(name = "uploadeddate_original")
    public String uploadeddateOriginal;

    @Expose
    @Column(name = "publishedAt")
    public String publishedAt;

    @Expose
    @Column(name = "created_at")
    public String createdAt;

    @Expose
    @Column(name = "updated_at")
    public String updatedAt;

    @Expose
    @Column(name = "removefile")
    public String removefile;

    @Expose
    @Column(name = "deleted_at")
    public String deletedAt;

    @Expose
    @Column(name = "statusyt")
    public String statusyt;

    @Expose
    @Column(name = "category_yt")
    public String categoryYt;

    @Expose
    @Column(name = "filesize")
    public String filesize;

    @Expose
    @Column(name = "filename")
    public String filename;

    @Expose
    @Column(name = "contentDetails_duration")
    public Integer contentDetailsDuration;

    @Expose
    @Column(name = "contentDetails_definition")
    public String contentDetailsDefinition;

    @Expose
    @Column(name = "status_uploadStatus")
    public String statusUploadStatus;

    @Expose
    @Column(name = "status_privacyStatus")
    public String statusPrivacyStatus;

    @Expose
    @Column(name = "status_license")
    public String statusLicense;

    @Expose
    @Column(name = "status_embeddable")
    public String statusEmbeddable;

    @Expose
    @Column(name = "status_publicStatsViewable")
    public String statusPublicStatsViewable;

    @Expose
    @Column(name = "statistics_viewCount")
    public int statisticsViewCount;

    @Expose
    @Column(name = "statistics_likeCount")
    public int statisticsLikeCount;

    @Expose
    @Column(name = "statistics_dislikeCount")
    public int statisticsDislikeCount;

    @Expose
    @Column(name = "statistics_favoriteCount")
    public int statisticsFavoriteCount;

    @Expose
    @Column(name = "statistics_commentCount")
    public int statisticsCommentCount;

    @Expose
    @Column(name = "fileDetails_container")
    public String fileDetailsContainer;

    @Expose
    @Column(name = "fileDetails_videoStreams_frameRateFps")
    public String fileDetailsVideoStreamsFrameRateFps;

    @Expose
    @Column(name = "fileDetails_videoStreams_aspectRatio")
    public String fileDetailsVideoStreamsAspectRatio;

    @Expose
    @Column(name = "fileDetails_videoStreams_codec")
    public String fileDetailsVideoStreamsCodec;

    @Expose
    @Column(name = "fileDetails_videoStreams_widthPixels")
    public String fileDetailsVideoStreamsWidthPixels;

    @Expose
    @Column(name = "fileDetails_videoStreams_heightPixels")
    public String fileDetailsVideoStreamsHeightPixels;

    @Expose
    @Column(name = "source")
    public String source;

    @Expose
    @Column(name = "thumburl")
    public String thumburl;

    @Expose
    @Column(name = "publishedCommentLast")
    public int publishedCommentLast;

    @Expose
    @Column(name = "statistics_viewCount_hier")
    public int statisticsViewCountHier;

    @Expose
    @Column(name = "listeNew")
    public int listeNew;

    @Expose
    @Column(name = "listeView")
    public int listeView;

    @Expose
    @Column(name = "listeLike")
    public int listeLike;

    @Expose
    @Column(name = "listeComment")
    public int listeComment;

    @Expose
    @Column(name = "listeDuration")
    public int listeDuration;

    @Expose
    @Column(name = "listeCommentDate")
    public int listeCommentDate;

    @Expose
    @Column(name = "listeCommentlast")
    public int listeCommentlast;

    @Expose
    @Column(name = "listeViewHier")
    public int listeViewHier;

    public TableVideos() {
        super();
    }

    public static int getAllCount(){
        return new Select().from(TableVideos.class).count();
    }

    public static int getAllCount(String serverid){
        return new Select().from(TableVideos.class).where("serverid = ?", serverid).count();
    }

    public static From getFromSingleVideo(String videoid) {
        return new Select().from(TableVideos.class).where("serverid = ?", videoid);
    }
    public static int getCountSingleVideo(String videoid) {
        return getFromSingleVideo(videoid).count();
    }
    public static VideoSingleGson getSingleVideo(String videoid) {
        Log.d(Config.TAGKEY,"Fragment STEP onResume getSingleVideo : "+videoid );
        From from = getFromSingleVideo(videoid).limit(1);
        TableVideos v = from.executeSingle();
        Log.d(Config.TAGKEY,"Fragment STEP onResume doInBackground SQL : "+from.toSql() );
        List<TablePlaylists> pl = getAllPlaylistsByVideo(v.serverid);
        if ( pl==null ){
            return null;
        }

        VideoSingleGson info = new VideoSingleGson();


        Videogson vg = new Videogson();
        vg.setId(v.serverid);
        vg.setContentDetailsDefinition(v.contentDetailsDefinition);
        vg.setContentDetailsDuration(String.valueOf(v.contentDetailsDuration));
        vg.setTitre(v.titre);
        vg.setTitreYt(v.titre);
        vg.setStatisticsViewCount(String.valueOf(v.statisticsViewCount));
        vg.setStatisticsLikeCount(String.valueOf(v.statisticsLikeCount));
        vg.setStatisticsDislikeCount(String.valueOf(v.statisticsDislikeCount));
        vg.setStatisticsCommentCount(String.valueOf(v.statisticsCommentCount));
        vg.setPublishedAt(v.publishedAt);
        vg.setThumburl(v.titre);
        vg.setVideoytid(v.videoytid);
        vg.setUploadeddateOriginal(v.uploadeddateOriginal);

        List<VideoSingleGsonPlaylist> playlisteinfo = new ArrayList<VideoSingleGsonPlaylist>();
        List<VideoSingleGsonPlaylisteinfo> playlisteinfo2 = new ArrayList<VideoSingleGsonPlaylisteinfo>();

        if ( pl.size() > 0 ) {
            for( int i = 0; i < pl.size(); i++ ) {
                TablePlaylists tpl = pl.get(i);
                VideoSingleGsonPlaylist list = new VideoSingleGsonPlaylist();
                list.setId( tpl.serverid  );
                list.setImage(tpl.image);
                list.setFirstvideoid(tpl.firstvideoid);
                list.setNbVideos(tpl.nbVideos);
                list.setNom(tpl.nom);
                list.setObjetnom(tpl.objetnom);
                list.setObjetype(tpl.objetype);
                list.setPlaylisteid(tpl.playlisteid);
                list.setStatus(tpl.status);

                playlisteinfo.add(list);


            }
        }
        vg.setPlaylisteinfo(playlisteinfo);
        info.setPlaylisteinfo(playlisteinfo2);
        info.setInfo(vg);
        return info ;
    }
    public static String getLastVideoId() {
        TableVideos row = new Select().from(TableVideos.class).orderBy("publishedAt DESC").limit(1).executeSingle();
        return row.serverid;
    }

    public static int getAllVideosInPlaylistCount(String playlisteid) {
        return new Select()
                .from(TableFavorivideos.class)
                .as("a")
                .innerJoin(TableVideos.class)
                .as("b")
                .on("b.serverid = a.videoid")
                .count();
    }

    public static List<TablePlaylists> getAllPlaylistsByVideo(String uid) {
        Log.d(Config.TAGKEY, "DEBUG BDD - SQL : " + new Select()
                .from(TablePlaylists.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.playlisteid = a.serverid").
                        innerJoin(TableVideos.class).
                        as("c")
                .on("c.serverid = b.videoid")
                .where("b.videoid = ?", uid ).count());
        if ( new Select()
                .from(TablePlaylists.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.playlisteid = a.serverid").
                        innerJoin(TableVideos.class).
                        as("c")
                .on("c.serverid = b.videoid")
                .where("b.videoid = ?", uid).count() > 0 ) {
            return new Select()
                    .from(TablePlaylists.class)
                    .as("a")
                    .innerJoin(TableVideosPlaylists.class)
                    .as("b")
                    .on("b.playlisteid = a.serverid").
                            innerJoin(TableVideos.class).
                            as("c")
                    .on("c.serverid = b.videoid")
                    .where("b.videoid = ?", uid)
                    .execute();
        }
        return null;
    }

     public static List<TableVideos> getRelatedVideoByPlaylists(String uid, String type ) {

        //Get all playliste related to this video
        List<TableVideosPlaylists> list =  TableVideosPlaylists.getRelatedPlaylists(uid);
        String[] tabplaylisteid = new String[list.size()];


        Log.d(Config.TAGKEY, "DEBUG BDD - SQL : " + new Select()
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.videoid = a.serverid").
                        innerJoin(TablePlaylists.class).
                        as("c")
                .on("c.serverid = b.playlisteid")
                .groupBy("a.serverid")
                .where("b.playlisteid IN (" + TextUtils.join(",", tabplaylisteid) + ")")
                .where("c.objetype = ?", type)
                .where("a.serverid NOT IN (" + uid+ ")")
                .toSql() );


        return new Select()
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.videoid = a.serverid").
                        innerJoin(TablePlaylists.class).
                        as("c")
                .on("c.serverid = b.playlisteid")
                .groupBy("a.serverid")
                .where("b.playlisteid IN (" + TextUtils.join(",", tabplaylisteid) + ")")
                .where("c.objetype = ?", type)
                .where("a.serverid NOT IN (" + uid+ ")"  )
                .execute();
    }
    public static int getCountRelatedvideos(String videoid, String type ) {
        return getFromRelatedvideos(videoid, type).count();
    }

    public static From getFromRelatedvideos(String videoid, String type ) {

        String[] tabid = new String[0];
        String[] tabplayliste = new String[0];

        //get all related playlists
        List<TableVideosPlaylists> list = TableVideosPlaylists.getRelatedPlaylists(videoid);
        tabplayliste = new String[list.size()];

        if ( list.size() > 0) {
            for ( int i = 0; i<list.size(); i++) {
                tabplayliste[i] = list.get(i).playlisteid;
            }
        }

        if ( type.equals("artiste") ) {
            List<TableVideos> listvideo = getRelatedVideoByPlaylists(videoid, type );
            tabid = new String[listvideo.size()];
            if ( listvideo.size() > 0) {
                for ( int i = 0; i<listvideo.size(); i++) {
                    tabid[i] = listvideo.get(i).serverid;
                }
            }
        }
        Log.d(Config.TAGKEY, "DEBUG BDD - SQL : " + new Select()
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.videoid = a.serverid").
                        innerJoin(TablePlaylists.class).
                        as("c")
                .on("c.serverid = b.playlisteid")
                .groupBy("a.serverid")
                .where("b.playlisteid IN (" + TextUtils.join(",", tabplayliste) + ")")
                .where("c.objetype = ?", type)
                .where("a.serverid NOT IN (" + TextUtils.join(",", tabplayliste) + ")")
                .toCountSql()
        );
        return new Select()
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosPlaylists.class)
                .as("b")
                .on("b.videoid = a.serverid").
                        innerJoin(TablePlaylists.class).
                        as("c")
                .on("c.serverid = b.playlisteid")
                .groupBy("a.serverid")
                .where("b.playlisteid IN (" + TextUtils.join(",", tabplayliste) + ")")
                .where("c.objetype = ?", type)
                .where("a.serverid NOT IN (" + TextUtils.join(",", tabplayliste) + ")") ;
    }

    public static List<TableVideos> getNewVideosAdded(int limit, int offset) {
        if ( limit > 0 ) {
            if ( offset > 0 ) {
                return new Select().from(TableVideos.class).orderBy("publishedAt DESC, serverid DESC").limit(limit).offset(offset).execute();
            }
            return new Select().from(TableVideos.class).orderBy("publishedAt DESC, serverid DESC").limit(limit).execute();

        }
        return new Select().from(TableVideos.class).orderBy("publishedAt DESC, serverid DESC").execute();

    }

    public static List<TableVideos> getNewVideosCommented(int limit, int offset) {
        if ( limit > 0 ) {
            if ( offset > 0 ) {
                return new Select().from(TableVideos.class).orderBy("publishedCommentLast DESC").limit(limit).offset(offset).execute();
            }
            return new Select().from(TableVideos.class).orderBy("publishedCommentLast DESC").limit(limit).execute();

        }
        return new Select().from(TableVideos.class).orderBy("publishedCommentLast DESC").execute();

    }

    public static List<TableVideos> getAllVideosPopular(int limit, int offset) {
        if ( limit > 0 ) {
            if ( offset > 0 ) {
                return new Select().from(TableVideos.class).orderBy("statistics_viewCount DESC").limit(limit).offset(offset).execute();
            }
            return new Select().from(TableVideos.class).orderBy("statistics_viewCount DESC").limit(limit).execute();


        }
        return new Select().from(TableVideos.class).orderBy("statistics_viewCount DESC").execute();
    }

    public static List<TableVideos> getAllVideosPopularYesterday(int limit, int offset) {
        if ( limit > 0 ) {
            if ( offset > 0 ) {
                return new Select().from(TableVideos.class).orderBy("statistics_viewCount_hier DESC").limit(limit).offset(offset).execute();
            }
            return new Select().from(TableVideos.class).orderBy("statistics_viewCount_hier DESC").limit(limit).execute();
        }
        return new Select().from(TableVideos.class).orderBy("statistics_viewCount_hier DESC").execute();
    }

    public static From getFromAllVideosInHome() {
        return new Select()
                .from(TableVideos.class)
                .as("a")
                .innerJoin(TableVideosHome.class)
                .as("b")
                .on("b.videoid = a.videoid") ;
    }
    public static int  getCountAllVideosInHome() {
        return getFromAllVideosInHome().count();
    }

    public static List<TableVideos>  getAllVideosInHome() {
        return getFromAllVideosInHome().execute();
    }


}

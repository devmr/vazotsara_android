package vazo.tsara.models;

import java.io.Serializable;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class artistemodel  implements Serializable {

    String objetnom;
    String nbVideos;

    public artistemodel(){}

    public artistemodel(String objetnom, String nbVideos)
    {

        this.objetnom = objetnom;
        this.nbVideos = nbVideos;
    }

    public String getObjetnom()
    {
        return this.objetnom;
    }
    public String getNbVideos()
    {
        return this.nbVideos;
    }

    public void setObjetnom(String objetnom)
    {
        this.objetnom = objetnom;
    }
    public void setNbVideos(String nbVideos)
    {
        this.nbVideos = nbVideos;
    }
}

package vazo.tsara.models;

import java.io.Serializable;

/**
 * Created by rabehasy on 01/06/2015.
 */
public class profileModel implements Serializable {

    String id;
    String nom;
    String provider;
    String avatar;

    public profileModel(){}



    public profileModel(String id, String nom, String avatar ,String provider)
    {

        this.id = id;
        this.nom = nom;
        this.avatar = avatar;
        this.provider = provider;
    }

    public String getId(){
        return this.id;
    }

    public String getNom(){
        return this.nom;
    }

    public String getAvatar(){
        return this.avatar;
    }

    public String getProvider(){
        return this.provider;
    }

}

package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class FieldAddVideo {
    final String titre;
    final String artiste;
    final String realisation;
    final String rythme;
    final String playlisteyt;
    final String statutyt;
    final String category_yt;
    final String removefile;
    final String descyt;
    final String type_dl;
    final String url_dl;
    final String title_dl;
    final String description_dl;
    final String author_dl;
    final String createdby;

    public FieldAddVideo(String titre, String artiste, String realisation, String rythme, String playlisteyt, String statutyt, String category_yt, String removefile, String descyt, String type_dl, String url_dl, String title_dl, String description_dl, String author_dl, String createdby ) {
        this.titre = titre;
        this.artiste = artiste;
        this.realisation = realisation;
        this.rythme = rythme;
        this.playlisteyt = playlisteyt;
        this.statutyt = statutyt;
        this.category_yt = category_yt;
        this.removefile = removefile;
        this.descyt = descyt;
        this.type_dl = type_dl;
        this.url_dl = url_dl;
        this.title_dl = title_dl;
        this.description_dl = description_dl;
        this.author_dl = author_dl;
        this.createdby = createdby;
    }
}

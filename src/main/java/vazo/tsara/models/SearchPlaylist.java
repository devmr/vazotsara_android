package vazo.tsara.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rabehasy on 24/08/2015.
 */
public class SearchPlaylist {

    @Expose
    private List<SingleResultSearchArtiste> results = new ArrayList<SingleResultSearchArtiste>();


    /**
     *
     * @return
     * The results
     */
    public List<SingleResultSearchArtiste> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<SingleResultSearchArtiste> results) {
        this.results = results;
    }



}

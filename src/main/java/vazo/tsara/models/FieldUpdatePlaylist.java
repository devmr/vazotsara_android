package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class FieldUpdatePlaylist {
    final String objetnom;
    final String nom;
    final String description_yt;
    final String eviter_doublons;
    final String rename_all;

    final String objetype;
    final String videosinpl;

    final String status;
    final String createdby;
    String o_videosinpl;

    public FieldUpdatePlaylist(String objetnom, String nom, String description_yt, String objetype, String videosinpl, String status, String createdby, String eviter_doublons, String rename_all, String o_videosinpl) {
        this.objetnom = objetnom;
        this.nom = nom;
        this.description_yt = description_yt;
        this.eviter_doublons = eviter_doublons;
        this.rename_all = rename_all;
        this.o_videosinpl = o_videosinpl;
        this.objetype = objetype;
        this.videosinpl = videosinpl;
        this.status = status;
        this.createdby = createdby;
    }

}

package vazo.tsara.models;

import java.io.Serializable;

/**
 * Created by rabehasy on 18/06/2015.
 */
public class Person implements Serializable {
    private String name;
    private String email;

    public Person(String n, String e) {
        name = n;
        email = e;
    }

    public String getName() { return name; }
    public String getEmail() { return email; }

    @Override
    public String toString() { return name; }
}

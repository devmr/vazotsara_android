package vazo.tsara.models;

/**
 * Created by rabehasy on 22/06/2015.
 */

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "tx_videos_playlistes")
public class TableVideosPlaylists extends Model {

    @Column(name = "videoid")
    public String videoid;

    @Column(name = "playlisteid")
    public String playlisteid;

    public TableVideosPlaylists(){
        super();
    }

    public static boolean relationExists(String videoid, String playlisteid) {
        return new Select().from(TableVideosPlaylists.class).where("videoid = ?", videoid ).where("playlisteid = ?",playlisteid ).count()>0;
    }

    public static void saveRelation(String videoid, String playlisteid) {
        TableVideosPlaylists item = new TableVideosPlaylists();
        item.videoid = videoid;
        item.playlisteid = playlisteid;
        item.save();
    }

    public static List<TableVideosPlaylists> getRelatedPlaylists(String uid){
        return new Select()
                .from(TableVideosPlaylists.class)
                .where("videoid = ?", uid)
                .execute();
    }
}

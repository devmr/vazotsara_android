package vazo.tsara.models;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;


public class VideoPojo {

    @Expose
    private String limit;
    @Expose
    private Integer total;
    @Expose
    private Integer shown;
    @Expose
    private List<Videogson> videos = new ArrayList<Videogson>();

    @Expose
    private Integer skip;

    @Expose
    private Integer page;

    /**
     *
     * @return
     * The limit
     */
    public String getLimit() {
        return limit;
    }

    /**
     *
     * @param limit
     * The limit
     */
    public void setLimit(String limit) {
        this.limit = limit;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The shown
     */
    public Integer getShown() {
        return shown;
    }

    /**
     *
     * @param shown
     * The shown
     */
    public void setShown(Integer shown) {
        this.shown = shown;
    }

    /**
     *
     * @return
     * The videos
     */
    public List<Videogson> getVideos() {
        return videos;
    }

    /**
     *
     * @param videos
     * The videos
     */
    public void setVideos(List<Videogson> videos) {
        this.videos = videos;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getSkip() {
        return skip;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getPage() {
        return page;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setPage(Integer page) {
        this.page = page;
    }

}

package vazo.tsara.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class PlaylisteSingle {
    @Expose
    private Playlist info;

    /**
     *
     * @return
     * The info
     */
    public Playlist getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(Playlist info) {
        this.info = info;
    }



}

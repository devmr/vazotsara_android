package vazo.tsara.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rabehasy on 09/03/2015.
 */
public class video {

    private String titre;
    private String thumburl;
    private String videoytid;
    private String id;
    private String statistics_viewCount;
    private String statistics_likeCount;
    private String statistics_dislikeCount;
    private String statistics_commentCount;
    private String contentDetails_definition;

    private String format;
    private String duree;

    private String type;

    private List<VideoSingleGsonPlaylist> playlisteinfo = new ArrayList<VideoSingleGsonPlaylist>();

    public video()
    {

    }

    public video(String id, String titre, String thumburl, String videoytid, String statistics_viewCount, String statistics_likeCount, String statistics_dislikeCount, String statistics_commentCount, String format, String duree, String type, List<VideoSingleGsonPlaylist> playlisteinfo, String contentDetails_definition)
    {
        this.id = id;
        this.titre = titre;
        this.videoytid = videoytid;
        this.thumburl = thumburl;
        this.statistics_viewCount = statistics_viewCount;
        this.statistics_likeCount = statistics_likeCount;
        this.statistics_dislikeCount = statistics_dislikeCount;
        this.statistics_commentCount = statistics_commentCount;
        this.format = format;
        this.duree = duree;
        this.type = type;
        this.playlisteinfo = playlisteinfo;
        this.contentDetails_definition = contentDetails_definition;
    }

    public video(String id, String videoytid, String contentDetails_definition)
    {
        this.id = id;
        this.videoytid = videoytid;
        this.contentDetails_definition = contentDetails_definition;
    }

    public video(String titre, String statistics_viewCount, String statistics_commentCount, String statistics_likeCount)
    {
        this.statistics_viewCount = statistics_viewCount;
        this.titre = titre;
        this.statistics_commentCount = statistics_commentCount;
        this.statistics_likeCount = statistics_likeCount;
    }

    public String getContentDetails_definition() { return this.contentDetails_definition; }

    public void setContentDetails_definition(String contentDetails_definition) { this.contentDetails_definition = contentDetails_definition; }

    public String getTitre()
    {
        return this.titre;
    }

    public void setTitre(String titre)
    {
        this.titre = titre;
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getThumburl()
    {
        return this.thumburl;
    }

    public void setThumburl(String thumburl)
    {
        this.thumburl = thumburl;
    }

    public String getVideoytid()
    {
        return this.videoytid;
    }

    public void setVideoytid(String videoytid)
    {
        this.videoytid = videoytid;
    }

    public String getStatistics_viewCount()
    {
        return this.statistics_viewCount;
    }

    public void setStatistics_viewCount(String statistics_viewCount)
    {
        this.statistics_viewCount = statistics_viewCount;
    }
    public String getStatistics_likeCount()
    {
        return this.statistics_likeCount;
    }

    public void setStatistics_likeCount(String statistics_likeCount)
    {
        this.statistics_likeCount = statistics_likeCount;
    }
    public String getStatistics_dislikeCount()
    {
        return this.statistics_dislikeCount;
    }

    public void setStatistics_dislikeCount(String statistics_dislikeCount)
    {
        this.statistics_dislikeCount = statistics_dislikeCount;
    }

    public String getStatistics_commentCount()
    {
        return this.statistics_commentCount;
    }

    public void setStatistics_commentCount(String statistics_commentCount)
    {
        this.statistics_commentCount = statistics_commentCount;
    }

    public String getFormat()
    {
        return this.format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public String getDuree()
    {
        return this.duree;
    }

    public void setDuree(String duree)
    {
        this.duree = duree;
    }

    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public List<VideoSingleGsonPlaylist> getPlaylisteinfo() { return this.playlisteinfo; }

    public void setPlaylisteinfo(List<VideoSingleGsonPlaylist> playlisteinfo)
    {
        this.playlisteinfo = playlisteinfo;
    }




}

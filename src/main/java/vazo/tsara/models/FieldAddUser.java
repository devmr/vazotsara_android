package vazo.tsara.models;

/**
 * Created by rabehasy on 07/09/2015.
 */
public class FieldAddUser {


    final String provider_id;
    final String name;
    final String avatar;
    final String createdby;
    final String email;
    final String provider;


    public FieldAddUser(String provider_id, String name, String email, String avatar, String provider,  String createdby) {
        this.provider_id = provider_id;
        this.name = name;
        this.avatar = avatar;
        this.createdby = createdby;
        this.email = email;
        this.provider = provider;
    }


}

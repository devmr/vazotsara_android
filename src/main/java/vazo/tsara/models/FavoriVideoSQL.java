package vazo.tsara.models;

/**
 * Created by rabehasy on 31/08/2015.
 */
/**
 * Created by Miary on 28/08/2015.
 */
public class FavoriVideoSQL {

    /**
     * Variables
     */
    int id;
    String titre;
    String videoyt;
    String videoformat;
    int videoid;
    int isdeleted;

    public FavoriVideoSQL() {}

    public FavoriVideoSQL(int id, String titre, int videoid, int isdeleted, String videoyt, String videoformat) {
        this.id = id;
        this.titre = titre;
        this.videoid = videoid;
        this.isdeleted = isdeleted;
        this.videoyt = videoyt;
        this.videoformat = videoformat;
    }

    public FavoriVideoSQL(String titre, int videoid, int isdeleted, String videoyt, String videoformat) {

        this.titre = titre;
        this.videoid = videoid;
        this.isdeleted = isdeleted;
        this.videoyt = videoyt;
        this.videoformat = videoformat;
    }

    public FavoriVideoSQL( int videoid ) {
        this.videoid = videoid;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getVideoid() {
        return this.videoid;
    }

    public void setVideoid(int videoid) {
        this.videoid = videoid;
    }

    public int getIsdeleted() {
        return this.isdeleted;
    }

    public void setIsdeleted(int isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getVideoyt() {
        return this.videoyt;
    }

    public void setVideoyt(String videoyt) {
        this.videoyt = videoyt;
    }

    public String getVideoformat() {
        return this.videoformat;
    }

    public void setVideoformat(String videoformat) {
        this.videoformat = videoformat;
    }


}

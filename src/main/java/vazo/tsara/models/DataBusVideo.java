package vazo.tsara.models;

/**
 * Created by Miary on 02/10/2015.
 */
public class DataBusVideo {

    public String infovideo_format;
    public String LikeCount;
    public String ViewCount;
    public int position;

    public DataBusVideo(String infovideo_format, String LikeCount, String ViewCount, int position) {
        this.infovideo_format = infovideo_format;
        this.LikeCount = LikeCount;
        this.ViewCount = ViewCount;
        this.position = position;
    }
}

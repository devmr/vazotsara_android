package vazo.tsara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoSingleGsonPlaylisteinfo {
    @Expose
    private String id;
    @Expose
    private String playlisteid;
    @Expose
    private String nom;
    @Expose
    private String image;
    @Expose
    private String created;
    @SerializedName("description_yt")
    @Expose
    private String descriptionYt;
    @SerializedName("nb_videos")
    @Expose
    private String nbVideos;
    @Expose
    private String objetnom;
    @Expose
    private String objetype;
    @Expose
    private String objetid;
    @Expose
    private String firstvideoid;
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @Expose
    private VideoSingleGsonPivot_ pivot;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The playlisteid
     */
    public String getPlaylisteid() {
        return playlisteid;
    }

    /**
     *
     * @param playlisteid
     * The playlisteid
     */
    public void setPlaylisteid(String playlisteid) {
        this.playlisteid = playlisteid;
    }

    /**
     *
     * @return
     * The nom
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     * The nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     * The nom
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param nom
     * The nom
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The created
     */
    public String getCreated() {
        return created;
    }

    /**
     *
     * @param created
     * The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     *
     * @return
     * The descriptionYt
     */
    public String getDescriptionYt() {
        return descriptionYt;
    }

    /**
     *
     * @param descriptionYt
     * The description_yt
     */
    public void setDescriptionYt(String descriptionYt) {
        this.descriptionYt = descriptionYt;
    }

    /**
     *
     * @return
     * The nbVideos
     */
    public String getNbVideos() {
        return nbVideos;
    }

    /**
     *
     * @param nbVideos
     * The nb_videos
     */
    public void setNbVideos(String nbVideos) {
        this.nbVideos = nbVideos;
    }

    /**
     *
     * @return
     * The objetnom
     */
    public String getObjetnom() {
        return objetnom;
    }

    /**
     *
     * @param objetnom
     * The objetnom
     */
    public void setObjetnom(String objetnom) {
        this.objetnom = objetnom;
    }

    /**
     *
     * @return
     * The objetype
     */
    public String getObjetype() {
        return objetype;
    }

    /**
     *
     * @param objetype
     * The objetype
     */
    public void setObjetype(String objetype) {
        this.objetype = objetype;
    }

    /**
     *
     * @return
     * The objetid
     */
    public String getObjetid() {
        return objetid;
    }

    /**
     *
     * @param objetid
     * The objetid
     */
    public void setObjetid(String objetid) {
        this.objetid = objetid;
    }

    /**
     *
     * @return
     * The firstvideoid
     */
    public String getFirstvideoid() {
        return firstvideoid;
    }

    /**
     *
     * @param firstvideoid
     * The firstvideoid
     */
    public void setFirstvideoid(String firstvideoid) {
        this.firstvideoid = firstvideoid;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The pivot
     */
    public VideoSingleGsonPivot_ getPivot() {
        return pivot;
    }

    /**
     *
     * @param pivot
     * The pivot
     */
    public void setPivot(VideoSingleGsonPivot_ pivot) {
        this.pivot = pivot;
    }

}

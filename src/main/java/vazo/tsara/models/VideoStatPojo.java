package vazo.tsara.models;

/**
 * Created by Miary on 28/04/2015.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class VideoStatPojo {

    @Expose
    private StatGeneralinfo generalinfo;

    @SerializedName("videos_stats")
    @Expose
    private List<VideosStatPojo> videosStatPojos = new ArrayList<VideosStatPojo>();


    /**
     *
     * @return
     * The generalinfo
     */
    public StatGeneralinfo getStatGeneralinfo() {
        return generalinfo;
    }

    /**
     *
     * @param generalinfo
     * The generalinfo
     */
    public void setStatGeneralinfo(StatGeneralinfo generalinfo) {
        this.generalinfo = generalinfo;
    }

    /**
     *
     * @return
     * The videosStats
     */
    public List<VideosStatPojo> getVideosStatPojos() {
        return videosStatPojos;
    }

    /**
     *
     * @param videosStatPojos
     * The videos_stats
     */
    public void setVideosStatPojos(List<VideosStatPojo> videosStatPojos) {
        this.videosStatPojos = videosStatPojos;
    }
}
package vazo.tsara.models;


import android.content.Context;
import android.database.Cursor;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import vazo.tsara.helpers.VazotsaraShareFunc;

@Table(name = "tx_favoriplaylists")
public class TableFavoriplaylists extends Model {

    @Column(name = "playlisteid")
    public String playlisteid;

    @Column(name = "ishidden")
    public int ishidden;

    @Column(name = "isdeleted")
    public int isdeleted;


    public TableFavoriplaylists() {
        super();
    }

    public static int getCountArtistInFavori(){
        return new Select().from(TableFavoriplaylists.class).count();
    }

    public static int getCountArtistInFavoriIsHidden(int hidden){
        return new Select().from(TableFavoriplaylists.class).where("ishidden = ?", hidden).count();
    }

    public static boolean isArtistInFavori(String playlisteid ) {
        int nbr = new Select().from(TableFavoriplaylists.class).where("playlisteid = ?", playlisteid).count();
        return nbr>0;
    }

    public static void addArtistInFavori(String playlistid , Context context) {
        //Sauvegarder sur le serveur
        VazotsaraShareFunc.addPlaylisteidInFavori(playlistid, context);

        TableFavoriplaylists fa = new TableFavoriplaylists();
        fa.playlisteid = playlistid;
        fa.save();
    }

    public static void deleteArtistInFavori(String playlistid , Context context ) {

        //Sauvegarder sur le serveur
        VazotsaraShareFunc.deletePlaylisteidInFavori(playlistid, context);

        new Delete().from(TableFavoriplaylists.class).where("playlisteid = ?", playlistid).execute();
    }

    public static List<FavoriPlaylistSQL> getAllFavoriArtist(int hidden) {

        List<FavoriPlaylistSQL> modelList = new ArrayList<FavoriPlaylistSQL>();
        From query = new Select("a.playlisteid, b.objetnom, b.image, a.ishidden, a.isdeleted")
                .from(TableFavoriplaylists.class)
                .as("a")
                .innerJoin(TablePlaylists.class)
                .as("b")
                .on("b.serverid = a.playlisteid")
                .where("a.ishidden = ?", hidden)
                ;

        Cursor cursor = Cache.openDatabase().rawQuery(query.toSql(), query.getArguments());
        /**
         * Boucle
         */
        if (cursor.moveToFirst()) {

            do {
                FavoriPlaylistSQL modelRow = new FavoriPlaylistSQL();
                modelRow.setId(Integer.parseInt(cursor.getString(0)));
                modelRow.setTitre(cursor.getString(1));
                modelRow.setIsdeleted(Integer.parseInt(cursor.getString(3)));
                modelRow.setIshidden(Integer.parseInt(cursor.getString(4)));
                modelRow.setImage(cursor.getString(2));



                /**
                 * Ajouter la ligne à la liste
                 */
                modelList.add(modelRow);
            } while( cursor.moveToNext() );
        }

        return modelList;

    }


}
